package com.xianzaishi.trade.facade.message;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xianzaishi.trade.client.MessageService;

/**
 * 
 *
 * @author nianyue.hyj
 * @since 2016
 */
@Service(value = "messageServiceProxy")
public class MessageServiceImpl implements MessageService {

	@Autowired
	private com.xianzaishi.service.MessageService messageService;

	@SuppressWarnings("deprecation")
	public boolean send(String businessCode, String mobile, String content, boolean needStatus) {
		return messageService.sendSmsMessage(mobile, content).isSuccess();
	}

	@SuppressWarnings("deprecation")
	public boolean systemSend(String mobile, String content) {
		return messageService.sendSmsMessage(mobile, content).isSuccess();
	}

	public boolean sendVericationCode(String mobile, String code) {
		Map<String, String> context = new HashMap<String, String>();
		context.put("code", code);
		return messageService.sendSmsMessage(mobile, "user_register_code", context).isSuccess();
	}

	public boolean sendResetPassword(String mobile, String code) {
		Map<String, String> context = new HashMap<String, String>();
		context.put("code", code);
		return messageService.sendSmsMessage(mobile, "reset_pwd_code", context).isSuccess();
	}
}
