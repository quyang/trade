package com.xianzaishi.trade.dal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShoppingCartExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ShoppingCartExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Long value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Long value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Long value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Long value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Long value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Long> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Long> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Long value1, Long value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Long value1, Long value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andItemIdIsNull() {
            addCriterion("item_id is null");
            return (Criteria) this;
        }

        public Criteria andItemIdIsNotNull() {
            addCriterion("item_id is not null");
            return (Criteria) this;
        }

        public Criteria andItemIdEqualTo(Long value) {
            addCriterion("item_id =", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotEqualTo(Long value) {
            addCriterion("item_id <>", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdGreaterThan(Long value) {
            addCriterion("item_id >", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdGreaterThanOrEqualTo(Long value) {
            addCriterion("item_id >=", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdLessThan(Long value) {
            addCriterion("item_id <", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdLessThanOrEqualTo(Long value) {
            addCriterion("item_id <=", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdIn(List<Long> values) {
            addCriterion("item_id in", values, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotIn(List<Long> values) {
            addCriterion("item_id not in", values, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdBetween(Long value1, Long value2) {
            addCriterion("item_id between", value1, value2, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotBetween(Long value1, Long value2) {
            addCriterion("item_id not between", value1, value2, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdIsNull() {
            addCriterion("item_sku_id is null");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdIsNotNull() {
            addCriterion("item_sku_id is not null");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdEqualTo(Long value) {
            addCriterion("item_sku_id =", value, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdNotEqualTo(Long value) {
            addCriterion("item_sku_id <>", value, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdGreaterThan(Long value) {
            addCriterion("item_sku_id >", value, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdGreaterThanOrEqualTo(Long value) {
            addCriterion("item_sku_id >=", value, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdLessThan(Long value) {
            addCriterion("item_sku_id <", value, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdLessThanOrEqualTo(Long value) {
            addCriterion("item_sku_id <=", value, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdIn(List<Long> values) {
            addCriterion("item_sku_id in", values, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdNotIn(List<Long> values) {
            addCriterion("item_sku_id not in", values, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdBetween(Long value1, Long value2) {
            addCriterion("item_sku_id between", value1, value2, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuIdNotBetween(Long value1, Long value2) {
            addCriterion("item_sku_id not between", value1, value2, "itemSkuId");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoIsNull() {
            addCriterion("item_sku_info is null");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoIsNotNull() {
            addCriterion("item_sku_info is not null");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoEqualTo(String value) {
            addCriterion("item_sku_info =", value, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoNotEqualTo(String value) {
            addCriterion("item_sku_info <>", value, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoGreaterThan(String value) {
            addCriterion("item_sku_info >", value, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoGreaterThanOrEqualTo(String value) {
            addCriterion("item_sku_info >=", value, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoLessThan(String value) {
            addCriterion("item_sku_info <", value, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoLessThanOrEqualTo(String value) {
            addCriterion("item_sku_info <=", value, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoLike(String value) {
            addCriterion("item_sku_info like", value, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoNotLike(String value) {
            addCriterion("item_sku_info not like", value, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoIn(List<String> values) {
            addCriterion("item_sku_info in", values, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoNotIn(List<String> values) {
            addCriterion("item_sku_info not in", values, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoBetween(String value1, String value2) {
            addCriterion("item_sku_info between", value1, value2, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemSkuInfoNotBetween(String value1, String value2) {
            addCriterion("item_sku_info not between", value1, value2, "itemSkuInfo");
            return (Criteria) this;
        }

        public Criteria andItemNameIsNull() {
            addCriterion("item_name is null");
            return (Criteria) this;
        }

        public Criteria andItemNameIsNotNull() {
            addCriterion("item_name is not null");
            return (Criteria) this;
        }

        public Criteria andItemNameEqualTo(String value) {
            addCriterion("item_name =", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameNotEqualTo(String value) {
            addCriterion("item_name <>", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameGreaterThan(String value) {
            addCriterion("item_name >", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameGreaterThanOrEqualTo(String value) {
            addCriterion("item_name >=", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameLessThan(String value) {
            addCriterion("item_name <", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameLessThanOrEqualTo(String value) {
            addCriterion("item_name <=", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameLike(String value) {
            addCriterion("item_name like", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameNotLike(String value) {
            addCriterion("item_name not like", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameIn(List<String> values) {
            addCriterion("item_name in", values, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameNotIn(List<String> values) {
            addCriterion("item_name not in", values, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameBetween(String value1, String value2) {
            addCriterion("item_name between", value1, value2, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameNotBetween(String value1, String value2) {
            addCriterion("item_name not between", value1, value2, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlIsNull() {
            addCriterion("item_icon_url is null");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlIsNotNull() {
            addCriterion("item_icon_url is not null");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlEqualTo(String value) {
            addCriterion("item_icon_url =", value, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlNotEqualTo(String value) {
            addCriterion("item_icon_url <>", value, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlGreaterThan(String value) {
            addCriterion("item_icon_url >", value, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlGreaterThanOrEqualTo(String value) {
            addCriterion("item_icon_url >=", value, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlLessThan(String value) {
            addCriterion("item_icon_url <", value, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlLessThanOrEqualTo(String value) {
            addCriterion("item_icon_url <=", value, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlLike(String value) {
            addCriterion("item_icon_url like", value, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlNotLike(String value) {
            addCriterion("item_icon_url not like", value, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlIn(List<String> values) {
            addCriterion("item_icon_url in", values, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlNotIn(List<String> values) {
            addCriterion("item_icon_url not in", values, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlBetween(String value1, String value2) {
            addCriterion("item_icon_url between", value1, value2, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemIconUrlNotBetween(String value1, String value2) {
            addCriterion("item_icon_url not between", value1, value2, "itemIconUrl");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceIsNull() {
            addCriterion("item_bill_price is null");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceIsNotNull() {
            addCriterion("item_bill_price is not null");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceEqualTo(Double value) {
            addCriterion("item_bill_price =", value, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceNotEqualTo(Double value) {
            addCriterion("item_bill_price <>", value, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceGreaterThan(Double value) {
            addCriterion("item_bill_price >", value, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("item_bill_price >=", value, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceLessThan(Double value) {
            addCriterion("item_bill_price <", value, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceLessThanOrEqualTo(Double value) {
            addCriterion("item_bill_price <=", value, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceIn(List<Double> values) {
            addCriterion("item_bill_price in", values, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceNotIn(List<Double> values) {
            addCriterion("item_bill_price not in", values, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceBetween(Double value1, Double value2) {
            addCriterion("item_bill_price between", value1, value2, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemBillPriceNotBetween(Double value1, Double value2) {
            addCriterion("item_bill_price not between", value1, value2, "itemBillPrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceIsNull() {
            addCriterion("item_effe_price is null");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceIsNotNull() {
            addCriterion("item_effe_price is not null");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceEqualTo(Double value) {
            addCriterion("item_effe_price =", value, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceNotEqualTo(Double value) {
            addCriterion("item_effe_price <>", value, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceGreaterThan(Double value) {
            addCriterion("item_effe_price >", value, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceGreaterThanOrEqualTo(Double value) {
            addCriterion("item_effe_price >=", value, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceLessThan(Double value) {
            addCriterion("item_effe_price <", value, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceLessThanOrEqualTo(Double value) {
            addCriterion("item_effe_price <=", value, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceIn(List<Double> values) {
            addCriterion("item_effe_price in", values, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceNotIn(List<Double> values) {
            addCriterion("item_effe_price not in", values, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceBetween(Double value1, Double value2) {
            addCriterion("item_effe_price between", value1, value2, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemEffePriceNotBetween(Double value1, Double value2) {
            addCriterion("item_effe_price not between", value1, value2, "itemEffePrice");
            return (Criteria) this;
        }

        public Criteria andItemCountIsNull() {
            addCriterion("item_count is null");
            return (Criteria) this;
        }

        public Criteria andItemCountIsNotNull() {
            addCriterion("item_count is not null");
            return (Criteria) this;
        }

        public Criteria andItemCountEqualTo(Double value) {
            addCriterion("item_count =", value, "itemCount");
            return (Criteria) this;
        }

        public Criteria andItemCountNotEqualTo(Double value) {
            addCriterion("item_count <>", value, "itemCount");
            return (Criteria) this;
        }

        public Criteria andItemCountGreaterThan(Double value) {
            addCriterion("item_count >", value, "itemCount");
            return (Criteria) this;
        }

        public Criteria andItemCountGreaterThanOrEqualTo(Double value) {
            addCriterion("item_count >=", value, "itemCount");
            return (Criteria) this;
        }

        public Criteria andItemCountLessThan(Double value) {
            addCriterion("item_count <", value, "itemCount");
            return (Criteria) this;
        }

        public Criteria andItemCountLessThanOrEqualTo(Double value) {
            addCriterion("item_count <=", value, "itemCount");
            return (Criteria) this;
        }

        public Criteria andItemCountIn(List<Double> values) {
            addCriterion("item_count in", values, "itemCount");
            return (Criteria) this;
        }

        public Criteria andItemCountNotIn(List<Double> values) {
            addCriterion("item_count not in", values, "itemCount");
            return (Criteria) this;
        }

        public Criteria andItemCountBetween(Double value1, Double value2) {
            addCriterion("item_count between", value1, value2, "itemCount");
            return (Criteria) this;
        }

        public Criteria andItemCountNotBetween(Double value1, Double value2) {
            addCriterion("item_count not between", value1, value2, "itemCount");
            return (Criteria) this;
        }

        public Criteria andAttributeIsNull() {
            addCriterion("attribute is null");
            return (Criteria) this;
        }

        public Criteria andAttributeIsNotNull() {
            addCriterion("attribute is not null");
            return (Criteria) this;
        }

        public Criteria andAttributeEqualTo(String value) {
            addCriterion("attribute =", value, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeNotEqualTo(String value) {
            addCriterion("attribute <>", value, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeGreaterThan(String value) {
            addCriterion("attribute >", value, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeGreaterThanOrEqualTo(String value) {
            addCriterion("attribute >=", value, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeLessThan(String value) {
            addCriterion("attribute <", value, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeLessThanOrEqualTo(String value) {
            addCriterion("attribute <=", value, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeLike(String value) {
            addCriterion("attribute like", value, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeNotLike(String value) {
            addCriterion("attribute not like", value, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeIn(List<String> values) {
            addCriterion("attribute in", values, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeNotIn(List<String> values) {
            addCriterion("attribute not in", values, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeBetween(String value1, String value2) {
            addCriterion("attribute between", value1, value2, "attribute");
            return (Criteria) this;
        }

        public Criteria andAttributeNotBetween(String value1, String value2) {
            addCriterion("attribute not between", value1, value2, "attribute");
            return (Criteria) this;
        }

        public Criteria andBillAmountIsNull() {
            addCriterion("bill_amount is null");
            return (Criteria) this;
        }

        public Criteria andBillAmountIsNotNull() {
            addCriterion("bill_amount is not null");
            return (Criteria) this;
        }

        public Criteria andBillAmountEqualTo(Double value) {
            addCriterion("bill_amount =", value, "billAmount");
            return (Criteria) this;
        }

        public Criteria andBillAmountNotEqualTo(Double value) {
            addCriterion("bill_amount <>", value, "billAmount");
            return (Criteria) this;
        }

        public Criteria andBillAmountGreaterThan(Double value) {
            addCriterion("bill_amount >", value, "billAmount");
            return (Criteria) this;
        }

        public Criteria andBillAmountGreaterThanOrEqualTo(Double value) {
            addCriterion("bill_amount >=", value, "billAmount");
            return (Criteria) this;
        }

        public Criteria andBillAmountLessThan(Double value) {
            addCriterion("bill_amount <", value, "billAmount");
            return (Criteria) this;
        }

        public Criteria andBillAmountLessThanOrEqualTo(Double value) {
            addCriterion("bill_amount <=", value, "billAmount");
            return (Criteria) this;
        }

        public Criteria andBillAmountIn(List<Double> values) {
            addCriterion("bill_amount in", values, "billAmount");
            return (Criteria) this;
        }

        public Criteria andBillAmountNotIn(List<Double> values) {
            addCriterion("bill_amount not in", values, "billAmount");
            return (Criteria) this;
        }

        public Criteria andBillAmountBetween(Double value1, Double value2) {
            addCriterion("bill_amount between", value1, value2, "billAmount");
            return (Criteria) this;
        }

        public Criteria andBillAmountNotBetween(Double value1, Double value2) {
            addCriterion("bill_amount not between", value1, value2, "billAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountIsNull() {
            addCriterion("effe_amount is null");
            return (Criteria) this;
        }

        public Criteria andEffeAmountIsNotNull() {
            addCriterion("effe_amount is not null");
            return (Criteria) this;
        }

        public Criteria andEffeAmountEqualTo(Double value) {
            addCriterion("effe_amount =", value, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountNotEqualTo(Double value) {
            addCriterion("effe_amount <>", value, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountGreaterThan(Double value) {
            addCriterion("effe_amount >", value, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountGreaterThanOrEqualTo(Double value) {
            addCriterion("effe_amount >=", value, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountLessThan(Double value) {
            addCriterion("effe_amount <", value, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountLessThanOrEqualTo(Double value) {
            addCriterion("effe_amount <=", value, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountIn(List<Double> values) {
            addCriterion("effe_amount in", values, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountNotIn(List<Double> values) {
            addCriterion("effe_amount not in", values, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountBetween(Double value1, Double value2) {
            addCriterion("effe_amount between", value1, value2, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andEffeAmountNotBetween(Double value1, Double value2) {
            addCriterion("effe_amount not between", value1, value2, "effeAmount");
            return (Criteria) this;
        }

        public Criteria andDeviceIdIsNull() {
            addCriterion("device_id is null");
            return (Criteria) this;
        }

        public Criteria andDeviceIdIsNotNull() {
            addCriterion("device_id is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceIdEqualTo(Integer value) {
            addCriterion("device_id =", value, "deviceId");
            return (Criteria) this;
        }

        public Criteria andDeviceIdNotEqualTo(Integer value) {
            addCriterion("device_id <>", value, "deviceId");
            return (Criteria) this;
        }

        public Criteria andDeviceIdGreaterThan(Integer value) {
            addCriterion("device_id >", value, "deviceId");
            return (Criteria) this;
        }

        public Criteria andDeviceIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("device_id >=", value, "deviceId");
            return (Criteria) this;
        }

        public Criteria andDeviceIdLessThan(Integer value) {
            addCriterion("device_id <", value, "deviceId");
            return (Criteria) this;
        }

        public Criteria andDeviceIdLessThanOrEqualTo(Integer value) {
            addCriterion("device_id <=", value, "deviceId");
            return (Criteria) this;
        }

        public Criteria andDeviceIdIn(List<Integer> values) {
            addCriterion("device_id in", values, "deviceId");
            return (Criteria) this;
        }

        public Criteria andDeviceIdNotIn(List<Integer> values) {
            addCriterion("device_id not in", values, "deviceId");
            return (Criteria) this;
        }

        public Criteria andDeviceIdBetween(Integer value1, Integer value2) {
            addCriterion("device_id between", value1, value2, "deviceId");
            return (Criteria) this;
        }

        public Criteria andDeviceIdNotBetween(Integer value1, Integer value2) {
            addCriterion("device_id not between", value1, value2, "deviceId");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("operator is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("operator is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(Integer value) {
            addCriterion("operator =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(Integer value) {
            addCriterion("operator <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(Integer value) {
            addCriterion("operator >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(Integer value) {
            addCriterion("operator >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(Integer value) {
            addCriterion("operator <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(Integer value) {
            addCriterion("operator <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<Integer> values) {
            addCriterion("operator in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<Integer> values) {
            addCriterion("operator not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(Integer value1, Integer value2) {
            addCriterion("operator between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(Integer value1, Integer value2) {
            addCriterion("operator not between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Short value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Short value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Short value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Short value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Short value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Short value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Short> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Short> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Short value1, Short value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Short value1, Short value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifyIsNull() {
            addCriterion("gmt_modify is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifyIsNotNull() {
            addCriterion("gmt_modify is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifyEqualTo(Date value) {
            addCriterion("gmt_modify =", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyNotEqualTo(Date value) {
            addCriterion("gmt_modify <>", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyGreaterThan(Date value) {
            addCriterion("gmt_modify >", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modify >=", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyLessThan(Date value) {
            addCriterion("gmt_modify <", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modify <=", value, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyIn(List<Date> values) {
            addCriterion("gmt_modify in", values, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyNotIn(List<Date> values) {
            addCriterion("gmt_modify not in", values, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyBetween(Date value1, Date value2) {
            addCriterion("gmt_modify between", value1, value2, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andGmtModifyNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modify not between", value1, value2, "gmtModify");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdIsNull() {
            addCriterion("item_category_id is null");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdIsNotNull() {
            addCriterion("item_category_id is not null");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdEqualTo(Integer value) {
            addCriterion("item_category_id =", value, "itemCategoryId");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdNotEqualTo(Integer value) {
            addCriterion("item_category_id <>", value, "itemCategoryId");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdGreaterThan(Integer value) {
            addCriterion("item_category_id >", value, "itemCategoryId");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("item_category_id >=", value, "itemCategoryId");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdLessThan(Integer value) {
            addCriterion("item_category_id <", value, "itemCategoryId");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdLessThanOrEqualTo(Integer value) {
            addCriterion("item_category_id <=", value, "itemCategoryId");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdIn(List<Integer> values) {
            addCriterion("item_category_id in", values, "itemCategoryId");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdNotIn(List<Integer> values) {
            addCriterion("item_category_id not in", values, "itemCategoryId");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdBetween(Integer value1, Integer value2) {
            addCriterion("item_category_id between", value1, value2, "itemCategoryId");
            return (Criteria) this;
        }

        public Criteria andItemCategoryIdNotBetween(Integer value1, Integer value2) {
            addCriterion("item_category_id not between", value1, value2, "itemCategoryId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}