package com.xianzaishi.trade.dal.daonew;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.trade.dal.model.OrderDiscount;

public interface OrderDiscountMapper {

  /**
   * 插入一条记录
   * @param record
   * @return
   */
  int insert(OrderDiscount record);

  /**
   * 批量插入
   * @param orderDiscountList
   * @return
   */
  int batchInsert(List<OrderDiscount> orderDiscountList);
  
  /**
   * 根据订单查询优惠信息
   * @param orderId
   * @return
   */
  List<OrderDiscount> select(@Param("orderId") Long orderId,@Param("type") Short type);
  
  /**
   * 按照订单id更新状态
   * @param orderId
   * @param status
   * @return
   */
  int updateDiscountStatus(@Param("orderId") Long orderId, @Param("status") Short status);
  
  /**
   * 更新属性
   * @param record
   * @return
   */
  int update(OrderDiscount record);
}
