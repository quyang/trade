package com.xianzaishi.trade.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 *
 * @author nianyue.hyj
 * @since 2016.10.17
 */
public class MD5Utils {

	private final static Random RAND = new Random(System.currentTimeMillis());

	private final static Logger logger = LoggerFactory.getLogger(MD5Utils.class);

	public static long rand() {
		return RAND.nextLong();
	}

	/**
	 * 返回[0,bound)的随机数
	 * 
	 * @param bound
	 * @return
	 */
	public static int rand(int bound) {
		return RAND.nextInt(bound);
	}

	public static String md5(String data) {
		String result = "";
		if (data != null) {
			try {
				MessageDigest mdInst = MessageDigest.getInstance("MD5");
				mdInst.update(data.getBytes());
				byte[] md5 = mdInst.digest();
				result = Hex.encodeHexString(md5).toUpperCase();
			} catch (NoSuchAlgorithmException e) {
				logger.error("No MD5 algorithm in MD5Utils", e);
			}

		}
		return result;
	}

	public static String randString() {
		return md5(String.valueOf(rand()));
	}

}
