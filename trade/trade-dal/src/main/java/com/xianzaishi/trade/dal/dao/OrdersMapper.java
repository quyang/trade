package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.Orders;
import com.xianzaishi.trade.dal.model.OrdersExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface OrdersMapper {
    @SelectProvider(type=OrdersSqlProvider.class, method="countByExample")
    int countByExample(OrdersExample example);

    @DeleteProvider(type=OrdersSqlProvider.class, method="deleteByExample")
    int deleteByExample(OrdersExample example);

    @Delete({
        "delete from orders",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into orders (user_id, pay_amount, ",
        "effe_amount, channel_type, ",
        "device_code, cashier_id, ",
        "user_address_id, shop_id, ",
        "attribute, status, ",
        "gmt_create, gmt_modify, ",
        "bill_amount, coupon_id, ",
        "discount_amount, gmt_pay, ",
        "gmt_receive, seq, ",
        "user_address, gmt_distribution, ",
        "credit, order_type)",
        "values (#{userId,jdbcType=BIGINT}, #{payAmount,jdbcType=DOUBLE}, ",
        "#{effeAmount,jdbcType=DOUBLE}, #{channelType,jdbcType=SMALLINT}, ",
        "#{deviceCode,jdbcType=VARCHAR}, #{cashierId,jdbcType=INTEGER}, ",
        "#{userAddressId,jdbcType=BIGINT}, #{shopId,jdbcType=INTEGER}, ",
        "#{attribute,jdbcType=VARCHAR}, #{status,jdbcType=SMALLINT}, ",
        "#{gmtCreate,jdbcType=TIMESTAMP}, #{gmtModify,jdbcType=TIMESTAMP}, ",
        "#{billAmount,jdbcType=DOUBLE}, #{couponId,jdbcType=BIGINT}, ",
        "#{discountAmount,jdbcType=DOUBLE}, #{gmtPay,jdbcType=TIMESTAMP}, ",
        "#{gmtReceive,jdbcType=TIMESTAMP}, #{seq,jdbcType=INTEGER}, ",
        "#{userAddress,jdbcType=VARCHAR}, #{gmtDistribution,jdbcType=TIMESTAMP}, ",
        "#{credit,jdbcType=INTEGER}, #{orderType,jdbcType=SMALLINT})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(Orders record);

    @InsertProvider(type=OrdersSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insertSelective(Orders record);

    @SelectProvider(type=OrdersSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="pay_amount", property="payAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="effe_amount", property="effeAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="channel_type", property="channelType", jdbcType=JdbcType.SMALLINT),
        @Result(column="device_code", property="deviceCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="cashier_id", property="cashierId", jdbcType=JdbcType.INTEGER),
        @Result(column="user_address_id", property="userAddressId", jdbcType=JdbcType.BIGINT),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="attribute", property="attribute", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="bill_amount", property="billAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="coupon_id", property="couponId", jdbcType=JdbcType.BIGINT),
        @Result(column="discount_amount", property="discountAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="gmt_pay", property="gmtPay", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_receive", property="gmtReceive", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="seq", property="seq", jdbcType=JdbcType.INTEGER),
        @Result(column="user_address", property="userAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="gmt_distribution", property="gmtDistribution", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="credit", property="credit", jdbcType=JdbcType.INTEGER),
        @Result(column="order_type", property="orderType", jdbcType=JdbcType.SMALLINT)
    })
    List<Orders> selectByExample(OrdersExample example);

    @Select({
        "select",
        "id, user_id, pay_amount, effe_amount, channel_type, device_code, cashier_id, ",
        "user_address_id, shop_id, attribute, status, gmt_create, gmt_modify, bill_amount, ",
        "coupon_id, discount_amount, gmt_pay, gmt_receive, seq, user_address, gmt_distribution, ",
        "credit, order_type",
        "from orders",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="pay_amount", property="payAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="effe_amount", property="effeAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="channel_type", property="channelType", jdbcType=JdbcType.SMALLINT),
        @Result(column="device_code", property="deviceCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="cashier_id", property="cashierId", jdbcType=JdbcType.INTEGER),
        @Result(column="user_address_id", property="userAddressId", jdbcType=JdbcType.BIGINT),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="attribute", property="attribute", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="bill_amount", property="billAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="coupon_id", property="couponId", jdbcType=JdbcType.BIGINT),
        @Result(column="discount_amount", property="discountAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="gmt_pay", property="gmtPay", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_receive", property="gmtReceive", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="seq", property="seq", jdbcType=JdbcType.INTEGER),
        @Result(column="user_address", property="userAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="gmt_distribution", property="gmtDistribution", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="credit", property="credit", jdbcType=JdbcType.INTEGER),
        @Result(column="order_type", property="orderType", jdbcType=JdbcType.SMALLINT)
    })
    Orders selectByPrimaryKey(Long id);

    @UpdateProvider(type=OrdersSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Orders record, @Param("example") OrdersExample example);

    @UpdateProvider(type=OrdersSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Orders record, @Param("example") OrdersExample example);

    @UpdateProvider(type=OrdersSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Orders record);

    @Update({
        "update orders",
        "set user_id = #{userId,jdbcType=BIGINT},",
          "pay_amount = #{payAmount,jdbcType=DOUBLE},",
          "effe_amount = #{effeAmount,jdbcType=DOUBLE},",
          "channel_type = #{channelType,jdbcType=SMALLINT},",
          "device_code = #{deviceCode,jdbcType=VARCHAR},",
          "cashier_id = #{cashierId,jdbcType=INTEGER},",
          "user_address_id = #{userAddressId,jdbcType=BIGINT},",
          "shop_id = #{shopId,jdbcType=INTEGER},",
          "attribute = #{attribute,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=SMALLINT},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP},",
          "bill_amount = #{billAmount,jdbcType=DOUBLE},",
          "coupon_id = #{couponId,jdbcType=BIGINT},",
          "discount_amount = #{discountAmount,jdbcType=DOUBLE},",
          "gmt_pay = #{gmtPay,jdbcType=TIMESTAMP},",
          "gmt_receive = #{gmtReceive,jdbcType=TIMESTAMP},",
          "seq = #{seq,jdbcType=INTEGER},",
          "user_address = #{userAddress,jdbcType=VARCHAR},",
          "gmt_distribution = #{gmtDistribution,jdbcType=TIMESTAMP},",
          "credit = #{credit,jdbcType=INTEGER},",
          "order_type = #{orderType,jdbcType=SMALLINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Orders record);
}