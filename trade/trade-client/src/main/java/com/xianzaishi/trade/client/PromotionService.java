package com.xianzaishi.trade.client;

import java.util.List;

import com.xianzaishi.trade.client.vo.UserCouponDiscontQueryVO;
import com.xianzaishi.trade.client.vo.UserCouponDiscontResultVO;
import com.xianzaishi.trade.client.vo.UserCouponVO;

/**
 * 优惠券对外新接口
 * 
 * @author zhancang
 */
public interface PromotionService {

  Result<UserCouponDiscontResultVO> selectMathCoupon(UserCouponDiscontQueryVO query);

  Result<UserCouponDiscontResultVO> calculateDiscountPriceWithCoupon(UserCouponDiscontQueryVO query);
  
  Result<List<UserCouponVO>> selectUserCoupon(UserCouponDiscontQueryVO query);

}
