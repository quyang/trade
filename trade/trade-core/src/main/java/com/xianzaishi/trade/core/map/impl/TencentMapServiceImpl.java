package com.xianzaishi.trade.core.map.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.xianzaishi.trade.core.map.MapService;
import com.xianzaishi.trade.utils.HttpClientUtils;

/**
 * 
 * 日调用量：1万次 / Key 并发数：5次 / key / 秒
 * 
 * @author Croky.Zheng 2016年8月27日
 */
@Component(value = "tencentMapService")
public class TencentMapServiceImpl implements MapService {
	private static final Logger log = LoggerFactory.getLogger(TencentMapServiceImpl.class);

	private HttpClientUtils httpClient = HttpClientUtils.getInstance();

	private AtomicInteger requestCount = new AtomicInteger(0);

	private final static String[] keys = new String[] { "T6IBZ-R2LRR-ELHWB-WSJPL-O4T35-QHFDT",
			"7N5BZ-W3WWX-WRA4D-ZFNJX-MR3TE-DKFQI", "LJYBZ-RHT3P-QBMD6-VDKP5-7PO56-J3BOR",
			"IDUBZ-Z3TRP-2SHDB-LMH4R-RFAMO-TUBKU", "QRHBZ-GW73V-HGMP7-UNBXN-IPWG3-QVFEO" };

	/**
	 * 根据经纬度获取对应的地址 lat<纬度>,lng<经度> location 必 位置坐标，格式： location=lat<纬度>,lng
	 * <经度> location= 39.984154,116.307490 coord_type - 输入的locations的坐标类型
	 * 可选值为[1,6]之间的整数，每个数字代表的类型说明： 1 GPS坐标 2 sogou经纬度 3 baidu经纬度 4 mapbar经纬度 5
	 * [默认]腾讯、google、高德坐标 6 sogou墨卡托 coord_type=3 get_poi - 是否返回周边POI列表：
	 * 1.返回；0不返回(默认) get_poi=1 poi_options - 用于控制Poi列表： 1
	 * poi_options=address_format=short 返回短地址，缺省时返回长地址 2 poi_options=radius=5000
	 * 半径，取值范围 1-5000（米） 3 poi_options=page_size=20 每页条数，取值范围 1-20 4
	 * poi_options=page_index=1 页码，取值范围 1-20 5 poi_options=policy=1/2/3 控制返回场景，
	 * policy=1[默认] 以地标+主要的路+近距离poi为主，着力描述当前位置； policy=2
	 * 到家场景：筛选合适收货的poi，并会细化收货地址，精确到楼栋； policy=3
	 * 出行场景：过滤掉车辆不易到达的POI(如一些景区内POI)，增加道路出路口、交叉口、大区域出入口类POI，
	 * 排序会根据真实API大用户的用户点击自动优化。 6 poi_options=category=分类词1,分类词2，
	 * 指定分类，多关键词英文逗号分隔； poi_filter=category<>分类词1,分类词2， 指定不包含分类，多关键词英文逗号分隔
	 * （支持类别参见：附录） 【单个参数写法示例】： poi_options=address_format=short
	 * 【多个参数英文分号间隔，写法示例】： poi_options=address_format=short;radius=5000;
	 * page_size=20;page_index=1;policy=2 key 必 开发密钥（key）
	 * key=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77 output - 返回格式：支持json/jsonp，默认json
	 * output=json callback - jsonp方式回调函数 callback=function1
	 * 
	 * @return
	 *         <table>
	 *         <thead>
	 *         <tr>
	 *         <th colspan="5">名称</th>
	 *         <th>类型</th>
	 *         <th>必有</th>
	 *         <th>说明</th>
	 *         </tr>
	 *         </thead><tbody>
	 *         <tr>
	 *         <td colspan="5">status</td>
	 *         <td>number</td>
	 *         <td>是</td>
	 *         <td>状态码，0为正常,<br/>
	 *         310请求参数信息有误，<br/>
	 *         311key格式错误,<br/>
	 *         306请求有护持信息请检查字符串,<br/>
	 *         110请求来源未被授权<br/>
	 *         </td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="5">message</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>状态说明</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="5">result</td>
	 *         <td>object</td>
	 *         <td>是</td>
	 *         <td>逆地址解析结果</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="51"></td>
	 *         <td colspan="4">address</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>地址描述</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="4">formatted_addresses</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>位置描述</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="2"></td>
	 *         <td colspan="3">recommend</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>经过腾讯地图优化过的描述方式，更具人性化特点</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">rough</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>大致位置，可用于对位置的粗略描述</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="4">address_component</td>
	 *         <td>object</td>
	 *         <td>是</td>
	 *         <td>地址部件，address不满足需求时可自行拼接</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="6"></td>
	 *         <td colspan="3">nation</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>国家</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">province</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>省</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">city</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>市</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">district</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>区，可能为空字串</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">street</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>街道，可能为空字串</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">street_number</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>门牌，可能为空字串</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="4">ad_info</td>
	 *         <td>object</td>
	 *         <td>是</td>
	 *         <td>行政区划信息</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="9"></td>
	 *         <td colspan="3">adcode</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>行政区划代码</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">name</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>行政区划名称</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">location</td>
	 *         <td>object</td>
	 *         <td>是</td>
	 *         <td>行政区划中心点坐标</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="2"></td>
	 *         <td colspan="2">lat</td>
	 *         <td>number</td>
	 *         <td>是</td>
	 *         <td>纬度</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="2">lng</td>
	 *         <td>number</td>
	 *         <td>是</td>
	 *         <td>经度</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">nation</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>国家</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">province</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>省 / 直辖市</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">city</td>
	 *         <td>string</td>
	 *         <td>是</td>
	 *         <td>市 / 地级区 及同级行政区划</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">district</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>区 / 县级市 及同级行政区划</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="4">address_reference</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>坐标相对位置参考</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="20"></td>
	 *         <td colspan="3">famous_area</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>知名区域，如商圈或人们普遍认为有较高知名度的区域</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="6"></td>
	 *         <td colspan="2">title</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>名称/标题</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="2">location</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>坐标</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="2"></td>
	 *         <td>lat</td>
	 *         <td>number</td>
	 *         <td></td>
	 *         <td>纬度</td>
	 *         </tr>
	 *         <tr>
	 *         <td>lng</td>
	 *         <td>number</td>
	 *         <td></td>
	 *         <td>经度</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="2">_distance</td>
	 *         <td>number</td>
	 *         <td></td>
	 *         <td>此参考位置到输入坐标的直线距离</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="2">_dir_desc</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>此参考位置到输入坐标的方位关系，如：北、南、内</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">town</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>乡镇街道</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="6"></td>
	 *         <td colspan="2">title</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>名称/标题</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="2">location</td>
	 *         <td>objct</td>
	 *         <td></td>
	 *         <td>坐标</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="2"></td>
	 *         <td colspan="1">lat</td>
	 *         <td>number</td>
	 *         <td></td>
	 *         <td>纬度</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="1">lng</td>
	 *         <td>number</td>
	 *         <td></td>
	 *         <td>经度</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="2">_distance</td>
	 *         <td>number</td>
	 *         <td></td>
	 *         <td>此参考位置到输入坐标的直线距离</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="2">_dir_desc</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>此参考位置到输入坐标的方位关系，如：北、南、内</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">landmark_l1</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>一级地标，可识别性较强、规模较大的地点、小区等</br>
	 *         【注】对象结构同 famous_area</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">landmark_l2</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>二级地标，较一级地标更为精确，规模更小</br>
	 *         【注】：对象结构同 famous_area</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">street</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>街道&nbsp;&nbsp;&nbsp;&nbsp;【注】：对象结构同 famous_area</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">street_number</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>门牌&nbsp;&nbsp;&nbsp;&nbsp;【注】：对象结构同 famous_area</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">crossroad</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>交叉路口&nbsp;&nbsp;&nbsp;&nbsp;【注】：对象结构同 famous_area</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">water</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>水系&nbsp;&nbsp;&nbsp;&nbsp;【注】：对象结构同 famous_area</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="4">pois</td>
	 *         <td>array</td>
	 *         <td></td>
	 *         <td>POI数组，对象中每个子项为一个POI对象</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="8"></td>
	 *         <td colspan="3">id</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>POI唯一标识</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">title</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>poi名称</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">address</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>地址</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">category</td>
	 *         <td>string</td>
	 *         <td></td>
	 *         <td>POI分类</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="3">location</td>
	 *         <td>object</td>
	 *         <td></td>
	 *         <td>提示所述位置坐标</td>
	 *         </tr>
	 *         <tr>
	 *         <td rowspan="2"></td>
	 *         <td colspan="2">lat</td>
	 *         <td>number</td>
	 *         <td></td>
	 *         <td>纬度</td>
	 *         </tr>
	 *         <tr>
	 *         <td colspan="2">lng</td>
	 *         <td>number</td>
	 *         <td></td>
	 *         <td>经度</td>
	 *         </tr>
	 * 
	 *         <tr>
	 *         <td colspan="3">_distance</td>
	 *         <td>number</td>
	 *         <td></td>
	 *         <td>该POI到逆地址解析传入的坐标的直线距离</td>
	 *         </tr>
	 * 
	 *         </tbody>
	 *         </table>
	 */
	@Override
	public String geocoder(double lat, double lng) {
		// http://apis.map.qq.com/uri/v1/geocoder?coord=39.904956,116.389449&referer=myapp
		StringBuffer sb = new StringBuffer();
		sb.append("http://apis.map.qq.com/ws/geocoder/v1/").append("?").append("location=")
				.append(getParam(lat + "," + lng)).append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());

	}

	/**
	 * 根据地址获取经纬度
	 * http://apis.map.qq.com/ws/geocoder/v1/?address=北京市海淀区彩和坊路海淀西大街74号&key=
	 * OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77
	 * 
	 * @param address
	 * @return
	 */
	@Override
	public String geocoder(String address) {
		// http://apis.map.qq.com/uri/v1/geocoder?coord=39.904956,116.389449&referer=myapp
		StringBuffer sb = new StringBuffer();
		sb.append("http://apis.map.qq.com/ws/geocoder/v1/").append("?").append("address=").append(getParam(address))
				.append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());

	}

	/**
	 * http://apis.map.qq.com/ws/place/v1/search 参数 必填 说明 示例 keyword 是
	 * POI搜索关键字，用于全文检索字段
	 * keyword=酒店，注意键值要进行URL编码（推荐encodeURI），如keyword=%e9%85%92%e5%ba%97 boundary
	 * 是 搜索地理范围 [查看boundary语法] 示例1，指定地区名称，不自动扩大范围： boundary=region(北京,0)
	 * 示例2，周边搜索（圆形范围）：boundary=nearby(39.908491,116.374328,1000)
	 * 示例3，矩形区域范围：boundary=rectangle(39.9072,116.3689,39.9149,116.3793) filter 否
	 * 筛选条件： 最多支持五个分类 [查看filter语法] 搜索指定分类 filter=category=公交站 搜索多个分类
	 * filter=category=大学,中学 排除指定分类 filter=category<>商务楼宇 （注意参数值要进行url编码）
	 * orderby 否 排序方式 [查看orderby语法] 例1：orderby=_distance desc
	 * 例2：orderby=_distance 默认升序（低到高） page_size 否 每页条目数，最大限制为20条。 page_size=10
	 * page_index 否 第x页，默认第1页 page_index=2 key 是 开发密钥（key）
	 * key=d84d6d83e0e51e481e50454ccbe8986b output 否 返回格式： 支持json/jsonp，默认json
	 * output=json callback 否 jsonp方式回调函数 callback=function1
	 * 
	 * @return
	 */
	@Override
	public String search(String keyWord) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://apis.map.qq.com/ws/place/v1/search").append("?").append("keyword=").append(getParam(keyWord))
				.append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	/**
	 * 用于获取输入关键字的补完与提示，帮助用户快速输入
	 * 
	 * @param keyWord
	 * @return
	 */
	@Override
	public String suggestion(String keyWord, String region) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://apis.map.qq.com/ws/place/v1/suggestion").append("?").append("keyword=")
				.append(getParam(keyWord)).append("&region=").append(getParam(region)).append("&region_fix=")
				.append("1")// 固定城市
				.append("&policy=").append("1").append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	@Override
	public String district() {
		// list接口： 获取全部行政区划数据。http://apis.map.qq.com/ws/district/v1/list
		// getchildren接口：获取指定行政区划的子级行政区划
		// http://apis.map.qq.com/ws/district/v1/getchildren
		// 参数:id=
		// search接口：根据关键词搜索行政区划 http://apis.map.qq.com/ws/district/v1/search
		// 参数keyword=xxxx&key=xxxx
		return null;
	}

	/**
	 * 将其他类型的坐标系转换成腾讯地图坐标系 locations 必 预转换的坐标，支持批量转换，
	 * 格式：纬度前，经度后，纬度和经度之间用","分隔，每组坐标之间使用";"分隔； 批量支持坐标个数以HTTP GET方法请求上限为准
	 * locations=39.12,116.83;30.21,115.43 type 必 输入的locations的坐标类型
	 * 可选值为[1,6]之间的整数，每个数字代表的类型说明： 1 GPS坐标 2 sogou经纬度 3 baidu经纬度 4 mapbar经纬度 5
	 * [默认]腾讯、google、高德坐标 6 sogou墨卡托 type=3 key 必 开发密钥（key）
	 * key=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77 output - 返回格式：支持json/jsonp，默认json
	 * output=json callback - jsonp方式回调函数 callback=function1
	 * 
	 * @return 参数名 类型 备注 status number 状态码，0为正常, 310请求参数信息有误， 311key格式错误,
	 *         306请求有护持信息请检查字符串, 110请求来源未被授权 message string 对status的描述 locations
	 *         json array 坐标转换结果，转换后的坐标顺序与输入顺序一致 lat number 纬度 lng number 经度
	 */
	@Override
	public String translate(double lat, double lng) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://apis.map.qq.com/ws/coord/v1/translate").append("?").append("location=")
				.append(getParam(lat + "," + lng)).append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
		// http://apis.map.qq.com/ws/coord/v1/translate?locations=39.12,116.83;30.21,115.43&type=3&key=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77
	}

	/**
	 * 根据IP地址定位
	 * 
	 * @param ip
	 * @return
	 */
	@Override
	public String getPositionByIP(String ip) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://apis.map.qq.com/ws/location/v1/ip").append("?").append("ip=").append(getParam(ip))
				.append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	/**
	 * 
	 * 参数 必填 说明 示例 mode - 计算方式：driving（驾车）、walking（步行） 默认：driving mode=driving
	 * from 必 起点坐标，格式：lat,lng;lat,lng... （经度与纬度用英文逗号分隔，坐标间用英文分号分隔）
	 * from=39.071510,117.190091 to 必 终点坐标，格式：lat,lng;lat,lng...
	 * （经度与纬度用英文逗号分隔，坐标间用英文分号分隔） 注意：本服务支持单起点到多终点，或多起点到单终点，from和to参数仅可有一个为多坐标
	 * to=39.071510,117.190091;40.007632, 116.389160;39.840177,116.463318 output
	 * - 返回格式：支持json/jsonp，默认json output=json callback - jsonp方式回调函数
	 * callback=function1 key 必 开发密钥（key）
	 * key=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77
	 * 
	 * @param fromLat
	 *            来自维度
	 * @param fromLng
	 *            来自经度
	 * @param toLat
	 *            目标维度
	 * @param toLng
	 *            目标经度
	 * @return
	 */
	@Override
	public String distance(double fromLat, double fromLng, double toLat, double toLng) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://apis.map.qq.com/ws/distance/v1/").append("?").append("mode=").append(getParam("walking"))
				.append("&from=").append(getParam(fromLat + "," + fromLng)).append("&to=")
				.append(getParam(toLat + "," + toLng)).append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());

	}

	@Override
	public double getDistance(double fromLat, double fromLng, double toLat, double toLng) {
		double radFromLat = rad(fromLat);
		double radToLat = rad(toLat);
		double a = radFromLat - radToLat;
		double b = rad(fromLng) - rad(toLng);
		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(radFromLat) * Math.cos(radToLat) * Math.pow(Math.sin(b / 2), 2)));
		s = s * 6378.137;// EARTH_RADIUS地球半径
		s = Math.round(s * 10000) / 10000;
		return s;

	}

	/**
	 * id 三选一 街景场景id，街景场景id可以通过地图工具>街景拾取器查询。id和location必须且只能存在一个
	 * id=10011504120403141232200 location 位置坐标，格式： location=lat<纬度>,lng
	 * <经度> id和location必须且只能存在一个 location= 39.984154,116.307490 poi 能过指定地点id（poi
	 * id）获取街景信息 poi=16459339205568630404 radius 否
	 * 计算半径，取该坐标指定半径范围内的全景点ID。最大值：200，单位：米， 默认值：200。 radius=50 output 否
	 * 数据返回格式，支持：json/jsonp，默认json output=jsonp或者 output=json callback 否
	 * 回调函数，output=jsonp时设置 callback=function1 key 是 开发者密钥，申请密钥
	 * key=OB4BZ-D4W3U-7BVVO-4PJWW-6TKDJ-WPB77
	 * 
	 * @return
	 */
	@Override
	public String streetview(double lat, double lng, int radius) {
		if (radius < 1) {
			radius = 1;
		}
		if (radius > 200) {
			radius = 200;
		}
		StringBuffer sb = new StringBuffer();
		sb.append("http://apis.map.qq.com/ws/streetview/v1/getpano").append("?").append("location=")
				.append(getParam(lat + "," + lng)).append("&radius=").append(radius).append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	private double rad(double d) {
		return d * Math.PI / 180.0;
	}

	@SuppressWarnings("deprecation")
	private String getParam(Object obj) {
		try {
			return URLEncoder.encode(obj.toString(), "utf-8");
		} catch (UnsupportedEncodingException e) {
			return URLEncoder.encode(obj.toString());
		}
	}

	private String getKey() {
		if (log.isDebugEnabled()) {
			log.debug("request count:" + requestCount.get());
		}
		return keys[requestCount.incrementAndGet() % 5];
	}

}
