package com.xianzaishi.trade.core.order.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.config.AlipayConfigV2;
import com.croky.util.DateUtils;
import com.croky.util.NetUtils;
import com.croky.util.StringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.common.domain.RpcResult;
import com.xianzaishi.couponcenter.client.discount.DiscountInfoService;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountCalResultDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountDetailDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.discount.enums.DiscountTypeEnum;
import com.xianzaishi.couponcenter.client.discount.query.DiscountQueryDTO;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.customercenter.client.customerservice.CustomerService;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.SkuTagDetail;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.swift.sdk.domain.SwiftRefundRequest;
import com.xianzaishi.swift.sdk.domain.SwiftRefundResponse;
import com.xianzaishi.trade.account.api.AliPayService;
import com.xianzaishi.trade.account.api.SwiftPayService;
import com.xianzaishi.trade.account.api.WeiXinPayService;
import com.xianzaishi.trade.client.vo.DiscountInfoVO;
import com.xianzaishi.trade.core.address.DeliveryAddressManager;
import com.xianzaishi.trade.core.coupon.PromotionManager;
import com.xianzaishi.trade.core.coupon.UserCouponManager;
import com.xianzaishi.trade.core.credit.UserCreditManager;
import com.xianzaishi.trade.core.order.OrdersManager;
import com.xianzaishi.trade.dal.dao.extend.CashierOperatorLogDAO;
import com.xianzaishi.trade.dal.dao.extend.OrderItemDAO;
import com.xianzaishi.trade.dal.dao.extend.OrderPayLogDAO;
import com.xianzaishi.trade.dal.dao.extend.OrderRefoudDAO;
import com.xianzaishi.trade.dal.dao.extend.OrdersDAO;
import com.xianzaishi.trade.dal.daonew.OrderDiscountMapper;
import com.xianzaishi.trade.dal.model.CashierOperatorLog;
import com.xianzaishi.trade.dal.model.OrderDiscount;
import com.xianzaishi.trade.dal.model.OrderItem;
import com.xianzaishi.trade.dal.model.OrderItemExample;
import com.xianzaishi.trade.dal.model.OrderPayLog;
import com.xianzaishi.trade.dal.model.OrderPayLogExample;
import com.xianzaishi.trade.dal.model.OrderRefoud;
import com.xianzaishi.trade.dal.model.Orders;
import com.xianzaishi.trade.dal.model.OrdersExample;
import com.xianzaishi.trade.dal.model.OrdersExample.Criteria;
import com.xianzaishi.trade.dal.model.ShoppingCart;
import com.xianzaishi.trade.dal.model.extend.OrderInfo;
import com.xianzaishi.trade.pay.ali.AliPay;
import com.xianzaishi.trade.pay.ali.AlipayConfig;
import com.xianzaishi.trade.pay.util.PayUtil;
import com.xianzaishi.trade.pay.weixin.GongZongHaoPayModel;
import com.xianzaishi.trade.pay.weixin.MobilePayModel;
import com.xianzaishi.trade.pay.weixin.UnifiedorderModel;
import com.xianzaishi.trade.pay.weixin.UnifiedorderReturnValue;
import com.xianzaishi.trade.pay.weixin.WeiXinPayConfig;
import com.xianzaishi.trade.pay.weixin.WeiXinPayHttpsRequest;
import com.xianzaishi.trade.utils.MD5Utils;
import com.xianzaishi.trade.utils.PageList;
import com.xianzaishi.trade.utils.Pagination;
import com.xianzaishi.trade.utils.TradeLog;
import com.xianzaishi.trade.utils.enums.OrderStatus;
import com.xianzaishi.trade.utils.enums.PayWay;
import com.xianzaishi.trade.utils.enums.Status;
import com.xianzaishi.trade.utils.env.SpringContextUtil;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.weixin.sdk.domain.WeiXinRefundRequest;
import com.xianzaishi.weixin.sdk.domain.WeiXinRefundResponse;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;
import com.xianzaishi.wms.trade.callback.client.itf.ITrackCallbackClient;
import com.xianzaisih.alipay.sdk.domain.AliPayAuthCodePayRequest;
import com.xianzaisih.alipay.sdk.domain.AliPayAuthCodePayResponse;
import com.xianzaisih.alipay.sdk.domain.AliPayQrPayRequest;
import com.xianzaisih.alipay.sdk.domain.AliPayQrPayResponse;
import com.xianzaisih.alipay.sdk.domain.AliPayRefundRequest;
import com.xianzaisih.alipay.sdk.domain.AliPayRefundResponse;

/**
 * 
 * @author Croky.Zheng 2016年8月22日
 */
@Component(value = "ordersManager")
// 默认只对RuntimeException.class有效
// rollbackFor = Exception.class
// noRollbackFor = NumberFormatException.class
public class OrdersManagerImpl implements OrdersManager {
    protected static final Logger log = LoggerFactory.getLogger(OrdersManagerImpl.class);

    @Resource
    protected OrdersDAO ordersDAO;

    @Resource
    protected OrderPayLogDAO orderPayLogDAO;

    @Resource
    protected OrderDiscountMapper orderDiscountMapper;
    
    @Resource
    protected CashierOperatorLogDAO cashierOperatorLogDAO;

    @Resource
    protected OrderRefoudDAO orderRefoudDAO;

    @Resource
    protected OrderItemDAO orderItemDAO;

    @Autowired
    protected UserCouponManager userCouponManager;

    @Autowired
    protected PromotionManager promotionManager;

    @Autowired
    private DeliveryAddressManager deliveryAddressManager;

    @Autowired
    private UserCreditManager userCreditManager;

    @Autowired
    private IInventory2CDomainClient iInventory2CDomainClient;

    @Autowired
    private ITrackCallbackClient iTrackCallbackClient;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private WeiXinPayService weiXinPayService;

    @Autowired
    private AliPayService aliPayService;

    @Autowired
    private SwiftPayService swiftPayService;

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserCouponService newUserCouponService;

    @Autowired
    private DiscountInfoService discountInfoService;

    @Autowired
    protected SkuService skuService;
    

    protected final static String appId = "2016063001569813";
    protected final static String pid = "2088421284384567";
    protected final static String sellerId = "pay@xianzaishi.com";
    protected final static String app_protected_key = "MIICXQIBAAKBgQCwcTbif+c+wzbd9laG5nCFHR+FD9yY1bew9ZW6r4j/s9Mh4JByfPOtiLvi08avZi3jbC6zxGE4aQDjrLpPQOH9hq53h8mpUGwcuHiZsSOgiAXdYy1tpeG/N6nALK/fvzURKAcoQUnKeEgLNs1iOTMMDn9CqsuXyNjfx1BfvlAQgwIDAQABAoGBAKUpZSoCdlFe5SZDKF3gqeVBDLc+0M1UCT4htQXquMA68WEd17kD1ApWGyJKAQtBB6WCJ/lo02S9jfKRRllXr/JWzrLq1Debe0P39D7cWxmDWt4x6MhOPC5aINfrb72zgSnNfUqH9dZgrn6g4MXPItzlNbIbTajoLbkm8lF2P6LpAkEA36U070jyhnPZ7gEadYzeO7JSM9HjClZTHzHcxB7fHH/GmcbeOX6zHRPX87nvseSt5VSwoxPFcYMuAxA3RupbdQJBAMn30og/mvXoWxS2K/vBQ/nTKvxzkp+y6bSLoFbP0VztmJRSiq5xrOFkETaNGXgXwX+VBq10BsePcWQw1UrUVRcCQE8echCpHOuF9rYle8fUUxaJal1cxlZ03akuiax0Q3ggmBD08s8iTJlf2MknoW2sufxkrqyypOoYf2GkDfovlLUCQCoIKElwq0g4BhSGYRrwurvYRZ7qUn5n1plbYZAPievECrf7gZ8SSz9Q+wAWV1GV6BAsLIqWlf6cDKYkP49mJxsCQQDNYmbNi8sASVsqBbcHJEF+32Sr8fGtLjzrt0HnldKuc6n/cALEzzgXbfwwZ3UIej/j6plxtZL9V6afIOLs8Klw/6FTFY99uhpiq0qadD/uSzQsefWo0aTvP/65zi3eof7TcZ32oWpwIDAQAB";
    protected final static String app_protected_pkcs8_key = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBANXenWCCjvXwdDKEQNsfJgouLggj/Cx87LfSJ0MyL7UQH+atx8ShXgRVj4OZL6I/QYynzS5iihrCD5jo0dN84FG+SlN2/g7x8iaYJ+kqq4Gf6BWs2I7IZgRkzRaq1nhB1+nFtWDU/NbgJYFkgoKEijVl0hox47ksWk0LVeWdLvtjAgMBAAECgYEAzRTXXMBVTkjxY6+mcXiBBaoawfufyRlR9UM0Gx941+tKa+2gblE+0nEpWUv/fVmjBbmy6xPa0qXcRwiajG5murqMpbUkHdkq6zkNDmxSV1JrzwuYHAG3CCgDZT2kdNMEDF1njscQNx73YHW4pTafGryabwVdNhTytriw3J98NOECQQD3LsPjiLzCcvC5oM6VHYXHdYTC2udi/Dk1Ly8WftxfWpSE1vyvs2r4qWl+EZ1Lif9Hi1MXYHKjWVSyMyoLrjYdAkEA3X+jtWAoqZIgZLoCagNaaiN3rTM/XaUQll7fgnGzzpMKIXQecbYUU1v2mzz8bcKx3/Ips4giLHs+dw3BrWU/fwJBAJZ3Wzsow27CtRLqdpaC8CqouPY8dtnkm5ZqcImLE+7fnsT2cb8qwpU320Wox01yZXlRsHTsexxAhrQrPQ77L2kCQDIQcJNFactyIOpDdNo7actFuv4l8DOdZJNoXEKiqo6Ng6OuGFeBXTS+O445CaFReVzx4mUW5wqAzMyiCl3D3ccCQQDrfk2sUPU1KvjdpKhluFDws8tCvfGrhDDVM+FtWYFpnEW0MibOjEh+N/c+Bveyjs4bK3CVySPxbY6H5NGOQrmY";
    protected final static String alipay_public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";

    private static long PATCH_SWIFT_MCH_ID_CHANGE_TIMESTAMP;

    static {
        try {
            PATCH_SWIFT_MCH_ID_CHANGE_TIMESTAMP = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2016-12-22 22:30:00").getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    // "&out_trade_no=0819145412-6177&subject=测试&body=测试测试&total_fee=0.01&notify_url="http://notify.msp.hk/notify.htm"&service="mobile.securitypay.pay"&payment_type="1"&_input_charset="utf-8"&it_b_pay=30m"
    protected static HashMap<String, String> bizContentMap = new HashMap<String, String>();

    static {
        bizContentMap.put("subject", "XIANZAISHI_PAY");
        bizContentMap.put("body", "PAY");
        bizContentMap.put("notify_url", AlipayConfigV2.notify_url);
        bizContentMap.put("service", AlipayConfigV2.service);
        bizContentMap.put("payment_type", "1");
        bizContentMap.put("_input_charset", "utf-8");
        bizContentMap.put("it_b_pay", "30m");
        bizContentMap.put("partner", AlipayConfigV2.partner);
        bizContentMap.put("seller_id", AlipayConfigV2.seller_id);
        bizContentMap.put("show_url", "m.alipay.com");
    }

    protected AtomicInteger atomicSeq = new AtomicInteger(1);

    @Override
    public long createOrder(long userId, int shopId, List<ShoppingCart> items, long userCouponId, long addressId, String deviceCode, int casherId,
            Date distribution, int credit, String description) {
        return createOrder(userId, shopId, items, userCouponId, addressId, deviceCode, casherId, distribution, credit, description, (short) 0);
    }

    @Override
  public PageList<OrderInfo> getOrdersByTime(Date begin, Date end, OrderStatus[] status,
      int pageNo, int pageSize) {
      try {
        int total = this.getOrderCountByTime(begin,end, status);
        Pagination pagin = Pagination.getPagination(pageNo, pageSize, total);
        if (pagin.limit() > 0) {
            List<OrderInfo> ordersList;
            if (null != status) {
                ordersList = ordersDAO.selectOrderByTime(begin, end, "and status in (" + orderStatus2String(status) + ")", pagin);
            } else {
                ordersList = ordersDAO.selectOrderByTime(begin, end, "", pagin);
            }
            if (CollectionUtils.isNotEmpty(ordersList)) {
                List<OrderInfo> orderList = new ArrayList<OrderInfo>(ordersList.size());
                for (Orders orders : ordersList) {
                    OrderInfo orderInfo = new OrderInfo(orders);
                    orderInfo.setItems(this.getOrderItems(orders.getId()));
                    orderList.add(orderInfo);
                }
                return new PageList<OrderInfo>(pagin, orderList);
            }
        }
    } catch (DataAccessException e) {
        log.error("get order failed", e);
    }
    return null;
  }

  @Override
  public int getOrderCountByTime(Date begin, Date end, OrderStatus[] status) {
    OrdersExample example = new OrdersExample();
    Criteria criteria = example.createCriteria().andGmtModifyBetween(begin, end);
    if (null != status) {
        criteria.andStatusIn(orderStatus2List(status));
    }
    try {
        return ordersDAO.countByExample(example);
    } catch (DataAccessException e) {
        log.error("get orderPayLogs failed", e);
    }
    return 0;
  }

  @Override
    public int getOrderCountByUserId(long uid, OrderStatus[] status) {
        OrdersExample example = new OrdersExample();
        Criteria criteria = example.createCriteria().andUserIdEqualTo(uid);
        if (null != status) {
            criteria.andStatusIn(orderStatus2List(status));
        }
        try {
            return ordersDAO.countByExample(example);
        } catch (DataAccessException e) {
            log.error("get orderPayLogs failed", e);
        }
        return 0;
    }

    public List<OrderItem> getOrderItems(long orderId) {
        OrderItemExample example = new OrderItemExample();
        example.createCriteria().andOrdersIdEqualTo(orderId);
        try {
            return orderItemDAO.selectByExample(example);
        } catch (DataAccessException e) {
            log.error("get orderItems failed", e);
        }
        return null;
    }

    @Override
    public boolean pay(long orderId, double amount, PayWay payWay, String account, String content) {
        Orders orders = get(orderId);
        if (null != orders) {
            // 支付记录插入失败，支付失败
            if (insertOrderPayLog(orderId, amount, payWay, account, content) <= 0L) {
                throw new RuntimeException("添加支付记录失败");
            }
            Date distribution = orders.getGmtDistribution();
            if ((null != distribution) && (distribution.getTime() < new Date().getTime())) {
                throw new RuntimeException("定时送达时间已经作废,不能支付");
            }
            // 只有未支付状态的订单能够支付
            if (orders.getStatus().shortValue() == OrderStatus.INIT.getValue()) {
                // 支付金额
                double payAmount = (null == orders.getPayAmount()) ? 0.0 : orders.getPayAmount().doubleValue();
                // 应付金额
                double effeAmount = (null == orders.getEffeAmount()) ? 0.0 : orders.getEffeAmount().doubleValue();

                double payTotalAmount = payAmount;
                // 积分支付
                if (payWay.getValue() == PayWay.CREDIT.getValue()) {
                    //在支付中使用积分支付的，先在创建订单后直接扣除掉使用的积分额度
                    userCreditManager.add(orders.getUserId(), 0, (int) (amount * 100) * -1, "购物使用 " + orders.getId());
                } else if (payWay.getValue() != PayWay.COUPON.getValue()) {
                    // 添加积分，但不包括优惠相关金额
                    userCreditManager.add(orders.getUserId(), 0, (int) (amount / 2), "购物获取 " + orders.getId());
                    payTotalAmount += amount;
                }
                // 支付完成
//              if (payTotalAmount >= effeAmount) {
                  //创建订单时会可能调用，支付回调也可能调用，所以只有总价达到支付金额，才会改交易状态
                  if (payTotalAmount >= effeAmount || isRightMoney(payTotalAmount,effeAmount)) {
                    // 线下订单
                    if (orders.getChannelType().shortValue() == 2) {
                        orders.setStatus(OrderStatus.VALID.getValue());
                    } else {
                        orders.setStatus(OrderStatus.PAY.getValue());
                    }
                    if(orders.getDiscountAmount().doubleValue() > 0){//如果有优惠，就把临期的优惠券设置不可用，把优惠订单状态设置已享用优惠
                      updateOrderDisountSnapshot(orders.getId());
                    }
                }
                orders.setPayAmount(payTotalAmount);
                orders.setGmtPay(new Date());

                if (update(orders)) {

//                  if (payTotalAmount >= effeAmount) {
                  //只有交易回调或者直接调用，达到金额后，才会触发物流状态
                  if (payTotalAmount >= effeAmount || isRightMoney(payTotalAmount,effeAmount)) {
                        // 物流回调
                        try {
                            iTrackCallbackClient.onOrderCreate(orderId);
                        } catch (Exception e) {
                            log.error("Fail to call back track service.", e);
                        }
                    }

                    return true;
                }
                return false;
            } else {
                log.error("order:" + orderId + " status!=INIT");
                throw new java.lang.RuntimeException("不符合付款标准");
            }
        } else {
            log.error("order:" + orderId + " is null!");
            throw new java.lang.RuntimeException("找不到要退款的订单");
        }
    }

    /**
     * 原来的傻逻辑，全部用double计算，导致精度出现严重问题，现在解决只能取绝对值差不到3分钱的，都认为是正常的订单了
     * @return
     */
    private static boolean isRightMoney(double input1,double input2){
      Object o = new BigDecimal(input1).subtract(new BigDecimal(input2)).abs();
      System.out.println(o);
      return new BigDecimal(input1).subtract(new BigDecimal(input2)).abs().compareTo(new BigDecimal(0.011)) <= 0;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    @Override
    public boolean refund(long orderId, double amount, PayWay payWay, String account, String payContent, String cause, int memberId) {
        Orders orders = get(orderId);
        if (null != orders) {
            short status = orders.getStatus().shortValue();
            if ((status == OrderStatus.PAY.getValue()) || (status == OrderStatus.PICKING.getValue())) {
                try {
                    CustomerServiceTaskDTO task = getCustomerServiceTask(orders.getId());
                    if (task != null) {
                        customerService.insertCustomerServiceTask(task);
                    } else {
                        log.error("创建客服任务失败 -> orderId" + orders.getId());
                        throw new RuntimeException("服务繁忙,请稍后重试...");
                    }
                } catch (Exception e) {
                    log.error("交易退款申请客服调用失败 -> orderId" + orders.getId(), e);
                    throw new RuntimeException("服务繁忙,请稍后重试...");
                }
                orders.setStatus(OrderStatus.REFOUDING.getValue());
                if (update(orders)) {
                    return true;
                } else {
                    log.error("update status of order failed!");
                }
                return false;
            } else {
                log.error("order:" + orderId + " status!=3,4");
                throw new java.lang.RuntimeException("不符合退款标准");
            }
        } else {
            log.error("order:" + orderId + " is null!");
            throw new java.lang.RuntimeException("找不到要退款的订单");
        }
    }

    private static final String LEFT_BRACKET = "(";

    private static final String RIGHT_BRACKET = ")";
    private static final String UNDER_LINE = "_";
    private CustomerServiceTaskDTO getCustomerServiceTask(Long orderId) {
        CustomerServiceTaskDTO customerServiceTask = new CustomerServiceTaskDTO();
        OrderInfo orderInfo = getOrder(orderId);// 订单详情数据
        if (null == orderInfo) {
            return null;
        }
        Long userId = orderInfo.getUserId();
        Result<? extends BaseUserDTO> userResult = userService.queryUserByUserId(userId, false);
        if (null == userResult || !userResult.getSuccess() || null == userResult.getModule()) {
            return null;
        }
        BaseUserDTO baseUserDTO = userResult.getModule();
        String userName = baseUserDTO.getName();// 用户名
        Long phone = baseUserDTO.getPhone();// 用户手机号
        StringBuffer customTitle = new StringBuffer();// 客服任务标题，用户名+用户手机号+任务创建时间
        customTitle.append(userName);
        customTitle.append(LEFT_BRACKET);
        customTitle.append(phone);
        customTitle.append(RIGHT_BRACKET);
        customTitle.append(UNDER_LINE);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        customTitle.append(sdf.format(customerServiceTask.getGmtCreate()));
        customerServiceTask.setTitle(customTitle.toString());
        customerServiceTask.setBizId(orderId.toString());// 订单id
        customerServiceTask.setCustomerUser(userId);// 用户id
        return customerServiceTask;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean payback(long orderId, double amount, PayWay payWay, String account, String payContent, String cause, int memberId) {
        Orders order = get(orderId);
        if (null != order) {
            short status = order.getStatus().shortValue();
            if (status != OrderStatus.INIT.getValue() && status != OrderStatus.INVALID.getValue() && status != OrderStatus.REFOUDED.getValue()) {

                boolean success = false;
                List<OrderPayLog> payLogs = null;
                try {
                    OrderPayLogExample example = new OrderPayLogExample();
                    example.createCriteria().andOrderIdEqualTo(orderId).andPayWayIn(Lists.newArrayList(PayWay.ALIPAY.getValue(),
                            PayWay.WEIXIN.getValue(), PayWay.WEIXINCODE.getValue(), PayWay.ALIPAYCODE.getValue(), PayWay.ALIPAY_QR.getValue()));
                    payLogs = orderPayLogDAO.selectByExample(example);
                    success = true;
                } catch (DataAccessException e) {
                    log.error("get orderPayLogs failed", e);
                }

                // 退款动作 - 校验方式
                if (!success || payLogs == null || (order.getEffeAmount() > 0 && payLogs.isEmpty())) {
                    TradeLog.warn("Try to refund for userId[%s], orderId[%s], order[%s], but system can't find any righ pay way.", order.getUserId(),
                            order.getId(), JSON.toJSONString(order));
                    throw new RuntimeException("没有支付记录.");
                }
                OrderPayLog payLog = null;
                try {
                    for (OrderPayLog log : payLogs) {
                        if (log.getPayWay().shortValue() == PayWay.ALIPAY.getValue() || log.getPayWay().shortValue() == PayWay.WEIXIN.getValue()
                                || log.getPayWay().shortValue() == PayWay.WEIXINCODE.getValue()
                                || log.getPayWay().shortValue() == PayWay.ALIPAYCODE.getValue()
                                || log.getPayWay().shortValue() == PayWay.ALIPAY_QR.getValue()) {
                            if (payLog == null) {
                                payLog = log;
                            } else {
                                TradeLog.warn("Try to refund for userId[%s], orderId[%s], order[%s], but system find more than one pay way.",
                                        order.getUserId(), order.getId(), JSON.toJSONString(order));
                                throw new RuntimeException("多条支付记录,系统需要人工确认.");
                            }
                        }
                    }
                    if (payLog == null && order.getEffeAmount() > 0) {
                        TradeLog.warn("Try to refund for userId[%s], orderId[%s], order[%s], but system can't find any righ pay way.",
                                order.getUserId(), order.getId(), JSON.toJSONString(order));
                        throw new RuntimeException("没有支付记录.");
                    }
                } catch (Exception e) {
                    success = false;
                    TradeLog.warn(e, "Try to refund for userId[%s], orderId[%s], order[%s], but system validate pay way fail.", order.getUserId(),
                            order.getId(), JSON.toJSONString(order));
                }

                // 添加一条退款记录
                PayWay way = (payLog != null && payLog.getPayWay() != null) ? PayWay.from(payLog.getPayWay().shortValue()) : PayWay.CREDIT;
                String bizAccount = payLog != null && payLog.getAccount() != null ? payLog.getAccount() : "0";

                long id = 0;
                try {
                    TradeLog.warn("Try to refund for userId[%s], orderId[%s], order[%s]", order.getUserId(), order.getId(), JSON.toJSONString(order));
                    id = insertOrderRefoud(orderId, amount, way, bizAccount, payContent, cause, memberId);
                } catch (Exception e) {
                    log.error(String.format("Fail to insert refund log for order[%s]", JSON.toJSONString(order)), e);
                }
                success = id > 0;

                if (!success) {
                    TradeLog.warn("Try to refund for userId[%s], orderId[%s], order[%s], insert order refund fail.", order.getUserId(), order.getId(),
                            JSON.toJSONString(order));
                    throw new RuntimeException("退款记录写入失败,请重试.");
                }

                int credit = 0;

                if (payLog != null) {
                    try {
                        success = false;

                        // 线下交易扫码交易
                        if (payLog.getPayWay().shortValue() == PayWay.ALIPAYCODE.getValue()
                                || payLog.getPayWay().shortValue() == PayWay.WEIXINCODE.getValue()) {
                            SwiftRefundRequest request = new SwiftRefundRequest();
                            request.setOutRefundNo(Long.toString(order.getId()));
                            request.setOutTradeNo(Long.toString(order.getId()));
                            request.setTotalFee((int) (order.getEffeAmount() * 100));
                            request.setRefundFee((int) (order.getEffeAmount() * 100));
                            request.setOpUserId("103560000081");
                            request.setNonceStr(MD5Utils.randString());

                            RpcResult<SwiftRefundResponse> result = null;
                            if (payLog.getGmtCreate().getTime() > PATCH_SWIFT_MCH_ID_CHANGE_TIMESTAMP) {
                                result = swiftPayService.swiftRefund(request);
                            } else {
                                result = swiftPayService.swiftRefundOld(request);
                            }

                            success = result != null && result.isSuccess() && result.getData() != null && result.getData().isSuccess();

                            if (!success) {
                                TradeLog.error("Try refund for userId[%s], orderId[%s], order[%s], but account service error[%s].", order.getUserId(),
                                        order.getId(), JSON.toJSONString(order), JSON.toJSONString(result));
                                throw new RuntimeException("远程服务退款失败 ->" + JSONObject.toJSONString(result));
                            }
                        } else if (payLog.getPayWay().shortValue() == PayWay.CASH.getValue()
                                || payLog.getPayWay().shortValue() == PayWay.YINTONG.getValue()) {
                            // 现金&pos直接信任退款结果
                            success = true;
                        }

                        // 线下支付宝-当面付
                        if (payLog.getPayWay().shortValue() == PayWay.ALIPAY_QR.getValue()) {
                            AliPayRefundRequest request = new AliPayRefundRequest();
                            request.setAppId("2016112403191069");
                            request.setCharset("utf-8");
                            request.setFormat("JSON");
                            request.setSignType("RSA");
                            request.setTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                            request.setVersion("1.0");
                            AliPayRefundRequest.BizContent content = request.createBizContent();
                            content.setOutTradeNo(Long.toString(order.getId()));
                            content.setRefundAmount(order.getEffeAmount().toString());
                            content.setRefundReason("鲜在时-取消订单交易退款");
                            content.setOutRequestNo(Long.toString(order.getId()));

                            RpcResult<AliPayRefundResponse> result = aliPayService.aliPayRefund(request);

                            success = result != null && result.isSuccess() && result.getData() != null && result.getData().isSuccess();
                            if (!success) {
                                TradeLog.error("Try refund for userId[%s], orderId[%s], order[%s], but account service error[%s].", order.getUserId(),
                                        order.getId(), JSON.toJSONString(order), JSON.toJSONString(result));
                                throw new RuntimeException("远程服务退款失败 ->" + JSONObject.toJSONString(result));
                            }
                        }

                        // 线上支付宝
                        if (payLog.getPayWay().shortValue() == PayWay.ALIPAY.getValue()) {
                            AliPayRefundRequest request = new AliPayRefundRequest();
                            request.setAppId(AlipayConfig.APP_ID);
                            request.setMethod("alipay.trade.refund");
                            request.setCharset(AlipayConfig.V1_INPUT_CHARSET);
                            request.setSignType(AlipayConfig.V1_SIGN_TYPE);
                            request.setTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                            request.setVersion("1.0");
                            AliPayRefundRequest.BizContent content = request.createBizContent();
                            content.setOutTradeNo(Long.toString(order.getId()));
                            content.setTradeNo(payLog.getAccount());
                            content.setRefundAmount(order.getEffeAmount().toString());
                            content.setRefundReason("鲜在时-取消订单交易退款");
                            content.setOutRequestNo(order.getId().toString());
                            RpcResult<AliPayRefundResponse> result = aliPayService.aliPayRefundV1(request);

                            success = result != null && result.isSuccess() && result.getData() != null && result.getData().isSuccess();

                            if (!success) {
                                TradeLog.error("Try refund for userId[%s], orderId[%s], order[%s], but account service error[%s].", order.getUserId(),
                                        order.getId(), JSON.toJSONString(order), JSON.toJSONString(result));
                                throw new RuntimeException("远程服务退款失败 ->" + JSONObject.toJSONString(result));
                            } else {
                                // state change will be in callback
                                return true;
                            }
                        }

                        // 线上微信
                        if (payLog.getPayWay().shortValue() == PayWay.WEIXIN.getValue()) {
                            WeiXinRefundRequest request = new WeiXinRefundRequest();
                            request.setAppId(WeiXinPayConfig.APPID);
                            request.setMchId(WeiXinPayConfig.MCHID);
                            request.setNonceStr(MD5Utils.randString());
                            request.setTransactionId(payLog.getAccount());
                            request.setOutRefundNo(order.getId().toString());
                            int payBack = new BigDecimal(order.getEffeAmount()).multiply(new BigDecimal(100)).intValue();
                            request.setTotalFee(payBack);
                            request.setRefundFee(payBack);
                            request.setOpUserId(WeiXinPayConfig.APPID);

                            RpcResult<WeiXinRefundResponse> result = weiXinPayService.weixinRefund(request);

                            success = result != null && result.isSuccess() && result.getData() != null && result.getData().isSuccess()
                                    && "SUCCESS".equals(result.getData().getResultCode());

                            if (!success) {
                                TradeLog.error("Try refund for userId[%s], orderId[%s], order[%s], but account service error[%s].", order.getUserId(),
                                        order.getId(), JSON.toJSONString(order), JSON.toJSONString(result));
                                throw new RuntimeException("远程服务退款失败 ->" + JSONObject.toJSONString(result));
                            }
                        }

                        if (success) {
                            credit = -(int) (order.getEffeAmount() / 2);
                        }
                    } catch (Exception e) {
                        success = false;
                        TradeLog.error(e, "Try to refund for userId[%s], orderId[%s], order[%s], but account service error.", order.getUserId(),
                                order.getId(), JSON.toJSONString(order));
                        throw new RuntimeException("远程服务退款异常 ->" + e.getMessage());
                    }
                }

                credit += order.getCredit() != null ? order.getCredit() : 0;

                // 退积分动作
                if (success && credit != 0) {
                    try {
                        userCreditManager.add(order.getUserId(), 10, credit, "取消交易变动-退款");
                    } catch (Exception e) {
                        TradeLog.error("Fail to return user credit when cancal trade for userId[%s], orderId[%s], credit[%s]", order.getUserId(),
                                order.getId(), order.getCredit());
                    }
                }

                // 退优惠券动作
                if (success && order.getCouponId() > 0) {
                    try {
                      Result<Boolean> updateResult = newUserCouponService.updateCouponStatus(order.getCouponId(), Status.VALID.getValue());
                        if (null == updateResult || !updateResult.getSuccess() || !updateResult.getModule()) {
                            TradeLog.error("Fail to return user credit when cancal trade for userId[%s], orderId[%s], couponId[%s]",
                                    order.getUserId(), order.getId(), order.getCouponId());
                        }
                    } catch (Exception e) {
                        TradeLog.error("Fail to return user credit when cancal trade for userId[%s], orderId[%s], couponId[%s]", order.getUserId(),
                                order.getId(), order.getCouponId());
                    }
                }

                // 更新订单状态
                if (success) {
                    order.setStatus(OrderStatus.REFOUDED.getValue());
                    if (update(order)) {
                        return true;
                    } else {
                        log.error("update status of order failed!");
                        throw new RuntimeException("退款成功, 更新订单失败, 转入人工处理.");
                    }
                }
                return false;
            } else {
                log.error("order:" + orderId + " status!= 9");
                throw new java.lang.RuntimeException("未付款订单不能发起退款");
            }
        } else

        {
            log.error("order:" + orderId + " is null!");
            throw new java.lang.RuntimeException("找不到要退款的订单");
        }

    }

    public boolean paybackDirect(long orderId, double amount, PayWay payWay, String account, String cause, int memberId) {
        Orders order = get(orderId);
        if (null != order && amount >= order.getEffeAmount()) {

            // 添加一条退款记录
            long id = 0;
            try {
                TradeLog.warn("Try to refund for userId[%s], orderId[%s], order[%s]", order.getUserId(), order.getId(), JSON.toJSONString(order));
                id = insertOrderRefoud(orderId, amount, payWay, account, null, cause, memberId);
            } catch (Exception e) {
                log.error(String.format("Fail to insert refund log for order[%s]", JSON.toJSONString(order)), e);
            }

            boolean success = id > 0;

            if (!success) {
                TradeLog.warn("Try to refund for userId[%s], orderId[%s], order[%s], insert order refund fail.", order.getUserId(), order.getId(),
                        JSON.toJSONString(order));
                return false;
            }

            int credit = order.getCredit() != null ? order.getCredit() : 0;

            if (order.getEffeAmount() > 0) {
                credit -= (int) (order.getEffeAmount() / 2);
            }

            // 退积分动作
            if (success && credit != 0) {
                try {
                    userCreditManager.add(order.getUserId(), 10, credit, "取消交易变动-退款");
                } catch (Exception e) {
                    TradeLog.error("Fail to return user credit when cancal trade for userId[%s], orderId[%s], credit[%s]", order.getUserId(),
                            order.getId(), order.getCredit());
                }
            }

            // 退优惠券动作
            if (success && order.getCouponId() > 0) {
                try {
                  Result<Boolean> updateResult = newUserCouponService.updateCouponStatus(order.getCouponId(), Status.VALID.getValue());
                  if (null == updateResult || !updateResult.getSuccess() || !updateResult.getModule()) {
                        TradeLog.error("Fail to return user credit when cancal trade for userId[%s], orderId[%s], couponId[%s]", order.getUserId(),
                                order.getId(), order.getCouponId());
                    }
                } catch (Exception e) {
                    TradeLog.error("Fail to return user credit when cancal trade for userId[%s], orderId[%s], couponId[%s]", order.getUserId(),
                            order.getId(), order.getCouponId());
                }
            }

            // 更新订单状态
            if (success) {
                order.setStatus(OrderStatus.REFOUDED.getValue());
                if (update(order)) {
                    return true;
                } else {
                    log.error("update status of order failed!");
                    orderRefoudDAO.deleteByPrimaryKey(id);
                }
            }
            return false;
        } else {
            log.error("order:" + orderId + " is null!");
            throw new java.lang.RuntimeException("找不到要退款的订单");
        }

    }

    @Override
    public synchronized String getAlipaySignature(long orderId) {
        Orders order = get(orderId);
        if (null == order) {
            throw new RuntimeException("没有id:" + orderId + "对应的订单");
        }
        if (order.getStatus() != OrderStatus.INIT.getValue()) {
            throw new RuntimeException("订单:" + orderId + "当前状态不允许支付");
        }
        // bizContentMap.put("out_trade_no", this.getOutTradeNo(order));
        // bizContentMap.put("total_fee",
        // String.valueOf(order.getEffeAmount()));
        // // 将post接收到的数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串。需要排序。
        // String data = AlipayCore.createLinkString(bizContentMap);
        //
        // // 打印待签名字符串。工程目录下的log文件夹中。
        // // AlipayCore.logResult(data,"datashuju");
        //
        // // 将待签名字符串使用私钥签名。
        // String rsa_sign = "";
        // try {
        // // AlipayConfig.protected_key
        // rsa_sign = URLEncoder.encode(RSA.sign(data, app_protected_pkcs8_key,
        // AlipayConfigV2.input_charset), AlipayConfigV2.input_charset);
        // } catch (UnsupportedEncodingException e) {
        // log.error("getAlipaySignature:" + e.getMessage());
        // }
        //
        // // 把签名得到的sign和签名类型sign_type拼接在待签名字符串后面。
        // data = data + "&sign=\"" + rsa_sign + "\"&sign_type=\"" +
        // AlipayConfigV2.sign_type + "\"";
        // // data = data +
        // "&sign="+rsa_sign+"&sign_type="+AlipayConfig.sign_type;
        // // System.out.println(data);
        // log.error("sign=" + data);

        String data = AliPay.tradeOrderSignature(order.getId(), order.getEffeAmount(), "鲜在时-订单编号" + order.getId(), "PAY");

        TradeLog.info("[AliPay]-[PreSign] PARAMS[%s]", data);

        if (log.isDebugEnabled()) {
            log.debug(String.format("alipay sign -> %s", data));
        }

        return data;
    }

    @Override
    public synchronized String getAlipayQrSignature(long orderId) {
        Orders order = get(orderId);
        if (null == order) {
            throw new RuntimeException("没有id:" + orderId + "对应的订单");
        }
        if (order.getStatus() != OrderStatus.INIT.getValue()) {
            throw new RuntimeException("订单:" + orderId + "当前状态不允许支付");
        }

        AliPayQrPayRequest request = new AliPayQrPayRequest();
        request.setAppId("2016102000726768");
        request.setCharset("utf-8");
        request.setFormat("JSON");
        request.setNotifyUrl("http://trade.xianzaishi.net/alipay/qrPayCallback.htm");
        request.setSignType("RSA");
        request.setTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        request.setVersion("1.0");
        AliPayQrPayRequest.BizContent bizContent = request.createBizContent();
        bizContent.setOutTradeNo(order.getId().toString());
        bizContent.setTotalAmount(String.format("%.2f", order.getEffeAmount()));
        bizContent.setSubject("鲜在时");

        RpcResult<AliPayQrPayResponse> result = aliPayService.aliPayQrPay(request);

        if (result.isSuccess()) {
            TradeLog.info("[AliPay]-[PreSignQr] PARAMS[%s]", result.getData().getQrCode());
            return result.getData().getQrCode();
        } else {
            return null;
        }

    }

    @Override
    public MobilePayModel getWeixinpaySignature(long orderId, String ip) {
        Orders order = get(orderId);
        if (null == order) {
            throw new RuntimeException("没有id:" + orderId + "对应的订单");
        }
        if (order.getStatus() != OrderStatus.INIT.getValue()) {
            throw new RuntimeException("订单:" + orderId + "当前状态不允许支付");
        }
        UnifiedorderModel model = new UnifiedorderModel();
        model.setAppid(WeiXinPayConfig.APPID);
        model.setMch_id(WeiXinPayConfig.MCHID);
        model.setDevice_info(WeiXinPayConfig.DEVICE_INFO);
        model.setNonce_str(MD5Utils.randString().substring(0, 32));
        // model.setSign("");设置完成最后加签
        model.setBody(String.format("鲜在时-订单编号%d", order.getId()));
        // model.setDetail(detail);
        model.setAttach(String.valueOf(order.getSeq()));
        model.setOut_trade_no(this.getOutTradeNo(order));
        model.setFee_type("CNY");
        model.setTotal_fee((int) (new BigDecimal(order.getEffeAmount()).multiply(new BigDecimal(100)).intValue()));
        model.setSpbill_create_ip(StringUtils.isBlank(ip) ? NetUtils.getLocalAddress() : ip);

        Date now = new Date();
        model.setTime_start(DateUtils.yyyyMMddhhmmss(now));
        model.setTime_expire(DateUtils.yyyyMMddhhmmss(DateUtils.dateAddDays(now, 1)));

        // model.setGoods_tag(goods_tag);
        if(SpringContextUtil.isOnline()){
          model.setNotify_url(WeiXinPayConfig.NOTIFY_CALLBACK);
        }else{
          model.setNotify_url(WeiXinPayConfig.NOTIFY_CALLBACK_DAILY);
        }
        model.setTrade_type(WeiXinPayConfig.APP_TRADE_TYPE);

        model.setSign(PayUtil.signByWeiXin(model));
        String xml = PayUtil.objectToXML(model, "utf-8");
        try {

            if (log.isDebugEnabled()) {
                log.debug("weixin pay 1 -> " + xml);
            }
            TradeLog.info("[WeiXin]-[PreSign] PARAMS[%s]", xml != null ? xml.replaceAll("\r|\n", "") : xml);
            String result = WeiXinPayHttpsRequest.unifiedOrder(xml);
            TradeLog.info("[WeiXin]-[PreSignResult] PARAMS[%s]", result != null ? result.replaceAll("\r|\n", "") : result);

            if (log.isDebugEnabled()) {
                log.debug("weixin pay 2 -> " + result);
            }

            if (StringUtils.isNotEmpty(result)) {
                UnifiedorderReturnValue returnValue = (UnifiedorderReturnValue) PayUtil.xmlToObject(result, UnifiedorderReturnValue.class);
                if (null != returnValue) {
                    if (log.isDebugEnabled()) {
                        log.debug("weixin pay 3 -> prepay_id:" + returnValue.getPrepay_id());
                    }

                    if (StringUtils.isNotEmpty(returnValue.getPrepay_id())) {
                        MobilePayModel mobileModel = new MobilePayModel();
                        mobileModel.setPrepayid(returnValue.getPrepay_id());
                        mobileModel.setSign(PayUtil.signByWeiXin(mobileModel));
                        return mobileModel;
                    }
                    throw new java.lang.RuntimeException("统一下单失败");
                }
                throw new java.lang.RuntimeException("统一下单返回值解析失败");
            }
            throw new java.lang.RuntimeException("发起统一下单失败");
        } catch (Exception e) {
            throw new java.lang.RuntimeException(e.getMessage());
        }
    }

    @Override
    public GongZongHaoPayModel getWeixinGongZongHaoPaySignature(long userId, String openId) {
        UnifiedorderModel model = new UnifiedorderModel();
        model.setAttach(String.valueOf(this.atomicSeq.get()));
        // model.setNonce_str(StringUtils.rightPad(String.valueOf(order.getSeq()),
        // 16));
        model.setOut_trade_no(String.format("%032d", userId));
        Date now = new Date();
        // model.setAppid("wx4b6d100929d957c6");
        model.setAppid("wx4b6d100929d957c6");
        // model.setMch_id("1370613902");
        model.setMch_id("1370613902");
        model.setTime_start(DateUtils.yyyyMMddhhmmss(now));
        model.setTime_expire(DateUtils.yyyyMMddhhmmss(DateUtils.dateAddDays(now, 1)));
        model.setTotal_fee(100);
        model.setNotify_url("http://trade.xianzaishi.com/order/weixinPayCallBack2.htm");
        // String key = "d050d557f809e4ee14e4aa215a9d68d4";
        String key = "d7Da7b62646b2a7dd0d62a2b3d0d123T";
        model.setTrade_type("JSAPI");
        model.setOpenid(openId);
        model.setBody("鲜在时早餐券");
        model.setSign(PayUtil.signByWeiXin(model, key));
        String xml = PayUtil.objectToXML(model, "utf-8");
        String result;
        try {
            // result = postDataToWeiXin(xml);
            // System.out.println("result:" + result);
            result = WeiXinPayHttpsRequest.unifiedOrder(xml);
            if (StringUtils.isNotEmpty(result)) {
                UnifiedorderReturnValue returnValue = (UnifiedorderReturnValue) PayUtil.xmlToObject(result, UnifiedorderReturnValue.class);
                if (null != returnValue) {
                    log.error("prepay_id:" + returnValue.getPrepay_id());
                    if (StringUtils.isNotEmpty(returnValue.getPrepay_id())) {
                        GongZongHaoPayModel payModel = new GongZongHaoPayModel();
                        payModel.setPack("prepay_id=" + returnValue.getPrepay_id());
                        payModel.setPaySign(PayUtil.signByWeiXin(payModel, key));
                        payModel.setPrice(10000L);
                        payModel.setDiscountPrice(100L);
                        return payModel;
                    }
                    throw new java.lang.RuntimeException("统一下单失败");
                }
                throw new java.lang.RuntimeException("统一下单返回值解析失败");
            }
            throw new java.lang.RuntimeException("发起统一下单失败");
        } catch (Exception e) {
            throw new java.lang.RuntimeException(e.getMessage());
        }
    }

    @Override
    public OrderInfo getOrder(long id) {
        Orders order = get(id);
        if (null != order) {
            OrderInfo orderInfo = new OrderInfo(order);
            orderInfo.setItems(this.getOrderItems(id));
            return orderInfo;
        }
        throw new RuntimeException("没有此ID:" + id + "对应的订单");
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @Override
    public boolean updateStatus(long orderId, OrderStatus status) {
        Orders orders = get(orderId);
        if (null != orders) {
            short oldStatus = orders.getStatus();
            if(oldStatus == OrderStatus.VALID.getValue() && status.getValue() == OrderStatus.REFOUDING.getValue()){
              long current = System.currentTimeMillis();
              long orderTime = orders.getGmtCreate().getTime();
              if(current - orderTime >= 48*3600*1000){
                log.error("ID:" + orderId + "对应的订单不能从状态" + OrderStatus.from(orders.getStatus()).getDescription() + "更换到" + status.getDescription()+" 超过48小时，不允许修改");
                return false;
              }else{
                orders.setStatus(status.getValue());
                return update(orders);
              }
            }
            // 删除，交易成功，关闭交易三个不允许修改状态
            if ((oldStatus == OrderStatus.INVALID.getValue()) || (oldStatus == OrderStatus.VALID.getValue())
                    || (oldStatus == OrderStatus.CLOSED.getValue())) {
              log.error("ID:" + orderId + "对应的订单不能从状态" + OrderStatus.from(orders.getStatus()).getDescription() + "更换到" + status.getDescription() +" 三种固定不允许状态");
              return false;
            }
            boolean allowUpdate = false;
            // 默认情况可以更新到下一步
            if (status.getValue() == (orders.getStatus().shortValue() + 1) || status.getValue() == (orders.getStatus().shortValue() + 2)) {
                allowUpdate = true;
            } else {
                // 从待付款到交易关闭
                if ((status.getValue() == OrderStatus.CLOSED.getValue()) && (oldStatus == OrderStatus.INIT.getValue())) {
                    allowUpdate = true;
                }

                // 允许退款中
                if (oldStatus != OrderStatus.REFOUDED.getValue() && status.getValue() == OrderStatus.REFOUDING.getValue()) {
                    allowUpdate = true;
                }
            }
            if (allowUpdate) {
                orders.setStatus(status.getValue());
                return update(orders);
            }
            String errorInfo =  "ID:" + orderId + "对应的订单不能从状态" + OrderStatus.from(orders.getStatus()).getDescription() + "更换到" + status.getDescription();
            log.error(errorInfo);
            throw new RuntimeException(errorInfo);
        }

        throw new RuntimeException("没有此ID:" + orderId + "对应的订单");
    }

    protected Orders get(long id) {
        Orders orders = null;
        try {
            orders = ordersDAO.selectByPrimaryKey(id);
        } catch (DataAccessException e) {
            log.error("get order failed", e);
        }
        return orders;
    }

    public List<OrderPayLog> getOrderPayLog(long orderId) {
        OrderPayLogExample example = new OrderPayLogExample();
        example.createCriteria().andOrderIdEqualTo(orderId);
        try {
            return orderPayLogDAO.selectByExample(example);
        } catch (DataAccessException e) {
            log.error("get orderPayLogs failed", e);
        }
        return null;
    }

    protected boolean update(Orders orders) {
        Date now = new Date();
        orders.setGmtModify(now);
        try {
            return ordersDAO.updateByPrimaryKey(orders) > 0;
        } catch (DataAccessException e) {
            log.error("update order failed.", e);
        }
        return false;
    }

    protected Long insertOrderItem(long orderId, ShoppingCart cart) {
        OrderItem item = new OrderItem();
        item.setItemBillAmount(cart.getBillAmount());
        item.setItemBillPrice(cart.getItemBillPrice());
        item.setItemCategoryId(cart.getItemCategoryId());
        item.setItemCount(cart.getItemCount());
        item.setItemEffeAmount(cart.getEffeAmount());
        item.setItemEffePrice(cart.getItemEffePrice());
        item.setItemIconUrl(cart.getItemIconUrl());
        item.setItemId(cart.getItemId());
        item.setItemName(cart.getItemName());
        item.setItemSkuId(cart.getItemSkuId());
        item.setItemSkuInfo(cart.getItemSkuInfo());
        item.setOrdersId(orderId);
        return insertOrderItem(item);
    }

    protected Long insertOrderItem(OrderItem item) {
        Date now = new Date();
        item.setGmtCreate(now);
        item.setGmtModify(now);
        item.setStatus(Status.INIT.getValue());
        try {
            if (orderItemDAO.insert(item) > 0) {
                return item.getId();
            }
        } catch (DataAccessException e) {
            log.error("insert orderItem failed.", e);
        }
        return 0L;
    }

    protected Long insertOrderRefoud(long ordersId, double amount, PayWay payWay, String account, String payContent, String cause, int memberId) {
        OrderRefoud ordersRefoud = new OrderRefoud();
        ordersRefoud.setOrdersId(ordersId);
        ordersRefoud.setAfterSaleMemberId(memberId);
        ordersRefoud.setMemberId(memberId);
        ordersRefoud.setPayWay(payWay.getValue());
        ordersRefoud.setRefoudAmount(amount);
        ordersRefoud.setRefoudCause(cause);
        ordersRefoud.setAccount(account);
        ordersRefoud.setPayContent(payContent);
        return insertOrderRefoud(ordersRefoud);
    }

    protected Long insertOrderRefoud(OrderRefoud orderRefoud) {
        Date now = new Date();
        orderRefoud.setGmtCreate(now);
        orderRefoud.setGmtModify(now);
        orderRefoud.setStatus(Status.INIT.getValue());
        try {
            if (orderRefoudDAO.insert(orderRefoud) > 0) {
                return orderRefoud.getId();
            }
        } catch (DataAccessException e) {
            log.error("insert orderRefoud failed.", e);
        }
        return 0L;
    }

    protected Long insertOrderPayLog(long ordersId, double amount, PayWay payWay, String account, String callbackContent) {
        OrderPayLog payLog = new OrderPayLog();
        payLog.setOrderId(ordersId);
        payLog.setPayWay(payWay.getValue());
        payLog.setPayAmount(amount);
        payLog.setAccount(account);
        payLog.setCallbackcontent(callbackContent);
        return insertOrderPayLog(payLog);
    }

    // 添加支付记录
    protected Long insertOrderPayLog(OrderPayLog orderPayLog) {
        Date now = new Date();
        orderPayLog.setGmtCreate(now);
        orderPayLog.setGmtModify(now);
        orderPayLog.setStatus(Status.INIT.getValue());
        try {
            if (orderPayLogDAO.insert(orderPayLog) > 0) {
                return orderPayLog.getId();
            }
        } catch (DataAccessException e) {
            log.error("insert orderPayLog failed.", e);
        }
        return 0L;
    }

    protected Long insert(Orders orders) {
        Date now = new Date();
        orders.setGmtCreate(now);
        orders.setGmtModify(now);
        orders.setStatus(OrderStatus.INIT.getValue());
        try {
            if (ordersDAO.insert(orders) > 0) {
                return orders.getId();
            }
        } catch (DataAccessException e) {
            log.error("insert order failed.", e);
        }
        return 0L;
    }

    protected Integer insertCashierOperatorLog(int deviceId, String deviceCode, short operateCode, int memberId) {
        CashierOperatorLog cashierOperatorLog = new CashierOperatorLog();
        cashierOperatorLog.setDeviceId(deviceId);
        cashierOperatorLog.setDeviceCode(deviceCode);
        cashierOperatorLog.setMemberId(memberId);
        cashierOperatorLog.setOperateCode(operateCode);
        return insertCashierOperatorLog(cashierOperatorLog);
    }

    protected Integer insertCashierOperatorLog(CashierOperatorLog cashierOperatorLog) {
        Date now = new Date();
        cashierOperatorLog.setGmtCreate(now);
        cashierOperatorLog.setGmtModify(now);
        try {
            if (cashierOperatorLogDAO.insert(cashierOperatorLog) > 0) {
                return cashierOperatorLog.getId();
            }
        } catch (DataAccessException e) {
            log.error("insert cashierOperatorLog failed.", e);
        }
        return 0;
    }

    protected String orderStatus2String(OrderStatus[] statusArr) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < statusArr.length; i++) {
            buf.append(statusArr[i].getValue());
            if (i < statusArr.length - 1) {
                buf.append(",");
            }
        }
        return buf.toString();
    }

    protected List<Short> orderStatus2List(OrderStatus[] statusArr) {
        ArrayList<Short> statusList = new ArrayList<Short>(statusArr.length);
        for (OrderStatus status : statusArr) {
            statusList.add(status.getValue());
        }
        return statusList;
    }

    private String getOutTradeNo(Orders order) {
        String orderIdStr = StringUtils.leftPad(String.valueOf(order.getId()), 16, '0');
        String seqStr = StringUtils.rightPad(String.valueOf(order.getSeq()), 8, '0');
        return seqStr + orderIdStr;
    }

    @Override
    public long createOrder(long userId, int shopId, List<ShoppingCart> items, long userCouponId, long addressId, String deviceCode, int casherId,
            Date distribution, int credit, String description, short orderType) {
        Orders order = new Orders();
        order.setAttribute(description);
        order.setCashierId(casherId);
        order.setCouponId(userCouponId);
        order.setDeviceCode(deviceCode);
        order.setSeq(atomicSeq.incrementAndGet());
        order.setShopId(shopId);
        order.setUserAddressId(addressId);
        order.setGmtDistribution(distribution);
        order.setOrderType(orderType);
        boolean offline = StringUtils.isNotEmpty(deviceCode) || casherId > 0;
        short channelType = (offline) ? (short) 2 : (short) 1;
        order.setChannelType(channelType);

        order.setAttribute(description);
        if (addressId > 0) {
            String address = deliveryAddressManager.getLinkmanAddress(addressId);
            if (null == address) {
                throw new RuntimeException("无法获取收货地址");
            }
            order.setUserAddress(address);
        }
        order.setUserId(userId);
        // double billAmount = 0.0;
        BigDecimal effeAmount = new BigDecimal("0.0");
        for (ShoppingCart cart : items) {
            // billAmount += (cart.getItemBillPrice() * cart.getItemCount());
            BigDecimal itemSinglePrice = new BigDecimal(String.valueOf(cart.getItemEffePrice())).multiply(new BigDecimal(String.valueOf(cart.getItemCount())));
            effeAmount = effeAmount.add(itemSinglePrice);
//            effeAmount += (cart.getItemEffePrice() * cart.getItemCount());
            log.error("Trade Query Inventory result,skuId:"+cart.getId());
            if(!offline){
              SimpleResultVO<InventoryVO> storeResult =
                  iInventory2CDomainClient.getInventoryByGovAndSKU(new Long(shopId), cart.getItemSkuId());
                if (!isInventoryEnough(offline,storeResult,cart)) {
                  log.error("Trade Query Inventory. check result false mayresult:"+(storeResult.getTarget().getMaySale())+",num:"+storeResult.getTarget().getNumber()+",skuType:"+cart.getSkuType()+",skuUnitWeight:"+cart.getUnitWeightNum()+",buynum:"+cart.getItemCount());
                  throw new RuntimeException("对应店铺商品" + cart.getItemName() + "库存不足");
                }else{
                  log.error("Trade Query Inventory. check result true mayresult:"+(storeResult.getTarget().getMaySale())+",num:"+storeResult.getTarget().getNumber()+",skuType:"+cart.getSkuType()+",skuUnitWeight:"+cart.getUnitWeightNum()+",buynum:"+cart.getItemCount());
                }
            }
        }
        // 商品总额(单价*数量)
        order.setBillAmount(effeAmount.doubleValue());

        List<BuySkuInfoDTO> skuList = Lists.newArrayList();
        List<Long> skuIdList = Lists.newArrayList();
        for (ShoppingCart cart : items) {
            if (cart == null) {
                continue;
            }
            BuySkuInfoDTO sku = new BuySkuInfoDTO();
            sku.setSkuId(cart.getItemSkuId());
            sku.setBuyCount(cart.getItemCount().toString());
            sku.setSkuBarCode(cart.getSkuBarCode());
            skuList.add(sku);
            skuIdList.add(cart.getItemSkuId());
        }

        Result<Boolean> limitBuyResult = checkSkuLimitBuy(skuIdList, items);
        if(null != limitBuyResult && limitBuyResult.getResultCode() == -1){
          log.error("Trade limit check. check result false:"+(limitBuyResult.getErrorMsg()));
          throw new RuntimeException(limitBuyResult.getErrorMsg());
        }
        
        BigDecimal discountAmount = new BigDecimal(0);//all discount price
        // call all discount process
        DiscountResultDTO dis = getOrderProcessDiscount(userId, skuList, effeAmount,userCouponId,channelType, 0);
        if(null != dis){
          int discount = dis.getOrinalPrice() - dis.getDiscountPrice();
          discountAmount = new BigDecimal(discount).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
        }
        
     // 计算邮费添加邮费
        if (effeAmount.compareTo(new BigDecimal("59.00"))<0&& !offline) {
          effeAmount = effeAmount.add(new BigDecimal("6.0"));
        }
        
        effeAmount = effeAmount.subtract(discountAmount);
        if (effeAmount.compareTo(new BigDecimal("0"))<0) {
          effeAmount = new BigDecimal(0);
        }
        
        order.setDiscountAmount(discountAmount.doubleValue());

        // 验证积分
        if (userCreditManager.getCreditByUserId(userId) < credit) {
            throw new RuntimeException("账户积分不足");
        }
        // 计算最高可折扣积分
        if ((effeAmount.compareTo(new BigDecimal("0.01")) >= 0) && (new BigDecimal(String.valueOf(credit)).compareTo(effeAmount.multiply(new BigDecimal("100.0")))>=0)) {
            credit = (int) Math.ceil(effeAmount.multiply(new BigDecimal("100")).doubleValue());
        }
        order.setCredit(credit);
        // 积分抵扣金额
        BigDecimal creditAmount = new BigDecimal(String.valueOf(credit)).multiply(new BigDecimal("1.0")).divide(new BigDecimal("100"));
//        double creditAmount = (credit * 1.0) / 100;
        // 扣除积分抵扣金额
//        effeAmount -= creditAmount;
        effeAmount = effeAmount.subtract(creditAmount);
        order.setEffeAmount(effeAmount.doubleValue());
        order.setDiscountAmount(discountAmount.doubleValue());
        order.setPayAmount(0.0);
        long orderId = insert(order);
        if (orderId > 0) {
            for (ShoppingCart cart : items) {
                if (this.insertOrderItem(orderId, cart) <= 0) {
                    log.error("insert orderItem failed!shoppingCartId=" + cart.getId());
                    throw new RuntimeException("添加订单商品时出错");
                }
            }
            if (credit > 0) {
                this.pay(orderId, creditAmount.doubleValue(), PayWay.CREDIT, String.valueOf(userId), "积分支付");
            }
            if(discountAmount.doubleValue() > 0){
              createOrderDisountSnapshot(dis,order);
              if (userCouponId > 0) {
                newUserCouponService.updateCouponOrder(userCouponId, orderId);
                  try {
                      this.pay(orderId, discountAmount.doubleValue(), PayWay.COUPON, String.valueOf(userId), "优惠券支付");
                  } catch (Exception e) {
                      // ignore
                  }
              }
            }
        }
        return orderId;
    }

  private DiscountResultDTO getOrderProcessDiscount(Long userId,List<BuySkuInfoDTO> skuList,BigDecimal effeAmount, Long userCouponId, short channelType, int baseDiscountPrice) {
    DiscountQueryDTO query = new DiscountQueryDTO();
    query.setBuySkuDetail(skuList);
    query.setCouponId(userCouponId);
    query.setPayType((int)channelType);
    query.setUserId(userId);
    if(baseDiscountPrice >= 0){
      query.setDiscountBasicPrice(baseDiscountPrice);
    }
    Result<DiscountResultDTO> discountResult = discountInfoService.calculate(query);
    log.error("Query discount info is:"+JackSonUtil.getJson(query)+" Result is:"+JackSonUtil.getJson(discountResult));
    if(null == discountResult || !discountResult.getSuccess() || null == discountResult.getModule()){
      return null;
    }
    
    DiscountResultDTO dis = discountResult.getModule();
    if(dis.getOrinalPrice() == null || dis.getDiscountPrice() == null || dis.getOrinalPrice().compareTo(dis.getDiscountPrice()) <= 0){
      return null;
    }
    
    return dis;
//    int discount = dis.getOrinalPrice() - dis.getDiscountPrice();
//    return new BigDecimal(discount).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
//      
//      
//      
//      
//    try {
//      discountInfoService.removePromotionChainContext(userId);
//    } catch (Exception e) {
//      log.error("优惠清除context异常" + e.getMessage());
//    }
//    DiscountQueryDTO discountQueryDTO = new DiscountQueryDTO();
//    discountQueryDTO.setUserId(userId);
//    discountQueryDTO.setBuySkuDetail(skuList);
//    Result<Map<String, DiscountCalResultDTO>> discountMapResult = null;
//    try {
//      discountMapResult = discountInfoService.cal(discountQueryDTO);
//    } catch (Exception e) {
//      log.error("优惠计算异常" + e.getMessage());
//    }
//    BigDecimal discountAmount = new BigDecimal(0);
//    if (discountMapResult != null && discountMapResult.getSuccess()
//        && discountMapResult.getModule() != null) {
//      Map<String, DiscountCalResultDTO> discountCalResultDTOMap = discountMapResult.getModule();
//      DiscountCalResultDTO fullReduce =
//          discountCalResultDTOMap.get(DiscountTypeEnum.FULL_REDUCE.getCode()
//              + DiscountTypeEnum.FULL_REDUCE.getValue());
//      BigDecimal baseNum = new BigDecimal("100");
//      BigDecimal originPrice =
//          new BigDecimal(String.valueOf(fullReduce.getOriginPrice())).divide(baseNum).setScale(2,
//              BigDecimal.ROUND_DOWN);
//      if ((fullReduce.getTotalDiscountPrice() > 0 || fullReduce.isGetDiscount())
//          && originPrice.compareTo(effeAmount) == 0) {
//        BigDecimal discountPrice =
//            new BigDecimal(String.valueOf(fullReduce.getTotalDiscountPrice())).divide(baseNum);
//        discountAmount = discountAmount.add(discountPrice);
//        if (discountAmount.compareTo(new BigDecimal("0")) > 0) {
//          updateDiscountStatus(discountCalResultDTOMap, userId);
//        }
//      }
//    }
//    
// // 优惠券优惠金额
//    if (null != userCouponId && userCouponId > 0L) {
//        try {
//          UserCouponDiscontQueryDTO query = new UserCouponDiscontQueryDTO();
//            query.setCouponId(userCouponId);
//            query.setPayType((int)channelType);
//            query.setUserId(userId);
//            query.setCalculateAndUse(true);
//            query.setBuySkuDetail(skuList);
//            Result<UserCouponDiscontResultDTO> discountResult = newUserCouponService.calculateDiscountPriceWithCoupon(query);
//            if (null != discountResult && discountResult.getSuccess() && null != discountResult.getModule()) {
//                discountAmount = discountAmount.add(new BigDecimal(String.valueOf(discountResult.getModule().getDiscountPrice())));
//                if (discountAmount.compareTo(new BigDecimal("0")) > 0) {
//                    discountAmount = discountAmount.divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN);
//                }
//            }
//            log.error("Use coupon get coupon price ." + discountAmount + ",query is:" + JackSonUtil.getJson(query));
//        } catch (Exception e) {
//            log.error("Use coupon get coupon price failed." + e.getMessage());
//            e.printStackTrace();
//        }
//    }
//    return discountAmount;
  }
  
  /**
   * 记录订单优惠信息
   */
  private void createOrderDisountSnapshot(DiscountResultDTO discount, Orders order){
    if(null == discount || null == order){
      return;
    }
    List<OrderDiscount> resultList = Lists.newArrayList();
    Long userId = order.getUserId();
    Long orderId = order.getId();
    OrderDiscount totalDiscount = new OrderDiscount();
    totalDiscount.setType((short)0);//总优惠
    totalDiscount.setDisname("优惠");
    totalDiscount.setIgnorePrice(discount.getOrinalPrice());
    totalDiscount.setOrderId(orderId);
    totalDiscount.setOriginPrice(discount.getOrinalPrice());
    totalDiscount.setPrice(discount.getDiscountPrice());
    totalDiscount.setStatus((short)0);
    totalDiscount.setUserId(userId);
    resultList.add(totalDiscount);
    if(CollectionUtils.isNotEmpty(discount.getTotalDiscount())){
      for(DiscountDetailDTO dis:discount.getTotalDiscount()){
        OrderDiscount subDiscount = new OrderDiscount();
        subDiscount.setType(dis.getType());//总优惠
        subDiscount.setDisname(dis.getDiscountName());
        subDiscount.setIgnorePrice(dis.getIgnorePromotionPrice());
        subDiscount.setOrderId(orderId);
        subDiscount.setOriginPrice(dis.getOriginPrice());
        subDiscount.setPrice(dis.getDiscountPrice());
        subDiscount.setStatus((short)0);
        subDiscount.setUserId(userId);
        resultList.add(subDiscount);
      }
    }
    if(null != discount.getDiscountDetailMap() && CollectionUtils.isNotEmpty(discount.getDiscountDetailMap().keySet())){
      for(Long skuId:discount.getDiscountDetailMap().keySet()){
        List<DiscountDetailDTO> disList = discount.getDiscountDetailMap().get(skuId);
        if(CollectionUtils.isNotEmpty(disList)){
          for(DiscountDetailDTO dis:disList){
            OrderDiscount subDiscount = new OrderDiscount();
            subDiscount.setType(dis.getType());//总优惠
            subDiscount.setDisname(dis.getDiscountName());
            subDiscount.setIgnorePrice(dis.getIgnorePromotionPrice());
            subDiscount.setOrderId(orderId);
            subDiscount.setOriginPrice(dis.getOriginPrice());
            subDiscount.setPrice(dis.getDiscountPrice());
            subDiscount.setStatus((short)0);
            subDiscount.setUserId(userId);
            subDiscount.setSkuId(skuId);
            List<Long> discountIds = dis.getDiscountIds();
            String discountDetail = "";
            for(Long id:discountIds){
              discountDetail = discountDetail + id.toString()+",";
            }
            subDiscount.setDiscountdetail(discountDetail);
            resultList.add(subDiscount);
          }
        }
      }
    }
    if(resultList.size() > 0){
      try{
        int count = orderDiscountMapper.batchInsert(resultList);
        if(count != resultList.size()){
          log.error("Trade insert orderdiscount failed,insert count result is "+count+" list size "+resultList.size());
        }
      }catch(Exception e){
        log.error("Trade insert orderdiscount failed:"+e.getMessage());
        e.printStackTrace();
      }
    }
  }
  
  /**
   * 更新优惠状态
   */
  private void updateOrderDisountSnapshot(Long orderId){
    try{
      List<OrderDiscount> disList = orderDiscountMapper.select(orderId, (short)13);
      String discountIds = "";
      for(OrderDiscount dis:disList){
        if(StringUtils.isNotEmpty(dis.getDiscountdetail())){
          discountIds = discountIds +dis.getDiscountdetail()+ ",";
        }
      }
      String idArray[] = discountIds.split(",");
      for(String id:idArray){
        if(org.apache.commons.lang.StringUtils.isNotEmpty(id) && org.apache.commons.lang.StringUtils.isNumeric(id)){
          Long discountId = Long.valueOf(id);
          //更新订单id过去并且设置状态为无效，不允许优惠再使用
          newUserCouponService.updateCouponOrder(discountId, orderId);
        }
      }
    }catch(Exception e){
      log.error(e.getMessage());
    }
    
    
  }
    /**
     * 判断是否限购
     * @param skuIdList
     * @param items
     * @return
     */
    private Result<Boolean> checkSkuLimitBuy(List<Long> skuIdList, List<ShoppingCart> items){
      try{
        Map<Long,ShoppingCart> skuCountM = Maps.newHashMap();
        for(ShoppingCart cart:items){
          skuCountM.put(cart.getItemSkuId(),cart);
        }

        Result<List<ItemSkuTagsDTO>> skuResult = skuService.querySkuTagsDetail(skuIdList);
        if(null != skuResult && skuResult.getSuccess()){
          for(ItemSkuTagsDTO skuTags:skuResult.getModule()){
            if(null == skuTags || CollectionUtils.isEmpty(skuTags.getSkuTagDetailList())){
              continue;
            }
            Long skuId = skuTags.getSkuId();
            for(SkuTagDetail tagDetail:skuTags.getSkuTagDetailList()){
              if(tagDetail.getTagId().equals("6")){//限购标
                int limitCount = Integer.valueOf(tagDetail.getTagRule());
                ShoppingCart item = skuCountM.get(skuId);
                if(null != item){
                  Double buyCount = item.getItemCount();
                  if(null != buyCount && new BigDecimal(buyCount.toString()).compareTo(new BigDecimal(limitCount)) >= 1){
                    String skuName = item.getItemName();
                    return Result.getErrDataResult(-1, "超买限购"+limitCount + "件商品 "+skuName);
                  }
                }

              }
            }
          }
        }
      }catch(Exception e){
        log.error("Query skuTag failed,error info "+e.getMessage());
        e.printStackTrace();
      }
      return Result.getSuccDataResult(true);
    }

     /**
     * 更新特殊优惠的状态
     * @param discountCalResultDTOMap
     */
    private void updateDiscountStatus(Map<String,DiscountCalResultDTO> discountCalResultDTOMap, long userId){
      if(null == discountCalResultDTOMap || CollectionUtils.isEmpty(discountCalResultDTOMap.keySet())){
        return;
      }
      List<Integer> updateDetailIds = Lists.newArrayList();
      for(String key:discountCalResultDTOMap.keySet()){
        if(StringUtils.isNotEmpty(key) && key.startsWith(DiscountTypeEnum.SKU_DISCOUNT.getCode()+DiscountTypeEnum.SKU_DISCOUNT.getValue())){
          DiscountCalResultDTO result = discountCalResultDTOMap.get(key);
          if(null != result  && result.isGetDiscount() && null != result.getDiscountInfoDTO() && result.getDiscountInfoDTO().getDiscountDetailId() != null){
            updateDetailIds.add(result.getDiscountInfoDTO().getDiscountDetailId());
          }
        }
      }
      if(CollectionUtils.isNotEmpty(updateDetailIds) && 0 < userId){
        discountInfoService.updateByUserList(updateDetailIds, userId, (short)0);
      }

    }


    /**
     * 判断库存是否足够
     * @return
     */
    private boolean isInventoryEnough(boolean offline, SimpleResultVO<InventoryVO> inventoryResult,ShoppingCart cart){
      if(offline){//线下直接通过
        return true;
      }
      if(null == cart || null == inventoryResult || !inventoryResult.isSuccess() || null == inventoryResult.getTarget()){
        return false;
      }
      if(cart.getSkuType() == 2){
        if(!inventoryResult.getTarget().getMaySale()){//不允许销售
          return false;
        }
        int saleInventoryNum = inventoryResult.getTarget().getNumber();
        int cmpNum = saleInventoryNum*1000;
        int userCatNum = new BigDecimal(cart.getItemCount()).multiply(new BigDecimal(cart.getUnitWeightNum())).intValue();
        if(cmpNum >=userCatNum ){
          return true;
        }else{
          return false;
        }
      }else{//历史商品或者非称重商品，暂时都直接比较库存数量
        if(inventoryResult.getTarget().getMaySale() && inventoryResult.getTarget().getNumber() > 0 && inventoryResult.getTarget().getNumber() >= cart.getItemCount()){
          return true;//库存数量足够
        }else{
          return false;//库存不足
        }
      }
    }
    @Override
    public boolean alipayByAuthCode(long orderId, String authCode) {
        Orders order = get(orderId);
        if (null == order) {
            throw new RuntimeException("没有id:" + orderId + "对应的订单");
        }
        if (order.getStatus() != OrderStatus.INIT.getValue()) {
            throw new RuntimeException("订单:" + orderId + "当前状态不允许支付");
        }

        AliPayAuthCodePayRequest request = new AliPayAuthCodePayRequest();

        request.setAppId("2016112403191069");
        request.setCharset("utf-8");
        request.setFormat("JSON");
        request.setNotifyUrl("http://trade.xianzaishi.net/alipay/qrPayCallback.htm");
        request.setSignType("RSA");
        request.setTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        request.setVersion("1.0");
        AliPayAuthCodePayRequest.BizContent bizContent = request.createBizContent();
        bizContent.setOutTradeNo(order.getId().toString());
        bizContent.setTotalAmount(String.format("%.2f", order.getEffeAmount()));
        bizContent.setSubject("鲜在时(蓝村路店)");
        bizContent.setAuthCode(authCode);
        bizContent.setScene("bar_code");

        RpcResult<AliPayAuthCodePayResponse> result = aliPayService.aliPayAuthCodePay(request);

        if (result.isSuccess() || (result.getData() != null && "10003".equals(result.getData().getCode()))) {
            TradeLog.info("[AliPay]-[PreSignQr] PARAMS[%s]", JSONObject.toJSONString(result));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public PageList<OrderInfo> getOrdersByUserId(long uid, OrderStatus[] status, int pageNo, int pageSize, boolean isOrderByDesc) {
        try {
            int total = this.getOrderCountByUserId(uid, status);
            Pagination pagin = Pagination.getPagination(pageNo, pageSize, total);
            if (pagin.limit() > 0) {
                List<OrderInfo> ordersList;
                String sort = "asc";
                if(isOrderByDesc){
                    sort = "desc";
                }
                if (null != status) {
                    ordersList = ordersDAO.selectByUserId(uid, "and status in (" + orderStatus2String(status) + ")", pagin, sort);
                } else {
                    ordersList = ordersDAO.selectByUserId(uid, "", pagin, sort);
                }
                if (CollectionUtils.isNotEmpty(ordersList)) {
                    List<OrderInfo> orderList = new ArrayList<OrderInfo>(ordersList.size());
                    for (Orders orders : ordersList) {
                        OrderInfo orderInfo = new OrderInfo(orders);
                        orderInfo.setItems(this.getOrderItems(orders.getId()));
                        orderList.add(orderInfo);
                    }
                    return new PageList<OrderInfo>(pagin, orderList);
                }
            }
        } catch (DataAccessException e) {
            log.error("get order failed", e);
        }
        return null;
    }

    @Override
    public Map<String,DiscountInfoVO> getOrderDiscount(long orderId) {
      List<OrderDiscount> orderDiscountList = orderDiscountMapper.select(orderId, null);
      Map<Long,List<OrderDiscount>> orderGroupM = Maps.newHashMap();
      for(OrderDiscount discount:orderDiscountList){
        Short type = discount.getType();
        Long skuId = discount.getSkuId();
        if(type == 0){//计算总优惠
          List<OrderDiscount> orderList = orderGroupM.get(type);
          if(null == orderList){
            orderList = Lists.newArrayList();
          }
          orderList.add(discount);
          orderGroupM.put(0L, orderList);
        }else if(null != skuId && skuId > 0){
          List<OrderDiscount> orderList = orderGroupM.get(skuId);
          if(null == orderList){
            orderList = Lists.newArrayList();
          }
          orderList.add(discount);
          orderGroupM.put(skuId, orderList);
        }
      }
      if(CollectionUtils.isEmpty(orderGroupM.keySet())){
        return null;
      }
      Map<String,DiscountInfoVO> result = Maps.newHashMap();
      for(Long id:orderGroupM.keySet()){
        List<OrderDiscount> discountList = orderGroupM.get(id);
        DiscountInfoVO discount = getDiscountInfoVO(discountList);
        result.put(id.toString(), discount);
      }
      return result;
    }
    
    private DiscountInfoVO getDiscountInfoVO(List<OrderDiscount> disList){
      if(CollectionUtils.isEmpty(disList)){
        return null;
      }
      DiscountInfoVO disinfo = new DiscountInfoVO();
      int orignalPrice = 0;
      int discountPrice = 0;
      String discountDesc = "";
      for(OrderDiscount dis:disList){
        orignalPrice = orignalPrice + dis.getOriginPrice();
        discountPrice = discountPrice + dis.getPrice();
        if(dis.getType() == 0){
          discountDesc = "总优惠";
        }else if(dis.getType() == 13){
          discountDesc = "店铺A类优惠";
        }
      }
      int disP = orignalPrice - discountPrice;
      String currPrice = new BigDecimal(discountPrice).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString();
      String discountPriceStr = new BigDecimal(disP).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString();
      disinfo.setCurrPrice(currPrice);
      disinfo.setDiscountPrice(discountPriceStr);
      disinfo.setDiscountDesc(discountDesc);
      return disinfo;
    }
}
