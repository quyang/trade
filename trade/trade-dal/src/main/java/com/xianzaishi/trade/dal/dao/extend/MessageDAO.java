package com.xianzaishi.trade.dal.dao.extend;

import com.xianzaishi.trade.dal.dao.MessageMapper;

/**
 * 
 * @author Croky.Zheng
 * 2016年8月16日
 */
public interface MessageDAO extends MessageMapper {

}
