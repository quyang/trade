package com.xianzaishi.trade.dal.model.query;

import java.util.List;

import com.xianzaishi.trade.client.query.OrderBaseQuery;

/**
 * 购物车对外查询接口请求参数对象
 * @author zhancang
 */
public class ShoppingCartQuery extends OrderBaseQuery {

  private List<Long> idList;

  private Long userId;

  private Long skuId;

  public List<Long> getIdList() {
    return idList;
  }

  public void setIdList(List<Long> idList) {
    this.idList = idList;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }
}
