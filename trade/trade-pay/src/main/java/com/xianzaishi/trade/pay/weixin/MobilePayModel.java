package com.xianzaishi.trade.pay.weixin;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.xianzaishi.trade.pay.util.PayUtil;

/**
 * 手机APP支付
 * @author Croky.Zheng
 * 2016年9月13日
 */
@XmlRootElement(name="xml")
@XmlAccessorType(XmlAccessType.FIELD)//PROPERTY,对应的是get方法, 如果设置这个值@XmlElement等换name会异常
//@XmlType(name="",propOrder={})
public class MobilePayModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2071713740081972442L;

	/**
	 * 应用ID
	 */
	private String appid = WeiXinPayConfig.APPID;
	
	/**
	 * 商户号
	 */
	private String partnerid = WeiXinPayConfig.MCHID;

	/**
	 * 预支付交易ID	
	 */
	private String prepayid;
	/**
	 * 
	 */
	//@XmlElementWrapper(name="package")
	@XmlElement(name="package")
	private String pack = "Sign=WXPay";
	
	/**
	 * 随机字符串
	 */
	private String noncestr = PayUtil.genRandomString();
	/**
	 * 时间戳
	 */
	private long timestamp = new Date().getTime() / 1000;
	
	/**
	 * 签名
	 */
	private String sign;
	public String getAppid() {
		return appid;
	}
	
	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	public String getPartnerid() {
		return partnerid;
	}
	
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	
	public String getPrepayid() {
		return prepayid;
	}
	
	public void setPrepayid(String prepayid) {
		this.prepayid = prepayid;
	}
	
	public String getPack() {
		return pack;
	}
	
	public void setPack(String pack) {
		this.pack = pack;
	}
	
	public String getNoncestr() {
		return noncestr;
	}
	
	public void setNoncestr(String noncestr) {
		this.noncestr = noncestr;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getSign() {
		return sign;
	}
	
	public void setSign(String sign) {
		this.sign = sign;
	}
}
