package com.xianzaishi.trade.web.form;

/**
 * 员工收费记录，包括总销售额，支付方式，员工信息
 */
public class ChargeRecord{
    private double amount;   //支付金额
    private int  payWay;   //支付方式
    private String account;//支付账户(不是用户ID，现金则为null)
    private String callbackContent;//支付方式返回的值
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getPayWay() {
		return payWay;
	}
	public void setPayWay(int payWay) {
		this.payWay = payWay;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getCallbackContent() {
		return callbackContent;
	}
	public void setCallbackContent(String callbackContent) {
		this.callbackContent = callbackContent;
	}
}