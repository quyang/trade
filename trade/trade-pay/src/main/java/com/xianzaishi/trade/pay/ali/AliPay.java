package com.xianzaishi.trade.pay.ali;

public class AliPay {

	/**
	 * 获取订单签名
	 * 
	 * @param orderId
	 * @param totalFee
	 * @param subject
	 * @param body
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static String tradeOrderSignature(long orderId, double totalFee, String subject, String body) throws IllegalArgumentException {

		AliPayRequestBuilder builder = AliPayRequestBuilder.newInstance();

		builder.setOutTradeNo(String.valueOf(orderId));
		builder.setTotal_fee(String.valueOf(totalFee));
		builder.setSubject(subject);
		builder.setBody(body);

		if (builder.valid()) {
			return builder.getRequestStringWithSign();
		} else {
			throw new IllegalArgumentException(
					String.format("订单参数错误 -> orderId[%s], totalFee[%s], subject[%s], body[%s]", orderId, totalFee, subject, body));
		}
	}
	
}
