package com.xianzaishi.trade.dal.model.extend;

import java.util.ArrayList;
import java.util.List;
import com.xianzaishi.trade.dal.model.OrderItem;
import com.xianzaishi.trade.dal.model.Orders;

public class OrderInfo extends Orders {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5309154646358187281L;

	public OrderInfo(){}
	
	public OrderInfo(Orders orders) {
		this.setAttribute(orders.getAttribute());
		this.setBillAmount(orders.getBillAmount());
		this.setCashierId(orders.getCashierId());
		this.setCouponId(orders.getCouponId());
		this.setDeviceCode(orders.getDeviceCode());
		this.setEffeAmount(orders.getEffeAmount());
		this.setDiscountAmount(orders.getDiscountAmount());
		this.setGmtCreate(orders.getGmtCreate());
		this.setGmtModify(orders.getGmtModify());
		this.setId(orders.getId());
		this.setPayAmount(orders.getPayAmount());
		this.setStatus(orders.getStatus());
		this.setOrderType(orders.getOrderType());
		this.setUserAddressId(orders.getUserAddressId());
		this.setUserId(orders.getUserId());
		this.setGmtPay(orders.getGmtPay());
		this.setGmtReceive(orders.getGmtReceive());
		this.setSeq(orders.getSeq());
		this.setUserAddress(orders.getUserAddress());
		this.setCredit(orders.getCredit());
		this.setGmtDistribution(orders.getGmtDistribution());
		this.setChannelType(orders.getChannelType());
		this.setOrderType(orders.getOrderType());
	}
	
	private List<OrderItem> items;
	
	public void add(OrderItem item) {
		if (null == items) {
			items = new ArrayList<OrderItem>();
		}
		items.add(item);
	}

	public List<OrderItem> getItems() {
		return items;
	}

	public void setItems(List<OrderItem> items) {
		this.items = items;
	}
}
