package com.xianzaishi.trade.web.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jackson.JsonNode;

import com.xianzaishi.trade.utils.AttachPeriodConvert;
import com.xianzaishi.trade.utils.JsonConvert;
import com.xianzaishi.trade.utils.enums.DatePeriod;


/**
 * 
 * @author Croky.Zheng
 * 2016年4月9日
 */
public class DataTableParam {

	private int sEcho = 0;
	private int iDisplayStart = 0;
	private int iDisplayLength = 10;
	private int zoneId = 0;
	private int energyTypeId = 0;
	private int attachPeriod = 0;
	private int oneAttachPeriod=0;
	private int pageNum = 0;
	private int houseId = 0;
	private long billId = 0L;
	private long billTypeId=0;
	private Map<String,String> nameValueMap = new HashMap<String,String>();
	
	public int getsEcho() {
		return sEcho;
	}
	public void setsEcho(int sEcho) {
		this.sEcho = sEcho;
	}
	
	public int getiDisplayStart() {
		return iDisplayStart;
	}
	
	public void setiDisplayStart(int iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}
	
	public int getiDisplayLength() {
		return iDisplayLength;
	}
	
	public void setiDisplayLength(int iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}
	
	public int getZoneId() {
		return zoneId;
	}
	
	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}
	
	public int getEnergyTypeId() {
		return energyTypeId;
	}
	
	public void setEnergyTypeId(int energyTypeId) {
		this.energyTypeId = energyTypeId;
	}
	
	public int getAttachPeriod() {
		return attachPeriod;
	}
	
	public void setAttachPeriod(int attachPeriod) {
		this.attachPeriod = attachPeriod;
	}
	
	public int getHouseId() {
		return houseId;
	}
	
	public void setHouseId(int houseId) {
		this.houseId = houseId;
	}
	
	public long getBillId() {
		return billId;
	}
	
	public void setBillId(long billId) {
		this.billId = billId;
	}
	
	public int getPageNum() {
		return pageNum;
	}
	
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	
	public int getPageSize() {
		return this.iDisplayLength;
	}
	
	protected void put(String name,String value) {
		//1.8 this.nameValueMap.putIfAbsent(name, value);
		this.nameValueMap.put(name, value);
	}
	
	public String get(String name) {
		return this.nameValueMap.get(name);
	}
	

	public static String getValue(String json,String name) {
		//JsonNode node = JsonConvert.json2Node(json);
		/*
		List<LinkedHashMap<String,String>> aoData = JsonConvert.json2Object(json, List.class);
		for(Object obj : aoData) {
			System.out.println(obj.toString());
		}
		*/
		JsonNode node = JsonConvert.json2Node(json);
		Iterator<JsonNode> it = node.getElements();
		while (it.hasNext()) {
			JsonNode tmp = it.next();
			if (tmp.get("name").asText().equals(name)) {
				if (null != tmp.get("value")) {
					return tmp.get("value").getTextValue();
				}
			}
		}
		return null;
	}
	
	public static DataTableParam json2Object(String json) {
		//JsonNode node = JsonConvert.json2Node(json);
		/*
		List<LinkedHashMap<String,String>> aoData = JsonConvert.json2Object(json, List.class);
		for(Object obj : aoData) {
			System.out.println(obj.toString());
		}
		*/
		JsonNode node = JsonConvert.json2Node(json);
		DataTableParam param = new DataTableParam();
		Iterator<JsonNode> it = node.getElements();
		while (it.hasNext()) {
			JsonNode tmp = it.next();
			if((null != tmp.get("name")) && (null != tmp.get("value"))) {
				param.put(tmp.get("name").asText(), tmp.get("value").asText());
			}
			if (tmp.get("name").asText().equals("sEcho")) {
				if (null != tmp.get("value")) {
					param.setsEcho(tmp.get("value").getIntValue());
				} else {
					param.setsEcho(0);
				}
			}
			if (tmp.get("name").asText().equals("iDisplayStart")) {
				if (null != tmp.get("value")) {
					param.setiDisplayStart(tmp.get("value").getIntValue());
				} else {
					param.setiDisplayStart(0);
				}
			}
			if (tmp.get("name").asText().equals("iDisplayLength")) {
				if (null != tmp.get("value")) {
					param.setiDisplayLength(tmp.get("value").getIntValue());
				} else {
					param.setiDisplayLength(0);
				}
			}
			if (tmp.get("name").asText().equals("zoneId")) {
				if (null != tmp.get("value")) {
					param.setZoneId(tmp.get("value").getIntValue());
				} else {
					param.setZoneId(0);
				}
			}
			if (tmp.get("name").asText().equals("energyTypeId")) {
				if (null != tmp.get("value")) {
					param.setEnergyTypeId(tmp.get("value").getIntValue());
				} else {
					param.setEnergyTypeId(0);
				}
			}
			if (tmp.get("name").asText().equals("attachPeriod")) {
				if (null != tmp.get("value")) {
					param.setAttachPeriod(AttachPeriodConvert.convert(tmp.get("value").asText(),DatePeriod.MONTH));
				} else {
					param.setAttachPeriod(0);
				}
			}
			if (tmp.get("name").asText().equals("oneAttachPeriod")) {
				if (null != tmp.get("value")) {
					param.setOneAttachPeriod(tmp.get("value").getIntValue());
				} else {
					param.setOneAttachPeriod(0);
				}
			}
			if (tmp.get("name").asText().equals("billTypeId")) {
				if (null != tmp.get("value")) {
					param.setBillTypeId(tmp.get("value").getIntValue());
				} else {
					param.setBillTypeId(0);
				}
			}
			if (tmp.get("name").asText().equals("houseId")) {
				if (null != tmp.get("value")) {
					param.setHouseId(tmp.get("value").getIntValue());
				} else {
					param.setHouseId(0);
				}
			}
			if (tmp.get("name").asText().equals("billId")) {
				if (null != tmp.get("value")) {
					param.setBillId(tmp.get("value").getLongValue());
				} else {
					param.setBillId(0L);
				}
			}
		}
		if (param.getiDisplayLength() > 0) {
			int pageNum = param.getiDisplayStart() / param.getiDisplayLength() + 1;
			param.setPageNum(pageNum);
		}
		return param;
	}
	public int getOneAttachPeriod() {
		return oneAttachPeriod;
	}
	public void setOneAttachPeriod(int oneAttachPeriod) {
		this.oneAttachPeriod = oneAttachPeriod;
	}
	public long getBillTypeId() {
		return billTypeId;
	}
	public void setBillTypeId(long billTypeId) {
		this.billTypeId = billTypeId;
	}

}
