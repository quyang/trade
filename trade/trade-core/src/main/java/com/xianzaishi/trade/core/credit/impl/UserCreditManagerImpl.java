package com.xianzaishi.trade.core.credit.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.croky.util.CollectionUtils;
import com.xianzaishi.trade.core.credit.UserCreditManager;
import com.xianzaishi.trade.dal.dao.extend.UserCreditDAO;
import com.xianzaishi.trade.dal.dao.extend.UserCreditLogDAO;
import com.xianzaishi.trade.dal.model.UserCredit;
import com.xianzaishi.trade.dal.model.UserCreditExample;
import com.xianzaishi.trade.dal.model.UserCreditLog;
import com.xianzaishi.trade.dal.model.UserCreditLogExample;

/**
 * 
 * @author Croky.Zheng
 * 2016年9月19日
 */
@Transactional(readOnly = true)
@Component(value = "userCreditManager")
public class UserCreditManagerImpl implements UserCreditManager {
	private static final Logger log = LoggerFactory.getLogger(UserCreditManagerImpl.class);
	
	
	@Resource
	private UserCreditDAO userCreditDAO;

	@Resource
	private UserCreditLogDAO userCreditLogDAO;
	
	@Override
	public int getCreditByUserId(long userId) {
		UserCredit userCredit = get(userId);
		if (null != userCredit) {
			return userCredit.getCredit();
		}
		return 0;
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW,rollbackFor=RuntimeException.class)
	public int add(long userId,int appId,int credit,String description) {
		if (insertUserCreditLog(userId,appId,credit,description) > 0) {
			try {
				UserCredit userCredit = userCreditDAO.getUserCreditForUpdate(userId);
				Date now = new Date();
				if (null == userCredit) {
					userCredit = new UserCredit();
					userCredit.setUserId(userId);
					userCredit.setCredit(credit);
					userCredit.setGmtCreate(now);
					userCredit.setGmtModify(now);
					userCreditDAO.insert(userCredit);
				} else {
					userCredit.setCredit(userCredit.getCredit() + credit);
					userCredit.setGmtModify(now);
					userCreditDAO.updateByPrimaryKey(userCredit);
				}
				return userCredit.getCredit();
			} catch (DataAccessException e) {
				log.error("select UserCredit failed.",e);
				throw new RuntimeException(e);
			}
		}
		throw new RuntimeException("添加用户积分失败");
	}

	@Override
	public List<UserCreditLog> getUserCreditLogs(long userId) {
		UserCreditLogExample example = new UserCreditLogExample();
		example.createCriteria().andUserIdEqualTo(userId);
		try {
			return userCreditLogDAO.selectByExample(example);
		} catch (DataAccessException e) {
			log.error("select UserCreditLog failed.",e);
		}
		return null;
	}
	private long insertUserCreditLog(long userId,int appId,int amount,String description) {
		UserCreditLog userCreditLog = new UserCreditLog();
		userCreditLog.setAmount(amount);
		userCreditLog.setAppId(appId);
		userCreditLog.setDescription(description);
		userCreditLog.setUserId(userId);
		return insertUserCreditLog(userCreditLog);
	}
	
	private long insertUserCreditLog(UserCreditLog userCreditLog) {
		Date now = new Date();
		userCreditLog.setGmtCreate(now);
		userCreditLog.setGmtModify(now);
		try {
			if (userCreditLogDAO.insert(userCreditLog) > 0) {
				return userCreditLog.getId();
			}
		} catch (DataAccessException e) {
			log.error("select UserCredit failed.",e);
		}
		return 0L;
	}
	private UserCredit get(long userId) {
		UserCreditExample example = new UserCreditExample();
		example.createCriteria().andUserIdEqualTo(userId);
		try {
			List<UserCredit> userCredits = userCreditDAO.selectByExample(example);
			if (CollectionUtils.isNotEmpty(userCredits)) {
				return userCredits.get(0);
			}
		} catch (DataAccessException e) {
			log.error("select UserCredit failed.",e);
		}
		return null;
	}

	public UserCreditDAO getUserCreditDAO() {
		return userCreditDAO;
	}

	public void setUserCreditDAO(UserCreditDAO userCreditDAO) {
		this.userCreditDAO = userCreditDAO;
	}

	public UserCreditLogDAO getUserCreditLogDAO() {
		return userCreditLogDAO;
	}

	public void setUserCreditLogDAO(UserCreditLogDAO userCreditLogDAO) {
		this.userCreditLogDAO = userCreditLogDAO;
	}
}
