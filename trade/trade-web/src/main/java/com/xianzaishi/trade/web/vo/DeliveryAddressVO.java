package com.xianzaishi.trade.web.vo;

import java.io.Serializable;

/**
 *  收货地址实例
 * @author Croky.Zheng
 * 2016年8月3日
 */
public class DeliveryAddressVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 83606194919337538L;

	/**
	 * 地址ID
	 */
	private long id;
	
	/**
	 * 收货人姓名
	 */
	private String name;
	
	/**
	 * 收货人电话号码
	 */
	private String phone;
	
	/**
	 * 四级行政区划编号
	 */
	private String code;
	
	private String level1Name;
	
	private String level2Name;
	
	private String level3Name;
	
	private String level4Name;
	
	/**
	 * 完整地址
	 */
	private String address;
	
	private boolean defaultAddress;
	
	

	public String getLevel1Name() {
		return level1Name;
	}

	public void setLevel1Name(String level1Name) {
		this.level1Name = level1Name;
	}

	public String getLevel2Name() {
		return level2Name;
	}

	public void setLevel2Name(String level2Name) {
		this.level2Name = level2Name;
	}

	public String getLevel3Name() {
		return level3Name;
	}

	public void setLevel3Name(String level3Name) {
		this.level3Name = level3Name;
	}

	public String getLevel4Name() {
		return level4Name;
	}

	public void setLevel4Name(String level4Name) {
		this.level4Name = level4Name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isDefaultAddress() {
		return defaultAddress;
	}

	public void setDefaultAddress(boolean defaultAddress) {
		this.defaultAddress = defaultAddress;
	}
}
