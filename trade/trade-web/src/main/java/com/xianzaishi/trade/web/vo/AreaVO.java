package com.xianzaishi.trade.web.vo;

import java.io.Serializable;

public class AreaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5937151044687855055L;

	public AreaVO(String code,String areaName) {
		this.code = code;
		this.areaName = areaName;
	}
	/**
	 * 编码
	 */
	private String code;
	
	/**
	 * 区域名称
	 */
	private String areaName;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
}
