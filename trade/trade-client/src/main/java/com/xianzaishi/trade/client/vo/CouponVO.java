package com.xianzaishi.trade.client.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * 这个类没有任何注释，靠猜加注释。擦<br/>
 * 针对类型为1、11、77类型的优惠券，价格单位全部为元为单位的浮点数<br/>
 * 针对其它类型的优惠券，价格单位全部升级为分为单位的整数<br/>
 * 
 * @change by zhancang
 */
public class CouponVO implements Serializable {

  private Long id;

  private String title;

  private String rules;

  /**
   * 该字段不知道干嘛的，没有调用 by zhancang
   */
  private Long crowdId;

  /**
   * type=1 看样子是现金券<br/>
   * type=77 早餐券<br/>
   * type=11 新人礼包券<br/>
   */
  private Short type;

  private Date gmtStart;

  private Date gmtEnd;

  private Date gmtCreate;

  private Date gmtModify;

  private Short status;

  /**
   * 抵扣金额 by zhancang<br/>
   * 丢失各种精度，不推荐使用
   */
  private Double amount;
  
  /**
   * 推荐使用分为单位价格
   */
  private Integer amountCent;

  /**
   * channelType=1代表线上渠道<br/>
   * channelType=2代表线下渠道<br/>
   */
  private Short channelType;

  /**
   * 折扣使用前置价格条件 by zhancang<br/>
   * 丢失各种精度，不推荐使用
   */
  private Double amountLimit;
  
  /**
   * 分为单价的价格限制
   */
  private Integer amountLimitCent;

  private static final long serialVersionUID = 1L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public String getRules() {
    return rules;
  }

  public void setRules(String rules) {
    this.rules = rules == null ? null : rules.trim();
  }

  public Long getCrowdId() {
    return crowdId;
  }

  public void setCrowdId(Long crowdId) {
    this.crowdId = crowdId;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Date getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(Date gmtStart) {
    this.gmtStart = gmtStart;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModify() {
    return gmtModify;
  }

  public void setGmtModify(Date gmtModify) {
    this.gmtModify = gmtModify;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  @Deprecated
  public Double getAmount() {
    return amount;
  }

  @Deprecated
  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  @Deprecated
  public Double getAmountLimit() {
    return amountLimit;
  }

  @Deprecated
  public void setAmountLimit(Double amountLimit) {
    this.amountLimit = amountLimit;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    CouponVO other = (CouponVO) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
        && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(
            other.getTitle()))
        && (this.getRules() == null ? other.getRules() == null : this.getRules().equals(
            other.getRules()))
        && (this.getCrowdId() == null ? other.getCrowdId() == null : this.getCrowdId().equals(
            other.getCrowdId()))
        && (this.getType() == null ? other.getType() == null : this.getType().equals(
            other.getType()))
        && (this.getGmtStart() == null ? other.getGmtStart() == null : this.getGmtStart().equals(
            other.getGmtStart()))
        && (this.getGmtEnd() == null ? other.getGmtEnd() == null : this.getGmtEnd().equals(
            other.getGmtEnd()))
        && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate()
            .equals(other.getGmtCreate()))
        && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify()
            .equals(other.getGmtModify()))
        && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(
            other.getStatus()))
        && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(
            other.getAmount()))
        && (this.getChannelType() == null ? other.getChannelType() == null : this.getChannelType()
            .equals(other.getChannelType()))
        && (this.getAmountLimit() == null ? other.getAmountLimit() == null : this.getAmountLimit()
            .equals(other.getAmountLimit()))
        && (this.getAmountLimitCent() == null ? other.getAmountLimitCent() == null : this.getAmountLimitCent()
        .equals(other.getAmountLimitCent()))
        && (this.getAmountCent() == null ? other.getAmountCent() == null : this.getAmountCent()
        .equals(other.getAmountCent()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
    result = prime * result + ((getRules() == null) ? 0 : getRules().hashCode());
    result = prime * result + ((getCrowdId() == null) ? 0 : getCrowdId().hashCode());
    result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
    result = prime * result + ((getGmtStart() == null) ? 0 : getGmtStart().hashCode());
    result = prime * result + ((getGmtEnd() == null) ? 0 : getGmtEnd().hashCode());
    result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
    result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
    result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
    result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
    result = prime * result + ((getChannelType() == null) ? 0 : getChannelType().hashCode());
    result = prime * result + ((getAmountLimit() == null) ? 0 : getAmountLimit().hashCode());
    result = prime * result + ((getAmountLimitCent() == null) ? 0 : getAmountLimitCent().hashCode());
    result = prime * result + ((getAmountCent() == null) ? 0 : getAmountCent().hashCode());
    return result;
  }
  
  public Integer getAmountCent() {
    return amountCent;
  }

  public void setAmountCent(Integer amountCent) {
    this.amountCent = amountCent;
  }

  public Integer getAmountLimitCent() {
    return amountLimitCent;
  }

  public void setAmountLimitCent(Integer amountLimitCent) {
    this.amountLimitCent = amountLimitCent;
  }

  /**
   * 优惠券是否以分为单位的价格单位
   * true:分为价格单位
   * false:元为价格单位
   * @return
   */
  public boolean isFenPriceTypeUserCoupon(){
    if(null == this.getType()){
      throw new RuntimeException("Coupon type is error");
    }
    
    short couponType = this.getType();
    if(couponType == 1 || couponType == 11 || couponType == 77 || couponType == 12){
      return false;
    }else{
      return true;
    }
  }
}
