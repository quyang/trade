package com.xianzaishi.trade.facade;

import java.net.MalformedURLException;

import org.junit.Test;
//import org.springframework.remoting.caucho.HessianServiceExporter;

import com.caucho.hessian.client.HessianProxyFactory;
import com.xianzaishi.trade.client.MessageService;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.client.Result;
import com.xianzaishi.trade.client.vo.OrderVO;

public class HessianServiceExporterTest {
	
	//private MessageService messageService = new MessageServiceImpl();
	
	@Test
	public void testLong() {
		long id = Long.parseLong("00000000100028");
		System.out.println(id);
		
	}
	
	@Test
	public void testRemoteConnect() {
//		HessianServiceExporter exporter = new HessianServiceExporter();
//		exporter.setServiceInterface(com.xianzaishi.trade.client.MessageService.class);
//		exporter.setService(messageService);
//		exporter.setSerializerFactory(serializerFactory);
//		HessianProxyFactoryBean bean = new HessianProxyFactoryBean();
//		bean.setServiceUrl("http://139.196.94.125/hessian/messageService");
//		bean.setServiceInterface(com.xianzaishi.trade.client.MessageService.class);
		MessageService service = creatMessageService();
		service.sendVericationCode("15657189965", "888888");
//		OrderService orderService = creatOrderService();
//		Result<OrderVO> result = orderService.getOrder(10015L);
//		if (result.isSuccess() && (null != result.getModel())) {
//			System.out.println(result.getModel().toString());
//		}
	}
	
	private MessageService creatMessageService() {
	    HessianProxyFactory factory = new HessianProxyFactory();// hessian服务
	    factory.setOverloadEnabled(true);
	    MessageService service = null;
	    try {
	      service = (MessageService) factory.create(MessageService.class, "http://trade.xianzaishi.net/hessian/messageService");// 创建IService接口的实例对象
	    } catch (MalformedURLException e) {
	      e.printStackTrace();
	    }
	    return service;
	  }
	
	private OrderService creatOrderService() {
	    HessianProxyFactory factory = new HessianProxyFactory();// hessian服务
	    factory.setOverloadEnabled(true);
	    factory.setChunkedPost(false);
	    //factory.setSerializerFactory(factory);
	    OrderService service = null;
	    try {
	      service = (OrderService) factory.create(OrderService.class, "http://localhost:8080/hessian/orderService");// 创建IService接口的实例对象
	    } catch (MalformedURLException e) {
	      e.printStackTrace();
	    }
	    return service;
	  }
}
