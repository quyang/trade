package com.xianzaishi.trade.core.address.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.organization.OrganizationService;
import com.xianzaishi.purchasecenter.client.organization.dto.OrganizationDTO;
import com.xianzaishi.trade.core.address.AreaManager;
import com.xianzaishi.trade.core.address.DeliveryAddressManager;
import com.xianzaishi.trade.core.map.MapService;
import com.xianzaishi.trade.dal.dao.extend.DeliveryAddressDAO;
import com.xianzaishi.trade.dal.model.DeliveryAddress;
import com.xianzaishi.trade.dal.model.DeliveryAddressExample;
import com.xianzaishi.trade.utils.JsonConvert;
import com.xianzaishi.trade.utils.enums.Status;

/**
 * 
 * @author Croky.Zheng 2016年8月17日
 */
@Component(value = "deliveryAddressManager")
public class DeliveryAddressManagerImpl implements DeliveryAddressManager {
	private static final Logger log = LoggerFactory.getLogger(DeliveryAddressManagerImpl.class);

	@Resource
	private DeliveryAddressDAO deliveryAddressDAO;

	@Resource
	private AreaManager areaManager;

	@Resource
	private OrganizationService organizationService;

	@Resource
	private MapService mapService;

	@Override
	public long addLinkman(long userId, String name, String code, String address, String mobile, int shopId) {

		String codeAddress = areaManager.getAreaAddress(code);
		if (null == codeAddress) {
			throw new RuntimeException("无法获取行政区划编码:" + code + "对应的地址");
		}
		String json = mapService.geocoder(codeAddress + address);
		String location = getLocation(code, json);

		if (StringUtils.isBlank(location) || location.split(",").length != 2) {
			throw new RuntimeException("地址不在店铺所在服务区");
		}

		String[] split = location.split(",");
		double longitude = Double.parseDouble(split[0]);// 经度
		double latitude = Double.parseDouble(split[1]);// 纬度

		if (checkShopArea(longitude,latitude,shopId)) {
			DeliveryAddress deliveryAddress = new DeliveryAddress();
			deliveryAddress.setUserId(userId);
			deliveryAddress.setAreaCode(code);
			deliveryAddress.setName(name);
			deliveryAddress.setPhone(mobile);
			deliveryAddress.setZipCode(0);
			deliveryAddress.setAddress(address);
			deliveryAddress.setCodeAddress(areaManager.getAreaAddress(code));
			deliveryAddress.setShopId(shopId);
			deliveryAddress.setLatitude((float) latitude);
			deliveryAddress.setLongitude((float) longitude);
			return insert(deliveryAddress);
		}
		throw new RuntimeException("地址不在店铺所在服务区");
	}

	@Override
	public boolean deleteLinkman(long id) {

		DeliveryAddress deliveryAddress = getLinkman(id);
		// 设置默认联系人
		if (null != deliveryAddress) {
			deliveryAddress.setStatus(Status.INVALID.getValue());
			return update(deliveryAddress);
		}
		return false;
	}

	@Override
	public boolean updateLinkman(long id, String name, String mobile, String code, String address, int shopId) {
		DeliveryAddress deliveryAddress = null;
		try {
			deliveryAddress = deliveryAddressDAO.selectByPrimaryKey(id);
		} catch (DataAccessException e) {
			log.error("select linkman to DeliverAddress failed.", e);
		}
		if (null != deliveryAddress) {
			
			String codeAddress = areaManager.getAreaAddress(code);
			if (null == codeAddress) {
				throw new RuntimeException("无法获取行政区划编码:" + code + "对应的地址");
			}
			String json = mapService.geocoder(codeAddress + address);
			String location = getLocation(code, json);

			if (StringUtils.isBlank(location) || location.split(",").length != 2) {
				throw new RuntimeException("地址不在店铺所在服务区");
			}

			String[] split = location.split(",");
			double longitude = Double.parseDouble(split[0]);// 经度
			double latitude = Double.parseDouble(split[1]);// 纬度
			
			
			if (checkShopArea(longitude, latitude, shopId)) {
				deliveryAddress.setAreaCode(code);
				deliveryAddress.setName(name);
				deliveryAddress.setPhone(mobile);
				deliveryAddress.setZipCode(0);
				deliveryAddress.setAddress(address);
				deliveryAddress.setShopId(shopId);
				deliveryAddress.setCodeAddress(areaManager.getAreaAddress(code));
				return update(deliveryAddress);
			}
			throw new RuntimeException("地址不在店铺所在服务区");
		}
		throw new RuntimeException("不存在id=" + id + "对应的地址");
	}

	@Override
	public boolean setDefaultLinkman(long id) {
		DeliveryAddress deliveryAddress = getLinkman(id);
		boolean updated = false;
		// 设置默认联系人
		if (null != deliveryAddress) {
			List<DeliveryAddress> addressList = getAllLinkman(deliveryAddress.getUserId());
			for (DeliveryAddress address : addressList) {
				if (id != address.getId().longValue()) {
					address.setStatus(Status.VALID.getValue());
					update(address);
				}
			}
			deliveryAddress.setStatus(Status.INIT.getValue());
			updated = update(deliveryAddress);
		}
		return updated;
	}

	@Override
	public DeliveryAddress getLinkman(long id) {
		DeliveryAddress deliveryAddress = null;
		try {
			deliveryAddress = deliveryAddressDAO.selectByPrimaryKey(id);
		} catch (DataAccessException e) {
			log.error("select linkman to DeliverAddress failed.", e);
		}
		return deliveryAddress;
	}

	@Override
	public String getLinkmanAddress(long id) {
		DeliveryAddress deliveryAddress = getLinkman(id);
		if (null != deliveryAddress) {
			return deliveryAddress.getCodeAddress() + deliveryAddress.getAddress();
		}
		return null;
	}

	@Override
	public List<DeliveryAddress> getAllLinkman(long userId) {
		DeliveryAddressExample example = new DeliveryAddressExample();
		example.createCriteria().andUserIdEqualTo(userId).andStatusNotEqualTo(Status.INVALID.getValue());
		example.setOrderByClause(" id desc ");
		List<DeliveryAddress> addressList = null;
		try {
			addressList = deliveryAddressDAO.selectByExample(example);
		} catch (DataAccessException e) {
			log.error("select linkmen to DeliverAddress failed.", e);
		}
		return addressList;
	}

	@Override
	public DeliveryAddress getDefaultLinkman(long userId) {
		DeliveryAddressExample example = new DeliveryAddressExample();
		example.createCriteria().andUserIdEqualTo(userId).andStatusEqualTo(Status.INIT.getValue());
		List<DeliveryAddress> addressList = null;
		try {
			addressList = deliveryAddressDAO.selectByExample(example);
		} catch (DataAccessException e) {
			log.error("select linkman to DeliverAddress failed.", e);
		}
		if (CollectionUtils.isNotEmpty(addressList)) {
			return addressList.get(0);
		}
		return null;
	}

	private boolean update(DeliveryAddress deliveryAddress) {
		Date now = new Date();
		deliveryAddress.setGmtModify(now);
		try {
			return deliveryAddressDAO.updateByPrimaryKey(deliveryAddress) > 0;
		} catch (DataAccessException e) {
			log.error("delete linkman to DeliverAddress failed.", e);
		}
		return false;

	}

	private long insert(DeliveryAddress deliveryAddress) {
		Date now = new Date();
		deliveryAddress.setGmtCreate(now);
		deliveryAddress.setGmtModify(now);
		deliveryAddress.setStatus((short) 1);
		try {
			int result = deliveryAddressDAO.insert(deliveryAddress);
			if (result > 0) {
				setDefaultLinkman(result);
				return deliveryAddress.getId();
			}
		} catch (DataAccessException e) {
			log.error("insert linkman to DeliverAddress failed.", e);
		}
		return 0L;
	}

	@SuppressWarnings("deprecation")
	private boolean checkShopArea(String areaCode, String address, int shopId) {
		try {
			Result<OrganizationDTO> result = organizationService.queryOrganization(shopId);
			if ((null != result) && result.getSucccess()) {
				OrganizationDTO org = result.getModule();
				double lat = Double.parseDouble(org.getPoiY());// 纬度
				double lng = Double.parseDouble(org.getPoiX());// 经度
				String codeAddress = areaManager.getAreaAddress(areaCode);
				if (null == codeAddress) {
					throw new RuntimeException("无法获取行政区划编码:" + areaCode + "对应的地址");
				}
				String json = mapService.geocoder(codeAddress + address);
				String location = getLocation(areaCode, json);
				if (null != location) {
					String[] locations = location.split(",");
					String lngStr = locations[0];
					String latStr = locations[1];
					if ((null != lngStr) && (null != latStr)) {
						double distances = mapService.getDistance(lat, lng, Double.parseDouble(latStr), Double.parseDouble(lngStr));
						if (distances <= 3.0) {
							return true;
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("check Shop Area excepiton", e);
		}
		return false;
	}

	private boolean checkShopArea(double longitude, double latitude, int shopId) {
		try {
			Result<OrganizationDTO> result = organizationService.queryOrganization(shopId);
			if ((null != result) && result.getSuccess()) {
				OrganizationDTO org = result.getModule();
				double distances = mapService.getDistance(Double.parseDouble(org.getPoiY()), Double.parseDouble(org.getPoiX()), latitude, longitude);
				if (distances <= 3.0) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error("check Shop Area excepiton", e);
		}
		return false;
	}

	private String getLocation(String areaCode, String json) {
		if (StringUtils.isNotEmpty(json)) {
			try {
				String countStr = JsonConvert.getValue(json, "count");
				int count = Integer.parseInt(countStr);
				for (int i = 0; i < count; i++) {
					String code = JsonConvert.getValue(json, "geocodes[" + i + "]" + ".adcode");
					if (areaCode.startsWith(code)) {
						return JsonConvert.getValue(json, "geocodes[" + i + "]" + ".location");
					}
				}
			} catch (Exception e) {

			}
		}
		return null;
	}
}
