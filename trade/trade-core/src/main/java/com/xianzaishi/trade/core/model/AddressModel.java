package com.xianzaishi.trade.core.model;

import java.util.List;

import com.xianzaishi.trade.dal.model.Area;

public class AddressModel {
	
	private List<Area> level1Area;
	
	private List<Area> level2Area;
	
	private List<Area> level3Area;
	
	private List<Area> level4Area;

	public List<Area> getLevel1Area() {
		return level1Area;
	}

	public void setLevel1Area(List<Area> level1Area) {
		this.level1Area = level1Area;
	}

	public List<Area> getLevel2Area() {
		return level2Area;
	}

	public void setLevel2Area(List<Area> level2Area) {
		this.level2Area = level2Area;
	}

	public List<Area> getLevel3Area() {
		return level3Area;
	}

	public void setLevel3Area(List<Area> level3Area) {
		this.level3Area = level3Area;
	}

	public List<Area> getLevel4Area() {
		return level4Area;
	}

	public void setLevel4Area(List<Area> level4Area) {
		this.level4Area = level4Area;
	}
}
