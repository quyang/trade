package com.xianzaishi.trade.web.vo;

import java.io.Serializable;

import com.xianzaishi.trade.client.vo.SkuInfoVO;

/**
 * 购物车商品实例
 * 
 * @author Croky.Zheng 2016年7月18日
 */
public class ShoppingCarItemVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7330200713687133941L;

    /**
     * ID
     */
    private long id;

    /**
     * 商品名称
     */
    private String itemName;

    /**
     * 商品图标URL
     */
    private String itemIconUrl;

    /**
     * 商品账单价格
     */
    private String itemPayPrice;

    /**
     * 购买数量
     */
    private String itemCount;

    /**
     * SKU信息
     */
    private SkuInfoVO skuInfo;

    /**
     * SKU ID
     */
    private long skuId;

    /**
     * 商品ID
     */
    private long itemId;

    /**
     * 商品实际价格
     */
    private String itemEffePrice;

    /**
     * 商品总价
     */
    private String payAmount;

    /**
     * 商品实际总价
     */
    private String effeAmount;

    /**
     * 商品库存
     */
    private String number;

    private String itemTags;


    /**
     * 计算结果
     */
    private DiscountInfoVO discountInfo;

    /*
     * public void addSkuInfo(SkuVO sku) { if (null == skuInfo) { skuInfo = new
     * ArrayList<SkuVO>(); } skuInfo.add(sku); }
     */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DiscountInfoVO getDiscountInfo() {
        return discountInfo;
    }

    public void setDiscountInfo(DiscountInfoVO discountInfo) {
        this.discountInfo = discountInfo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public SkuInfoVO getSkuInfo() {
        return skuInfo;
    }

    public void setSkuInfo(SkuInfoVO skuInfo) {
        this.skuInfo = skuInfo;
    }

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }

    public String getItemIconUrl() {
        return itemIconUrl;
    }

    public void setItemIconUrl(String itemIconUrl) {
        this.itemIconUrl = itemIconUrl;
    }

    public String getItemPayPrice() {
        return itemPayPrice;
    }

    public void setItemPayPrice(String itemPayPrice) {
        this.itemPayPrice = itemPayPrice;
    }

    public String getItemCount() {
        return itemCount;
    }

    public void setItemCount(String itemCount) {
        this.itemCount = itemCount;
    }

    public String getItemEffePrice() {
        return itemEffePrice;
    }

    public void setItemEffePrice(String itemEffePrice) {
        this.itemEffePrice = itemEffePrice;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getEffeAmount() {
        return effeAmount;
    }

    public void setEffeAmount(String effeAmount) {
        this.effeAmount = effeAmount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getItemTags() {
        return itemTags;
    }

    public void setItemTags(String itemTags) {
        this.itemTags = itemTags;
    }

}