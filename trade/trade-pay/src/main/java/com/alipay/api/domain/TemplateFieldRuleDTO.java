package com.alipay.api.domain;

import com.alipay.api.AlipayObject;
import com.alipay.api.internal.mapping.ApiField;

/**
 * 模板字段规则
 *
 * @author auto create
 * @since 1.0, 2016-10-10 10:11:17
 */
public class TemplateFieldRuleDTO extends AlipayObject {

	private static final long serialVersionUID = 8765745599377271737L;

	/**
	 * 字段名称，现在支持如下几个Key（暂不支持自定义）
Balance：金额
Point：整数
Level：任意字符串
OpenDate：开卡日期
ValidDate：过期日期
	 */
	@ApiField("field_name")
	private String fieldName;

	/**
	 * 规则名
ASSIGN_FROM_REQUEST:  以rule_value为key值，从开卡请求中取指定值
DATE_IN_FUTURE: 生成一个未来的日期（格式YYYY-MM-DD)
CONST: 常量
	 */
	@ApiField("rule_name")
	private String ruleName;

	/**
	 * 根据rule_name，采取相应取值策略
CONST：直接取rule_value作为卡属性值
DATE_IN_FUTURE：10m或10d 分别表示10个月或10天
ASSIGN_FROM_REQUEST：在开卡Reuqest请求中按rule_value取值，现在和field_name对应的为（OpenDate、ValidDate、Level、Point、Balance）
	 */
	@ApiField("rule_value")
	private String ruleValue;

	public String getFieldName() {
		return this.fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getRuleName() {
		return this.ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleValue() {
		return this.ruleValue;
	}
	public void setRuleValue(String ruleValue) {
		this.ruleValue = ruleValue;
	}

}
