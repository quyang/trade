package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.Area;
import com.xianzaishi.trade.dal.model.AreaExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface AreaMapper {
    @SelectProvider(type=AreaSqlProvider.class, method="countByExample")
    int countByExample(AreaExample example);

    @DeleteProvider(type=AreaSqlProvider.class, method="deleteByExample")
    int deleteByExample(AreaExample example);

    @Delete({
        "delete from area",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into area (code, parent_code, ",
        "name, short_name, ",
        "longitude, latitude, level, ",
        "sort, status, gmt_create, ",
        "gmt_modify)",
        "values (#{code,jdbcType=VARCHAR}, #{parentCode,jdbcType=VARCHAR}, ",
        "#{name,jdbcType=VARCHAR}, #{shortName,jdbcType=VARCHAR}, ",
        "#{longitude,jdbcType=REAL}, #{latitude,jdbcType=REAL}, #{level,jdbcType=INTEGER}, ",
        "#{sort,jdbcType=INTEGER}, #{status,jdbcType=INTEGER}, #{gmtCreate,jdbcType=TIMESTAMP}, ",
        "#{gmtModify,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insert(Area record);

    @InsertProvider(type=AreaSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insertSelective(Area record);

    @SelectProvider(type=AreaSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="code", property="code", jdbcType=JdbcType.VARCHAR),
        @Result(column="parent_code", property="parentCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="short_name", property="shortName", jdbcType=JdbcType.VARCHAR),
        @Result(column="longitude", property="longitude", jdbcType=JdbcType.REAL),
        @Result(column="latitude", property="latitude", jdbcType=JdbcType.REAL),
        @Result(column="level", property="level", jdbcType=JdbcType.INTEGER),
        @Result(column="sort", property="sort", jdbcType=JdbcType.INTEGER),
        @Result(column="status", property="status", jdbcType=JdbcType.INTEGER),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    List<Area> selectByExample(AreaExample example);

    @Select({
        "select",
        "id, code, parent_code, name, short_name, longitude, latitude, level, sort, status, ",
        "gmt_create, gmt_modify",
        "from area",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="code", property="code", jdbcType=JdbcType.VARCHAR),
        @Result(column="parent_code", property="parentCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="short_name", property="shortName", jdbcType=JdbcType.VARCHAR),
        @Result(column="longitude", property="longitude", jdbcType=JdbcType.REAL),
        @Result(column="latitude", property="latitude", jdbcType=JdbcType.REAL),
        @Result(column="level", property="level", jdbcType=JdbcType.INTEGER),
        @Result(column="sort", property="sort", jdbcType=JdbcType.INTEGER),
        @Result(column="status", property="status", jdbcType=JdbcType.INTEGER),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    Area selectByPrimaryKey(Integer id);

    @UpdateProvider(type=AreaSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Area record, @Param("example") AreaExample example);

    @UpdateProvider(type=AreaSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Area record, @Param("example") AreaExample example);

    @UpdateProvider(type=AreaSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Area record);

    @Update({
        "update area",
        "set code = #{code,jdbcType=VARCHAR},",
          "parent_code = #{parentCode,jdbcType=VARCHAR},",
          "name = #{name,jdbcType=VARCHAR},",
          "short_name = #{shortName,jdbcType=VARCHAR},",
          "longitude = #{longitude,jdbcType=REAL},",
          "latitude = #{latitude,jdbcType=REAL},",
          "level = #{level,jdbcType=INTEGER},",
          "sort = #{sort,jdbcType=INTEGER},",
          "status = #{status,jdbcType=INTEGER},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Area record);
}