package com.xianzaishi.trade.utils;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class CollectionTest {

	@Test
	public void testList() {
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		for (int a : list) {
			if (a < 5) {
				list.remove(a);
			}
		}
		System.out.println(list.size());
	}
}
