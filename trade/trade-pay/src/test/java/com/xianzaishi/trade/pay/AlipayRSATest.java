package com.xianzaishi.trade.pay;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.junit.Test;

import com.alipay.config.AlipayConfigV2;
import com.alipay.sign.RSA;

public class AlipayRSATest {
	private final static String appId = "2016063001569813";
	private final static String pid ="2088421284384567";
	private final static String sellerId = "pay@xianzaishi.com";
	private final static String app_private_key = "MIICXQIBAAKBgQCwcTbif+c+wzbd9laG5nCFHR+FD9yY1bew9ZW6r4j/s9Mh4JByfPOtiLvi08avZi3jbC6zxGE4aQDjrLpPQOH9hq53h8mpUGwcuHiZsSOgiAXdYy1tpeG/N6nALK/fvzURKAcoQUnKeEgLNs1iOTMMDn9CqsuXyNjfx1BfvlAQgwIDAQABAoGBAKUpZSoCdlFe5SZDKF3gqeVBDLc+0M1UCT4htQXquMA68WEd17kD1ApWGyJKAQtBB6WCJ/lo02S9jfKRRllXr/JWzrLq1Debe0P39D7cWxmDWt4x6MhOPC5aINfrb72zgSnNfUqH9dZgrn6g4MXPItzlNbIbTajoLbkm8lF2P6LpAkEA36U070jyhnPZ7gEadYzeO7JSM9HjClZTHzHcxB7fHH/GmcbeOX6zHRPX87nvseSt5VSwoxPFcYMuAxA3RupbdQJBAMn30og/mvXoWxS2K/vBQ/nTKvxzkp+y6bSLoFbP0VztmJRSiq5xrOFkETaNGXgXwX+VBq10BsePcWQw1UrUVRcCQE8echCpHOuF9rYle8fUUxaJal1cxlZ03akuiax0Q3ggmBD08s8iTJlf2MknoW2sufxkrqyypOoYf2GkDfovlLUCQCoIKElwq0g4BhSGYRrwurvYRZ7qUn5n1plbYZAPievECrf7gZ8SSz9Q+wAWV1GV6BAsLIqWlf6cDKYkP49mJxsCQQDNYmbNi8sASVsqBbcHJEF+32Sr8fGtLjzrt0HnldKuc6n/cALEzzgXbfwwZ3UIej/j6plxtZL9V6afIOLs8Klw/6FTFY99uhpiq0qadD/uSzQsefWo0aTvP/65zi3eof7TcZ32oWpwIDAQAB";
	private final static String app_private_pkcs8_key = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBANXenWCCjvXwdDKEQNsfJgouLggj/Cx87LfSJ0MyL7UQH+atx8ShXgRVj4OZL6I/QYynzS5iihrCD5jo0dN84FG+SlN2/g7x8iaYJ+kqq4Gf6BWs2I7IZgRkzRaq1nhB1+nFtWDU/NbgJYFkgoKEijVl0hox47ksWk0LVeWdLvtjAgMBAAECgYEAzRTXXMBVTkjxY6+mcXiBBaoawfufyRlR9UM0Gx941+tKa+2gblE+0nEpWUv/fVmjBbmy6xPa0qXcRwiajG5murqMpbUkHdkq6zkNDmxSV1JrzwuYHAG3CCgDZT2kdNMEDF1njscQNx73YHW4pTafGryabwVdNhTytriw3J98NOECQQD3LsPjiLzCcvC5oM6VHYXHdYTC2udi/Dk1Ly8WftxfWpSE1vyvs2r4qWl+EZ1Lif9Hi1MXYHKjWVSyMyoLrjYdAkEA3X+jtWAoqZIgZLoCagNaaiN3rTM/XaUQll7fgnGzzpMKIXQecbYUU1v2mzz8bcKx3/Ips4giLHs+dw3BrWU/fwJBAJZ3Wzsow27CtRLqdpaC8CqouPY8dtnkm5ZqcImLE+7fnsT2cb8qwpU320Wox01yZXlRsHTsexxAhrQrPQ77L2kCQDIQcJNFactyIOpDdNo7actFuv4l8DOdZJNoXEKiqo6Ng6OuGFeBXTS+O445CaFReVzx4mUW5wqAzMyiCl3D3ccCQQDrfk2sUPU1KvjdpKhluFDws8tCvfGrhDDVM+FtWYFpnEW0MibOjEh+N/c+Bveyjs4bK3CVySPxbY6H5NGOQrmY";
	private final static String alipay_public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";
	
	@Test
	public void rsa() {
		String str  = "partner=\"2088421284384567\"&seller_id=\"pay@xianzaishi.com\"&out_trade_no=\"10000\"&subject=\"测试商品\"&body=\"无描述\"&total_fee=\"0.01\"&notify_url=\"http://ceshi.fenqilema.com/index.php/admin/merchant/notity_alipay\"&service=\"mobile.securitypay.pay\"&payment_type=\"1\"&_input_charset=\"utf-8\"&it_b_pay=\"30m\"";
		String rsa_sign = "";
		try {
			//AlipayConfig.private_key
			rsa_sign = URLEncoder.encode(RSA.sign(str, app_private_pkcs8_key, AlipayConfigV2.input_charset),AlipayConfigV2.input_charset);
			System.out.println(rsa_sign);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
