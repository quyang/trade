package com.xianzaishi.trade.pay.weixin;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Croky.Zheng 2016年9月16日
 */
public class WeiXinPayHttpsRequest {

	private static ByteArrayInputStream bis = new ByteArrayInputStream(WeiXinPayConfig.PKCS12);
	// 表示请求器是否已经做了初始化工作

	// 连接超时时间，默认10秒
	private static int socketTimeout = 10000;

	// 传输超时时间，默认30秒
	private static int connectTimeout = 30000;

	// 请求器的配置
	private static RequestConfig requestConfig;

	// HTTP请求器
	private static CloseableHttpClient httpClient;

	private final static Logger logger = LoggerFactory.getLogger(WeiXinPayHttpsRequest.class);

	/*
	 * public HttpsRequest() throws UnrecoverableKeyException,
	 * KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
	 * IOException { init(); }
	 */
	static {
		try {
			// bis.setBuf(keyBytes);
			init();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void init() throws IOException, KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyManagementException {

		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		// FileInputStream is = new
		// FileInputStream("/Users/croky/workspaces/trade/trade-pay/src/main/resources/apiclient_cert.p12");//加载本地的证书进行https加密传输
		// 加上"/"则取com同目录文件
		// InputStream is =
		// WeiXinPayHttpsRequest.class.getResourceAsStream("/apiclient_cert.p12");
		try {
			keyStore.load(bis, WeiXinPayConfig.MCHID.toCharArray());// 设置证书密码
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} finally {
			bis.close();
		}

		// Trust own CA and all self-signed certs
		SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, WeiXinPayConfig.MCHID.toCharArray()).build();
		// Allow TLSv1 protocol only
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1" }, null,
				SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);

		httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

		// 根据默认超时限制初始化requestConfig
		requestConfig = RequestConfig.custom().setSocketTimeout(socketTimeout).setConnectTimeout(connectTimeout).build();
	}

	/**
	 * 通过Https往API post xml数据
	 *
	 * @param url
	 *            API地址
	 * @param xmlObj
	 *            要提交的XML数据对象
	 * @return API回包的实际数据
	 * @throws IOException
	 * @throws KeyStoreException
	 * @throws UnrecoverableKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public static String unifiedOrder(String content)
			throws IOException, KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyManagementException {
		return post("https://api.mch.weixin.qq.com/pay/unifiedorder", content);
	}

	/**
	 * 发送退款请求
	 * 
	 * @param content
	 *            XML文本
	 * @return 退款返回的数据
	 * @throws IOException
	 * @throws KeyStoreException
	 * @throws UnrecoverableKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public static String refund(String content)
			throws IOException, KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyManagementException {
		return post("https://api.mch.weixin.qq.com/secapi/pay/refund", content);
	}

	private static String post(String url, String content) {
		String result = null;
		HttpPost httpPost = new HttpPost(url);
		StringEntity postEntity = new StringEntity(content, "UTF-8");
		httpPost.addHeader("Content-Type", "text/xml");
		httpPost.setEntity(postEntity);
		httpPost.setConfig(requestConfig);
		try {
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			result = EntityUtils.toString(entity, "UTF-8");
		} catch (Exception e) {
			logger.error(String.format("Fail to post url[%s], content[%s]", url, content), e);
		} finally {
			httpPost.abort();
		}
		return result;
	}

}
