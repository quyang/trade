package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.ShoppingCart;
import com.xianzaishi.trade.dal.model.ShoppingCartExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

/*
 * 废弃，新逻辑  com.xianzaishi.trade.dal.daonew.ShoppingCartSqlMapper
 */
@Deprecated
public interface ShoppingCartMapper {
    @SelectProvider(type=ShoppingCartSqlProvider.class, method="countByExample")
    int countByExample(ShoppingCartExample example);

    @DeleteProvider(type=ShoppingCartSqlProvider.class, method="deleteByExample")
    int deleteByExample(ShoppingCartExample example);

    @Delete({
        "delete from shopping_cart",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into shopping_cart (user_id, item_id, ",
        "item_sku_id, item_sku_info, ",
        "item_name, item_icon_url, ",
        "item_bill_price, item_effe_price, ",
        "item_count, attribute, ",
        "bill_amount, effe_amount, ",
        "device_id, operator, ",
        "status, gmt_create, ",
        "gmt_modify, item_category_id)",
        "values (#{userId,jdbcType=BIGINT}, #{itemId,jdbcType=BIGINT}, ",
        "#{itemSkuId,jdbcType=BIGINT}, #{itemSkuInfo,jdbcType=VARCHAR}, ",
        "#{itemName,jdbcType=VARCHAR}, #{itemIconUrl,jdbcType=VARCHAR}, ",
        "#{itemBillPrice,jdbcType=DOUBLE}, #{itemEffePrice,jdbcType=DOUBLE}, ",
        "#{itemCount,jdbcType=DOUBLE}, #{attribute,jdbcType=VARCHAR}, ",
        "#{billAmount,jdbcType=DOUBLE}, #{effeAmount,jdbcType=DOUBLE}, ",
        "#{deviceId,jdbcType=INTEGER}, #{operator,jdbcType=INTEGER}, ",
        "#{status,jdbcType=SMALLINT}, #{gmtCreate,jdbcType=TIMESTAMP}, ",
        "#{gmtModify,jdbcType=TIMESTAMP}, #{itemCategoryId,jdbcType=INTEGER})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(ShoppingCart record);

    @InsertProvider(type=ShoppingCartSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insertSelective(ShoppingCart record);

    @SelectProvider(type=ShoppingCartSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_id", property="itemId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_sku_id", property="itemSkuId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_sku_info", property="itemSkuInfo", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_name", property="itemName", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_icon_url", property="itemIconUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_bill_price", property="itemBillPrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_effe_price", property="itemEffePrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_count", property="itemCount", jdbcType=JdbcType.DOUBLE),
        @Result(column="attribute", property="attribute", jdbcType=JdbcType.VARCHAR),
        @Result(column="bill_amount", property="billAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="effe_amount", property="effeAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="device_id", property="deviceId", jdbcType=JdbcType.INTEGER),
        @Result(column="operator", property="operator", jdbcType=JdbcType.INTEGER),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="item_category_id", property="itemCategoryId", jdbcType=JdbcType.INTEGER)
    })
    List<ShoppingCart> selectByExample(ShoppingCartExample example);

    @Select({
        "select",
        "id, user_id, item_id, item_sku_id, item_sku_info, item_name, item_icon_url, ",
        "item_bill_price, item_effe_price, item_count, attribute, bill_amount, effe_amount, ",
        "device_id, operator, status, gmt_create, gmt_modify, item_category_id",
        "from shopping_cart",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_id", property="itemId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_sku_id", property="itemSkuId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_sku_info", property="itemSkuInfo", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_name", property="itemName", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_icon_url", property="itemIconUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_bill_price", property="itemBillPrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_effe_price", property="itemEffePrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_count", property="itemCount", jdbcType=JdbcType.DOUBLE),
        @Result(column="attribute", property="attribute", jdbcType=JdbcType.VARCHAR),
        @Result(column="bill_amount", property="billAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="effe_amount", property="effeAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="device_id", property="deviceId", jdbcType=JdbcType.INTEGER),
        @Result(column="operator", property="operator", jdbcType=JdbcType.INTEGER),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="item_category_id", property="itemCategoryId", jdbcType=JdbcType.INTEGER)
    })
    ShoppingCart selectByPrimaryKey(Long id);

    @UpdateProvider(type=ShoppingCartSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") ShoppingCart record, @Param("example") ShoppingCartExample example);

    @UpdateProvider(type=ShoppingCartSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") ShoppingCart record, @Param("example") ShoppingCartExample example);

    @UpdateProvider(type=ShoppingCartSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(ShoppingCart record);

    @Update({
        "update shopping_cart",
        "set user_id = #{userId,jdbcType=BIGINT},",
          "item_id = #{itemId,jdbcType=BIGINT},",
          "item_sku_id = #{itemSkuId,jdbcType=BIGINT},",
          "item_sku_info = #{itemSkuInfo,jdbcType=VARCHAR},",
          "item_name = #{itemName,jdbcType=VARCHAR},",
          "item_icon_url = #{itemIconUrl,jdbcType=VARCHAR},",
          "item_bill_price = #{itemBillPrice,jdbcType=DOUBLE},",
          "item_effe_price = #{itemEffePrice,jdbcType=DOUBLE},",
          "item_count = #{itemCount,jdbcType=DOUBLE},",
          "attribute = #{attribute,jdbcType=VARCHAR},",
          "bill_amount = #{billAmount,jdbcType=DOUBLE},",
          "effe_amount = #{effeAmount,jdbcType=DOUBLE},",
          "device_id = #{deviceId,jdbcType=INTEGER},",
          "operator = #{operator,jdbcType=INTEGER},",
          "status = #{status,jdbcType=SMALLINT},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP},",
          "item_category_id = #{itemCategoryId,jdbcType=INTEGER}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(ShoppingCart record);
}