package com.xianzaishi.trade.web.controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
	private static final Logger log = LoggerFactory.getLogger(IndexController.class);

	// required=true会要求用户强制输入，没有参数则会直接返回400错误
	@RequestMapping(value = "/login.htm", method = { RequestMethod.GET })
	public ModelAndView login(@RequestParam(required = false) String account,
			@RequestParam(required = false) String password, @RequestParam(required = false) String url,
			HttpSession session) throws FileNotFoundException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("login.htm?account=" + account + ",password=" + password + ",url=" + url);
		}
		ModelAndView modelAndView = new ModelAndView();
		return modelAndView;
	}

	@RequestMapping(value = "/logout.htm", method = { RequestMethod.GET })
	public String logout(HttpSession session) {
		//session.setAttribute(Constants.LOGIN_MEMBER_KEY, null);
		return "login";
	}
 
	@RequestMapping("/index.htm")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("index");
		return modelAndView;
	}
}
