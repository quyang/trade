package com.xianzaishi.trade.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.croky.util.StringUtils;
import com.xianzaishi.trade.core.address.AreaManager;
import com.xianzaishi.trade.core.address.DeliveryAddressManager;
import com.xianzaishi.trade.core.address.ShopServiceAreaManager;
import com.xianzaishi.trade.dal.model.Area;
import com.xianzaishi.trade.dal.model.DeliveryAddress;
import com.xianzaishi.trade.utils.enums.Status;
import com.xianzaishi.trade.web.model.ResponseModel;
import com.xianzaishi.trade.web.utils.ConstantUtils;
import com.xianzaishi.trade.web.vo.AreaVO;
import com.xianzaishi.trade.web.vo.DeliveryAddressVO;

@Controller
public class DeliveryAddressController {

	@Autowired
	private DeliveryAddressManager deliveryAddressManager;

	@Autowired
	private AreaManager areaManager;

	@Autowired
	private ShopServiceAreaManager shopServiceAreaManager;

	/**
	 * 获取四级行政区划的一级地名
	 * 
	 * @return 一级地名的code和name
	 */
	@RequestMapping(value = "/da/getRootArea.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getRootArea() {
		List<Area> areaList = areaManager.getRootArea();
		List<AreaVO> areaVOList = this.area2VO(areaList);
		if (CollectionUtils.isEmpty(areaVOList)) {
			return ConstantUtils.errorReponse("获取地址信息失败");
		}
		return ConstantUtils.normalResponse(areaVOList);
	}

	/**
	 * 获取一个区域的所有子区域，遵守四级行政区划规则
	 * 
	 * @param code
	 *            父区域编码
	 * @return 子区域的code和name
	 * @see AreaVO
	 */
	@RequestMapping(value = "/da/getChildArea.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getChildArea(@RequestParam String code) {
		List<Area> areaList = areaManager.getChildArea(code);
		List<AreaVO> areaVOList = this.area2VO(areaList);
		if (CollectionUtils.isEmpty(areaVOList)) {
			return ConstantUtils.errorReponse("获取地址信息失败");
		}
		return ConstantUtils.normalResponse(areaVOList);
	}

	/**
	 * 获取一个区域的信息
	 * 
	 * @param code
	 *            区域编码
	 * @return 区域信息,参考
	 * @see Area
	 */
	@RequestMapping(value = "/da/getArea.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getArea(@RequestParam String code) {
		Area area = areaManager.getArea(code);
		if (null == area) {
			return ConstantUtils.errorReponse("获取地址信息失败");
		}
		return ConstantUtils.normalResponse(area);
	}

	/**
	 * 获取店铺的服务区域列表
	 * 
	 * @return 所有的店铺服务区域列表
	 */
	@RequestMapping(value = "/da/getShopServiceArea.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getShopServiceArea(@RequestParam(value = "shop_id", defaultValue = "1", required = false) int shopId) {
		try {
			return ConstantUtils.normalResponse(shopServiceAreaManager.getShopServiceAreaList());
		} catch (Exception e) {
			return ConstantUtils.errorReponse("获取店铺服务区域信息失败:" + e.getMessage());
		}
	}

	/**
	 * 获取店铺的区域详细信息
	 * 
	 * @param shopId
	 *            店铺ID
	 * @return 某个店铺的详细信息，包括所有服务区域
	 */
	@RequestMapping(value = "/da/getShopServiceAreaDetail.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getShopServiceAreaDetail(@RequestParam(value = "shop_id", defaultValue = "0", required = true) int shopId) {
		try {
			return ConstantUtils.normalResponse(shopServiceAreaManager.getShopServiceAreaDetail(shopId));
		} catch (Exception e) {
			return ConstantUtils.errorReponse("获取店铺服务区域信息失败:" + e.getMessage());
		}
	}

	/**
	 * 添加收货人
	 * 
	 * @param uid
	 *            用户ID
	 * @param code
	 *            四级行政区划代码
	 * @param address
	 *            详细地址
	 * @param phone
	 *            电话号码（手机号码）
	 * @param name
	 *            名称
	 * @return 返回收货地址ID
	 */
	@RequestMapping(value = "/da/addLinkman.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel addLinkman(@RequestParam Long uid, @RequestParam String code, @RequestParam String address,
			@RequestParam String phone, @RequestParam String name) {
		Area area = areaManager.getArea(code);
		if (null == area) {
			return ConstantUtils.errorReponse("获取code:" + code + "对应的地址失败!");
		}

		// 检查地址

		if (StringUtils.isBlank(address)) {
			return ConstantUtils.errorReponse("地址不能为空");
		}

		if (StringUtils.isBlank(name)) {
			return ConstantUtils.errorReponse("联系人名称不能为空");
		}

		if (StringUtils.isBlank(phone)) {
			return ConstantUtils.errorReponse("必须要有联系人电话号码");
		}
		try {
			long addressId = deliveryAddressManager.addLinkman(uid, name, code, address, phone, 1);
			if (addressId > 0) {
				return ConstantUtils.normalResponse(addressId);
			} else {
				return ConstantUtils.errorReponse("添加联系人失败!");
			}
		} catch (Exception e) {
			return ConstantUtils.errorReponse("添加联系人失败:" + e.getMessage());
		}

	}

	/**
	 * 删除收货地址
	 * 
	 * @param uid
	 *            用户ID
	 * @param id
	 *            地址ID
	 * @return nothing
	 */
	@RequestMapping(value = "/da/delLinkman.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel delLinkman(@RequestParam Long uid, @RequestParam Long id) {
		if (!check(uid, id)) {
			return ConstantUtils.errorReponse("删除联系人信息失败,该联系人不属于用户");
		}
		if (deliveryAddressManager.deleteLinkman(id)) {
			return ConstantUtils.normalResponse();
		} else {
			return ConstantUtils.errorReponse("删除联系人信息失败");
		}
	}

	/**
	 * 修改收货人地址
	 * 
	 * @param uid
	 *            用户ID
	 * @param id
	 *            地址ID
	 * @param code
	 *            四级行政区划代码
	 * @param address
	 *            详细地址
	 * @param phone
	 *            电话号码（手机号码）
	 * @param name
	 *            名称
	 * @return 返回收货地址ID
	 */
	@RequestMapping(value = "/da/modifyLinkman.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel modifyLinkman(@RequestParam Long uid, @RequestParam Long id, @RequestParam String code,
			@RequestParam String address, @RequestParam String phone, @RequestParam String name) {

		if (!check(uid, id)) {
			return ConstantUtils.errorReponse("修改联系人信息失败,该联系人不属于用户");
		}
		try {
			ResponseModel del = delLinkman(uid, id);
			ResponseModel add = addLinkman(uid, code, address, phone, name);
			if (del.isSuccess() && add.isSuccess()) {
				return ConstantUtils.normalResponse();
			}
			/*
			 * if (deliveryAddressManager.updateLinkman(id, name, phone, code,
			 * address,1)) { return ConstantUtils.normalResponse(); }
			 */
		} catch (Exception e) {
			return ConstantUtils.errorReponse("修改联系人信息失败:" + e.getMessage());
		}
		return ConstantUtils.errorReponse("修改联系人信息失败");
	}

	/**
	 * 设置默认的收货地址
	 * 
	 * @param uid
	 *            用户ID
	 * @param id
	 *            地址ID
	 * @return nothing
	 */
	@RequestMapping(value = "/da/setDefaultAddress.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel setDefaultAddress(@RequestParam Long uid, @RequestParam Long id) {
		if (!check(uid, id)) {
			return ConstantUtils.errorReponse("设置默认收货地址失败,该地址不属于用户");
		}
		if (deliveryAddressManager.setDefaultLinkman(id)) {
			return ConstantUtils.normalResponse();
		}
		return ConstantUtils.errorReponse("设置默认收货地址失败");
	}

	/**
	 * 获取ID获取联系人信息
	 * 
	 * @param uid
	 *            用户ID
	 * @param id
	 *            联系人ID
	 * @return nothing
	 * @see DeliveryAddressVO
	 */
	@RequestMapping(value = "/da/getAddressById.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getAddressById(@RequestParam Long uid, @RequestParam Long id) {
		if (!check(uid, id)) {
			return ConstantUtils.errorReponse("获取联系人信息失败,该联系人不属于用户");
		}
		DeliveryAddress address = deliveryAddressManager.getLinkman(id);
		if (null != address) {
			return ConstantUtils.normalResponse(this.deliveryAddress2VO(address));
		} else {
			return ConstantUtils.normalResponse();// errorReponse("没有id=" + id +
													// "的联系人信息");
		}
	}

	/**
	 * 获取用户的默认收货地址
	 * 
	 * @param uid
	 *            用户ID
	 * @return nothing
	 * @see DeliveryAddressVO
	 */
	@RequestMapping(value = "/da/getDefaultAddress.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getDefaultAddress(@RequestParam Long uid) {
		DeliveryAddress address = deliveryAddressManager.getDefaultLinkman(uid);
		if (null != address) {
			return ConstantUtils.normalResponse(this.deliveryAddress2VO(address));
		} else {
			return ConstantUtils.normalResponse();// errorReponse("没有uid=" + uid
													// + "没有设置默认联系人");
		}
	}

	/**
	 * 获取用户的所有收货地址
	 * 
	 * @param uid
	 *            用户ID
	 * @return nothing
	 * @see DeliveryAddressVO
	 */
	@RequestMapping(value = "/da/getAllAddress.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getAllAddress(@RequestParam Long uid) {
		List<DeliveryAddress> addressList = deliveryAddressManager.getAllLinkman(uid);
		if (CollectionUtils.isNotEmpty(addressList)) {
			List<DeliveryAddressVO> voList = new ArrayList<DeliveryAddressVO>();
			for (DeliveryAddress address : addressList) {
				voList.add(this.deliveryAddress2VO(address));
			}
			return ConstantUtils.normalResponse(addressList);
		}
		return ConstantUtils.normalResponse();
	}

	private List<AreaVO> area2VO(List<Area> areaList) {
		if (CollectionUtils.isNotEmpty(areaList)) {
			List<AreaVO> areaVOList = new ArrayList<AreaVO>(areaList.size());
			for (Area area : areaList) {
				AreaVO areaVO = new AreaVO(area.getCode(), area.getName());
				areaVOList.add(areaVO);
			}
			return areaVOList;
		}
		return null;
	}

	private DeliveryAddressVO deliveryAddress2VO(DeliveryAddress deliveryAddress) {
		DeliveryAddressVO vo = new DeliveryAddressVO();
		vo.setAddress(deliveryAddress.getAddress());
		vo.setCode(deliveryAddress.getAreaCode());
		vo.setDefaultAddress(deliveryAddress.getStatus().shortValue() == Status.INIT.getValue());
		vo.setId(deliveryAddress.getId());
		vo.setName(deliveryAddress.getName());
		vo.setPhone(deliveryAddress.getPhone());
		String code = vo.getCode();
		Area area = areaManager.getArea(code);
		if (null != area) {
			vo.setLevel4Name(area.getName());
			area = areaManager.getArea(area.getParentCode());
		}
		if (null != area) {
			vo.setLevel3Name(area.getName());
			area = areaManager.getArea(area.getParentCode());
		}
		if (null != area) {
			vo.setLevel2Name(area.getName());
			area = areaManager.getArea(area.getParentCode());
		}
		if (null != area) {
			vo.setLevel1Name(area.getName());
			area = areaManager.getArea(area.getParentCode());
		}
		return vo;
	}

	private boolean check(long uid, long id) {
		DeliveryAddress deliveryAddress = deliveryAddressManager.getLinkman(id);
		if (null != deliveryAddress) {
			if (deliveryAddress.getUserId().longValue() == uid) {
				return true;
			}
		}
		return false;
	}
}
