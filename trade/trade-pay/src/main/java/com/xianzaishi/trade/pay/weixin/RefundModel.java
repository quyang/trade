package com.xianzaishi.trade.pay.weixin;

import java.io.Serializable;

public class RefundModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5329649384276880820L;

	/**
	 * 应用ID
	 */
	private String appid = WeiXinPayConfig.APPID ;
	
	/**
	 * 商户号
	 */
	private String mch_id = WeiXinPayConfig.MCHID;
	
	/**
	 * 设备信息
	 */
	private String device_info = WeiXinPayConfig.DEVICE_INFO;
	
	/**
	 * 随机字符串
	 */
	private String nonce_str;
	
	/**
	 * 签名
	 */
	private String sign;
	
	private String out_trade_no;
	
	/**
	 * 退款单号
	 */
	private String out_refund_no;
	
	private int total_fee;
	
	private int refund_fee;
	
	private String refund_fee_type = "CNY";
	
	private String op_user_id;
	
	/**
	 * 资金来源
	 * REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款/基本账户
	 * REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款
	 */
	private String refund_account = "REFUND_SOURCE_RECHARGE_FUNDS";

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getMch_id() {
		return mch_id;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	public String getDevice_info() {
		return device_info;
	}

	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getOut_refund_no() {
		return out_refund_no;
	}

	public void setOut_refund_no(String out_refund_no) {
		this.out_refund_no = out_refund_no;
	}

	public int getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(int total_fee) {
		this.total_fee = total_fee;
	}

	public int getRefund_fee() {
		return refund_fee;
	}

	public void setRefund_fee(int refund_fee) {
		this.refund_fee = refund_fee;
	}

	public String getRefund_fee_type() {
		return refund_fee_type;
	}

	public void setRefund_fee_type(String refund_fee_type) {
		this.refund_fee_type = refund_fee_type;
	}

	public String getOp_user_id() {
		return op_user_id;
	}

	public void setOp_user_id(String op_user_id) {
		this.op_user_id = op_user_id;
	}

	public String getRefund_account() {
		return refund_account;
	}

	public void setRefund_account(String refund_account) {
		this.refund_account = refund_account;
	}
}
