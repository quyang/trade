package com.xianzaishi.trade.client;

import java.util.Collection;
import java.util.List;

import com.xianzaishi.trade.client.vo.CouponVO;
import com.xianzaishi.trade.client.vo.UserCouponVO;

/**
 * 
 * 优惠券相关接口
 *
 * @author nianyue.hyj
 * @since 2016.10.21
 */
public interface UserCouponService {

	/**
	 * 发放优惠券组
	 * 
	 * @param userId
	 *            目标用户ID
	 * @param couponType
	 *            优惠券类型(11为新人大礼包)
	 * @param duration
	 *            优惠券有效持续时间毫秒,例如15天即为15 * 24 * 60 * 60 * 1000
	 * @return true成功 false失败
	 */
	public Result<Boolean> sendCouponPackage(long userId, short couponType, long duration);

	/**
	 * 获取所有可用优惠券
	 * 
	 * @param couponTypes
	 *            优惠券类型,不指定类型则返回所有类型优惠券
	 * @return 优惠券详情
	 */
	public Result<List<CouponVO>> getCouponByTypes(Collection<Short> couponTypes);

	/**
	 * 获取用户优惠券
	 * 
	 * @param userId
	 *            用户ID
	 * @param couponTypes
	 *            优惠券类型,不指定类型则返回所有类型优惠券
	 * @return
	 */
	public Result<List<UserCouponVO>> getUserCouponByTypes(long userId, Collection<Short> couponTypes);

	/**
	 * 发放优惠券
	 * 
	 * @param userId
	 *            目标用户ID
	 * @param couponId
	 *            优惠券ID
	 * @param duration
	 *            优惠券有效持续时间毫秒,例如15天即为15 * 24 * 60 * 60 * 1000
	 * @return true成功 false失败
	 */
	public Result<Boolean> sendCouponByCouponId(long userId, long couponId, long duration);

	/**
	 * 根据用户优惠券ID获取优惠券详情
	 * 
	 * @param couponId
	 * @return
	 */
	public Result<UserCouponVO> getUserCouponById(long couponId);
}
