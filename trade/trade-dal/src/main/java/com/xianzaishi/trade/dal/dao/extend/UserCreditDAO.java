package com.xianzaishi.trade.dal.dao.extend;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import com.xianzaishi.trade.dal.dao.UserCreditMapper;
import com.xianzaishi.trade.dal.model.UserCredit;

public interface UserCreditDAO extends UserCreditMapper {
	
	@Select({
		"select * from user_credit where user_id=${userId} for update"
	})
	@Results({ 
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="credit", property="credit", jdbcType=JdbcType.INTEGER),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
	})
	public UserCredit getUserCreditForUpdate(@Param("userId") long userId);

}
