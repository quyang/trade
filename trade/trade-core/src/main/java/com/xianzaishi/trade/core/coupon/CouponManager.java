package com.xianzaishi.trade.core.coupon;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.utils.PageList;

public interface CouponManager {

	long insert(String title, String rules, Date start, Date end, double amount);

	boolean delete(long id);

	Coupon getCoupon(long id);

	PageList<Coupon> getConpons(short[] status, int curPage, int pageSize);

	List<Coupon> getCouponsByType(short type);
	
	List<Coupon> getCouponsByCouponTypes(Collection<Short> couponTypes);
}
