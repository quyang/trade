package com.xianzaishi.trade.core.map.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.xianzaishi.trade.core.map.MapService;
import com.xianzaishi.trade.utils.HttpClientUtils;

/**
 * 高德地图相关操作
 * @author Croky.Zheng
 * 2016年8月29日
 */
@Component(value="mapService")
public class GaodeMapServiceImpl implements MapService {
	private static final Logger log = LoggerFactory.getLogger(GaodeMapServiceImpl.class);

	private HttpClientUtils httpClient = HttpClientUtils.getInstance();

	private String[] keys = new String[] { "8d0f9c46621862f005d11e6f1d4be412" };

	private AtomicInteger requestCount = new AtomicInteger(0);

	@Override
	public String geocoder(double lat, double lng) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://restapi.amap.com/v3/geocode/regeo").append("?").append("location=")
				.append(getParam(lng + "," + lat)).append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	@Override
	public String geocoder(String address) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://restapi.amap.com/v3/geocode/geo").append("?").append("address=").append(getParam(address))
				.append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	@Override
	public String search(String keywords) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://restapi.amap.com/v3/place/text").append("?").append("keywords=").append(getParam(keywords))
				.append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	@Override
	public String suggestion(String keyWord, String region) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://restapi.amap.com/v3/assistant/inputtips").append("?").append("keywords=")
				.append(getParam(keyWord)).append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	@Override
	public String district() {
		return null;
	}

	@Override
	public String translate(double lat, double lng) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://restapi.amap.com/v3/assistant/coordinate/convert").append("?").append("locations=")
				.append(getParam(lng + "," + lat))
				// .append("coordsys=").append(getParam(autonavi))//源坐标系gps;mapbar;baidu;autonavi(不进行转换)
				.append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	@Override
	public String getPositionByIP(String ip) {
		StringBuffer sb = new StringBuffer();
		sb.append("http://restapi.amap.com/v3/ip").append("?").append("ip=").append(getParam(ip)).append("&key=")
				.append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	@Override
	public String distance(double fromLat, double fromLng, double toLat, double toLng) {
		StringBuffer sb = new StringBuffer();
		// 驾车 http://restapi.amap.com/v3/direction/driving
		// 行车距离测试 http://restapi.amap.com/v3/distance
		// 公交规划 http://restapi.amap.com/v3/direction/transit/integrated
		sb.append("http://restapi.amap.com/v3/direction/walking").append("?").append("origin=")
				.append(getParam(fromLng + "," + fromLat)).append("&destination=").append(getParam(toLng + "," + toLat))
				.append("&key=").append(getKey());
		return httpClient.sendHttpGet(sb.toString());
	}

	@Override
	public double getDistance(double fromLat, double fromLng, double toLat, double toLng) {
		double radFromLat = rad(fromLat);
		double radToLat = rad(toLat);
		double a = radFromLat - radToLat;
		double b = rad(fromLng) - rad(toLng);
		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(radFromLat) * Math.cos(radToLat) * Math.pow(Math.sin(b / 2), 2)));
		s = s * 6378.137;
		s = Math.round(s * 10000) / 10000;
		return s;
	}

	@Override
	public String streetview(double lat, double lng, int radius) {
		return null;
	}

	private String getKey() {
		if (log.isDebugEnabled()) {
			log.debug("request count:" + requestCount.get());
		}
		return keys[requestCount.incrementAndGet() % 1];
	}

	private double rad(double d) {
		return d * Math.PI / 180.0;
	}

	@SuppressWarnings("deprecation")
	private String getParam(Object obj) {
		try {
			return URLEncoder.encode(obj.toString(), "utf-8");
		} catch (UnsupportedEncodingException e) {
			return URLEncoder.encode(obj.toString());
		}
	}
}
