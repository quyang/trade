package com.xianzaishi.trade.core.credit;

import java.util.List;

import com.xianzaishi.trade.dal.model.UserCreditLog;

public interface UserCreditManager {

	int getCreditByUserId(long userId);

	int add(long userId, int appId, int credit, String description);

	List<UserCreditLog> getUserCreditLogs(long userId);

}
