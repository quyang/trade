package com.xianzaishi.trade.core;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-dal.xml","classpath:spring-core.xml","classpath:spring-hessian-consumer.xml"})
@Transactional  
public class BaseCoreTest extends AbstractJUnit4SpringContextTests {
	public static final Logger log = LoggerFactory.getLogger(BaseCoreTest.class);

}
