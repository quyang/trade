package com.xianzaishi.trade.core.coupon;

import java.util.List;

import com.xianzaishi.trade.dal.model.OrderDiscount;

public interface OrderDiscountInterface {

  /**
   * 创建订单时录入订单和优惠信息
   * @param orderDiscountList
   * @return
   */
  public boolean insert(List<OrderDiscount> orderDiscountList);
  
  /**
   * 调用订单时取出优惠信息
   * @param orderId
   * @return
   */
  public List<OrderDiscount> getOrderDiscountList(Long orderId);
  
  /**
   * 支付时更新订单优惠到已使用，并且更新优惠券状态
   * @param orderId
   * @return
   */
  public boolean orderUseDiscount(Long orderId);
}
