package com.xianzaishi.trade.web.vo;

import java.io.Serializable;

/**
 * SKU信息
 * @author Croky.Zheng
 * 2016年7月18日
 */
public class SkuVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4069698776439111668L;

	/**
	 * ID
	 */
	private long id;
	
	/**
	 * KEY
	 */
	private String name;
	
	/**
	 * 值
	 */
	private String value;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
