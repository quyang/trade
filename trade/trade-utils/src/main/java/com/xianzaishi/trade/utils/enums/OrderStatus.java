package com.xianzaishi.trade.utils.enums;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author Croky.Zheng
 * 2016年8月13日
 */
public enum OrderStatus {
	INVALID("删除",(short)-1),
	INIT("待付款",(short)0x2),//已下单	-->交易关闭(24小时不支付取消订单)
	PAY("待拣货",(short)0x3),//已付款  -->退款中(3-5个工作日)
	PICKING("拣货中",(short)0x4),//   -->退款中(3-5个工作日)
	PICKED("待配送",(short)0x5),//拣货完成待配送
	DISPATCHING("配送中",(short)0x6),
	//DISPATCHED("已送达",(short)0x7),
	//RECEIVED("已签收",(short)0x8),
	VALID("成功",(short)7),//交易成功,确认收货
	AFTERSALES("客服处理",(short)0x8),//售后服务
	REFOUDING("退款中",(short)0x9),//客服已受理
	REFOUDED("退款成功",(short)0xA),	//部分退款仍然是交易成功，全部退款表示交易关闭
	CLOSED("交易关闭",(short)0xB);
	
	private String description;
	private short value;
	
	private OrderStatus(String description,short value) {
		this.description = description;
		this.value = value;
	}
	
	public static OrderStatus from(short value) {
		for (OrderStatus tmp : OrderStatus.values()) {
			if (tmp.getValue() == value) {
				return tmp;
			}
		}
		return null;
	}
	
	public static Map<Short,String> toMap() {
		Map<Short,String> statusMap = new LinkedHashMap<Short,String>();
		for (OrderStatus tmp : OrderStatus.values()) {
			statusMap.put(tmp.value, tmp.description);
		}
		return statusMap;
	}

	public String getDescription() {
		return description;
	}

	public short getValue() {
		return value;
	}
}
