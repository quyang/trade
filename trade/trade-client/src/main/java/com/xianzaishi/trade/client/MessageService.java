package com.xianzaishi.trade.client;

/**
 * 
 * @author Croky.Zheng
 * 2016年8月26日
 */
public interface MessageService {

	/**
	 * 发送消息到手机
	 * @param businessCode 	业务代码，当前随意拼接
	 * @param mobile		接收的手机号码
	 * @param content		发送的内容
	 * @param needStatus	是否需要状态报告
	 * @return
	 */
	boolean send(String businessCode, String mobile, String content, boolean needStatus);

	/**
	 * 发送信息到手机
	 * @param mobile		手机号码
	 * @param content		内容
	 * @return
	 */
	boolean systemSend(String mobile, String content);
	
	boolean sendVericationCode(String mobile,String code);
	
	boolean sendResetPassword(String mobile,String code);
}
