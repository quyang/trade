package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.OrderItem;
import com.xianzaishi.trade.dal.model.OrderItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface OrderItemMapper {
    @SelectProvider(type=OrderItemSqlProvider.class, method="countByExample")
    int countByExample(OrderItemExample example);

    @DeleteProvider(type=OrderItemSqlProvider.class, method="deleteByExample")
    int deleteByExample(OrderItemExample example);

    @Delete({
        "delete from order_item",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into order_item (orders_id, item_id, ",
        "item_sku_id, item_name, ",
        "item_bill_price, item_effe_price, ",
        "item_count, item_sku_info, ",
        "item_icon_url, item_category_id, ",
        "item_bill_amount, item_effe_amount, ",
        "gmt_create, gmt_modify, ",
        "status)",
        "values (#{ordersId,jdbcType=BIGINT}, #{itemId,jdbcType=BIGINT}, ",
        "#{itemSkuId,jdbcType=BIGINT}, #{itemName,jdbcType=VARCHAR}, ",
        "#{itemBillPrice,jdbcType=DOUBLE}, #{itemEffePrice,jdbcType=DOUBLE}, ",
        "#{itemCount,jdbcType=DOUBLE}, #{itemSkuInfo,jdbcType=VARCHAR}, ",
        "#{itemIconUrl,jdbcType=VARCHAR}, #{itemCategoryId,jdbcType=INTEGER}, ",
        "#{itemBillAmount,jdbcType=DOUBLE}, #{itemEffeAmount,jdbcType=DOUBLE}, ",
        "#{gmtCreate,jdbcType=TIMESTAMP}, #{gmtModify,jdbcType=TIMESTAMP}, ",
        "#{status,jdbcType=SMALLINT})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(OrderItem record);

    @InsertProvider(type=OrderItemSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insertSelective(OrderItem record);

    @SelectProvider(type=OrderItemSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="orders_id", property="ordersId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_id", property="itemId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_sku_id", property="itemSkuId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_name", property="itemName", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_bill_price", property="itemBillPrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_effe_price", property="itemEffePrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_count", property="itemCount", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_sku_info", property="itemSkuInfo", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_icon_url", property="itemIconUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_category_id", property="itemCategoryId", jdbcType=JdbcType.INTEGER),
        @Result(column="item_bill_amount", property="itemBillAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_effe_amount", property="itemEffeAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT)
    })
    List<OrderItem> selectByExample(OrderItemExample example);

    @Select({
        "select",
        "id, orders_id, item_id, item_sku_id, item_name, item_bill_price, item_effe_price, ",
        "item_count, item_sku_info, item_icon_url, item_category_id, item_bill_amount, ",
        "item_effe_amount, gmt_create, gmt_modify, status",
        "from order_item",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="orders_id", property="ordersId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_id", property="itemId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_sku_id", property="itemSkuId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_name", property="itemName", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_bill_price", property="itemBillPrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_effe_price", property="itemEffePrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_count", property="itemCount", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_sku_info", property="itemSkuInfo", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_icon_url", property="itemIconUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_category_id", property="itemCategoryId", jdbcType=JdbcType.INTEGER),
        @Result(column="item_bill_amount", property="itemBillAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_effe_amount", property="itemEffeAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT)
    })
    OrderItem selectByPrimaryKey(Long id);

    @UpdateProvider(type=OrderItemSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") OrderItem record, @Param("example") OrderItemExample example);

    @UpdateProvider(type=OrderItemSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") OrderItem record, @Param("example") OrderItemExample example);

    @UpdateProvider(type=OrderItemSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(OrderItem record);

    @Update({
        "update order_item",
        "set orders_id = #{ordersId,jdbcType=BIGINT},",
          "item_id = #{itemId,jdbcType=BIGINT},",
          "item_sku_id = #{itemSkuId,jdbcType=BIGINT},",
          "item_name = #{itemName,jdbcType=VARCHAR},",
          "item_bill_price = #{itemBillPrice,jdbcType=DOUBLE},",
          "item_effe_price = #{itemEffePrice,jdbcType=DOUBLE},",
          "item_count = #{itemCount,jdbcType=DOUBLE},",
          "item_sku_info = #{itemSkuInfo,jdbcType=VARCHAR},",
          "item_icon_url = #{itemIconUrl,jdbcType=VARCHAR},",
          "item_category_id = #{itemCategoryId,jdbcType=INTEGER},",
          "item_bill_amount = #{itemBillAmount,jdbcType=DOUBLE},",
          "item_effe_amount = #{itemEffeAmount,jdbcType=DOUBLE},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP},",
          "status = #{status,jdbcType=SMALLINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(OrderItem record);
}