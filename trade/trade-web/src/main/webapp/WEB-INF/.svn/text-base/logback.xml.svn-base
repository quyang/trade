<?xml version="1.0" encoding="UTF-8"?>
<!-- Logback Configuration.  -->
<configuration scan="true" scanPeriod="60 seconds" debug="false">
	<substitutionProperty name="logDir" value="${profile.loggingRoot}" />
	<property name="LOG_HOME" value="${profile.loggingRoot}" />
	
    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <target>System.out</target>
        <encoding>${profile.loggingEncoding}</encoding>
        <filter class="com.alibaba.citrus.logconfig.logback.LevelRangeFilter">
            <levelMax>INFO</levelMax>
        </filter>
        <layout class="ch.qos.logback.classic.PatternLayout">
            <pattern><![CDATA[
%n%-4r [%d{yyyy-MM-dd HH:mm:ss} %X{productionMode}] - %X{method} %X{requestURIWithQueryString} [ip=%X{remoteAddr}, ref=%X{referrer}, ua=%X{userAgent}, sid=%X{cookie.JSESSIONID}]%n  %-5level %logger{35} - %m%n
            ]]></pattern>
        </layout>
    </appender>
	
	<appender name="PROJECT" class="ch.qos.logback.core.rolling.RollingFileAppender">
	 <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
       <FileNamePattern>${profile.loggingRoot}/tnt.log.%d{yyyy-MM-dd}</FileNamePattern> 
       <MaxHistory>10</MaxHistory>
     </rollingPolicy>   
     <layout class="ch.qos.logback.classic.PatternLayout">  
      <pattern><![CDATA[%d{HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n]]></pattern>  
     </layout>
     <encoding>${profile.loggingEncoding}</encoding>
    </appender>
	
	<appender name="HIVE" class="ch.qos.logback.core.rolling.RollingFileAppender">
	 <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
       <FileNamePattern>${profile.loggingRoot}/hive.log.%d{yyyy-MM-dd}</FileNamePattern> 
       <MaxHistory>10</MaxHistory>
     </rollingPolicy>   
     <layout class="ch.qos.logback.classic.PatternLayout">  
      <pattern><![CDATA[%d{HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n]]></pattern>  
     </layout>
     <encoding>${profile.loggingEncoding}</encoding>
    </appender>
	
	<appender name="TASK" class="ch.qos.logback.core.rolling.RollingFileAppender">
	 <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
       <FileNamePattern>${profile.loggingRoot}/task.log.%d{yyyy-MM-dd}</FileNamePattern> 
       <MaxHistory>10</MaxHistory>
     </rollingPolicy>   
     <layout class="ch.qos.logback.classic.PatternLayout">  
      <pattern><![CDATA[%d{HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n]]></pattern>  
     </layout>
     <encoding>${profile.loggingEncoding}</encoding>
    </appender>
	
	<appender name="MANAGER" class="ch.qos.logback.core.rolling.RollingFileAppender">
	 <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
       <FileNamePattern>${profile.loggingRoot}/manager.log.%d{yyyy-MM-dd}</FileNamePattern> 
       <MaxHistory>10</MaxHistory>
     </rollingPolicy>   
     <layout class="ch.qos.logback.classic.PatternLayout">  
      <pattern><![CDATA[%d{HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n]]></pattern>  
     </layout>
     <encoding>${profile.loggingEncoding}</encoding>
    </appender>
	
	<appender name="SERVICE" class="ch.qos.logback.core.rolling.RollingFileAppender">
	 <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
       <FileNamePattern>${profile.loggingRoot}/service.log.%d{yyyy-MM-dd}</FileNamePattern> 
       <MaxHistory>10</MaxHistory>
     </rollingPolicy>   
     <layout class="ch.qos.logback.classic.PatternLayout">  
      <pattern><![CDATA[%d{HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n]]></pattern>  
     </layout>
     <encoding>${profile.loggingEncoding}</encoding>
    </appender>
	
	<appender name="SQL" class="ch.qos.logback.core.rolling.RollingFileAppender">
	 <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
       <FileNamePattern>${profile.loggingRoot}/sql.log.%d{yyyy-MM-dd}</FileNamePattern> 
       <MaxHistory>10</MaxHistory>
     </rollingPolicy>   
     <layout class="ch.qos.logback.classic.PatternLayout">  
      <pattern><![CDATA[%d{HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n]]></pattern>  
     </layout>
     <encoding>${profile.loggingEncoding}</encoding>
    </appender>
	
	<appender name="WEBX" class="ch.qos.logback.core.rolling.RollingFileAppender">
	 <file>${profile.loggingRoot}/webx.log</file>
	 <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">   
      <fileNamePattern>${profile.loggingRoot}/webx.%i.log.zip</fileNamePattern>   
      <minIndex>1</minIndex>   
      <maxIndex>3</maxIndex>   
     </rollingPolicy> 
     <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
         <MaxFileSize>200MB</MaxFileSize>
     </triggeringPolicy>
	 <encoder>   
      <pattern><![CDATA[%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n]]></pattern>   
     </encoder>
     <encoding>${profile.loggingEncoding}</encoding>
    </appender>

    <logger name="org.apache">
        <level value="${profile.loggingLevel}" />
    </logger>

    <logger name="org.springframework">
        <level value="${profile.loggingLevel}" />
    </logger>

    <logger name="com.alibaba.citrus.springext.support.context">
        <level value="${profile.loggingLevel}" />
    </logger>
	
    <logger name="com.taobao.tnt.core.task">
         <level value="${profile.loggingLevel}" /> 
         <appender-ref ref="TASK"/>      
    </logger>
	
    <logger name="com.taobao.tnt.core.manager">
         <level value="${profile.loggingLevel}" /> 
         <appender-ref ref="MANAGER"/>      
    </logger>
	
    <logger name="com.taobao.tnt.core.service">
         <level value="${profile.loggingLevel}" /> 
         <appender-ref ref="SERVICE"/>      
    </logger>
      
    <logger name="com.taobao.tnt.core.database">  
         <level value="${profile.loggingLevel}" /> 
         <appender-ref ref="HIVE"/>      
    </logger>
      
    <logger name="com.aliyun">  
         <level value="${profile.loggingLevel}" /> 
         <appender-ref ref="HIVE"/>      
    </logger>
      
    <logger name="org.apache.hadoop">  
         <level value="${profile.loggingLevel}" /> 
         <appender-ref ref="HIVE"/>      
    </logger>
    
    <logger name="com.alibaba.webx" additivity="false">
      	<level value="${profile.loggingLevel}"/>
        <appender-ref ref="WEBX"/>
    </logger>
    
    <logger name="com.taobao.tnt.web" additivity="false">
      	<level value="${profile.loggingLevel}"/>
        <appender-ref ref="WEBX"/>
    </logger>
    
    <logger name="com.alibaba.citrus.turbine" additivity="false">
      	<level value="${profile.loggingLevel}"/>
        <appender-ref ref="WEBX"/>
    </logger>
    
    <logger name="javax.servlet" additivity="false">
      	<level value="${profile.loggingLevel}"/>
        <appender-ref ref="WEBX"/>
    </logger>
	
	
    
    <logger name="java.sql" additivity="false">
     	<level value="${profile.loggingLevel}"/>
        <appender-ref ref="SQL"/>
    </logger>
    
    <logger name="org.springframework.jdbc" additivity="false">
     	<level value="${profile.loggingLevel}"/>
        <appender-ref ref="SQL"/>
    </logger>
    
    <logger name="org.springframework.orm" additivity="false">
     	<level value="${profile.loggingLevel}"/>
        <appender-ref ref="SQL"/>
    </logger>
    
    <logger name="com.ibatis" additivity="false">
     	<level value="${profile.loggingLevel}"/>
        <appender-ref ref="SQL"/>
    </logger>
    
    <logger name="com.mysql" additivity="false">
     	<level value="${profile.loggingLevel}"/>
        <appender-ref ref="SQL"/>
    </logger>
	
    <root>
        <level value="${profile.loggingLevel}" />
        <appender-ref ref="PROJECT" />
        <appender-ref ref="STDOUT" />
    </root>
</configuration>
