package com.xianzaishi.trade.web.controller;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.core.coupon.CouponManager;
import com.xianzaishi.trade.core.coupon.UserCouponManager;
import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.trade.utils.enums.Status;
import com.xianzaishi.trade.web.model.CouponSignature;
import com.xianzaishi.trade.web.model.ResponseModel;
import com.xianzaishi.trade.web.model.SignResponseModel;
import com.xianzaishi.trade.web.utils.SecurityUtils;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;

/**
 * 
 * 优惠券API
 *
 * @author nianyue.hyj
 * @since 2016.10.18
 */
@Controller
public class CouponController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserCouponManager userCouponManager;

	@Autowired
	private CouponManager couponManager;

	@Autowired
	private UserCouponService newUserCouponService;
	
	private final static long DEFAULT_EXPIRE_TIME = 15 * 60 * 1000;// 15分钟超时

	private Logger logger = LoggerFactory.getLogger(CouponController.class);

	@RequestMapping(value = "/coupon/getNewCouponPackage.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getNewCouponPackage(@RequestParam String token) {

//		if (StringUtils.isBlank(token)) {
//			return new ResponseModel(-1, "参数错误");
//		}
//
//		try {
//			Result<BaseUserDTO> userResult = userService.queryBaseUserByToken(token);
//			if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
//				return new ResponseModel(-1, "未登录");
//			}
//			BaseUserDTO user = userResult.getModule();
//
//			List<Coupon> coupons = couponManager.getCouponsByType((short) 11);
//			if (coupons != null) {
//				List<UserCoupon> userCoupons = userCouponManager.getUserCouponsByCouponType(user.getUserId(), Lists.newArrayList((short) 11));
//				Set<Long> exist = new HashSet<>();
//				for (UserCoupon userCoupon : userCoupons) {
//					exist.add(userCoupon.getCouponId());
//				}
//				for (Coupon coupon : coupons) {
//					if (!exist.contains(coupon.getId())) {
//						Date now = new Date();
//						coupon.setGmtStart(now);
//						coupon.setGmtEnd(DateUtils.dateAddDays(now, 15));
//						userCouponManager.insertCoupon(user.getUserId(), coupon);
//					}
//				}
//			}
//			return new ResponseModel(0, "新人礼包赠送成功");
//		} catch (Exception e) {
//			return new ResponseModel(-1, "服务器内部错误");
//		}
	  return new ResponseModel(-1, "服务器内部错误，接口过期，请升级新版本app");
	}

	/**
	 * 早餐券，即将过期
	 * @param token
	 * @param cid
	 * @return
	 */
	@RequestMapping(value = "/coupon/getCouponSignature.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getCouponSignature(@RequestParam String token, @RequestParam Long cid) {

		if (StringUtils.isBlank(token) || cid <= 0) {
			return new ResponseModel(-1, "参数错误");
		}

		try {
			Result<BaseUserDTO> userResult = userService.queryBaseUserByToken(token);
			if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
				return new ResponseModel(-1, "未登录");
			}
			BaseUserDTO user = userResult.getModule();

			UserCoupon userCoupon = userCouponManager.getUserCouponById(cid);
			if (userCoupon == null || userCoupon.getUserId() == null || userCoupon.getUserId().longValue() != user.getUserId()) {
				return new ResponseModel(-1, "优惠券不存在");
			}

			UserCouponParams params = new UserCouponParams(user.getUserId(), userCoupon.getId(), (new Date()).getTime());
			String data = SecurityUtils.base64UrlEncoder(SecurityUtils.encode(JSONObject.toJSONString(params)));
			String sign = SecurityUtils.sign(data);

			CouponSignature signature = new CouponSignature();
			signature.setCouponId(userCoupon.getId());
			signature.setCouponTitle(userCoupon.getCouponTitle());
			signature.setCouponStartTime(userCoupon.getCouponStartTime());
			signature.setCouponEndTime(userCoupon.getCouponEndTime());
			signature.setSecurity(data);
			signature.setSign(sign);

			return new ResponseModel(signature);

		} catch (Exception e) {
			return new ResponseModel(-1, "服务器内部错误");
		}
	}

	@RequestMapping(value = "/coupon/useCouponBySignature.json", method = { RequestMethod.POST })
	public @ResponseBody SignResponseModel useCouponBySignature(@RequestParam String security, @RequestParam String sign) {

		if (StringUtils.isBlank(security) || StringUtils.isBlank(sign)) {
			return new SignResponseModel(-1, "参数错误", "FAIL");
		}

		try {

			// if (!SecurityUtils.verify(security, sign)) {
			// return new SignResponseModel(-1, "签名错误", "FAIL");
			// }

			UserCouponParams params = JSON.parseObject(SecurityUtils.decode(SecurityUtils.base64UrlDecoder(security)), UserCouponParams.class);
			if (params == null || params.getCouponId() <= 0 || params.getUserId() <= 0
					|| (System.currentTimeMillis() - DEFAULT_EXPIRE_TIME > params.getTimestamp())) {
				return new SignResponseModel(-1, "二维码超时", "FAIL");
			}

			UserCoupon userCoupon = userCouponManager.getUserCouponById(params.getCouponId());
			if (userCoupon == null || userCoupon.getUserId().longValue() != params.getUserId()) {
				return new SignResponseModel(-1, "优惠券不存在", "FAIL");
			}

			if (userCoupon.getStatus().shortValue() != Status.VALID.getValue()) {
				return new SignResponseModel(-1, "优惠券已经使用过", "FAIL");
			}

			Result<Boolean> updateResult = newUserCouponService.updateCouponStatus(userCoupon.getId(), Status.INIT.getValue());
            if (null != updateResult && updateResult.getSuccess() && updateResult.getModule()) {
				return new SignResponseModel(0, "成功", "SUCCESS");
			}

		} catch (Exception e) {
			logger.error(String.format("Fail to use coupon by security[%s], sign[%s]", security, sign), e);
		}

		return new SignResponseModel(-1, "服务器内部错误", "FAIL");
	}

	public static class UserCouponParams implements Serializable {
		private static final long serialVersionUID = -7257715926620628506L;

		private long userId;

		private long couponId;

		private long timestamp;

		public UserCouponParams() {

		}

		public UserCouponParams(long userId, long couponId, long timestamp) {
			this.userId = userId;
			this.couponId = couponId;
			this.timestamp = timestamp;
		}

		public long getUserId() {
			return userId;
		}

		public void setUserId(long userId) {
			this.userId = userId;
		}

		public long getCouponId() {
			return couponId;
		}

		public void setCouponId(long couponId) {
			this.couponId = couponId;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(long timestamp) {
			this.timestamp = timestamp;
		}

	}

}
