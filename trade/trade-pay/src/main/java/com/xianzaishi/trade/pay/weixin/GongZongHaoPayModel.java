package com.xianzaishi.trade.pay.weixin;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.xianzaishi.trade.pay.util.PayUtil;

@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class GongZongHaoPayModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3665308801533724069L;

	private String appId = "wx4b6d100929d957c6";

	private String timeStamp = String.valueOf(System.currentTimeMillis() / 1000);

	private String nonceStr = PayUtil.genRandomString();

	private Long price;

	private Long discountPrice;

	@XmlElement(name = "package")
	private String pack = null;

	private String signType = "MD5";

	private String paySign = null;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getPack() {
		return pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getPaySign() {
		return paySign;
	}

	public void setPaySign(String paySign) {
		this.paySign = paySign;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Long discountPrice) {
		this.discountPrice = discountPrice;
	}

}
