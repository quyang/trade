package com.xianzaishi.trade.dal.dao.extend;

import com.xianzaishi.trade.dal.dao.OrderPayLogMapper;

/**
 * 
 * @author Croky.Zheng
 * 2016年8月22日
 */
public interface OrderPayLogDAO extends OrderPayLogMapper {

}
