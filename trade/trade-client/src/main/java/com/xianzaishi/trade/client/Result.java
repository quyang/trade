package com.xianzaishi.trade.client;

public class Result<T> implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1143479650730746041L;

	private int code = 0;
	
	private String message;
	
	private T model;
	
	public Result(){}
	
	public Result(int code,String message, T model) {
		this.code = code;
		this.message = message;
		this.model = model;
	}
	
	public Result(int code,String message) {
		this.code = code;
		this.message = message;
	}
	
	public Result(int code,T model) {
		this.code = Math.abs(code);
		this.model = model;
	}
	
	public Result(T model) {
		this(1,model);
	}

	public boolean isSuccess() {
		return code >= 0;
	}
	
	public boolean isError() {
		return code < 0;
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getModel() {
		return model;
	}

	public void setModel(T model) {
		this.model = model;
	}
}
