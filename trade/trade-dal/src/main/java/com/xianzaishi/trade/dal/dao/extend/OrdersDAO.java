package com.xianzaishi.trade.dal.dao.extend;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import com.xianzaishi.trade.dal.dao.OrdersMapper;
import com.xianzaishi.trade.dal.model.extend.OrderInfo;
import com.xianzaishi.trade.utils.Pagination;

/**
 * 
 * @author Croky.Zheng
 * 2016年8月24日
 */
public interface OrdersDAO extends OrdersMapper {
	
	@Select({
		"select * from orders where user_id=#{userId} ${status}",
		"order by id ${sort} limit #{pagination.offset},#{pagination.limit}"
	})
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="pay_amount", property="payAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="effe_amount", property="effeAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="device_code", property="deviceCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="cashier_id", property="cashierId", jdbcType=JdbcType.INTEGER),
        @Result(column="user_address_id", property="userAddressId", jdbcType=JdbcType.BIGINT),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="attribute", property="attribute", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="order_type", property="orderType", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="bill_amount", property="billAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="coupon_id", property="couponId", jdbcType=JdbcType.BIGINT),
        @Result(column="discount_amount", property="discountAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="gmt_pay", property="gmtPay", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_receive", property="gmtReceive", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="seq", property="seq", jdbcType=JdbcType.INTEGER),
        @Result(column="user_address", property="userAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="gmt_distribution", property="gmtDistribution", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="credit", property="credit", jdbcType=JdbcType.INTEGER),
        @Result(column="channel_type", property="channelType", jdbcType=JdbcType.SMALLINT)
    })
    List<OrderInfo> selectByUserId(@Param("userId")Long userId,@Param("status") String status,@Param("pagination") Pagination pagination ,@Param("sort") String sort);
	
	@Select({
      "select * from orders where "
      + "gmt_modify > #{begin} and"
      + "#{end} > gmt_modify ${status} ",
      "order by id desc limit #{pagination.offset},#{pagination.limit}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="pay_amount", property="payAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="effe_amount", property="effeAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="device_code", property="deviceCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="cashier_id", property="cashierId", jdbcType=JdbcType.INTEGER),
        @Result(column="user_address_id", property="userAddressId", jdbcType=JdbcType.BIGINT),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="attribute", property="attribute", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="bill_amount", property="billAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="coupon_id", property="couponId", jdbcType=JdbcType.BIGINT),
        @Result(column="discount_amount", property="discountAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="gmt_pay", property="gmtPay", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_receive", property="gmtReceive", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="seq", property="seq", jdbcType=JdbcType.INTEGER),
        @Result(column="user_address", property="userAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="gmt_distribution", property="gmtDistribution", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="credit", property="credit", jdbcType=JdbcType.INTEGER),
        @Result(column="channel_type", property="channelType", jdbcType=JdbcType.SMALLINT)
    })
    List<OrderInfo> selectOrderByTime(@Param("begin")Date begin,@Param("end")Date end,@Param("status") String status, @Param("pagination") Pagination pagination);
	
}
