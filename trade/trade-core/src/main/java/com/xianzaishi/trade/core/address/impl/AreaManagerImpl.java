package com.xianzaishi.trade.core.address.impl;

import java.io.IOException;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.xianzaishi.trade.core.address.AreaManager;
import com.xianzaishi.trade.dal.dao.extend.AreaDAO;
import com.xianzaishi.trade.dal.model.Area;
import com.xianzaishi.trade.dal.model.AreaExample;

/**
 * 
 * @author Croky.Zheng
 * 2016年8月18日
 */
@Component(value="areaManager")
public class AreaManagerImpl implements AreaManager,java.io.Closeable {
	private static final Logger log = LoggerFactory.getLogger(AreaManagerImpl.class);

	@Resource
	private AreaDAO areaDAO;
	
	private ConcurrentHashMap<String,Area> areaMap = new ConcurrentHashMap<String,Area>();
	
	@Override
	public Area getArea(String code) {
		Area area = areaMap.get(code);
		if (null == area) {
			AreaExample example = new AreaExample();
			example.createCriteria().andCodeEqualTo(code);
			try {
				List<Area> areaList = areaDAO.selectByExample(example);
				if (CollectionUtils.isNotEmpty(areaList)) {
					area = areaList.get(0);
					areaMap.put(area.getCode(), area);
				}
			} catch (DataAccessException e) {
				log.error("select linkmen to DeliverAddress failed.",e);
			}
		}
		return area;
	}
	
	@Override
	public String getAreaAddress(String code) {
		Area area = getArea(code);
		java.util.Stack<Area> areaStack = new Stack<Area>();
		while (null != area) {
			areaStack.push(area);
			area = getArea(area.getParentCode());
		}
		return getAddress(areaStack);
	}
	
	@Override
	public List<Area> getChildArea(String code) {
		AreaExample example = new AreaExample();
		example.createCriteria().andParentCodeEqualTo(code);
		try {
			return areaDAO.selectByExample(example);
		} catch (DataAccessException e) {
			log.error("select linkmen to DeliverAddress failed.",e);
		}
		return null;
	}
	
	@Override
	public List<Area> getRootArea() {
		return getChildArea("0");
	}

	@Override
	public void close() throws IOException {
		areaMap.clear();
		areaMap = null;
		
	}
	
	private String getAddress(Stack<Area> stack) {
		StringBuilder buf = new StringBuilder();
		try {
			Area area = stack.pop();
			while(null != area) {
				buf.append(area.getName());
				area = stack.pop();
			}
		} catch (EmptyStackException e) {
			
		}
		return buf.toString();
	}
}
