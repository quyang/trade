package com.xianzaishi.trade.client;

import com.xianzaishi.trade.client.vo.DeliveryVO;

import java.util.List;


/**
 * 
 * @author quyang 2017-2-9 14:33:46
 */
public interface DeliveryService {

	/**
	 * 获取用户常用配送地址
	 */
	Result<List<DeliveryVO>> getUseralwaysAddreses(Long userId);


}
