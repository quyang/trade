package com.xianzaishi.trade.core.address;

import java.util.List;

import com.xianzaishi.trade.dal.model.DeliveryAddress;


public interface DeliveryAddressManager {

	long addLinkman(long userId, String name, String code, String address, String mobile,int shopId);

	boolean deleteLinkman(long id);

	boolean updateLinkman(long id, String name, String mobile, String code, String address,int shopId);

	DeliveryAddress getLinkman(long id);

	List<DeliveryAddress> getAllLinkman(long userId);

	DeliveryAddress getDefaultLinkman(long userId);

	boolean setDefaultLinkman(long id);

	String getLinkmanAddress(long id);


}
