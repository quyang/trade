package com.xianzaishi.trade.core;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.config.AlipayConfigV2;
import com.alipay.util.AlipayNotify;

public class AlipayTest {

	@Test
	public void testVierfy() {
		Map<String,String> table = new HashMap<String,String>();
		table.put("body","PAY");
		table.put("buyer_email","18561530143");
		table.put("buyer_id","2088802889936102");
		//table.put("discount","0.00");
		table.put("gmt_create","2016-08-31 10:17:42");
		table.put("gmt_payment","2016-08-31 10:17:43");
		table.put("is_total_fee_adjust","N");
		table.put("notify_id","d7fd477c57de1ca16b96f1475b93c1fgru");
		table.put("notify_time","2016-08-31 10:41:19");
		table.put("notify_type","trade_status_sync");
		table.put("out_trade_no","10068");
		//table.put("payment_type","1");
		table.put("price","0.01");
		table.put("quantity","1");
		table.put("seller_email","pay@xianzaishi.com");
		table.put("seller_id","2088421284384567");
		table.put("sign","BDVdcob02cGVDeul7+Lx1WpaPxycV0kbNTvb8oNp4zMJDLwmL0GUOSCUzrGHWzb3AJV7BeXPcqsNS41Y59HDvxUQZt/HtwbC4mxVHpypkZm+gbKJVJNEtYbRHTgCNP39G1MK0QS/hagcP4cW2aEulHoXv0tzkkq2Ay8wfJQ0CRc=");
		table.put("sign_type","RSA");
		table.put("subject","XIANZAISHI_PAY");
		table.put("total_fee","0.01");
		table.put("trade_no","2016083121001004100205807530");
		table.put("trade_status","TRADE_SUCCESS");
		table.put("use_coupon","N");
		try {
			boolean result = AlipaySignature.rsaCheckV2(table, AlipayConfigV2.alipay_public_key,"utf-8");
			System.out.println("result1=" + result);
		} catch (AlipayApiException e) {
			e.printStackTrace();
		}
		boolean result = AlipayNotify.getSignVeryfy(table, "BDVdcob02cGVDeul7+Lx1WpaPxycV0kbNTvb8oNp4zMJDLwmL0GUOSCUzrGHWzb3AJV7BeXPcqsNS41Y59HDvxUQZt/HtwbC4mxVHpypkZm+gbKJVJNEtYbRHTgCNP39G1MK0QS/hagcP4cW2aEulHoXv0tzkkq2Ay8wfJQ0CRc=");
		System.out.println("result=" + result);
	}
}
