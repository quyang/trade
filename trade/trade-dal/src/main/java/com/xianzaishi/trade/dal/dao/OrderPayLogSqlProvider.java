package com.xianzaishi.trade.dal.dao;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import com.xianzaishi.trade.dal.model.OrderPayLog;
import com.xianzaishi.trade.dal.model.OrderPayLogExample.Criteria;
import com.xianzaishi.trade.dal.model.OrderPayLogExample.Criterion;
import com.xianzaishi.trade.dal.model.OrderPayLogExample;
import java.util.List;
import java.util.Map;

public class OrderPayLogSqlProvider {

    public String countByExample(OrderPayLogExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("order_pay_log");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(OrderPayLogExample example) {
        BEGIN();
        DELETE_FROM("order_pay_log");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(OrderPayLog record) {
        BEGIN();
        INSERT_INTO("order_pay_log");
        
        if (record.getOrderId() != null) {
            VALUES("order_id", "#{orderId,jdbcType=BIGINT}");
        }
        
        if (record.getPayWay() != null) {
            VALUES("pay_way", "#{payWay,jdbcType=SMALLINT}");
        }
        
        if (record.getPayAmount() != null) {
            VALUES("pay_amount", "#{payAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getGmtCreate() != null) {
            VALUES("gmt_create", "#{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            VALUES("gmt_modify", "#{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getAccount() != null) {
            VALUES("account", "#{account,jdbcType=VARCHAR}");
        }
        
        if (record.getCallbackcontent() != null) {
            VALUES("callbackContent", "#{callbackcontent,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            VALUES("status", "#{status,jdbcType=SMALLINT}");
        }
        
        return SQL();
    }

    public String selectByExample(OrderPayLogExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("order_id");
        SELECT("pay_way");
        SELECT("pay_amount");
        SELECT("gmt_create");
        SELECT("gmt_modify");
        SELECT("account");
        SELECT("callbackContent");
        SELECT("status");
        FROM("order_pay_log");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        OrderPayLog record = (OrderPayLog) parameter.get("record");
        OrderPayLogExample example = (OrderPayLogExample) parameter.get("example");
        
        BEGIN();
        UPDATE("order_pay_log");
        
        if (record.getId() != null) {
            SET("id = #{record.id,jdbcType=BIGINT}");
        }
        
        if (record.getOrderId() != null) {
            SET("order_id = #{record.orderId,jdbcType=BIGINT}");
        }
        
        if (record.getPayWay() != null) {
            SET("pay_way = #{record.payWay,jdbcType=SMALLINT}");
        }
        
        if (record.getPayAmount() != null) {
            SET("pay_amount = #{record.payAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getAccount() != null) {
            SET("account = #{record.account,jdbcType=VARCHAR}");
        }
        
        if (record.getCallbackcontent() != null) {
            SET("callbackContent = #{record.callbackcontent,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{record.status,jdbcType=SMALLINT}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("order_pay_log");
        
        SET("id = #{record.id,jdbcType=BIGINT}");
        SET("order_id = #{record.orderId,jdbcType=BIGINT}");
        SET("pay_way = #{record.payWay,jdbcType=SMALLINT}");
        SET("pay_amount = #{record.payAmount,jdbcType=DOUBLE}");
        SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        SET("account = #{record.account,jdbcType=VARCHAR}");
        SET("callbackContent = #{record.callbackcontent,jdbcType=VARCHAR}");
        SET("status = #{record.status,jdbcType=SMALLINT}");
        
        OrderPayLogExample example = (OrderPayLogExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(OrderPayLog record) {
        BEGIN();
        UPDATE("order_pay_log");
        
        if (record.getOrderId() != null) {
            SET("order_id = #{orderId,jdbcType=BIGINT}");
        }
        
        if (record.getPayWay() != null) {
            SET("pay_way = #{payWay,jdbcType=SMALLINT}");
        }
        
        if (record.getPayAmount() != null) {
            SET("pay_amount = #{payAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getAccount() != null) {
            SET("account = #{account,jdbcType=VARCHAR}");
        }
        
        if (record.getCallbackcontent() != null) {
            SET("callbackContent = #{callbackcontent,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{status,jdbcType=SMALLINT}");
        }
        
        WHERE("id = #{id,jdbcType=BIGINT}");
        
        return SQL();
    }

    protected void applyWhere(OrderPayLogExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}