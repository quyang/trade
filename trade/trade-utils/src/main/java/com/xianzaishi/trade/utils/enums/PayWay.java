package com.xianzaishi.trade.utils.enums;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author Croky.Zheng
 * 2016年7月16日
 */
public enum PayWay {
	//支付方式(1:保留 2:微信 3:支付宝 4:网银支付 5:信用卡 6:apple pay)
	YINTONG("银通",(short)0x1),
	CASH("现金",(short)0x2),
	WEIXIN("微信移动",(short)0x4),
	ALIPAY("支付宝移动",(short)0x8),
	NETBANK("网银",(short)0xF),
	CREDITCARD("信用卡",(short)0x10),
	APPLEPAY("apple",(short)0x20),
	WEIXINCODE("微信扫码",(short)0x40),
	ALIPAYCODE("支付宝扫码",(short)0x80),
	CREDIT("积分支付",(short)0x100),//积分支付;
	COUPON("优惠券支付",(short)0x200),//优惠券支付
	ALIPAY_QR("支付宝-当面扫码支付", (short)0x300);
	
	
	private String description;
	private short value;
	
	private PayWay(String description,short value) {
		this.description = description;
		this.value = value;
	}
	
	public static PayWay from(int value) {
		for (PayWay tmp : PayWay.values()) {
			if (tmp.getValue() == value) {
				return tmp;
			}
		}
		return null;
	}
	
	public static Map<Short,String> toMap() {
		Map<Short,String> payTypeMap = new LinkedHashMap<Short,String>();
		for (PayWay tmp : PayWay.values()) {
			payTypeMap.put(tmp.value, tmp.description);
		}
		return payTypeMap;
	}

	public String getDescription() {
		return description;
	}

	public short getValue() {
		return value;
	}
}
