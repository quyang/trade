package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.DeliveryAddress;
import com.xianzaishi.trade.dal.model.DeliveryAddressExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface DeliveryAddressMapper {
    @SelectProvider(type=DeliveryAddressSqlProvider.class, method="countByExample")
    int countByExample(DeliveryAddressExample example);

    @DeleteProvider(type=DeliveryAddressSqlProvider.class, method="deleteByExample")
    int deleteByExample(DeliveryAddressExample example);

    @Delete({
        "delete from delivery_address",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into delivery_address (user_id, name, ",
        "area_code, zip_code, ",
        "address, phone, ",
        "status, gmt_create, ",
        "gmt_modify, code_address, ",
        "shop_id, longitude, ",
        "latitude)",
        "values (#{userId,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{areaCode,jdbcType=VARCHAR}, #{zipCode,jdbcType=INTEGER}, ",
        "#{address,jdbcType=VARCHAR}, #{phone,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=SMALLINT}, #{gmtCreate,jdbcType=TIMESTAMP}, ",
        "#{gmtModify,jdbcType=TIMESTAMP}, #{codeAddress,jdbcType=VARCHAR}, ",
        "#{shopId,jdbcType=INTEGER}, #{longitude,jdbcType=REAL}, ",
        "#{latitude,jdbcType=REAL})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(DeliveryAddress record);

    @InsertProvider(type=DeliveryAddressSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insertSelective(DeliveryAddress record);

    @SelectProvider(type=DeliveryAddressSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="area_code", property="areaCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="zip_code", property="zipCode", jdbcType=JdbcType.INTEGER),
        @Result(column="address", property="address", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="code_address", property="codeAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="longitude", property="longitude", jdbcType=JdbcType.REAL),
        @Result(column="latitude", property="latitude", jdbcType=JdbcType.REAL)
    })
    List<DeliveryAddress> selectByExample(DeliveryAddressExample example);

    @Select({
        "select",
        "id, user_id, name, area_code, zip_code, address, phone, status, gmt_create, ",
        "gmt_modify, code_address, shop_id, longitude, latitude",
        "from delivery_address",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="area_code", property="areaCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="zip_code", property="zipCode", jdbcType=JdbcType.INTEGER),
        @Result(column="address", property="address", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="code_address", property="codeAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="longitude", property="longitude", jdbcType=JdbcType.REAL),
        @Result(column="latitude", property="latitude", jdbcType=JdbcType.REAL)
    })
    DeliveryAddress selectByPrimaryKey(Long id);

    @UpdateProvider(type=DeliveryAddressSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") DeliveryAddress record, @Param("example") DeliveryAddressExample example);

    @UpdateProvider(type=DeliveryAddressSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") DeliveryAddress record, @Param("example") DeliveryAddressExample example);

    @UpdateProvider(type=DeliveryAddressSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(DeliveryAddress record);

    @Update({
        "update delivery_address",
        "set user_id = #{userId,jdbcType=BIGINT},",
          "name = #{name,jdbcType=VARCHAR},",
          "area_code = #{areaCode,jdbcType=VARCHAR},",
          "zip_code = #{zipCode,jdbcType=INTEGER},",
          "address = #{address,jdbcType=VARCHAR},",
          "phone = #{phone,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=SMALLINT},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP},",
          "code_address = #{codeAddress,jdbcType=VARCHAR},",
          "shop_id = #{shopId,jdbcType=INTEGER},",
          "longitude = #{longitude,jdbcType=REAL},",
          "latitude = #{latitude,jdbcType=REAL}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(DeliveryAddress record);
}