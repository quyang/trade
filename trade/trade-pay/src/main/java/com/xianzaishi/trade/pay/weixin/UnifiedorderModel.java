package com.xianzaishi.trade.pay.weixin;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.croky.util.NetUtils;
import com.xianzaishi.trade.pay.util.PayUtil;

/**
 * 统一下单MODEL
 * @author Croky.Zheng
 * 2016年9月13日
 */
@XmlRootElement(name="xml")//(name="pay_model",namespace=null)
@XmlAccessorType 
public class UnifiedorderModel implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2071713740081972442L;

	/**
	 * 应用ID
	 */
	private String appid = WeiXinPayConfig.APPID;
	
	/**
	 * 商户号
	 */
	private String mch_id = WeiXinPayConfig.MCHID;
	
	/**
	 * 设备信息
	 */
	private String device_info = WeiXinPayConfig.DEVICE_INFO;
	
	/**
	 * 随机字符串
	 */
	private String nonce_str = PayUtil.genRandomString();
	
	/**
	 * 签名
	 */
	private String sign;
	
	/**
	 * 商品描述
	 */
	private String body = "PAY";
	
	/**
	 * 商品详情
	 */
	private String detail = "XIANZAISHI_PAY";
	
	/**
	 * 附加数据
	 */
	private String attach = "Croky.Zheng";
	
	/**
	 * 商户订单号
	 */
	private String out_trade_no;
	
	/**
	 * 货币类型
	 */
	private String fee_type = "CNY";
	
	/**
	 * 总金额，单位为分
	 */
	private int total_fee = 0;
	/**
	 * 终端IP
	 */
	private String spbill_create_ip = NetUtils.getLocalAddress();
	
	/**
	 * 交易起始时间yyyyMMddHHmmss
	 */
	private String time_start;
	
	/**
	 * 交易结束时间yyyyMMddHHmmss
	 */
	private String time_expire;
	
	/**
	 * 商品标记
	 */
	private String goods_tag = "Croky"; 
	
	/**
	 * 异步通知URL
	 */
	private String notify_url = "http://trade.xianzaishi.com/order/weixinPayCallBack.htm";
	
	/**
	 * 交易类型
	 */
	private String trade_type = "APP";
	
	/**
	 * 用户标识
	 */
	private String openid;
	

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getFee_type() {
		return fee_type;
	}

	public void setFee_type(String fee_type) {
		this.fee_type = fee_type;
	}

	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}

	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}

	public String getTime_start() {
		return time_start;
	}

	public void setTime_start(String time_start) {
		this.time_start = time_start;
	}

	public String getTime_expire() {
		return time_expire;
	}

	public void setTime_expire(String time_expire) {
		this.time_expire = time_expire;
	}

	public String getGoods_tag() {
		return goods_tag;
	}

	public void setGoods_tag(String goods_tag) {
		this.goods_tag = goods_tag;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getTrade_type() {
		return trade_type;
	}

	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	public int getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(int total_fee) {
		this.total_fee = total_fee;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getMch_id() {
		return mch_id;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	public String getDevice_info() {
		return device_info;
	}

	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
}
