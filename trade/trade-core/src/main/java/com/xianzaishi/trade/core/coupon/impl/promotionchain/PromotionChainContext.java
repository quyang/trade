package com.xianzaishi.trade.core.coupon.impl.promotionchain;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.trade.client.vo.BuySkuInfoVO;
import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.dal.model.UserCoupon;

public class PromotionChainContext {

  // 付款sku列表
  private Map<Long, ItemCommoditySkuDTO> skuMaps;
  
  //skuPriceMaps 记录总价的key，其余的价格先根据商品标为key
  public static final String TOTAL_PRICE_PRE_KEY = "p_";
  
  // sku价格分别计算
  private Map<String,Integer> skuPriceMaps;

  /**
   * 购买情况
   */
  private List<BuySkuInfoVO> buyInfoList;

  // 付款渠道
  private int payChannel;
  
  /**
   * 用户id
   */
  private Long userId;

  // 优惠券类型集合
  private Set<Coupon> CouponList;

  // 用户优惠券可用列表
  private List<UserCoupon> userCouponList;

  // 用户选择使用优惠列表
  private List<UserCoupon> userSelectedCouponList;

  // 符合使用优惠列表
  private List<UserCoupon> canUseCouponList;

  // 使用用户选择优惠券userSelectedCouponList后，减去多少钱
  private int price;

  public Map<Long, ItemCommoditySkuDTO> getSkuMaps() {
    return skuMaps;
  }

  public void setSkuMaps(Map<Long, ItemCommoditySkuDTO> skuMaps) {
    this.skuMaps = skuMaps;
  }
  
  public Map<String, Integer> getSkuPriceMaps() {
    return skuPriceMaps;
  }

  public void setSkuPriceMaps(Map<String, Integer> skuPriceMaps) {
    this.skuPriceMaps = skuPriceMaps;
  }

  public Set<Coupon> getCouponList() {
    return CouponList;
  }

  public void setCouponList(Set<Coupon> couponList) {
    CouponList = couponList;
  }

  public List<UserCoupon> getUserCouponList() {
    return userCouponList;
  }

  public void setUserCouponList(List<UserCoupon> userCouponList) {
    this.userCouponList = userCouponList;
  }

  public List<UserCoupon> getCanUseCouponList() {
    return canUseCouponList;
  }

  public void setCanUseCouponList(List<UserCoupon> canUseCouponList) {
    this.canUseCouponList = canUseCouponList;
  }

  public int getPayChannel() {
    return payChannel;
  }

  public void setPayChannel(int payChannel) {
    this.payChannel = payChannel;
  }

  public List<UserCoupon> getUserSelectedCouponList() {
    return userSelectedCouponList;
  }

  public void setUserSelectedCouponList(List<UserCoupon> userSelectedCouponList) {
    this.userSelectedCouponList = userSelectedCouponList;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public List<BuySkuInfoVO> getBuyInfoList() {
    return buyInfoList;
  }

  public void setBuyInfoList(List<BuySkuInfoVO> buyInfoList) {
    this.buyInfoList = buyInfoList;
  }
  
}
