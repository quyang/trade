package com.xianzaishi.trade.core.map;

/**
 * 
 * @author Croky.Zheng
 * 2016年8月29日
 */
public interface MapService {

	String geocoder(double lat, double lng);

	String geocoder(String address);

	String search(String keyWord);

	String suggestion(String keyWord, String region);

	String district();

	String translate(double lat, double lng);

	String getPositionByIP(String ip);

	String distance(double fromLat, double fromLng, double toLat, double toLng);

	double getDistance(double fromLat, double fromLng, double toLat, double toLng);

	String streetview(double lat, double lng, int radius);

}
