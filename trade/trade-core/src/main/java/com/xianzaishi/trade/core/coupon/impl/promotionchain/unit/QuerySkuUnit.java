package com.xianzaishi.trade.core.coupon.impl.promotionchain.unit;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.client.vo.BuySkuInfoVO;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.PromotionChainContext;

/**
 * 查询sku信息
 * @author zhancang
 */
@Component(value = "querySkuUnit")
public class QuerySkuUnit extends ProcessorUnit{

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    String errorInfo = "";
    if(CollectionUtils.isEmpty(context.getBuyInfoList())){
      errorInfo = "[PromotionProcessorUnit]Input parameter skuIdList is empty,return null";
      log.error(errorInfo);
      return Result.getErrDataResult(-1, errorInfo);
    }
    
    List<Long> skuIds = Lists.newArrayList();
    for(BuySkuInfoVO sku:context.getBuyInfoList()){
      skuIds.add(sku.getSkuId());
    }
    Result<List<ItemCommoditySkuDTO>> skuResultList =
        skuService.queryItemSkuList(SkuQuery.querySkuBySkuIds(skuIds));
    if (null == skuResultList || !skuResultList.getSuccess()
        || skuResultList.getModule().size() != skuIds.size()) {
      log.error("[PromotionProcessorUnit]query sku return null,skuIds is:"+skuIds);
      return null;
    }
    Map<Long, ItemCommoditySkuDTO> skuM= Maps.newHashMap();
    for(ItemCommoditySkuDTO sku:skuResultList.getModule()){
      skuM.put(sku.getSkuId(), sku);
    }
    context.setSkuMaps(skuM);
    return Result.getSuccDataResult(true);
  }

}
