package com.alipay.config;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *版本：1.0
 *日期：2016-06-06
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfigV2 {

	// ↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 合作身份者ID，签约账号，以2088开头由16位纯数字组成的字符串，查看地址：https://openhome.alipay.com/platform/keyManage.htm?keyType=partner
	public static String partner = "2088421284384567";

	// 商户的私钥,需要PKCS8格式，RSA公私钥生成：https://doc.open.alipay.com/doc2/detail.htm?spm=a219a.7629140.0.0.nBDxfy&treeId=58&articleId=103242&docType=1
	public static String private_key = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBANXenWCCjvXwdDKEQNsfJgouLggj/Cx87LfSJ0MyL7UQH+atx8ShXgRVj4OZL6I/QYynzS5iihrCD5jo0dN84FG+SlN2/g7x8iaYJ+kqq4Gf6BWs2I7IZgRkzRaq1nhB1+nFtWDU/NbgJYFkgoKEijVl0hox47ksWk0LVeWdLvtjAgMBAAECgYEAzRTXXMBVTkjxY6+mcXiBBaoawfufyRlR9UM0Gx941+tKa+2gblE+0nEpWUv/fVmjBbmy6xPa0qXcRwiajG5murqMpbUkHdkq6zkNDmxSV1JrzwuYHAG3CCgDZT2kdNMEDF1njscQNx73YHW4pTafGryabwVdNhTytriw3J98NOECQQD3LsPjiLzCcvC5oM6VHYXHdYTC2udi/Dk1Ly8WftxfWpSE1vyvs2r4qWl+EZ1Lif9Hi1MXYHKjWVSyMyoLrjYdAkEA3X+jtWAoqZIgZLoCagNaaiN3rTM/XaUQll7fgnGzzpMKIXQecbYUU1v2mzz8bcKx3/Ips4giLHs+dw3BrWU/fwJBAJZ3Wzsow27CtRLqdpaC8CqouPY8dtnkm5ZqcImLE+7fnsT2cb8qwpU320Wox01yZXlRsHTsexxAhrQrPQ77L2kCQDIQcJNFactyIOpDdNo7actFuv4l8DOdZJNoXEKiqo6Ng6OuGFeBXTS+O445CaFReVzx4mUW5wqAzMyiCl3D3ccCQQDrfk2sUPU1KvjdpKhluFDws8tCvfGrhDDVM+FtWYFpnEW0MibOjEh+N/c+Bveyjs4bK3CVySPxbY6H5NGOQrmY";

	// 支付宝的公钥，查看地址：https://openhome.alipay.com/platform/keyManage.htm?keyType=partner
	public static String alipay_public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";
	// public static String alipay_public_key =
	// "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDV3p1ggo718HQyhEDbHyYKLi4II/wsfOy30idDMi+1EB/mrcfEoV4EVY+DmS+iP0GMp80uYooawg+Y6NHTfOBRvkpTdv4O8fImmCfpKquBn+gVrNiOyGYEZM0WqtZ4QdfpxbVg1PzW4CWBZIKChIo1ZdIaMeO5LFpNC1XlnS77YwIDAQAB";
	// public static String alipay_public_key =
	// "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";
	// 签名方式
	public static String sign_type = "RSA";

	// 调试用，创建TXT日志文件夹路径，见AlipayCore.java类中的logResult(String sWord)打印方法。
	public static String log_path = "/usr/works/apache-tomcat-7.0.70/logs";

	// 字符编码格式 目前支持 gbk 或 utf-8
	public static String input_charset = "utf-8";

	// 接收通知的接口名
	public static String service = "mobile.securitypay.pay";

	// 接收通知的URL
	public static String notify_url = "http://trade.xianzaishi.com/order/alipayCallBack.htm";

	// 正式环境配置
	public static String notify_url_daily = "http://trade.xianzaishi.net/order/alipayCallBack.htm";
	
	// 商户ID
	public static String seller_id = "pay@xianzaishi.com";

	// ↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
}
