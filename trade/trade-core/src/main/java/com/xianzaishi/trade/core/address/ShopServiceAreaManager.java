package com.xianzaishi.trade.core.address;

import java.util.List;

import com.xianzaishi.trade.core.model.AddressModel;
import com.xianzaishi.trade.dal.model.ShopServiceArea;

public interface ShopServiceAreaManager {


	int insert(int shopId, String code, int memberId);

	boolean delete(int id);

	AddressModel getShopServiceAreaDetail(int shopId);

	List<ShopServiceArea> getShopServiceAreaList();
}
