package com.xianzaishi.trade.core.map;

import javax.annotation.Resource;

import org.junit.Test;

import com.greenpineyu.fel.common.StringUtils;
import com.xianzaishi.trade.core.BaseCoreTest;
import com.xianzaishi.trade.core.map.MapService;
import com.xianzaishi.trade.utils.JsonConvert;

public class MapTest extends BaseCoreTest {

	@Resource
	private MapService mapService;
	
	@Test
	public void testGeocode() {
		String json = mapService.geocoder("上海市浦东新区东方路1367号");
		System.out.println(json);
		String location = getLocation("310114003",json);//JsonConvert.getValue(json, "geocodes[0].location");
		if (null != location) {
			String[] locations = location.split(",");
			String lngStr = locations[0];
			String latStr = locations[1];
			log.error(lngStr + ":" + latStr);
		}
	}
	
	private String getLocation(String areaCode,String json) {
		if (StringUtils.isNotEmpty(json)) {
			try {
				String countStr = JsonConvert.getValue(json, "count");
				int count = Integer.parseInt(countStr);
				for (int i=0; i<count; i++) {
					String code = JsonConvert.getValue(json,"geocodes[" + i + "]" + ".adcode");
					if (areaCode.startsWith(code)) {
						return JsonConvert.getValue(json, "geocodes[" + i + "]" + ".location");
					}
				}
			} catch (Exception e) {
				
			}
		}
		return null;
	}
}
