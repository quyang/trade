package com.xianzaishi.trade.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Croky.Zheng
 * 2016年4月7日
 * @param <E>
 */
public class PageList<E> extends ArrayList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3740195037480497444L;

	private Pagination pagination;
	
	public PageList(){
		super();
	}
	
	public PageList(int total) {
		super(total);
	}
	
	public PageList(Pagination pagination,List<E> objects) {
		this.pagination = pagination;
		if (null != objects) {
			this.addAll(objects);
		}
	}

	public long getTotal() {
		return pagination.getTotal();
	}
	
	public long getTotalPage() {
		return pagination.getTotalPage();
	}
	
	public long getPageSize() {
		return pagination.getPageSize();
	}
	
	public long getCurrentPage() {
		return pagination.getCurrentPage();
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
}
