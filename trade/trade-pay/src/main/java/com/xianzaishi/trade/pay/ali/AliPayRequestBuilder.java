package com.xianzaishi.trade.pay.ali;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.alipay.sign.RSA;
import com.xianzaishi.trade.utils.env.SpringContextUtil;

public class AliPayRequestBuilder {

	private Logger logger = LoggerFactory.getLogger(AliPayRequestBuilder.class);

	private AliPayRequestBuilder() {

	}

	public static AliPayRequestBuilder newInstance() {
		AliPayRequestBuilder builder = new AliPayRequestBuilder();

		builder.service = AlipayConfig.V1_PAY_SERVICE;
		builder.partner = AlipayConfig.V1_PARTNER;
		builder.charset = AlipayConfig.V1_INPUT_CHARSET;
		builder.signType = AlipayConfig.V1_SIGN_TYPE;
		if(SpringContextUtil.isOnline()){
		  builder.notifyUrl = AlipayConfig.V1_NOTIFY_URL;
		}else{
		  builder.notifyUrl = AlipayConfig.V1_NOTIFY_URL_DAILY;
		}
		
		builder.payment_type = AlipayConfig.V1_PARMENT_TYPE;
		builder.sellerId = AlipayConfig.V1_SELLER_ID;
		builder.expire = AlipayConfig.V1_IT_B_PAY;

		return builder;
	}

	public boolean valid() {
		return StringUtils.isNotBlank(outTradeNo) && StringUtils.isNotBlank(subject) && StringUtils.isNotBlank(total_fee)
				&& StringUtils.isNotBlank(body);
	}

	public String getRequestString() {
		Map<String, String> params = new HashMap<>();
		params.put("service", service);
		params.put("partner", partner);
		params.put("_input_charset", charset);
		params.put("notify_url", notifyUrl);
		params.put("out_trade_no", outTradeNo);
		params.put("subject", subject);
		params.put("payment_type", payment_type);
		params.put("seller_id", sellerId);
		params.put("total_fee", total_fee);
		params.put("body", body);

		List<String> keys = new ArrayList<String>(params.keySet());
		Collections.sort(keys);

		StringBuilder builder = new StringBuilder();

		try {
			for (String key : keys) {
				builder.append(key).append("=\"").append(params.get(key)).append("\"&");
			}
			if (builder.lastIndexOf("&") == builder.length() - 1) {
				builder.deleteCharAt(builder.length() - 1);
			}
		} catch (Exception e) {
			logger.error(String.format("Fail to encode alipay params[%s]", JSONObject.toJSONString(params)), e);
		}

		return builder.toString();
	}

	public String getRequestStringWithSign() {
		String request = getRequestString();

		StringBuilder builder = new StringBuilder(request);

		try {
			String sign = URLEncoder.encode(RSA.sign(request, AlipayConfig.V1_APP_RSA_PRIVATE, AlipayConfig.V1_INPUT_CHARSET),
					AlipayConfig.V1_INPUT_CHARSET);
			builder.append("&sign=\"").append(sign).append("\"&sign_type=\"").append(AlipayConfig.V1_SIGN_TYPE).append("\"");
		} catch (Exception e) {
			logger.error(String.format("Fail to sign alipay request[%s]", request), e);
		}

		return builder.toString();
	}

	/**
	 * 接口名称，固定值mobile.securitypay.pay
	 */
	private String service;

	/**
	 * 签约的支付宝账号对应的支付宝唯一用户号。以2088开头的16位纯数字组成。
	 */
	private String partner;

	/**
	 * 商户网站使用的编码格式，固定为UTF-8。
	 */
	private String charset;

	/**
	 * 签名类型，目前仅支持RSA。
	 */
	private String signType;

	/**
	 * 请参见签名。https://doc.open.alipay.com/doc2/detail?treeId=59&articleId=103927&
	 * docType=1
	 */
	private String sign;

	/**
	 * 支付宝服务器主动通知商户网站里指定的页面http路径。
	 */
	private String notifyUrl;

	/**
	 * 支付宝合作商户网站唯一订单号。
	 */
	private String outTradeNo;

	/**
	 * 商品的标题/交易标题/订单标题/订单关键字等。该参数最长为128个汉字。
	 */
	private String subject;

	/**
	 * 支付类型。默认值为：1（商品购买）。
	 */
	private String payment_type;

	/**
	 * 卖家支付宝账号（邮箱或手机号码格式）或其对应的支付宝唯一用户号（以2088开头的纯16位数字）。
	 */
	private String sellerId;

	/**
	 * 该笔订单的资金总额，单位为RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位。
	 */
	private String total_fee;

	/**
	 * 对一笔交易的具体描述信息。如果是多种商品，请将商品描述字符串累加传给body。
	 */
	private String body;

	/**
	 * 设置未付款交易的超时时间，一旦超时，该笔交易就会自动被关闭。当用户输入支付密码、点击确认付款后（即创建支付宝交易后）开始计时。取值范围：1m～
	 * 15d，或者使用绝对时间（示例格式：2014-06-13
	 * 16:00:00）。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。该参数数值不接受小数点，如1.
	 * 5h，可转换为90m。
	 */
	private String expire;

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getService() {
		return service;
	}

	public String getPartner() {
		return partner;
	}

	public String getCharset() {
		return charset;
	}

	public String getSignType() {
		return signType;
	}

	public String getSign() {
		return sign;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public String getSellerId() {
		return sellerId;
	}

	public String getExpire() {
		return expire;
	}
}
