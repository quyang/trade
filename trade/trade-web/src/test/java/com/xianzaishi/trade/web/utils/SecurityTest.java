package com.xianzaishi.trade.web.utils;

import java.net.URLEncoder;
import java.util.Date;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.xianzaishi.trade.web.controller.CouponController.UserCouponParams;

public class SecurityTest {

	@Test
	public void test01() throws Exception{
		UserCouponParams params = new UserCouponParams(425, 1249, (new Date()).getTime());
		String data = SecurityUtils.encode(JSONObject.toJSONString(params));
		//String data = "jhpDOFMU6nxUgroLQfRVD+/ckJcopHt1gVG7ybeSX5RjY64esjiuwEE6nslqVRe6Gacz3XNU3a/fvjp6UNvIRg==";
		System.out.println("URLEncode -> " + URLEncoder.encode(data, "utf-8"));
		System.out.println("source -> " + SecurityUtils.decode(data));
		System.out.println("sign -> " + ClientSecurityUtils.sign(data));
		System.out.println("URLEncode -> " + URLEncoder.encode(ClientSecurityUtils.sign(data), "utf-8"));
		System.out.println("verify -> " + SecurityUtils.verify(data, ClientSecurityUtils.sign(data)));
	}
}
