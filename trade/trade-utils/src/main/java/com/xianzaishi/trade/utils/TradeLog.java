package com.xianzaishi.trade.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TradeLog {

	private final static Logger logger = LoggerFactory.getLogger("trade");

	public static void error(Throwable e, String format, Object... args) {
		if (args != null && args.length > 0) {
			logger.error(String.format(format, args), e);
		} else {
			logger.error(format, e);
		}
	}
	
	public static void error(String format, Object... args) {
		if (args != null && args.length > 0) {
			logger.error(String.format(format, args));
		} else {
			logger.error(format);
		}
	}

	public static void warn(Throwable e, String format, Object... args) {
		if (args != null && args.length > 0) {
			logger.warn(String.format(format, args), e);
		} else {
			logger.warn(format, e);
		}
	}
	
	public static void warn(String format, Object... args) {
		if (args != null && args.length > 0) {
			logger.warn(String.format(format, args));
		} else {
			logger.warn(format);
		}
	}

	public static void info(Throwable e, String format, Object... args) {
		if (args != null && args.length > 0) {
			logger.info(String.format(format, args), e);
		} else {
			logger.info(format, e);
		}
	}
	
	public static void info(String format, Object... args) {
		if (args != null && args.length > 0) {
			logger.info(String.format(format, args));
		} else {
			logger.info(format);
		}
	}

	public static void debug(Throwable e, String format, Object... args) {
		if (args != null && args.length > 0) {
			logger.debug(String.format(format, args), e);
		} else {
			logger.debug(format, e);
		}
	}
	
	public static void debug(String format, Object... args) {
		if (args != null && args.length > 0) {
			logger.debug(String.format(format, args));
		} else {
			logger.debug(format);
		}
	}

	public static Logger getLogger() {
		return logger;
	}

}
