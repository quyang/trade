package com.xianzaishi.trade.dal.dao;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.trade.dal.model.UserCouponExample.Criteria;
import com.xianzaishi.trade.dal.model.UserCouponExample.Criterion;
import com.xianzaishi.trade.dal.model.UserCouponExample;
import java.util.List;
import java.util.Map;

public class UserCouponSqlProvider {

    public String countByExample(UserCouponExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("user_coupon");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(UserCouponExample example) {
        BEGIN();
        DELETE_FROM("user_coupon");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(UserCoupon record) {
        BEGIN();
        INSERT_INTO("user_coupon");
        
        if (record.getUserId() != null) {
            VALUES("user_id", "#{userId,jdbcType=BIGINT}");
        }
        
        if (record.getCouponId() != null) {
            VALUES("coupon_id", "#{couponId,jdbcType=BIGINT}");
        }
        
        if (record.getCouponTitle() != null) {
            VALUES("coupon_title", "#{couponTitle,jdbcType=VARCHAR}");
        }
        
        if (record.getCouponStartTime() != null) {
            VALUES("coupon_start_time", "#{couponStartTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCouponEndTime() != null) {
            VALUES("coupon_end_time", "#{couponEndTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getOrderId() != null) {
            VALUES("order_id", "#{orderId,jdbcType=BIGINT}");
        }
        
        if (record.getCouponRules() != null) {
            VALUES("coupon_rules", "#{couponRules,jdbcType=VARCHAR}");
        }
        
        if (record.getGmtCreate() != null) {
            VALUES("gmt_create", "#{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            VALUES("gmt_modify", "#{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            VALUES("status", "#{status,jdbcType=SMALLINT}");
        }
        
        if (record.getAmount() != null) {
            VALUES("amount", "#{amount,jdbcType=DOUBLE}");
        }
        
        if (record.getCouponType() != null) {
            VALUES("coupon_type", "#{couponType,jdbcType=SMALLINT}");
        }
        
        if (record.getChannelType() != null) {
            VALUES("channel_type", "#{channelType,jdbcType=SMALLINT}");
        }
        
        if (record.getAmountLimit() != null) {
            VALUES("amount_limit", "#{amountLimit,jdbcType=DOUBLE}");
        }
        
        return SQL();
    }

    public String selectByExample(UserCouponExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("user_id");
        SELECT("coupon_id");
        SELECT("coupon_title");
        SELECT("coupon_start_time");
        SELECT("coupon_end_time");
        SELECT("order_id");
        SELECT("coupon_rules");
        SELECT("gmt_create");
        SELECT("gmt_modify");
        SELECT("status");
        SELECT("amount");
        SELECT("coupon_type");
        SELECT("channel_type");
        SELECT("amount_limit");
        FROM("user_coupon");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        UserCoupon record = (UserCoupon) parameter.get("record");
        UserCouponExample example = (UserCouponExample) parameter.get("example");
        
        BEGIN();
        UPDATE("user_coupon");
        
        if (record.getId() != null) {
            SET("id = #{record.id,jdbcType=BIGINT}");
        }
        
        if (record.getUserId() != null) {
            SET("user_id = #{record.userId,jdbcType=BIGINT}");
        }
        
        if (record.getCouponId() != null) {
            SET("coupon_id = #{record.couponId,jdbcType=BIGINT}");
        }
        
        if (record.getCouponTitle() != null) {
            SET("coupon_title = #{record.couponTitle,jdbcType=VARCHAR}");
        }
        
        if (record.getCouponStartTime() != null) {
            SET("coupon_start_time = #{record.couponStartTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCouponEndTime() != null) {
            SET("coupon_end_time = #{record.couponEndTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getOrderId() != null) {
            SET("order_id = #{record.orderId,jdbcType=BIGINT}");
        }
        
        if (record.getCouponRules() != null) {
            SET("coupon_rules = #{record.couponRules,jdbcType=VARCHAR}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{record.status,jdbcType=SMALLINT}");
        }
        
        if (record.getAmount() != null) {
            SET("amount = #{record.amount,jdbcType=DOUBLE}");
        }
        
        if (record.getCouponType() != null) {
            SET("coupon_type = #{record.couponType,jdbcType=SMALLINT}");
        }
        
        if (record.getChannelType() != null) {
            SET("channel_type = #{record.channelType,jdbcType=SMALLINT}");
        }
        
        if (record.getAmountLimit() != null) {
            SET("amount_limit = #{record.amountLimit,jdbcType=DOUBLE}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("user_coupon");
        
        SET("id = #{record.id,jdbcType=BIGINT}");
        SET("user_id = #{record.userId,jdbcType=BIGINT}");
        SET("coupon_id = #{record.couponId,jdbcType=BIGINT}");
        SET("coupon_title = #{record.couponTitle,jdbcType=VARCHAR}");
        SET("coupon_start_time = #{record.couponStartTime,jdbcType=TIMESTAMP}");
        SET("coupon_end_time = #{record.couponEndTime,jdbcType=TIMESTAMP}");
        SET("order_id = #{record.orderId,jdbcType=BIGINT}");
        SET("coupon_rules = #{record.couponRules,jdbcType=VARCHAR}");
        SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        SET("status = #{record.status,jdbcType=SMALLINT}");
        SET("amount = #{record.amount,jdbcType=DOUBLE}");
        SET("coupon_type = #{record.couponType,jdbcType=SMALLINT}");
        SET("channel_type = #{record.channelType,jdbcType=SMALLINT}");
        SET("amount_limit = #{record.amountLimit,jdbcType=DOUBLE}");
        
        UserCouponExample example = (UserCouponExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(UserCoupon record) {
        BEGIN();
        UPDATE("user_coupon");
        
        if (record.getUserId() != null) {
            SET("user_id = #{userId,jdbcType=BIGINT}");
        }
        
        if (record.getCouponId() != null) {
            SET("coupon_id = #{couponId,jdbcType=BIGINT}");
        }
        
        if (record.getCouponTitle() != null) {
            SET("coupon_title = #{couponTitle,jdbcType=VARCHAR}");
        }
        
        if (record.getCouponStartTime() != null) {
            SET("coupon_start_time = #{couponStartTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCouponEndTime() != null) {
            SET("coupon_end_time = #{couponEndTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getOrderId() != null) {
            SET("order_id = #{orderId,jdbcType=BIGINT}");
        }
        
        if (record.getCouponRules() != null) {
            SET("coupon_rules = #{couponRules,jdbcType=VARCHAR}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{status,jdbcType=SMALLINT}");
        }
        
        if (record.getAmount() != null) {
            SET("amount = #{amount,jdbcType=DOUBLE}");
        }
        
        if (record.getCouponType() != null) {
            SET("coupon_type = #{couponType,jdbcType=SMALLINT}");
        }
        
        if (record.getChannelType() != null) {
            SET("channel_type = #{channelType,jdbcType=SMALLINT}");
        }
        
        if (record.getAmountLimit() != null) {
            SET("amount_limit = #{amountLimit,jdbcType=DOUBLE}");
        }
        
        WHERE("id = #{id,jdbcType=BIGINT}");
        
        return SQL();
    }

    protected void applyWhere(UserCouponExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}