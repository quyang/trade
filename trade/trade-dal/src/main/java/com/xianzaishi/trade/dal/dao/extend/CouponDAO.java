package com.xianzaishi.trade.dal.dao.extend;

import java.util.List;

import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import com.xianzaishi.trade.dal.dao.CouponMapper;
import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.utils.Pagination;

public interface CouponDAO extends CouponMapper {
    @Select({
        "select",
        "id, title, rules, crowd_id, type, gmt_start, gmt_end, gmt_create, gmt_modify, ",
        "status, amount, channel_type, amount_limit",
        "from coupon",
        "where status in (${status})",
		"order by id desc limit #{pagination.offset},#{pagination.limit}"
    })
    @Options(useCache=true,flushCache=false, timeout=20000)
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="title", property="title", jdbcType=JdbcType.VARCHAR),
        @Result(column="rules", property="rules", jdbcType=JdbcType.VARCHAR),
        @Result(column="crowd_id", property="crowdId", jdbcType=JdbcType.BIGINT),
        @Result(column="type", property="type", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_start", property="gmtStart", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_end", property="gmtEnd", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount", property="amount", jdbcType=JdbcType.DOUBLE),
        @Result(column="channel_type", property="channelType", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount_limit", property="amountLimit", jdbcType=JdbcType.DOUBLE)
    })
    List<Coupon> selectByStatus(@Param("status") String status,@Param("pagination") Pagination pagination);
    
    @Select({
        "select",
        "count(1)",
        "from coupon",
        "where status in (${status})"
    })
    @Options(useCache=true,flushCache=false, timeout=20000)
    int countByStatus(@Param("status") String status);
}
