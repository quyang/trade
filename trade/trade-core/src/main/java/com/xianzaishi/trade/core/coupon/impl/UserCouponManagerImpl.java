package com.xianzaishi.trade.core.coupon.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.xianzaishi.trade.core.coupon.CouponManager;
import com.xianzaishi.trade.core.coupon.UserCouponManager;
import com.xianzaishi.trade.dal.dao.extend.UserCouponDAO;
import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.trade.dal.model.UserCouponExample;
import com.xianzaishi.trade.utils.ExpressionCalculator;
import com.xianzaishi.trade.utils.enums.Status;

/**
 * 
 * @author Croky.Zheng 2016年9月26日
 */
@Component(value = "userCouponManager")
public class UserCouponManagerImpl implements UserCouponManager {
    private static final Logger log = LoggerFactory.getLogger(UserCouponManagerImpl.class);

    @Autowired
    private CouponManager couponManager;

    @Resource
    private UserCouponDAO userCouponDAO;

    private Map<String, Double> valueMap = new HashMap<String, Double>();

    @Override
    public synchronized double calculate(long userCouponId, double amount) {
        UserCoupon userCoupon = get(userCouponId);
        return calculate(userCoupon, amount);
    }

    @Override
    public synchronized double calculate(UserCoupon userCoupon, double amount) {
        if (null != userCoupon) {
            valueMap.put("金额", amount);
            Object result = ExpressionCalculator.calcul(userCoupon.getCouponRules(), valueMap);
            if (null != result) {
                return (double) result;
            }
        }
        return 0.0;
    }

    @Override
    public double useUserCoupon(long userId, long userCouponId, long orderId, double amount) {
        if (use(userId, userCouponId, orderId)) {
            return calculate(userCouponId, amount);
        }
        return 0.0;
    }

    @Override
    public boolean updateUserCouponOrderId(long userId, long userCouponId, long orderId) {
        UserCoupon userCoupon = get(userCouponId);
        if (null != userCoupon) {
            if (userCoupon.getUserId() != userId) {
                throw new RuntimeException("关联他人的优惠券");
            }
            userCoupon.setOrderId(orderId);
            try {
                return userCouponDAO.updateByPrimaryKey(userCoupon) > 0;
            } catch (DataAccessException e) {
                log.error("select UserCoupon failed!", e);
            }
        }
        return false;
    }

    @Override
    public long insert(long userId, long couponId) {
        Coupon coupon = couponManager.getCoupon(couponId);
        if (null != coupon) {
            UserCoupon userCoupon = new UserCoupon();
            userCoupon.setCouponEndTime(coupon.getGmtEnd());
            userCoupon.setCouponId(coupon.getId());
            userCoupon.setCouponRules(coupon.getRules());
            userCoupon.setCouponStartTime(coupon.getGmtStart());
            userCoupon.setCouponTitle(coupon.getTitle());
            userCoupon.setAmount(coupon.getAmount());
            userCoupon.setCouponType(coupon.getType());
            userCoupon.setOrderId(0L);
            userCoupon.setUserId(userId);
            userCoupon.setChannelType(coupon.getChannelType());
            userCoupon.setAmountLimit(coupon.getAmountLimit());
            return insert(userCoupon);
        }
        throw new RuntimeException("不存在的优惠券ID");
    }

    @Override
    public long insertCoupon(long userId, Coupon coupon) {

        if (userId <= 0 || coupon == null) {
            throw new IllegalArgumentException("参数错误");
        }

        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setCouponEndTime(coupon.getGmtEnd());
        userCoupon.setCouponId(coupon.getId());
        userCoupon.setCouponRules(coupon.getRules());
        userCoupon.setCouponStartTime(coupon.getGmtStart());
        userCoupon.setCouponTitle(coupon.getTitle());
        userCoupon.setAmount(coupon.getAmount());
        userCoupon.setCouponType(coupon.getType());
        userCoupon.setOrderId(0L);
        userCoupon.setUserId(userId);
        userCoupon.setChannelType(coupon.getChannelType());
        userCoupon.setAmountLimit(coupon.getAmountLimit());
        return insert(userCoupon);
    }

    @Override
    public List<UserCoupon> getUserCoupons(long userId) {
        UserCouponExample example = new UserCouponExample();
        example.createCriteria().andUserIdEqualTo(userId);
        try {
            return userCouponDAO.selectByExample(example);
        } catch (DataAccessException e) {
            log.error("select UserCoupon failed!", e);
        }
        return null;
    }

    @Override
    public List<UserCoupon> getUserCouponByOrderId(long userId, long oid) {
        UserCouponExample example = new UserCouponExample();
        example.createCriteria().andUserIdEqualTo(userId).andOrderIdEqualTo(oid);
        try {
            return userCouponDAO.selectByExample(example);
        } catch (DataAccessException e) {
            log.error("select UserCoupon failed!", e);
        }
        return null;
    }

    @Override
    public boolean updateStatus(long userCouponId, Status status) {
        UserCoupon userCoupon = get(userCouponId);
        if (null != userCoupon) {
            userCoupon.setStatus(status.getValue());
            try {
                return userCouponDAO.updateByPrimaryKey(userCoupon) > 0;
            } catch (DataAccessException e) {
                log.error("select UserCoupon failed!", e);
            }
        }
        return false;
    }

    @Override
    public List<UserCoupon> getUserCouponsByStatus(long userId, List<Short> couponTypes, Status status) {
        UserCouponExample example = new UserCouponExample();
        example.createCriteria().andUserIdEqualTo(userId)// 用户
                .andCouponTypeIn(couponTypes)// 类型
                .andStatusEqualTo(status.getValue());// 状态
        try {
            return userCouponDAO.selectByExample(example);
        } catch (DataAccessException e) {
            log.error("select UserCoupon failed!", e);
        }
        return null;
    }

    private boolean use(long userId, long userCouponId, long orderId) {
        UserCoupon userCoupon = get(userCouponId);
        if (null != userCoupon) {
            if (userCoupon.getUserId() != userId) {
                throw new RuntimeException("不能使用他人的优惠券");
            }
            if (userCoupon.getCouponEndTime().getTime() < new Date().getTime()) {
                throw new RuntimeException("优惠券已经过期,不能使用");
            }
            if (Status.VALID.getValue() != userCoupon.getStatus()) {
                throw new RuntimeException("优惠券已经使用过");
            }
            userCoupon.setStatus(Status.INIT.getValue());
            userCoupon.setOrderId(orderId);
            try {
                return userCouponDAO.updateByPrimaryKey(userCoupon) > 0;
            } catch (DataAccessException e) {
                log.error("select UserCoupon failed!", e);
            }
        }
        return false;
    }

    private UserCoupon get(long userCouponId) {
        try {
            return userCouponDAO.selectByPrimaryKey(userCouponId);
        } catch (DataAccessException e) {
            log.error("select UserCoupon failed!", e);
        }
        return null;
    }

    private long insert(UserCoupon userCoupon) {
        Date now = new Date();
        userCoupon.setGmtCreate(now);
        userCoupon.setGmtModify(now);
        userCoupon.setStatus(Status.VALID.getValue());
        try {
            if (userCouponDAO.insert(userCoupon) > 0) {
                return userCoupon.getId();
            }
        } catch (DataAccessException e) {
            log.error("isnert UserCoupon failed!", e);
        }
        return 0L;
    }

    @Override
    public UserCoupon getUserCouponById(long couponId) {
        UserCoupon userCoupon = null;
        try {
            userCoupon = userCouponDAO.selectByPrimaryKey(couponId);
        } catch (Exception e) {
            log.error("Fail to query user coupon by id = " + couponId, e);
        }
        return userCoupon;
    }

    @Override
    public List<UserCoupon> getUserCouponsByCouponType(long userId, List<Short> couponTypes) {

        List<UserCoupon> results = new ArrayList<>();

        try {
            UserCouponExample query = new UserCouponExample();
            query.createCriteria().andUserIdEqualTo(userId).andCouponTypeIn(couponTypes);

            List<UserCoupon> db = userCouponDAO.selectByExample(query);
            if (db != null) {
                results.addAll(db);
            }
        } catch (Exception e) {
            log.error("Fail to query user coupons by couponTypes", e);
        }

        return results;
    }
}