package com.xianzaishi.trade.dal;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-dal.xml")
@Transactional  
public class BaseDalTest extends AbstractJUnit4SpringContextTests {
	public static final Logger log = LoggerFactory.getLogger(BaseDalTest.class);

}
