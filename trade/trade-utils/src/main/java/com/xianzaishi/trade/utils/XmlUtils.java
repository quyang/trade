package com.xianzaishi.trade.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.xianzaishi.trade.utils.model.WebCheckFilterParam;

/**
 * 
 * @author Croky.Zheng
 * 2016年9月17日
 */
public class XmlUtils {

	/**
	 * 获取WebInitCheckFilter
	 * @return
	 */
	public static WebCheckFilterParam getAllowURLsByWebInitCheckFilter(String filePath) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); 
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document document = db.parse(filePath); 
			WebCheckFilterParam param = new WebCheckFilterParam();
			Node securityNode = getChildByName(document.getFirstChild(),"security");
			Node loginUrlNode = getChildByName(securityNode,"loginUrl");
			if (null != loginUrlNode) {
				//logUrlNode.getNodeValue()
				param.setLoginUrl(loginUrlNode.getTextContent());
			}
			Node allowablesNode = getChildByName(securityNode,"allowables");
			List<Node> nodes = getChildsByName(allowablesNode,"allowable");
			if ((null != nodes) && (nodes.size() > 0)) {
				Set<String> allowUrlSet = new HashSet<String>(nodes.size() * 2);
				for (Node node : nodes) {
					allowUrlSet.add(node.getTextContent());
				}
				param.setAllowUrlSet(allowUrlSet);
			}
			return param;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	//获取所有的孩子节点 
	private static List<Node> getChildsByName(Node node,String targetName) {
		if (null != node) {
			NodeList nodes = node.getChildNodes();
			List<Node> nodeList = new ArrayList<Node>();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node child = nodes.item(i);
				if (child.getNodeName().equalsIgnoreCase(targetName)) {
					nodeList.add(child);
				}
			}
			return nodeList;
		}
		return null;
	}
	
	//返回第一个符合targetName的孩子节点
	private static Node getChildByName(Node node,String targetName) {
		NodeList nodes = node.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node child = nodes.item(i);
			if (child.getNodeName().equalsIgnoreCase(targetName)) {
				return child;
			}
		}
		return null;
	}
}
