package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.dal.model.CouponExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface CouponMapper {
    @SelectProvider(type=CouponSqlProvider.class, method="countByExample")
    int countByExample(CouponExample example);

    @DeleteProvider(type=CouponSqlProvider.class, method="deleteByExample")
    int deleteByExample(CouponExample example);

    @Delete({
        "delete from coupon",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into coupon (title, rules, ",
        "crowd_id, type, gmt_start, ",
        "gmt_end, gmt_day_duration, gmt_create, ",
        "gmt_modify, status, ",
        "amount, channel_type, ",
        "amount_limit)",
        "values (#{title,jdbcType=VARCHAR}, #{rules,jdbcType=VARCHAR}, ",
        "#{crowdId,jdbcType=BIGINT}, #{type,jdbcType=SMALLINT}, #{gmtStart,jdbcType=TIMESTAMP}, ",
        "#{gmtEnd,jdbcType=TIMESTAMP},  #{gmtDayDuration,jdbcType=INTEGER}, #{gmtCreate,jdbcType=TIMESTAMP}, ",
        "#{gmtModify,jdbcType=TIMESTAMP}, #{status,jdbcType=SMALLINT}, ",
        "#{amount,jdbcType=DOUBLE}, #{channelType,jdbcType=SMALLINT}, ",
        "#{amountLimit,jdbcType=DOUBLE})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(Coupon record);

    @InsertProvider(type=CouponSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insertSelective(Coupon record);

    @SelectProvider(type=CouponSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="title", property="title", jdbcType=JdbcType.VARCHAR),
        @Result(column="rules", property="rules", jdbcType=JdbcType.VARCHAR),
        @Result(column="crowd_id", property="crowdId", jdbcType=JdbcType.BIGINT),
        @Result(column="type", property="type", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_start", property="gmtStart", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_end", property="gmtEnd", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_day_duration", property="gmtDayDuration", jdbcType=JdbcType.INTEGER),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount", property="amount", jdbcType=JdbcType.DOUBLE),
        @Result(column="channel_type", property="channelType", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount_limit", property="amountLimit", jdbcType=JdbcType.DOUBLE)
    })
    List<Coupon> selectByExample(CouponExample example);

    @Select({
        "select",
        "id, title, rules, crowd_id, type, gmt_start, gmt_end, gmt_day_duration, gmt_create, gmt_modify, ",
        "status, amount, channel_type, amount_limit",
        "from coupon",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="title", property="title", jdbcType=JdbcType.VARCHAR),
        @Result(column="rules", property="rules", jdbcType=JdbcType.VARCHAR),
        @Result(column="crowd_id", property="crowdId", jdbcType=JdbcType.BIGINT),
        @Result(column="type", property="type", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_start", property="gmtStart", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_end", property="gmtEnd", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_day_duration", property="gmtDayDuration", jdbcType=JdbcType.INTEGER),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount", property="amount", jdbcType=JdbcType.DOUBLE),
        @Result(column="channel_type", property="channelType", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount_limit", property="amountLimit", jdbcType=JdbcType.DOUBLE)
    })
    Coupon selectByPrimaryKey(Long id);

    @UpdateProvider(type=CouponSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Coupon record, @Param("example") CouponExample example);

    @UpdateProvider(type=CouponSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Coupon record, @Param("example") CouponExample example);

    @UpdateProvider(type=CouponSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Coupon record);

    @Update({
        "update coupon",
        "set title = #{title,jdbcType=VARCHAR},",
          "rules = #{rules,jdbcType=VARCHAR},",
          "crowd_id = #{crowdId,jdbcType=BIGINT},",
          "type = #{type,jdbcType=SMALLINT},",
          "gmt_start = #{gmtStart,jdbcType=TIMESTAMP},",
          "gmt_end = #{gmtEnd,jdbcType=TIMESTAMP},",
          "gmt_day_duration = #{gmtDayDuration,jdbcType=INTEGER},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP},",
          "status = #{status,jdbcType=SMALLINT},",
          "amount = #{amount,jdbcType=DOUBLE},",
          "channel_type = #{channelType,jdbcType=SMALLINT},",
          "amount_limit = #{amountLimit,jdbcType=DOUBLE}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Coupon record);
}