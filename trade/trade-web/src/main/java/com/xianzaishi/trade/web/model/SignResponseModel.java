package com.xianzaishi.trade.web.model;

import com.xianzaishi.trade.web.utils.SecurityUtils;

/**
 * 统一JSON返回值
 * 
 * @author Croky.Zheng 2016年3月30日
 */
public class SignResponseModel {

	private String sign;

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public SignResponseModel() {
	}

	public SignResponseModel(int code, String message) {
		this.code = code;
		this.message = message;
		try {
			this.sign = SecurityUtils.sign(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public SignResponseModel(int code, String message, Object data) {
		this.code = code;
		this.message = message;
		this.data = data;
		try {
			this.sign = SecurityUtils.sign(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public SignResponseModel(Object data) {
		this.data = data;
		try {
			this.sign = SecurityUtils.sign(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected int code = 0;
	
	/**
	 * 错误信息
	 */
	protected String message = "成功";
	/**
	 * 返回的具体实体对象
	 */
	protected Object data = null;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
