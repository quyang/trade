package com.xianzaishi.trade.utils.env;

public class EnvUtil {

  private String env;

  public String getEnv() {
    return env;
  }

  public void setEnv(String env) {
    this.env = env;
  }
}
