package com.xianzaishi.trade.pay.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.croky.util.ByteUtils;
import com.croky.util.JvmUtils;
import com.croky.util.ReflectUtils;
import com.croky.util.StringUtils;
import com.xianzaishi.trade.pay.weixin.WeiXinPayConfig;

/**
 * 
 * @author Croky.Zheng
 * 2016年9月14日
 */
public class PayUtil {

	public static String obj2Link(Object obj) {
		return obj2Link(obj,"=","&",null);
	}

	private static Set<String> weixinFilterSet = new HashSet<String>(1);
	
	static {
		weixinFilterSet.add("sign");
	}
	
	public static String signByWeiXin(Object obj) {
		return signByWeiXin(obj,WeiXinPayConfig.APP_KEY);
	}
	
	public static String signByWeiXin(Object obj,String key) {
		String link = obj2Link(obj);
		return StringUtils.md5String(link + "&key=" + key).toUpperCase();
	}
	
	public static String obj2LinkByWeiXin(Object obj) {
		return obj2Link(obj,"=","&",weixinFilterSet);
	}
	
	public static String obj2Link(Object obj,String keyLinkSymbol,String fieldLinkSymbol,Set<String> filterSet) {
		Map<String,Object> fieldMap = ReflectUtils.objectToMap(obj);
		fieldMap.remove("serialVersionUID");
		fieldMap.remove("sign");
		fieldMap.put("package",fieldMap.remove("pack"));
		
		List<String> keys = new ArrayList<String>(fieldMap.keySet());
        Collections.sort(keys);
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
			if ((null != filterSet) && filterSet.contains(key)) {//key.equals("sign")
				continue;
			}
            Object vObj = fieldMap.get(key);
            if (null == vObj) {
            	continue;
            }
            String value = vObj.toString();

            if (i == keys.size() - 1) {//拼接时，不包括最后一个&字符
                buf.append(key).append(keyLinkSymbol).append(value);
            } else {
                buf.append(key).append(keyLinkSymbol).append(value).append(fieldLinkSymbol);
            }
        }
		return buf.toString();
	}
	

	public static String obj2XML(Object obj) {
		Map<String,Object> fieldMap = ReflectUtils.objectToMap(obj);
		StringBuffer buf = new StringBuffer();
		buf.append("<xml>").append(JvmUtils.getCrlfLine());
		for (Entry<String,Object> entry : fieldMap.entrySet()) {
			if (null == entry.getKey() || null == entry.getValue()) {
				continue;
			}
			buf.append("<")
			.append(entry.getKey())
			.append(">")
			.append("<![CDATA[")
			.append(entry.getValue())
			.append("]]>")
			.append("</")
			.append(entry.getKey())
			.append(">")
			.append(JvmUtils.getCrlfLine());
		}
		buf.append("</xml>");
		return buf.toString();
	}
	

	public static String objectToXML(Object object) {
		return objectToXML(object,"utf-8");
	}
	
	public static String objectToXML(Object object,String encoding) {
		try {
			JAXBContext context = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = context.createMarshaller();
			ByteArrayOutputStream baos = new ByteArrayOutputStream(4096);
			marshaller.setProperty(Marshaller.JAXB_ENCODING,encoding);
			//是否添加格式字符,默认false
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
			//不添加xml头,默认为false
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT,true);
			//添加xsi:noNamespaceSchemaLocation xmlns:xsi信息到根节点
			//marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION,"http://www.xianzaishi.com/xml/schema");
			//添加xsi:schemaLocation信息到根节点
			//marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,"/Users/croky/xml/schema");
			marshaller.marshal(object, baos);
			return new String(baos.toByteArray(),encoding);
		} catch (JAXBException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public static <T> T xmlToObject(String xml,Class<T> clazz) {
		return xmlToObject(xml,"utf-8",clazz);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T xmlToObject(String xml,String encoding,Class<T> clazz) {
		try {
			JAXBContext context = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			//unmarshaller.setProperty(Marshaller.JAXB_ENCODING,encoding);
			//是否添加格式字符,默认false
			//unmarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
			//不添加xml头,默认为false
			//unmarshaller.setProperty(Marshaller.JAXB_FRAGMENT,true);
			//添加xsi:noNamespaceSchemaLocation xmlns:xsi信息到根节点
			//marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION,"http://www.xianzaishi.com/xml/schema");
			//添加xsi:schemaLocation信息到根节点
			//marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,"/Users/croky/xml/schema");
			ByteArrayInputStream bis = new ByteArrayInputStream(xml.getBytes(encoding));
			return (T) unmarshaller.unmarshal(bis);
		} catch (JAXBException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	private static Random r = new Random();
	
	/*
	 * 生成随机字符串
	 */
	public static synchronized String genRandomString() {
		long a  = r.nextInt();
		long b = r.nextInt();
		long c = (a << 32)  | (b&0xFFFFFFFFL);
		return StringUtils.toHexBase(ByteUtils.fromLong(c),true,false);
	}
}
