package com.xianzaishi.trade.web.utils;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 *
 * @author nianyue.hyj
 * @since 2016.10.17
 */
public class HttpServletUtils {

	private final static Logger logger = LoggerFactory.getLogger(HttpServletUtils.class);

	public static void write(HttpServletResponse response, String content) {

		if (response != null && content != null) {
			PrintWriter writer = null;
			try {
				writer = response.getWriter();
				writer.write(content);
			} catch (Exception e) {
				logger.error(String.format("Fail to write content[%s] to http response.", content), e);
			} finally {
				if (writer != null) {
					writer.flush();
				}
			}
		}

	}

}
