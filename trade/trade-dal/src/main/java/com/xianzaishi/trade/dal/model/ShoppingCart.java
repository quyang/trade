package com.xianzaishi.trade.dal.model;

import java.io.Serializable;
import java.util.Date;

public class ShoppingCart implements Serializable {
    private Long id;

    private Long userId;

    private Long itemId;

    private Long itemSkuId;

    private String itemSkuInfo;

    private String itemName;

    private String itemIconUrl;

    private Double itemBillPrice;

    private Double itemEffePrice;

    private Double itemCount;
    
    private String itemTags;

    private String attribute;

    private Double billAmount;

    private Double effeAmount;

    private Integer deviceId;

    private Integer operator;

    private Short status;

    private Date gmtCreate;

    private Date gmtModify;

    private Integer itemCategoryId;

    /**
     * sku类型，称重类型或者非称重类型.1:非称重类型，2:称重类型
     */
    private Short skuType;
    
    /**
     * 一份商品的重量，针对非称重商品代表1，针对称重商品代表多少g，主要在计算库存时使用
     */
    private Integer unitWeightNum;
    
    //商品条形码
    private String skuBarCode;//商品条形码(处理临期商品或者批次商品时，用条形码确定某个确定的sku)
    
    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getItemSkuId() {
        return itemSkuId;
    }

    public void setItemSkuId(Long itemSkuId) {
        this.itemSkuId = itemSkuId;
    }

    public String getItemSkuInfo() {
        return itemSkuInfo;
    }

    public void setItemSkuInfo(String itemSkuInfo) {
        this.itemSkuInfo = itemSkuInfo == null ? null : itemSkuInfo.trim();
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName == null ? null : itemName.trim();
    }

    public String getItemIconUrl() {
        return itemIconUrl;
    }

    public void setItemIconUrl(String itemIconUrl) {
        this.itemIconUrl = itemIconUrl == null ? null : itemIconUrl.trim();
    }

    public Double getItemBillPrice() {
        return itemBillPrice;
    }

    public void setItemBillPrice(Double itemBillPrice) {
        this.itemBillPrice = itemBillPrice;
    }

    public Double getItemEffePrice() {
        return itemEffePrice;
    }

    public void setItemEffePrice(Double itemEffePrice) {
        this.itemEffePrice = itemEffePrice;
    }

    public Double getItemCount() {
        return itemCount;
    }

    public void setItemCount(Double itemCount) {
        this.itemCount = itemCount;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute == null ? null : attribute.trim();
    }

    public Double getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(Double billAmount) {
        this.billAmount = billAmount;
    }

    public Double getEffeAmount() {
        return effeAmount;
    }

    public void setEffeAmount(Double effeAmount) {
        this.effeAmount = effeAmount;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getOperator() {
        return operator;
    }

    public void setOperator(Integer operator) {
        this.operator = operator;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public Integer getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(Integer itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }
    
    public String getItemTags() {
		return itemTags;
	}

	public void setItemTags(String itemTags) {
		this.itemTags = itemTags;
	}

	public Short getSkuType() {
      return skuType;
    }
  
    public void setSkuType(Short skuType) {
      this.skuType = skuType;
    }
  
    public Integer getUnitWeightNum() {
      return unitWeightNum;
    }
  
    public void setUnitWeightNum(Integer unitWeightNum) {
      this.unitWeightNum = unitWeightNum;
    }
    
    public String getSkuBarCode() {
      return skuBarCode;
    }

    public void setSkuBarCode(String skuBarCode) {
      this.skuBarCode = skuBarCode;
    }

  @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ShoppingCart other = (ShoppingCart) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getItemId() == null ? other.getItemId() == null : this.getItemId().equals(other.getItemId()))
            && (this.getItemSkuId() == null ? other.getItemSkuId() == null : this.getItemSkuId().equals(other.getItemSkuId()))
            && (this.getItemSkuInfo() == null ? other.getItemSkuInfo() == null : this.getItemSkuInfo().equals(other.getItemSkuInfo()))
            && (this.getItemName() == null ? other.getItemName() == null : this.getItemName().equals(other.getItemName()))
            && (this.getItemIconUrl() == null ? other.getItemIconUrl() == null : this.getItemIconUrl().equals(other.getItemIconUrl()))
            && (this.getItemBillPrice() == null ? other.getItemBillPrice() == null : this.getItemBillPrice().equals(other.getItemBillPrice()))
            && (this.getItemEffePrice() == null ? other.getItemEffePrice() == null : this.getItemEffePrice().equals(other.getItemEffePrice()))
            && (this.getItemCount() == null ? other.getItemCount() == null : this.getItemCount().equals(other.getItemCount()))
            && (this.getAttribute() == null ? other.getAttribute() == null : this.getAttribute().equals(other.getAttribute()))
            && (this.getBillAmount() == null ? other.getBillAmount() == null : this.getBillAmount().equals(other.getBillAmount()))
            && (this.getEffeAmount() == null ? other.getEffeAmount() == null : this.getEffeAmount().equals(other.getEffeAmount()))
            && (this.getDeviceId() == null ? other.getDeviceId() == null : this.getDeviceId().equals(other.getDeviceId()))
            && (this.getOperator() == null ? other.getOperator() == null : this.getOperator().equals(other.getOperator()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
            && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify().equals(other.getGmtModify()))
            && (this.getItemCategoryId() == null ? other.getItemCategoryId() == null : this.getItemCategoryId().equals(other.getItemCategoryId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getItemId() == null) ? 0 : getItemId().hashCode());
        result = prime * result + ((getItemSkuId() == null) ? 0 : getItemSkuId().hashCode());
        result = prime * result + ((getItemSkuInfo() == null) ? 0 : getItemSkuInfo().hashCode());
        result = prime * result + ((getItemName() == null) ? 0 : getItemName().hashCode());
        result = prime * result + ((getItemIconUrl() == null) ? 0 : getItemIconUrl().hashCode());
        result = prime * result + ((getItemBillPrice() == null) ? 0 : getItemBillPrice().hashCode());
        result = prime * result + ((getItemEffePrice() == null) ? 0 : getItemEffePrice().hashCode());
        result = prime * result + ((getItemCount() == null) ? 0 : getItemCount().hashCode());
        result = prime * result + ((getAttribute() == null) ? 0 : getAttribute().hashCode());
        result = prime * result + ((getBillAmount() == null) ? 0 : getBillAmount().hashCode());
        result = prime * result + ((getEffeAmount() == null) ? 0 : getEffeAmount().hashCode());
        result = prime * result + ((getDeviceId() == null) ? 0 : getDeviceId().hashCode());
        result = prime * result + ((getOperator() == null) ? 0 : getOperator().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
        result = prime * result + ((getItemCategoryId() == null) ? 0 : getItemCategoryId().hashCode());
        return result;
    }
}