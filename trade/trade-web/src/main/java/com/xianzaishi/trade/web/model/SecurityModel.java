package com.xianzaishi.trade.web.model;

public class SecurityModel {

	private String encode;

	private String sign;

	public String getEncode() {
		return encode;
	}

	public void setEncode(String encode) {
		this.encode = encode;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
