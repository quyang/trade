package com.xianzaishi.trade.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.type.TypeReference;

/**
 * 
 * @author Croky.Zheng
 * 2016年7月16日
 */
@SuppressWarnings("deprecation")
public class JsonConvert {
	private JsonConvert(){}
	private static ObjectMapper mapper = new ObjectMapper();
	static {
		//mapper.getSerializationConfig().setSerializationInclusion(inclusion);
		//设置输入时忽略JSON字符串中存在而Java对象实际没有的属性 
		mapper.getDeserializationConfig().set(org.codehaus.jackson.map.DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		//mapper.getSerializationConfig().setDateFormat(df);
		//mapper.getDeserializationConfig().setDateFormat(df);
	}

	public static <T> T json2Object(String json,Class<T> clazz) {
		try {
			return mapper.readValue(json, clazz);
		} catch (Exception e) {
			return null;
		}
	}

	public static <T> T json2Object(String json,T value) {
		try {
			return mapper.readValue(json,new TypeReference<T>(){});
		} catch (Exception e) {
			return null;
		}
	}
	
	public static JsonNode json2Node(String json) {
		try {
			//JsonNode node = mapper.readTree(json);
			return mapper.readTree(json);
		} catch (Exception e) {
			return null;
		}
	}

	public static String object2Json(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (Exception e) {
			return null;
		}
	}
	


	public static String getValue(String json,String name) {
		//JsonNode node = JsonConvert.json2Node(json);
		/*
		List<LinkedHashMap<String,String>> aoData = JsonConvert.json2Object(json, List.class);
		for(Object obj : aoData) {
			System.out.println(obj.toString());
		}
		*/
		JsonNode node = JsonConvert.json2Node(json);
		String[] names = name.split("[.]");
		for (int i=0; i<names.length; i++) {
			if (null == node) return null;
			Path path = getPath(names[i]);
			if (null != path) {
				node = node.get(path.name);
				if (node.isArray()) {
					node = node.get(path.index);
				}
			} else {
				node = node.get(names[i]);
				if (node.isArray()) {
					node = node.get(0);
				}
			}
		}
		if (null != node) {
			return node.asText();
		}
		return null;
	}
	private static Path getPath(String str) {
		Pattern pattern = Pattern.compile("[0-9,a-z,A-Z,-_]+\\[\\d+\\]");
		Matcher matcher = pattern.matcher(str);
		if (matcher.matches()) {
			int left_idx = str.indexOf("[");
			String name = str.substring(0,left_idx);
			int index = Integer.parseInt(str.substring(left_idx+1,str.length() - 1));
			return new Path(name,index);
		}
		return null;
	}
	
	static class Path {
		public Path(String name,int index) {
			this.name = name;
			this.index = index;
		}
		public String name;
		public int index;
	}
}
