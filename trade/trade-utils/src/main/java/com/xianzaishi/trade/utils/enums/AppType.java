package com.xianzaishi.trade.utils.enums;

import java.util.LinkedHashMap;
import java.util.Map;


/**
 * 
 * @author Croky.Zheng
 * 2016年7月16日
 */
public enum AppType {
		OuterWeb("外部WEB应用",(short)0x2),
		OuterGeneral("外部一般应用",(short)0x4),
		OuterMobile("外部移动设备应用",(short)0x8),
		InnerWeb("内部WEB应用",(short)0xF),
		InnerGeneral("内部一般应用",(short)0x10),
		InnerMobile("内部移动设备应用",(short)0x20);
		
		private String description;
		private short value;
		
		private AppType(String description,short value) {
			this.description = description;
			this.value = value;
		}
		
		public static AppType from(int value) {
			for (AppType tmp : AppType.values()) {
				if (tmp.getValue() == value) {
					return tmp;
				}
			}
			return null;
		}
		
		public static Map<Short,String> toMap() {
			Map<Short,String> appTypeMap = new LinkedHashMap<Short,String>();
			for (AppType tmp : AppType.values()) {
				appTypeMap.put(tmp.value, tmp.description);
			}
			return appTypeMap;
		}

		public String getDescription() {
			return description;
		}

		public short getValue() {
			return value;
		}
}
