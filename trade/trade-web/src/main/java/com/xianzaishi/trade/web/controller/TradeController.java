package com.xianzaishi.trade.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.croky.util.StringUtils;
import com.xianzaishi.trade.core.order.OrdersManager;
import com.xianzaishi.trade.core.order.TradeManager;
import com.xianzaishi.trade.dal.model.extend.OrderInfo;
import com.xianzaishi.trade.utils.TradeLog;
import com.xianzaishi.trade.utils.enums.OrderStatus;
import com.xianzaishi.trade.utils.enums.PayWay;
import com.xianzaishi.trade.web.form.PayForm;
import com.xianzaishi.trade.web.model.ResponseModel;
import com.xianzaishi.trade.web.model.SignResponseModel;
import com.xianzaishi.trade.web.utils.HttpServletUtils;
import com.xianzaishi.trade.web.utils.SecurityUtils;

/**
 * 交易相关接口
 *
 * @author nianyue.hyj
 * @since 2016.10.19
 */
@Controller
public class TradeController {

	@Autowired
	private OrdersManager ordersManager;

	@Autowired
	private TradeManager tradeManager;

	private Logger logger = LoggerFactory.getLogger(TradeController.class);

	/**
	 * 收银方法 - 直接传入付款参数,用于支持收银机各种支付
	 * 
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/trade/pay.json", method = { RequestMethod.POST })
	public @ResponseBody SignResponseModel pay(@RequestBody PayForm params) {

		try {
			if (!SecurityUtils.verify(params, URLDecoder.decode(params.getSign(), "utf-8"))) {
				return new SignResponseModel(-1, "签名校验失败", "FAIL");
			}
			OrderInfo order = ordersManager.getOrder(params.getOrderId());
			if (order == null) {
				return new SignResponseModel(-1, "订单不存在", "FAIL");
			}
			if(order.getStatus().shortValue() == OrderStatus.VALID.getValue() && 
			    (params.getPayType().intValue() == 1 ||params.getPayType().intValue() == 4 || params.getPayType().intValue() == 8)){
			    return new SignResponseModel(1, "订单已成功，无需重新支付", "SUCCESS");
			}
			if (order.getStatus().shortValue() != OrderStatus.INIT.getValue()) {
				return new SignResponseModel(-1, "订单状态不正确", "FAIL");
			}
			double amount = (double) params.getPayAmount() / 100.0;
			if (amount <= 0 || params.getPayType() == null || PayWay.from(params.getPayType()) == null) {
				return new SignResponseModel(-1, "支付参数不正确", "FAIL");
			}

			PayWay payWay = PayWay.from(params.getPayType().shortValue());
			if (payWay == null) {
				return new SignResponseModel(-1, "支付途径非法", "FAIL");
			}

			if (payWay == PayWay.WEIXIN) {
				payWay = PayWay.WEIXINCODE;
			} else if (payWay == PayWay.ALIPAY) {
				payWay = PayWay.ALIPAYCODE;
			}

			if (ordersManager.pay(params.getOrderId(), amount, payWay, params.getAccount(), "收银机支付")) {
				TradeLog.info("[OffLine]-[PAY-SUCCESS] PARAMS[%s]", JSON.toJSONString(params));
				return new SignResponseModel(1, "支付成功", "SUCCESS");
			} else {
				TradeLog.info("[OffLine]-[PAY-FAIL] PARAMS[%s]", JSON.toJSONString(params));
				return new SignResponseModel(-1, "支付失败", "FAIL");
			}

		} catch (Exception e) {
			logger.error("Fail to pay for params - >" + JSON.toJSONString(params), e);
			return new SignResponseModel(-1, "支付失败", "FAIL");
		}
	}

	/**
	 * 银通回调接口预留，现在不用
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/trade/yintongPayCallback.htm", method = { RequestMethod.POST })
	public void yintongPayCallback(HttpServletRequest request, HttpServletResponse response) {

		logger.error("yintong pay callback -> " + JSON.toJSONString(request.getParameterMap()));

		int length = request.getContentLength();
		if (length > 0 && length < 2 * 1024 * 1024) {
			byte[] buffer = new byte[length];
			InputStream input = null;
			try {
				input = request.getInputStream();
				int size = input.read(buffer, 0, length);
				if (size > 0) {
					String result = new String(buffer, 0, size);
					TradeLog.info("[OffLine]-[CALLBACK] PARAMS[%s]", result);
				}
			} catch (Exception e) {
				// ignore
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						// ignore
					}
				}
			}
		}

		response.setContentType("text/html;charset=UTF-8");

		JSONObject json = new JSONObject();
		json.put("ret_code", "0");
		json.put("ret_msg", "成功");

		HttpServletUtils.write(response, json.toJSONString());
	}

	/**
	 * 安全关闭交易,即时退还积分优惠券 - 仅待付款状态可关闭
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/trade/safeCancelTrade.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel safeCancelTrade(@RequestParam String data) {

		if (StringUtils.isBlank(data)) {
			return new ResponseModel(-1, "参数错误");
		}

		try {
			long orderId = Long.parseLong(SecurityUtils.decode(data));
			OrderInfo order = ordersManager.getOrder(orderId);
			if (order == null) {
				return new ResponseModel(-1, "订单不存在[orderId]" + orderId);
			}
			if (order.getStatus().shortValue() != OrderStatus.INIT.getValue()) {
				return new ResponseModel(-1, "只能取消未付款订单");
			}
			if (tradeManager.tryCancelTrade(orderId, "线下-交易取消")) {
				return new ResponseModel(0, "取消订单成功");
			} else {
				return new ResponseModel(-1, "取消订单失败");
			}
		} catch (Exception e) {
			return new ResponseModel(-1, "参数错误或者订单不存在->" + data);
		}
	}

}
