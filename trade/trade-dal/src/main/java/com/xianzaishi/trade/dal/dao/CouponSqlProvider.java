package com.xianzaishi.trade.dal.dao;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.dal.model.CouponExample.Criteria;
import com.xianzaishi.trade.dal.model.CouponExample.Criterion;
import com.xianzaishi.trade.dal.model.CouponExample;
import java.util.List;
import java.util.Map;

public class CouponSqlProvider {

    public String countByExample(CouponExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("coupon");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(CouponExample example) {
        BEGIN();
        DELETE_FROM("coupon");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(Coupon record) {
        BEGIN();
        INSERT_INTO("coupon");
        
        if (record.getTitle() != null) {
            VALUES("title", "#{title,jdbcType=VARCHAR}");
        }
        
        if (record.getRules() != null) {
            VALUES("rules", "#{rules,jdbcType=VARCHAR}");
        }
        
        if (record.getCrowdId() != null) {
            VALUES("crowd_id", "#{crowdId,jdbcType=BIGINT}");
        }
        
        if (record.getType() != null) {
            VALUES("type", "#{type,jdbcType=SMALLINT}");
        }
        
        if (record.getGmtStart() != null) {
            VALUES("gmt_start", "#{gmtStart,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtEnd() != null) {
            VALUES("gmt_end", "#{gmtEnd,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtDayDuration() != null) {
          VALUES("gmt_day_duration", "#{gmtDayDuration,jdbcType=INTEGER}");
        }
        
        if (record.getGmtCreate() != null) {
            VALUES("gmt_create", "#{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            VALUES("gmt_modify", "#{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            VALUES("status", "#{status,jdbcType=SMALLINT}");
        }
        
        if (record.getAmount() != null) {
            VALUES("amount", "#{amount,jdbcType=DOUBLE}");
        }
        
        if (record.getChannelType() != null) {
            VALUES("channel_type", "#{channelType,jdbcType=SMALLINT}");
        }
        
        if (record.getAmountLimit() != null) {
            VALUES("amount_limit", "#{amountLimit,jdbcType=DOUBLE}");
        }
        
        return SQL();
    }

    public String selectByExample(CouponExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("title");
        SELECT("rules");
        SELECT("crowd_id");
        SELECT("type");
        SELECT("gmt_start");
        SELECT("gmt_end");
        SELECT("gmt_day_duration");
        SELECT("gmt_create");
        SELECT("gmt_modify");
        SELECT("status");
        SELECT("amount");
        SELECT("channel_type");
        SELECT("amount_limit");
        FROM("coupon");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        Coupon record = (Coupon) parameter.get("record");
        CouponExample example = (CouponExample) parameter.get("example");
        
        BEGIN();
        UPDATE("coupon");
        
        if (record.getId() != null) {
            SET("id = #{record.id,jdbcType=BIGINT}");
        }
        
        if (record.getTitle() != null) {
            SET("title = #{record.title,jdbcType=VARCHAR}");
        }
        
        if (record.getRules() != null) {
            SET("rules = #{record.rules,jdbcType=VARCHAR}");
        }
        
        if (record.getCrowdId() != null) {
            SET("crowd_id = #{record.crowdId,jdbcType=BIGINT}");
        }
        
        if (record.getType() != null) {
            SET("type = #{record.type,jdbcType=SMALLINT}");
        }
        
        if (record.getGmtStart() != null) {
            SET("gmt_start = #{record.gmtStart,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtEnd() != null) {
            SET("gmt_end = #{record.gmtEnd,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtDayDuration() != null) {
          SET("gmt_day_duration = #{record.gmtDayDuration,jdbcType=INTEGER}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{record.status,jdbcType=SMALLINT}");
        }
        
        if (record.getAmount() != null) {
            SET("amount = #{record.amount,jdbcType=DOUBLE}");
        }
        
        if (record.getChannelType() != null) {
            SET("channel_type = #{record.channelType,jdbcType=SMALLINT}");
        }
        
        if (record.getAmountLimit() != null) {
            SET("amount_limit = #{record.amountLimit,jdbcType=DOUBLE}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("coupon");
        
        SET("id = #{record.id,jdbcType=BIGINT}");
        SET("title = #{record.title,jdbcType=VARCHAR}");
        SET("rules = #{record.rules,jdbcType=VARCHAR}");
        SET("crowd_id = #{record.crowdId,jdbcType=BIGINT}");
        SET("type = #{record.type,jdbcType=SMALLINT}");
        SET("gmt_start = #{record.gmtStart,jdbcType=TIMESTAMP}");
        SET("gmt_end = #{record.gmtEnd,jdbcType=TIMESTAMP}");
        SET("gmt_day_duration = #{gmtDayDuration,jdbcType=INTEGER}");
        SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        SET("status = #{record.status,jdbcType=SMALLINT}");
        SET("amount = #{record.amount,jdbcType=DOUBLE}");
        SET("channel_type = #{record.channelType,jdbcType=SMALLINT}");
        SET("amount_limit = #{record.amountLimit,jdbcType=DOUBLE}");
        
        CouponExample example = (CouponExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(Coupon record) {
        BEGIN();
        UPDATE("coupon");
        
        if (record.getTitle() != null) {
            SET("title = #{title,jdbcType=VARCHAR}");
        }
        
        if (record.getRules() != null) {
            SET("rules = #{rules,jdbcType=VARCHAR}");
        }
        
        if (record.getCrowdId() != null) {
            SET("crowd_id = #{crowdId,jdbcType=BIGINT}");
        }
        
        if (record.getType() != null) {
            SET("type = #{type,jdbcType=SMALLINT}");
        }
        
        if (record.getGmtStart() != null) {
            SET("gmt_start = #{gmtStart,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtEnd() != null) {
            SET("gmt_end = #{gmtEnd,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtDayDuration() != null) {
            SET("gmt_day_duration = #{record.gmtDayDuration,jdbcType=INTEGER}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{status,jdbcType=SMALLINT}");
        }
        
        if (record.getAmount() != null) {
            SET("amount = #{amount,jdbcType=DOUBLE}");
        }
        
        if (record.getChannelType() != null) {
            SET("channel_type = #{channelType,jdbcType=SMALLINT}");
        }
        
        if (record.getAmountLimit() != null) {
            SET("amount_limit = #{amountLimit,jdbcType=DOUBLE}");
        }
        
        WHERE("id = #{id,jdbcType=BIGINT}");
        
        return SQL();
    }

    protected void applyWhere(CouponExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}