package com.xianzaishi.trade.core.address.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.croky.util.CollectionUtils;
import com.croky.util.StringUtils;
import com.xianzaishi.trade.core.address.AreaManager;
import com.xianzaishi.trade.core.address.ShopServiceAreaManager;
import com.xianzaishi.trade.core.model.AddressModel;
import com.xianzaishi.trade.dal.dao.extend.ShopServiceAreaDAO;
import com.xianzaishi.trade.dal.model.Area;
import com.xianzaishi.trade.dal.model.ShopServiceArea;
import com.xianzaishi.trade.dal.model.ShopServiceAreaExample;
import com.xianzaishi.trade.utils.enums.Status;


/**
 * 
 * @author Croky.Zheng
 * 2016年9月7日
 */
@Component(value="shopServiceAreaManager")
public class ShopServiceAreaManagerImpl implements ShopServiceAreaManager {
	private static final Logger log = LoggerFactory.getLogger(ShopServiceAreaManagerImpl.class);

	@Resource
	private ShopServiceAreaDAO shopServiceAreaDAO;
	
	@Resource
	private AreaManager areaManager;
	
	private ConcurrentHashMap<Integer,AddressModel> areaMap = new ConcurrentHashMap<Integer,AddressModel>();
	
	
	@Override
	public List<ShopServiceArea> getShopServiceAreaList() {
		return shopServiceAreaDAO.selectByExample(null);
	}
	
	@Override
	//List<Area>[] JAVA不允许泛型数组
	public  AddressModel getShopServiceAreaDetail(int shopId) {
		AddressModel model = areaMap.get(shopId);
		if (null == model) {
			model = new AddressModel();
			List<ShopServiceArea> shopServiceAreaList = getByShopId(shopId);
			model.setLevel1Area(getAreaList(getLeveAreaCode(shopServiceAreaList,1)));
			model.setLevel2Area(getAreaList(getLeveAreaCode(shopServiceAreaList,2)));
			model.setLevel3Area(getAreaList(getLeveAreaCode(shopServiceAreaList,3)));
			model.setLevel4Area(getAreaList(getLeveAreaCode(shopServiceAreaList,4)));
		}
		return model;
	}
	
	@Override
	public int insert(int shopId,String code,int memberId) {
		String address = areaManager.getAreaAddress(code);
		if (null != address) {
			ShopServiceArea shopServiceArea = new ShopServiceArea();
			shopServiceArea.setCode(code);
			shopServiceArea.setName(address);
			shopServiceArea.setMemberId(memberId);
			shopServiceArea.setShopId(shopId);
			return insert(shopServiceArea);
		}
		return 0;
	}
	
	@Override
	public boolean delete(int id) {
		ShopServiceArea shopServiceArea = get(id);
		if (null != shopServiceArea) {
			shopServiceArea.setGmtModify(new Date());
			shopServiceArea.setStatus(Status.INVALID.getValue());
			try {
				return shopServiceAreaDAO.updateByPrimaryKey(shopServiceArea) > 0;
			} catch (DataAccessException e) {
				log.error("delete ShopServiceArea failed.id=" + id,e);
			}
		}
		return false;
	}
	
	private List<ShopServiceArea> getByShopId(int shopId) {
		ShopServiceAreaExample example = new ShopServiceAreaExample();
		example.createCriteria().andShopIdEqualTo(shopId).andStatusEqualTo(Status.VALID.getValue());
		example.setOrderByClause(" id desc ");
		try {
			return shopServiceAreaDAO.selectByExample(example);
		} catch (DataAccessException e) {
			log.error("select ShopServiceArea failed.shopId=" + shopId,e);
		}
		return null;
	}
	
	private ShopServiceArea get(int id) {
		try {
			return shopServiceAreaDAO.selectByPrimaryKey(id);
		} catch (DataAccessException e) {
			log.error("select ShopServiceArea failed.id=" + id,e);
		}
		return null;
	}
	
	private int insert(ShopServiceArea shopServiceArea) {
		Date now = new Date();
		shopServiceArea.setGmtCreate(now);
		shopServiceArea.setGmtModify(now);
		shopServiceArea.setStatus(Status.VALID.getValue());
		try {
			if (shopServiceAreaDAO.insert(shopServiceArea) > 0) {
				return shopServiceArea.getId();
			}
		} catch (DataAccessException e) {
			log.error("insert ShopServiceArea failed.",e);
		}
		return 0;
	}
	

	private List<Area> getAreaList(Set<String> codeSet) {
		if (CollectionUtils.isNotEmpty(codeSet)) {
			List<Area> areaList = new ArrayList<Area>(codeSet.size());
			for (String code : codeSet) {
				Area area = areaManager.getArea(code);
				if (null != area) {
					areaList.add(area);
				}
			}
			return areaList;
		}
		return null;
	}
	
	private Set<String> getLeveAreaCode(List<ShopServiceArea> shopServiceAreaList,int level) {
		if (CollectionUtils.isNotEmpty(shopServiceAreaList)) {
			Set<String> areaCodeSet = new HashSet<String>();
			for (ShopServiceArea shopServiceArea : shopServiceAreaList) {
				String code = shopServiceArea.getCode();
				if (code.length() >= 6) {
					switch(level) {
					case 1:
						areaCodeSet.add(StringUtils.left(code, 2) + "0000");
						break;
					case 2:
						areaCodeSet.add(StringUtils.left(code, 4) + "00");
						break;
					case 3:
						areaCodeSet.add(StringUtils.left(code, 6));
						break;
					case 4:
						//第四级区划
						if (code.length() ==  9) {
							areaCodeSet.add(code);
						}
						break;
					}
				}
			}
			return areaCodeSet;
		}
		return null;
	}
}
