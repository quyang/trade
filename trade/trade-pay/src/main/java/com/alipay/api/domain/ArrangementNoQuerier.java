package com.alipay.api.domain;

import java.util.List;

import com.alipay.api.AlipayObject;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

/**
 * 合约号查询器，直接获得合约号
 *
 * @author auto create
 * @since 1.0, 2016-09-09 20:38:43
 */
public class ArrangementNoQuerier extends AlipayObject {

	private static final long serialVersionUID = 1536443361612745677L;

	/**
	 * 合约编号列表
	 */
	@ApiListField("ar_nos")
	@ApiField("string")
	private List<String> arNos;

	public List<String> getArNos() {
		return this.arNos;
	}
	public void setArNos(List<String> arNos) {
		this.arNos = arNos;
	}

}
