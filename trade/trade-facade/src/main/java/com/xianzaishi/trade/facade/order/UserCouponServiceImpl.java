package com.xianzaishi.trade.facade.order;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.trade.client.Result;
import com.xianzaishi.trade.client.UserCouponService;
import com.xianzaishi.trade.client.vo.CouponVO;
import com.xianzaishi.trade.client.vo.UserCouponVO;
import com.xianzaishi.trade.core.coupon.CouponManager;
import com.xianzaishi.trade.core.coupon.UserCouponManager;
import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;

@Service(value = "userCouponService")
public class UserCouponServiceImpl implements UserCouponService {

	@Autowired
	private UserService userService;

	@Autowired
	private UserCouponManager userCouponManager;

	@Autowired
	private CouponManager couponManager;

	private Logger logger = LoggerFactory.getLogger(UserCouponServiceImpl.class);

	private static final int DEFAULT_USER_COUPON_TIME = 30*24*3600*1000;
	@Override
	public Result<Boolean> sendCouponPackage(long userId, short couponType, long duration) {
//		if (userId <= 0 || couponType <= 0) {
//			return new Result<>(-1, "参数错误", false);
//		}
//
//		try {
//			com.xianzaishi.itemcenter.common.result.Result<? extends BaseUserDTO> userResult = userService.queryUserByUserId(userId, false);
//			if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
//				return new Result<>(-1, "用户不存在", false);
//			}
//			BaseUserDTO user = userResult.getModule();
//
//			List<Coupon> coupons = couponManager.getCouponsByType(couponType);
//			if (coupons != null) {
//				List<UserCoupon> userCoupons = userCouponManager.getUserCouponsByCouponType(user.getUserId(), Lists.newArrayList(couponType));
//				Set<Long> exist = new HashSet<>();
//				for (UserCoupon userCoupon : userCoupons) {
//					exist.add(userCoupon.getCouponId());
//				}
//				for (Coupon coupon : coupons) {
//					if (!exist.contains(coupon.getId())) {
//						Date now = new Date();
//						coupon.setGmtStart(now);
//						if(coupon.getGmtEnd().after(now)){
//						  //优先逻辑，优先使用优惠券本身有效期
//						}else if(null != coupon.getGmtDayDuration() && coupon.getGmtDayDuration() > 0){
//						  coupon.setGmtEnd(new Date(now.getTime() + (coupon.getGmtDayDuration()*24L*3600*1000L)));
//						}else if(duration > 0){
//						  coupon.setGmtEnd(new Date(now.getTime() + duration));
//						}else{
//						  coupon.setGmtEnd(new Date(now.getTime() + DEFAULT_USER_COUPON_TIME));
//						}
//						userCouponManager.insertCoupon(user.getUserId(), coupon);
//					}
//				}
//			}
//			return new Result<>(0, "新人礼包赠送成功", true);
//		} catch (Exception e) {
//			return new Result<>(-1, "服务器内部错误1", false);
//		}
	  return new Result<>(-1, "接口升级，不再支持", false);
	}

	@Override
	public Result<UserCouponVO> getUserCouponById(long couponId) {

		try {
			UserCoupon userCoupon = userCouponManager.getUserCouponById(couponId);
			if (userCoupon != null) {
				UserCouponVO wrapper = new UserCouponVO();
				BeanUtils.copyProperties(wrapper, userCoupon);
				return new Result<>(0, "新人礼包赠送成功", wrapper);
			}
		} catch (Exception e) {
			logger.error("Fail to query UserCoupon for couponId ->" + couponId, e);
		}

		return new Result<>(-1, "服务器内部错误2", null);
	}

	@Override
	public Result<List<CouponVO>> getCouponByTypes(Collection<Short> couponTypes) {

		List<Coupon> coupons = couponManager.getCouponsByCouponTypes(couponTypes);

		List<CouponVO> wrappers = new ArrayList<>();
		if (coupons != null && !coupons.isEmpty()) {
			for (Coupon coupon : coupons) {
				CouponVO wrapper = new CouponVO();
				try {
					BeanUtils.copyProperties(wrapper, coupon);
					wrappers.add(wrapper);
				} catch (Exception e) {
					logger.error("Fail to copy properties from coupon -> " + coupon.getId(), coupon);
				}
			}
		}

		return new Result<>(0, "查询成功", wrappers);
	}

	@Override
	public Result<Boolean> sendCouponByCouponId(long userId, long couponId, long duration) {
		if (userId <= 0 || couponId <= 0 || duration <= 0) {
			return new Result<>(-1, "参数错误", false);
		}

		try {
			com.xianzaishi.itemcenter.common.result.Result<? extends BaseUserDTO> userResult = userService.queryUserByUserId(userId, false);
			if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
				return new Result<>(-1, "用户不存在", false);
			}
			BaseUserDTO user = userResult.getModule();

			Coupon coupon = couponManager.getCoupon(couponId);

			if (coupon == null) {
				return new Result<>(-1, "优惠券不存在", false);
			}

			Date now = new Date();
            coupon.setGmtStart(now);
            if(coupon.getGmtEnd().after(now)){
              //优先逻辑，优先使用优惠券本身有效期
            }else if(coupon.getGmtDayDuration() != null && coupon.getGmtDayDuration() > 0){
              coupon.setGmtEnd(new Date(now.getTime() + (coupon.getGmtDayDuration()*24*3600*1000L)));
            }else if(duration > 0){
              coupon.setGmtEnd(new Date(now.getTime() + duration));
            }else{
              coupon.setGmtEnd(new Date(now.getTime() + DEFAULT_USER_COUPON_TIME));
            }
            
			userCouponManager.insertCoupon(user.getUserId(), coupon);

			return new Result<>(0, "赠送优惠券成功", true);
		} catch (Exception e) {
		    logger.error("Add coupon failed " + e.getMessage());
			return new Result<>(-1, "服务器内部错误3,"+e.getMessage(), false);
		}
	}

	@Override
	public Result<List<UserCouponVO>> getUserCouponByTypes(long userId, Collection<Short> couponTypes) {
//		try {
//			List<UserCoupon> userCoupons = userCouponManager.getUserCouponsByCouponType(userId,
//					couponTypes != null ? Lists.newArrayList(couponTypes) : null);
//			List<UserCouponVO> wrappers = new ArrayList<>();
//			if (userCoupons != null && !userCoupons.isEmpty()) {
//				for (UserCoupon userCoupon : userCoupons) {
//					UserCouponVO wrapper = new UserCouponVO();
//					BeanUtils.copyProperties(wrapper, userCoupon);
//					wrappers.add(wrapper);
//				}
//			}
//			return new Result<>(0, "查询用户优惠券成功", wrappers);
//		} catch (Exception e) {
//			logger.error("Fail to query UserCoupon for userId ->" + userId, e);
//		}

		return new Result<>(-1, "服务不再支持", null);
	}
}
