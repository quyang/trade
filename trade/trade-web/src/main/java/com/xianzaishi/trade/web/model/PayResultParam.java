package com.xianzaishi.trade.web.model;

import java.io.Serializable;

public class PayResultParam implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 661766928331063382L;

	private long insert_id;
	
	private String id;
	
	private String total;
	
	private String type;
	
	private String uname;
	
	private String device;
	
	private String expirce;
	
	private String date;

	public long getInsert_id() {
		return insert_id;
	}

	public void setInsert_id(long insert_id) {
		this.insert_id = insert_id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getExpirce() {
		return expirce;
	}

	public void setExpirce(String expirce) {
		this.expirce = expirce;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
}
