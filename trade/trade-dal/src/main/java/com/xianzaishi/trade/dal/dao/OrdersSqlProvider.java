package com.xianzaishi.trade.dal.dao;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import com.xianzaishi.trade.dal.model.Orders;
import com.xianzaishi.trade.dal.model.OrdersExample.Criteria;
import com.xianzaishi.trade.dal.model.OrdersExample.Criterion;
import com.xianzaishi.trade.dal.model.OrdersExample;
import java.util.List;
import java.util.Map;

public class OrdersSqlProvider {

    public String countByExample(OrdersExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("orders");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(OrdersExample example) {
        BEGIN();
        DELETE_FROM("orders");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(Orders record) {
        BEGIN();
        INSERT_INTO("orders");
        
        if (record.getUserId() != null) {
            VALUES("user_id", "#{userId,jdbcType=BIGINT}");
        }
        
        if (record.getPayAmount() != null) {
            VALUES("pay_amount", "#{payAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getEffeAmount() != null) {
            VALUES("effe_amount", "#{effeAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getChannelType() != null) {
            VALUES("channel_type", "#{channelType,jdbcType=SMALLINT}");
        }
        
        if (record.getDeviceCode() != null) {
            VALUES("device_code", "#{deviceCode,jdbcType=VARCHAR}");
        }
        
        if (record.getCashierId() != null) {
            VALUES("cashier_id", "#{cashierId,jdbcType=INTEGER}");
        }
        
        if (record.getUserAddressId() != null) {
            VALUES("user_address_id", "#{userAddressId,jdbcType=BIGINT}");
        }
        
        if (record.getShopId() != null) {
            VALUES("shop_id", "#{shopId,jdbcType=INTEGER}");
        }
        
        if (record.getAttribute() != null) {
            VALUES("attribute", "#{attribute,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            VALUES("status", "#{status,jdbcType=SMALLINT}");
        }
        
        if (record.getGmtCreate() != null) {
            VALUES("gmt_create", "#{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            VALUES("gmt_modify", "#{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getBillAmount() != null) {
            VALUES("bill_amount", "#{billAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getCouponId() != null) {
            VALUES("coupon_id", "#{couponId,jdbcType=BIGINT}");
        }
        
        if (record.getDiscountAmount() != null) {
            VALUES("discount_amount", "#{discountAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getGmtPay() != null) {
            VALUES("gmt_pay", "#{gmtPay,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtReceive() != null) {
            VALUES("gmt_receive", "#{gmtReceive,jdbcType=TIMESTAMP}");
        }
        
        if (record.getSeq() != null) {
            VALUES("seq", "#{seq,jdbcType=INTEGER}");
        }
        
        if (record.getUserAddress() != null) {
            VALUES("user_address", "#{userAddress,jdbcType=VARCHAR}");
        }
        
        if (record.getGmtDistribution() != null) {
            VALUES("gmt_distribution", "#{gmtDistribution,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCredit() != null) {
            VALUES("credit", "#{credit,jdbcType=INTEGER}");
        }
        
        if (record.getOrderType() != null) {
            VALUES("order_type", "#{orderType,jdbcType=SMALLINT}");
        }
        
        return SQL();
    }

    public String selectByExample(OrdersExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("user_id");
        SELECT("pay_amount");
        SELECT("effe_amount");
        SELECT("channel_type");
        SELECT("device_code");
        SELECT("cashier_id");
        SELECT("user_address_id");
        SELECT("shop_id");
        SELECT("attribute");
        SELECT("status");
        SELECT("gmt_create");
        SELECT("gmt_modify");
        SELECT("bill_amount");
        SELECT("coupon_id");
        SELECT("discount_amount");
        SELECT("gmt_pay");
        SELECT("gmt_receive");
        SELECT("seq");
        SELECT("user_address");
        SELECT("gmt_distribution");
        SELECT("credit");
        SELECT("order_type");
        FROM("orders");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        Orders record = (Orders) parameter.get("record");
        OrdersExample example = (OrdersExample) parameter.get("example");
        
        BEGIN();
        UPDATE("orders");
        
        if (record.getId() != null) {
            SET("id = #{record.id,jdbcType=BIGINT}");
        }
        
        if (record.getUserId() != null) {
            SET("user_id = #{record.userId,jdbcType=BIGINT}");
        }
        
        if (record.getPayAmount() != null) {
            SET("pay_amount = #{record.payAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getEffeAmount() != null) {
            SET("effe_amount = #{record.effeAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getChannelType() != null) {
            SET("channel_type = #{record.channelType,jdbcType=SMALLINT}");
        }
        
        if (record.getDeviceCode() != null) {
            SET("device_code = #{record.deviceCode,jdbcType=VARCHAR}");
        }
        
        if (record.getCashierId() != null) {
            SET("cashier_id = #{record.cashierId,jdbcType=INTEGER}");
        }
        
        if (record.getUserAddressId() != null) {
            SET("user_address_id = #{record.userAddressId,jdbcType=BIGINT}");
        }
        
        if (record.getShopId() != null) {
            SET("shop_id = #{record.shopId,jdbcType=INTEGER}");
        }
        
        if (record.getAttribute() != null) {
            SET("attribute = #{record.attribute,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{record.status,jdbcType=SMALLINT}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getBillAmount() != null) {
            SET("bill_amount = #{record.billAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getCouponId() != null) {
            SET("coupon_id = #{record.couponId,jdbcType=BIGINT}");
        }
        
        if (record.getDiscountAmount() != null) {
            SET("discount_amount = #{record.discountAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getGmtPay() != null) {
            SET("gmt_pay = #{record.gmtPay,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtReceive() != null) {
            SET("gmt_receive = #{record.gmtReceive,jdbcType=TIMESTAMP}");
        }
        
        if (record.getSeq() != null) {
            SET("seq = #{record.seq,jdbcType=INTEGER}");
        }
        
        if (record.getUserAddress() != null) {
            SET("user_address = #{record.userAddress,jdbcType=VARCHAR}");
        }
        
        if (record.getGmtDistribution() != null) {
            SET("gmt_distribution = #{record.gmtDistribution,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCredit() != null) {
            SET("credit = #{record.credit,jdbcType=INTEGER}");
        }
        
        if (record.getOrderType() != null) {
            SET("order_type = #{record.orderType,jdbcType=SMALLINT}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("orders");
        
        SET("id = #{record.id,jdbcType=BIGINT}");
        SET("user_id = #{record.userId,jdbcType=BIGINT}");
        SET("pay_amount = #{record.payAmount,jdbcType=DOUBLE}");
        SET("effe_amount = #{record.effeAmount,jdbcType=DOUBLE}");
        SET("channel_type = #{record.channelType,jdbcType=SMALLINT}");
        SET("device_code = #{record.deviceCode,jdbcType=VARCHAR}");
        SET("cashier_id = #{record.cashierId,jdbcType=INTEGER}");
        SET("user_address_id = #{record.userAddressId,jdbcType=BIGINT}");
        SET("shop_id = #{record.shopId,jdbcType=INTEGER}");
        SET("attribute = #{record.attribute,jdbcType=VARCHAR}");
        SET("status = #{record.status,jdbcType=SMALLINT}");
        SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        SET("bill_amount = #{record.billAmount,jdbcType=DOUBLE}");
        SET("coupon_id = #{record.couponId,jdbcType=BIGINT}");
        SET("discount_amount = #{record.discountAmount,jdbcType=DOUBLE}");
        SET("gmt_pay = #{record.gmtPay,jdbcType=TIMESTAMP}");
        SET("gmt_receive = #{record.gmtReceive,jdbcType=TIMESTAMP}");
        SET("seq = #{record.seq,jdbcType=INTEGER}");
        SET("user_address = #{record.userAddress,jdbcType=VARCHAR}");
        SET("gmt_distribution = #{record.gmtDistribution,jdbcType=TIMESTAMP}");
        SET("credit = #{record.credit,jdbcType=INTEGER}");
        SET("order_type = #{record.orderType,jdbcType=SMALLINT}");
        
        OrdersExample example = (OrdersExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(Orders record) {
        BEGIN();
        UPDATE("orders");
        
        if (record.getUserId() != null) {
            SET("user_id = #{userId,jdbcType=BIGINT}");
        }
        
        if (record.getPayAmount() != null) {
            SET("pay_amount = #{payAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getEffeAmount() != null) {
            SET("effe_amount = #{effeAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getChannelType() != null) {
            SET("channel_type = #{channelType,jdbcType=SMALLINT}");
        }
        
        if (record.getDeviceCode() != null) {
            SET("device_code = #{deviceCode,jdbcType=VARCHAR}");
        }
        
        if (record.getCashierId() != null) {
            SET("cashier_id = #{cashierId,jdbcType=INTEGER}");
        }
        
        if (record.getUserAddressId() != null) {
            SET("user_address_id = #{userAddressId,jdbcType=BIGINT}");
        }
        
        if (record.getShopId() != null) {
            SET("shop_id = #{shopId,jdbcType=INTEGER}");
        }
        
        if (record.getAttribute() != null) {
            SET("attribute = #{attribute,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{status,jdbcType=SMALLINT}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getBillAmount() != null) {
            SET("bill_amount = #{billAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getCouponId() != null) {
            SET("coupon_id = #{couponId,jdbcType=BIGINT}");
        }
        
        if (record.getDiscountAmount() != null) {
            SET("discount_amount = #{discountAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getGmtPay() != null) {
            SET("gmt_pay = #{gmtPay,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtReceive() != null) {
            SET("gmt_receive = #{gmtReceive,jdbcType=TIMESTAMP}");
        }
        
        if (record.getSeq() != null) {
            SET("seq = #{seq,jdbcType=INTEGER}");
        }
        
        if (record.getUserAddress() != null) {
            SET("user_address = #{userAddress,jdbcType=VARCHAR}");
        }
        
        if (record.getGmtDistribution() != null) {
            SET("gmt_distribution = #{gmtDistribution,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCredit() != null) {
            SET("credit = #{credit,jdbcType=INTEGER}");
        }
        
        if (record.getOrderType() != null) {
            SET("order_type = #{orderType,jdbcType=SMALLINT}");
        }
        
        WHERE("id = #{id,jdbcType=BIGINT}");
        
        return SQL();
    }

    protected void applyWhere(OrdersExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}