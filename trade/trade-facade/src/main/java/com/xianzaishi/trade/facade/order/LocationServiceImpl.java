package com.xianzaishi.trade.facade.order;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xianzaishi.trade.client.LocationService;
import com.xianzaishi.trade.client.Result;
import com.xianzaishi.trade.client.vo.DeliveryAddressVO;
import com.xianzaishi.trade.core.address.DeliveryAddressManager;
import com.xianzaishi.trade.dal.model.DeliveryAddress;

@Service(value = "locationService")
public class LocationServiceImpl implements LocationService {

	@Autowired
	private DeliveryAddressManager deliveryAddressManager;

	private Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);

	@Override
	public Result<DeliveryAddressVO> getDeliveryAddressById(long addressId) {

		Result<DeliveryAddressVO> result = new Result<>(-1, "未知错误");
		try {
			DeliveryAddress address = deliveryAddressManager.getLinkman(addressId);
			if (address != null) {
				DeliveryAddressVO wrapper = new DeliveryAddressVO();
				BeanUtils.copyProperties(wrapper, address);
				result.setModel(wrapper);
				result.setCode(0);
				result.setMessage("SUCCESS");
			} else {
				result.setCode(-1);
				result.setMessage("地址不存在");
			}
		} catch (Exception e) {
			logger.error(String.format("Fail to get address by id[%s]", addressId), e);
			result.setCode(-1);
			result.setMessage("服务器内部错误");
		}
		return result;
	}

}
