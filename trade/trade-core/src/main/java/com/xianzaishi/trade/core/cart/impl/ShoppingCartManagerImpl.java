package com.xianzaishi.trade.core.cart.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.discount.DiscountInfoService;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.discount.query.DiscountQueryDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.item.query.ItemQuery;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.client.vo.SkuInfoVO;
import com.xianzaishi.trade.core.address.DeliveryAddressManager;
import com.xianzaishi.trade.core.cart.ShoppingCartManager;
import com.xianzaishi.trade.core.order.OrdersManager;
import com.xianzaishi.trade.dal.daonew.ShoppingCartSqlMapper;
import com.xianzaishi.trade.dal.model.ShoppingCart;
import com.xianzaishi.trade.dal.model.query.ShoppingCartQuery;
import com.xianzaishi.trade.utils.FloatUtils;
import com.xianzaishi.trade.utils.JsonConvert;
import com.xianzaishi.trade.utils.PageList;
import com.xianzaishi.trade.utils.Pagination;
import com.xianzaishi.trade.utils.enums.Status;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;

/**
 * 
 * @author Croky.Zheng 2016年8月16日
 */
@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
@Component(value = "shoppingCartManager")
public class ShoppingCartManagerImpl implements ShoppingCartManager {
	private static final Logger log = LoggerFactory.getLogger(ShoppingCartManagerImpl.class);

//	@Resource
//	private ShoppingCartDAO shoppingCartDAO;

	@Resource
	private ShoppingCartSqlMapper shoppingCartSqlMapper;
	
    @Resource
	private CommodityService commodityService;
    
    @Resource
    private DiscountInfoService discountInfoService;

	@Resource
	private OrdersManager ordersManager;

	@Resource
	private DeliveryAddressManager deliveryAddressManager;
	
	@Autowired
    private IInventory2CDomainClient iInventory2CDomainClient;

	private static final int NOT_ALLOW_TRADE_ITEM_TAG = 128;//不允许交易标
	
	// @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	@Override
	public long toOrder(long userId, int shopId, Long[] sciIds, long couponId, long addressId, Date distribution, int credit, String description) {
		long orderId = 0L;
		ArrayList<Long> ids = new ArrayList<Long>(sciIds.length);
		for (Long sciId : sciIds) {
			ids.add(sciId);
		}
//		ShoppingCartExample example = new ShoppingCartExample();
//		example.createCriteria().andUserIdEqualTo(userId).andIdIn(ids).andStatusEqualTo(Status.VALID.getValue());
		try {
  		    ShoppingCartQuery query = new ShoppingCartQuery();
  		    query.setIdList(ids);
  		    query.setPageNum(0);
  		    query.setPageSize(ids.size());
  		    List<ShoppingCart> cartList = shoppingCartSqlMapper.select(query);
//			List<ShoppingCart> cartList = shoppingCartDAO.selectByExample(example);
			if (!CollectionUtils.isEmpty(cartList)) {

			  Map<Long,ItemBaseDTO> itemM = Maps.newHashMap();
			  
			  try {
			    List<Long> itemIds = Lists.newArrayList();
	              for (ShoppingCart cart : cartList) {
	                itemIds.add(cart.getItemId());
	              }
	              ItemListQuery it = new ItemListQuery();
	              it.setItemIds(itemIds);
	              Result<Map<String, Object>> itemResult = commodityService.queryCommodies(it);
	              
	              if(null != itemResult && itemResult.getSuccess() && null != itemResult.getModule()){
	                Map<String, Object> itemResultM = itemResult.getModule();
	                List<ItemDTO> itemList = (List<ItemDTO>) itemResultM.get("itemList");
	                for(ItemDTO item:itemList){
	                  itemM.put(item.getItemId(), item);
	                }
	              }
			  }catch(Exception e){
			    log.error("query item info failed!", e);
			  }
			  
				// 更新最新商品信息
				for (ShoppingCart cart : cartList) {
                    ItemBaseDTO item = itemM.get(cart.getItemId());
                    if(null == item){
                      continue;
                    }
                    ItemSkuDTO sku = item.getItemSkuDtos().get(0);
                    if(null == sku){
                      continue;
                    }
                    
                    cart.setItemEffePrice(sku.getDiscountPriceYuan().doubleValue());
                    cart.setItemIconUrl(item.getPicUrl());
                    cart.setItemName(item.getTitle());
                    cart.setItemBillPrice(sku.getPriceYuan().doubleValue());
                    cart.setItemCategoryId(item.getCategoryId());
                    SkuInfoVO skuInfo = skuDTO2SkuInfoVO(sku);
                    // json转换
                    cart.setItemSkuInfo(JsonConvert.object2Json(skuInfo));// item.getSku()
                    cart.setBillAmount(FloatUtils.getFixedScaleDouble(2, sku.getPriceYuan().doubleValue()
                        * cart.getItemCount()));
                    cart.setEffeAmount(FloatUtils.getFixedScaleDouble(2, sku.getDiscountPriceYuan()
                        .doubleValue() * cart.getItemCount()));
                    cart.setUserId(userId);
                    
                    if(sku.getIsSteelyardSku()){
                      cart.setSkuType((short) 2);
                      cart.setUnitWeightNum(sku.getSkuSpecificationNum());
                    }else{
                      cart.setSkuType((short) 1);
                      cart.setUnitWeightNum(1);
                    }
                    
                    update(cart);
				}

				orderId = ordersManager.createOrder(userId, shopId, cartList, couponId, addressId, null, 0, distribution, credit, description);
				if (orderId > 0) {
				    ShoppingCartQuery delQuery = new ShoppingCartQuery();
				    delQuery.setIdList(ids);
				    delQuery.setUserId(userId);
		            shoppingCartSqlMapper.delete(delQuery);
//					shoppingCartDAO.deleteByExample(example);// 测试不删除
				}
			} else {
				throw new RuntimeException("指定的用户和商品条目ID不存在要生成账单的商品");
			}
		} catch (DataAccessException e) {
			log.error("create Order failed.", e);
		}
		deliveryAddressManager.setDefaultLinkman(addressId);
		return orderId;
	}

	@Override
	public long insert(long userId, long itemId, long skuId, double itemCount) {
		return insert(userId, itemId, skuId, itemCount, 0, 0);
	}

	@Override
	public long insert(long userId, long itemId, long skuId, double itemCount, int deviceId, int operator) {
		if (itemCount <= 0) {
			throw new RuntimeException("商品数量不能<=0");
		}
		ItemQuery query = new ItemQuery();
		query.setItemId(itemId);
		query.setSkuId(skuId);
		Result<ItemBaseDTO> result = null;
		try {
			result = commodityService.queryCommodity(query);
		} catch (Exception e) {
			log.error("query item info failed!", e);
		}
		if (result == null || !result.getSuccess() || result.getModule() == null) {
			throw new RuntimeException("找不到对应商品");
		}
		ItemBaseDTO item = result.getModule();
		ItemSkuDTO sku = item.getItemSkuDtos().get(0);

		ShoppingCart cart = get(userId, itemId, skuId);
		if (null != cart) {
			itemCount += cart.getItemCount();
			cart.setItemCount(itemCount);
			cart.setItemEffePrice(sku.getDiscountPriceYuan().doubleValue());
			cart.setItemIconUrl(item.getPicUrl());
			cart.setItemId(itemId);
			cart.setItemName(item.getTitle());
			cart.setItemBillPrice(sku.getPriceYuan().doubleValue());
			cart.setItemSkuId(skuId);
			cart.setItemCategoryId(item.getCategoryId());
			SkuInfoVO skuInfo = skuDTO2SkuInfoVO(sku);
			// json转换
			cart.setItemSkuInfo(JsonConvert.object2Json(skuInfo));// item.getSku()
			cart.setBillAmount(FloatUtils.getFixedScaleDouble(2, sku.getPriceYuan().doubleValue() * itemCount));
			cart.setEffeAmount(FloatUtils.getFixedScaleDouble(2, sku.getDiscountPriceYuan().doubleValue() * itemCount));
			cart.setDeviceId(deviceId);
			cart.setOperator(operator);
			cart.setUserId(userId);
			if(sku.getIsSteelyardSku()){
			  cart.setSkuType((short) 2);
			  cart.setUnitWeightNum(sku.getSkuSpecificationNum());
			}else{
			  cart.setSkuType((short) 1);
			  cart.setUnitWeightNum(1);
			}
			update(cart);
			// updateItemCount(cart, cart.getItemCount().intValue() +
			// itemCount);
			return cart.getId();
		} else {
			cart = new ShoppingCart();
			cart.setItemCount(itemCount);
			cart.setItemEffePrice(sku.getDiscountPriceYuan().doubleValue());
			cart.setItemIconUrl(item.getPicUrl());
			cart.setItemId(itemId);
			cart.setItemName(item.getTitle());
			cart.setItemBillPrice(sku.getPriceYuan().doubleValue());
			cart.setItemSkuId(skuId);
			cart.setItemCategoryId(item.getCategoryId());
			SkuInfoVO skuInfo = skuDTO2SkuInfoVO(sku);
			// json转换
			cart.setItemSkuInfo(JsonConvert.object2Json(skuInfo));// item.getSku()
			cart.setBillAmount(FloatUtils.getFixedScaleDouble(2, sku.getPriceYuan().doubleValue() * itemCount));
			cart.setEffeAmount(FloatUtils.getFixedScaleDouble(2, sku.getDiscountPriceYuan().doubleValue() * itemCount));
			cart.setDeviceId(deviceId);
			cart.setOperator(operator);
			cart.setUserId(userId);
			if(sku.getIsSteelyardSku()){
              cart.setSkuType((short) 2);
              cart.setUnitWeightNum(sku.getSkuSpecificationNum());
            }else{
              cart.setSkuType((short) 1);
              cart.setUnitWeightNum(1);
            }
			return insert(cart);
		}
	}

	@Override
	public boolean updateItemCount(long userId, long itemId, long skuId, int itemCount) {
		ShoppingCart cart = get(userId, itemId, skuId);
		if (null != cart) {
			if (itemCount <= 0) {
				return deleteById(cart.getId(), userId);
			}
			return updateItemCount(cart, itemCount);
		}
		return false;
	}

	@Override
	public boolean subtract(long userId, long itemId, long skuId, double itemCount) {
		ShoppingCart cart = get(userId, itemId, skuId);
		if (null != cart) {
			// 减掉的数量大于总数，直接删除
			if (cart.getItemCount().intValue() <= itemCount) {
				return deleteById(cart.getId(), userId);
			} else {
				return updateItemCount(cart, itemCount - cart.getItemCount().intValue());
			}
		}
		return false;
	}

	@Override
	public ShoppingCart get(long userId, long itemId, long skuId) {
//		ShoppingCartExample example = getExample(userId, itemId, skuId);
		ShoppingCartQuery query = new ShoppingCartQuery();
        query.setUserId(userId);
        query.setSkuId(skuId);
		List<ShoppingCart> cartList = null;
		try {
//			cartList = shoppingCartDAO.selectByExample(example);
			cartList = shoppingCartSqlMapper.select(query);
		} catch (DataAccessException e) {
			log.error("select item to shoppingcart failed.", e);
		}
		if (CollectionUtils.isEmpty(cartList)) {
			log.error("don't select by userId=" + userId + " itemId=" + itemId + " skuId=" + skuId);
		} else {
			return cartList.get(0);
		}
		return null;
	}

	@Override
	public int getAllItemCount(long userId) {
//		ShoppingCartExample example = new ShoppingCartExample();
//		example.createCriteria().andUserIdEqualTo(userId).andStatusEqualTo(Status.VALID.getValue());
		try {
		    Integer count = shoppingCartSqlMapper.count(userId);
		    if(null == count || count <= 0){
		      return 0;
		    }
		    return count;
//			return shoppingCartDAO.countByExample(example);
		} catch (DataAccessException e) {
			log.error("count item to shoppingcart failed.", e);
		}
		return 0;
	}

	@Override
	public int getSumItemCount(long userId) {
		try {
//			Integer sumCount = shoppingCartDAO.getSumItemCount(userId);
		    Integer sumCount = shoppingCartSqlMapper.count(userId);
			if (null != sumCount) {
				return sumCount.intValue();
			}
		} catch (DataAccessException e) {
			log.error("sum item's count to shoppingcart failed.", e);
		}
		return 0;
	}

	@Override
	public List<ShoppingCart> getAllItem(long userId) {
//		ShoppingCartExample example = new ShoppingCartExample();
//		example.createCriteria().andUserIdEqualTo(userId).andStatusEqualTo(Status.VALID.getValue());
		ShoppingCartQuery query = new ShoppingCartQuery();
        query.setUserId(userId);
		List<ShoppingCart> cartList = null;
		try {
//			cartList = shoppingCartDAO.selectByExample(example);
			cartList = shoppingCartSqlMapper.select(query);
		} catch (DataAccessException e) {
			log.error("select item to shoppingcart failed.", e);
		}
		return cartList;
	}

	@Override
	public PageList<ShoppingCart> getItemsByUserId(long userId, int pageNo, int pageSize) {
		List<ShoppingCart> cartList = null;
		int total = this.getAllItemCount(userId);
		Pagination pagin = Pagination.getPagination(pageNo, pageSize, total);
//		if (pagin.limit() > 0) {
		if (total > 0) {
			try {
			    ShoppingCartQuery catQuery = new ShoppingCartQuery();
			    catQuery.setUserId(userId);
			    catQuery.setPageSize(pageSize);
			    catQuery.setPageNum(pageNo);
//				cartList = shoppingCartDAO.selectByUserId(userId, pagin);
			    cartList = shoppingCartSqlMapper.select(catQuery);
				if (cartList != null) {
				    List<Long> itemList = Lists.newArrayList();
    				for (ShoppingCart cart : cartList) {
    				  itemList.add(cart.getItemId());
    				}
    				ItemListQuery itemlistquery = new ItemListQuery();
    				itemlistquery.setItemIds(itemList);
    				itemlistquery.setHasItemSku(true);
    				com.xianzaishi.itemcenter.common.result.Result<Map<String,Object>> commodityList = commodityService.queryCommodies(itemlistquery);
//    				List<Long> removeList = Lists.newArrayList();//过期或者不可交易的直接删除掉
    				if(null != commodityList && commodityList.getSuccess() && null != commodityList.getModule()){
    				  List<ItemDTO> dtoResult = (List<ItemDTO>) commodityList.getModule().get("itemList");
    				  Map<Long,ItemDTO> commodityM = Maps.newHashMap();
    				  for(ItemDTO item:dtoResult){
    				    commodityM.put(item.getItemId(), item);
    				  }
    				  for (ShoppingCart cart : cartList) {
    				    Long itemId = cart.getItemId();
    				    ItemBaseDTO item = commodityM.get(itemId);
                        ItemSkuDTO sku = item.getItemSkuDtos().get(0);
                        int itemTag = sku.getTags() != null ? sku.getTags() : 0;
                        cart.setItemTags(Integer.toBinaryString(itemTag));
                        
                        cart.setItemEffePrice(sku.getDiscountPriceYuan().doubleValue());
                        cart.setItemIconUrl(item.getPicUrl());
                        cart.setItemName(item.getTitle());
                        cart.setItemBillPrice(sku.getPriceYuan().doubleValue());
                        cart.setItemCategoryId(item.getCategoryId());
                        
                        SkuInfoVO skuInfo = skuDTO2SkuInfoVO(sku);
                        // json转换
                        cart.setItemSkuInfo(JsonConvert.object2Json(skuInfo));// item.getSku()
                        cart.setBillAmount(FloatUtils.getFixedScaleDouble(2, sku.getPriceYuan().doubleValue() * cart.getItemCount()));
                        cart.setEffeAmount(FloatUtils.getFixedScaleDouble(2, sku.getDiscountPriceYuan().doubleValue() * cart.getItemCount()));
                        cart.setUserId(userId);
                        update(cart);
    				  }
    				}
				}
				return new PageList<ShoppingCart>(pagin, cartList);
			} catch (Exception e) {
				log.error("select item to shoppingcart failed.", e);
				e.printStackTrace();
			}
		}
		return new PageList<ShoppingCart>(pagin, Lists.<ShoppingCart>newArrayList());
	}

	@Override
	public boolean deleteById(long shoppingCartId, long userId) {
		try {
		    ShoppingCartQuery delQuery = new ShoppingCartQuery();
		    delQuery.setUserId(userId);
		    delQuery.setIdList(Arrays.asList(shoppingCartId));
		    return shoppingCartSqlMapper.delete(delQuery)  > 0;
//			return shoppingCartDAO.deleteByPrimaryKey(shoppingCartId) > 0;
		} catch (DataAccessException e) {
			log.error("delete item to shoppingcart failed.", e);
		}
		return false;
	}

	@Override
	public boolean clean(long userId) {
//		ShoppingCartExample example = new ShoppingCartExample();
//		example.createCriteria().andUserIdEqualTo(userId);
		ShoppingCartQuery delQuery = new ShoppingCartQuery();
        delQuery.setUserId(userId);
		try {
//			return shoppingCartDAO.deleteByExample(example) > 0;
		    return shoppingCartSqlMapper.delete(delQuery)  > 0;
		} catch (DataAccessException e) {
			log.error("clean item to shoppingcart failed.", e);
		}
		return false;
	}

	@Override
	public boolean delete(long userId, long itemId, long skuId) {
//		ShoppingCartExample example = getExample(userId, itemId, skuId);
	    ShoppingCartQuery delQuery = new ShoppingCartQuery();
	    delQuery.setUserId(userId);
	    delQuery.setSkuId(skuId);
		try {
//			return shoppingCartDAO.deleteByExample(example) > 0;
			return shoppingCartSqlMapper.delete(delQuery)  > 0;
		} catch (DataAccessException e) {
			log.error("delete item to shoppingcart failed.", e);
		}
		return false;
	}

	@Override
  public DiscountResultDTO getShoppingCartPromotionInfo(PageList<ShoppingCart> pageList, Long userId) {
	  if(CollectionUtils.isEmpty(pageList)){
	    return null;
	  }
	  DiscountQueryDTO query = new DiscountQueryDTO();
	  query.setUserId(userId);
	  query.setPayType(1);
	  List<BuySkuInfoDTO> buySkuDetail = Lists.newArrayList();
	  for(ShoppingCart cart:pageList){
	    BuySkuInfoDTO bInfo = new BuySkuInfoDTO();
	    bInfo.setBuyCount(cart.getItemCount().toString());
	    bInfo.setSkuId(cart.getItemSkuId());
	    buySkuDetail.add(bInfo);
	  }
	  query.setBuySkuDetail(buySkuDetail);
	  
	  
	  Result<DiscountResultDTO> result = discountInfoService.calculate(query);
	  if(null != result && result.getSuccess() && null != result.getModule()){
	    return result.getModule();
	  }
      return null;
  }

	/**
     * 判断库存是否足够
     * @return
     */
    private boolean isInventoryEnough(SimpleResultVO<InventoryVO> inventoryResult,ShoppingCart cart){
      if(null == cart || null == inventoryResult || !inventoryResult.isSuccess() || null == inventoryResult.getTarget()){
        return false;
      }
      if(inventoryResult.getTarget().getMaySale() && inventoryResult.getTarget().getNumber() > 0 && inventoryResult.getTarget().getNumber() >= cart.getItemCount()){
        return true;//库存数量足够
      }else{
        return false;//库存不足
      }
    }
    
  private boolean update(ShoppingCart cart) {
		try {
			Date now = new Date();
			cart.setGmtModify(now);
//			return shoppingCartDAO.updateByPrimaryKey(cart) > 0;
			return shoppingCartSqlMapper.update(cart) > 0;
		} catch (DataAccessException e) {
			log.error("update item to shoppingcart failed.", e);
		}
		return false;
	}

	private boolean updateItemCount(ShoppingCart cart, double itemCount) {
		cart.setItemCount(itemCount);
		cart.setBillAmount(FloatUtils.getFixedScaleDouble(2, cart.getItemBillPrice() * itemCount));
		cart.setEffeAmount(FloatUtils.getFixedScaleDouble(2, cart.getItemEffePrice() * itemCount));
		return update(cart);
	}

	private long insert(ShoppingCart cart) {
		Date now = new Date();
		cart.setGmtCreate(now);
		cart.setGmtModify(now);
		cart.setStatus(Status.VALID.getValue());
		try {
//		    if (shoppingCartDAO.insert(cart) > 0) {
			if (shoppingCartSqlMapper.insert(cart) > 0) {
				return cart.getId();
			}
		} catch (DataAccessException e) {
			log.error("insert item to shoppingcart failed.", e);
		}
		return 0L;
	}

//	public ShoppingCartDAO getShoppingCartDAO() {
//		return shoppingCartDAO;
//	}

//	public void setShoppingCartDAO(ShoppingCartDAO shoppingCartDAO) {
//		this.shoppingCartDAO = shoppingCartDAO;
//	}
 
	public ShoppingCartSqlMapper getShoppingCartSqlMapper() {
	  return shoppingCartSqlMapper;
	}

	public void setShoppingCartSqlMapper(ShoppingCartSqlMapper shoppingCartSqlMapper) {
	  this.shoppingCartSqlMapper = shoppingCartSqlMapper;
	}
	  
	public CommodityService getCommodityService() {
		return commodityService;
	}

	public void setCommodityService(CommodityService commodityService) {
		this.commodityService = commodityService;
	}

	private SkuInfoVO skuDTO2SkuInfoVO(ItemSkuDTO sku) {
		SkuInfoVO skuInfo = new SkuInfoVO();
		skuInfo.setSpec(sku.getSpecification());
		return skuInfo;
	}
}
