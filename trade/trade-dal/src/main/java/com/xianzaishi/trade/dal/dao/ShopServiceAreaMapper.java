package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.ShopServiceArea;
import com.xianzaishi.trade.dal.model.ShopServiceAreaExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface ShopServiceAreaMapper {
    @SelectProvider(type=ShopServiceAreaSqlProvider.class, method="countByExample")
    int countByExample(ShopServiceAreaExample example);

    @DeleteProvider(type=ShopServiceAreaSqlProvider.class, method="deleteByExample")
    int deleteByExample(ShopServiceAreaExample example);

    @Delete({
        "delete from shop_service_area",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into shop_service_area (code, name, ",
        "shop_id, member_id, ",
        "status, gmt_create, ",
        "gmt_modify)",
        "values (#{code,jdbcType=VARCHAR}, #{name,jdbcType=VARCHAR}, ",
        "#{shopId,jdbcType=INTEGER}, #{memberId,jdbcType=INTEGER}, ",
        "#{status,jdbcType=SMALLINT}, #{gmtCreate,jdbcType=TIMESTAMP}, ",
        "#{gmtModify,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insert(ShopServiceArea record);

    @InsertProvider(type=ShopServiceAreaSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insertSelective(ShopServiceArea record);

    @SelectProvider(type=ShopServiceAreaSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="code", property="code", jdbcType=JdbcType.VARCHAR),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.INTEGER),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ShopServiceArea> selectByExample(ShopServiceAreaExample example);

    @Select({
        "select",
        "id, code, name, shop_id, member_id, status, gmt_create, gmt_modify",
        "from shop_service_area",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="code", property="code", jdbcType=JdbcType.VARCHAR),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.INTEGER),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    ShopServiceArea selectByPrimaryKey(Integer id);

    @UpdateProvider(type=ShopServiceAreaSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") ShopServiceArea record, @Param("example") ShopServiceAreaExample example);

    @UpdateProvider(type=ShopServiceAreaSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") ShopServiceArea record, @Param("example") ShopServiceAreaExample example);

    @UpdateProvider(type=ShopServiceAreaSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(ShopServiceArea record);

    @Update({
        "update shop_service_area",
        "set code = #{code,jdbcType=VARCHAR},",
          "name = #{name,jdbcType=VARCHAR},",
          "shop_id = #{shopId,jdbcType=INTEGER},",
          "member_id = #{memberId,jdbcType=INTEGER},",
          "status = #{status,jdbcType=SMALLINT},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ShopServiceArea record);
}