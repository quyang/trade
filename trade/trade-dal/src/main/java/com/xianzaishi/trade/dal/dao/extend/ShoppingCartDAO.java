package com.xianzaishi.trade.dal.dao.extend;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import com.xianzaishi.trade.dal.dao.ShoppingCartMapper;
import com.xianzaishi.trade.dal.model.ShoppingCart;
import com.xianzaishi.trade.utils.Pagination;

/**
 * 
 * @author Croky.Zheng
 * 废弃，新逻辑  com.xianzaishi.trade.dal.daonew.ShoppingCartSqlMapper
 * 2016年8月16日
 */
@Deprecated
public interface ShoppingCartDAO extends ShoppingCartMapper {

	@Select({
		"select * from shopping_cart where status=1 and user_id=#{userId}",
		"order by id desc limit #{pagination.offset},#{pagination.limit}"
	})
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_id", property="itemId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_sku_id", property="itemSkuId", jdbcType=JdbcType.BIGINT),
        @Result(column="item_sku_info", property="itemSkuInfo", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_name", property="itemName", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_icon_url", property="itemIconUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="item_bill_price", property="itemBillPrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_effe_price", property="itemEffePrice", jdbcType=JdbcType.DOUBLE),
        @Result(column="item_count", property="itemCount", jdbcType=JdbcType.DOUBLE),
        @Result(column="attribute", property="attribute", jdbcType=JdbcType.VARCHAR),
        @Result(column="bill_amount", property="billAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="effe_amount", property="effeAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="device_id", property="deviceId", jdbcType=JdbcType.INTEGER),
        @Result(column="operator", property="operator", jdbcType=JdbcType.INTEGER),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ShoppingCart> selectByUserId(@Param("userId")Long userId,@Param("pagination") Pagination pagination);
	
	@Select({
		"select sum(item_count) from shopping_cart where status=1 and user_id=#{userId}"
	})
	Integer getSumItemCount(@Param("userId")Long userId);
}
