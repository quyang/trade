package com.xianzaishi.trade.web.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.xianzaishi.trade.utils.Base64;
import com.xianzaishi.trade.utils.DESCoder;
import com.xianzaishi.trade.utils.RSACoder;

/**
 * 
 *
 * @author nianyue.hyj
 * @since 2016.10.17
 */
public class SecurityUtils {

	private final static String RSA_PRIVATE_KEY = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAM2pKb+GiF6UP02P/C/BpGxxb4nxAMHWLoaP/AWRMStEdpT2G5SA37wIzF/HvIafZtUiPH6dw2mGlxVfSr6gu6+G7m7HcOd9+6TQ4Qxh4jwjFbezQ6BL9WKta8dfyD0UWL2HX7knESBJXsvbKgoxuKM/VbcV9WrRg5WHw8HzPst1AgMBAAECgYAlgNajHXZawO/2Re54Cvjf6aOsZO19a4Bae4E6xqpPuJFjQqnu371s/bG/OZ4wcnr/HNopQL20zZ1ZgWIQ9yCiYuSb+55c/CTTgI+1duVoJMrc9CM3xR6QOJ4991/+7hyVmNeHGUMcJOvYfqlfNWzj2w6TRZBGgrulBtGTZmia1QJBAOrDhqlppPMI5YkdyFB83KQaB1BlQyBkipv+c4dJDQa3e7Za04xfbsuj/v2mmvkFmv74TeJZ2tmgsmRTfzfTkZ8CQQDgQ7F3y6eCcr56vV/m69oau9FPWb/vLGUDV/rZt/e8U7LYTK3krVYRBPM9kTB7+t8f5zQ0HCvY6JGkzqhPVFJrAkEA5G42pLeoCddZSW8ST8b/BipRfZfaljVebeVMUpHxO9zjYo/EEm11qG69meA4ISCkLX48gof+HA5yJo7cVdzmbwJBAK6wzErOnrskem/NmCiey5RLfS3ccX/zCvRh0gtKHHXclNGcWVBqHULAomDw+d15e9i0FpwI3bbm4pyIRd8VheUCQD7MjWGJBLtzqh/H32xM/uGSYHRvE8iWFKhmiHLOl7vGsqZgB0SM1VelqU21yBxZ0gnyBMshjSDGf76vbTbRlZ4=";

	// private final static String RSA_PUBLIC_KEY =
	// "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDNqSm/hohelD9Nj/wvwaRscW+J8QDB1i6Gj/wFkTErRHaU9huUgN+8CMxfx7yGn2bVIjx+ncNphpcVX0q+oLuvhu5ux3Dnffuk0OEMYeI8IxW3s0OgS/VirWvHX8g9FFi9h1+5JxEgSV7L2yoKMbijP1W3FfVq0YOVh8PB8z7LdQIDAQAB";

	private final static String DES_SEDE_KEY = "s4mzyJSPI/vjm5hXuguGzSXfBA7+9yzV";

	// private final static String CLIENT_RSA_PRIVATE_KEY =
	// "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALfDjkvPMyG0HT/KsDEgPMTtU1j7zTIdYqtxDI9ExV4e58Tr87HzmCFNCyjhlhRU8bi2E24YdYS/71bjSVtLovAAWY7cyIfO65u2PDj1s1K7wj8c/5dxmmgwdIQ/HIqGcCg2W00pfastiphqy97cDfAVvxS1YlQuDRuIVz+T8pqDAgMBAAECgYEAt1FsSJAgsASgER0/+yHvvXwLY39+SfoqjmCuaNXwFl66UyYW63TYVJApC9VPBGVg5iUs6O2Zfp6CQqlmap7foN74whgku9CwGaHWK/XHvKfV3MFlAUfyDuY1NLP4FMfQxqJWkry9hg5X7TTzN5jh808d/2yre92DXCNCLEF7y7ECQQDnKyhlK3iti/jY+w6OFuhTA5Hh8MuYc6PI9Tz9M6/kqF7fNwQDLHbD3PO7n8sfPmfrOOdpHvkQY/tpvbpTdVJlAkEAy4DURnvotj2ZiY+1FT/i3YQ6kQgZCdd6n7HbkERuZxYBKnuD6UVEP5WuWEICujZhc4/yyXu7YBLjvrgIkeF2xwJAcl2Yq4y5NGrmXzq9tbBBmihO+Q/vOeokIpypYr86iw59eUM3mfZaf4YFk1J35Go0cVwCqLApGmHMlUDHONj3PQJAN6j75m9bngTJKtPQfohjv5y7/BEP8B4snuMhn5rn63AcT4dK9BGK8i05a1BauJ3JF7eGe09JV6Sy/+YMCgJ13wJAHPdYkbSxnnXTwAB9FRbtQBHJPzfK6ChGUPruheIM0TCvDbuZXhPqpqi4UtJ1ch+NlagQ6NjT91Bv2KKmM2Wyig==";

	private final static String CLIENT_RSA_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3w45LzzMhtB0/yrAxIDzE7VNY+80yHWKrcQyPRMVeHufE6/Ox85ghTQso4ZYUVPG4thNuGHWEv+9W40lbS6LwAFmO3MiHzuubtjw49bNSu8I/HP+XcZpoMHSEPxyKhnAoNltNKX2rLYqYasve3A3wFb8UtWJULg0biFc/k/KagwIDAQAB";

	private final static Logger logger = LoggerFactory.getLogger(SecurityUtils.class);

	public static String sign(String params) throws Exception {
		return RSACoder.sign(params.getBytes(), RSA_PRIVATE_KEY);
	}

	public static String sign(Object params) throws Exception {
		return RSACoder.sign(toJson(params).getBytes(), RSA_PRIVATE_KEY);
	}

	public static boolean verify(Object params, String sign) {
		if (params != null && StringUtils.isNotBlank(sign)) {
			try {
				return verify(toJson(params), sign);
			} catch (Exception e) {
				logger.error("Fail to verify params -> " + JSONObject.toJSONString(params), e);
			}
		}

		return false;
	}

	public static String toJson(Object params) throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = params.getClass().getDeclaredFields();
		Map<String, Object> wrapper = new HashMap<>();
		for (Field field : fields) {
			if ("sign".equals(field.getName())) {
				continue;
			}
			field.setAccessible(true);
			wrapper.put(field.getName(), field.get(params));
		}
		List<String> keys = new ArrayList<String>(wrapper.keySet());
		Collections.sort(keys);
		JSONObject json = new JSONObject();
		for (String key : keys) {
			json.put(key, wrapper.get(key));
		}

		return json.toJSONString();
	}

	public static boolean verify(String params, String sign) {
		try {
			return verify(params, sign, CLIENT_RSA_PUBLIC_KEY);
		} catch (Exception e) {
			// ignore
		}
		return false;
	}

	public static boolean verify(String params, String sign, String publicKey) throws Exception {
		if (StringUtils.isNotBlank(params) && StringUtils.isNotBlank(sign) && StringUtils.isNotBlank(publicKey)) {
			return RSACoder.verify(params.getBytes(), publicKey, sign);
		}
		return false;
	}

	public static String getClientIp(HttpServletRequest request) {
		String ip = "0.0.0.0";

		if (request == null) {
			return ip;
		}

		ip = request.getHeader("x-forwarded-for");
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		if (StringUtils.isNotBlank(ip) && ip.split(",").length > 1) {
			String[] splits = ip.split(",");
			ip = "0.0.0.0";
			for (String split : splits) {
				if (!"unknown".equalsIgnoreCase(ip)) {
					ip = split;
					break;
				}
			}
		}

		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = "0.0.0.0";
		}

		return ip;
	}

	public static String encode(String json) throws Exception {
		if (StringUtils.isNotBlank(json)) {
			byte[] encode = DESCoder.encrypt(json.getBytes(), DES_SEDE_KEY);
			return Base64.encode(encode);
		}
		return "";
	}

	public static String decode(String data) throws Exception {
		if (StringUtils.isNotBlank(data)) {
			byte[] encode = Base64.decode(data);
			return new String(DESCoder.decrypt(encode, DES_SEDE_KEY));
		}
		return "";
	}

	public static String base64UrlDecoder(String data) throws Exception {
		if (StringUtils.isNotBlank(data)) {
			data = data.replaceAll("-", "+").replaceAll("_", "/").replaceAll("\\.", "=");
		}
		return data;
	}

	public static String base64UrlEncoder(String data) throws Exception {
		if (StringUtils.isNotBlank(data)) {
			data = data.replaceAll("\\+", "-").replaceAll("/", "_").replaceAll("=", ".");
		}
		return data;
	}

}
