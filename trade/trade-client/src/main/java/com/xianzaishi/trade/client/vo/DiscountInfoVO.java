package com.xianzaishi.trade.client.vo;/**
 * Created by Administrator on 2016/12/29 0029.
 */

/**
 * 展示计算后的结果
 *
 * @author jianmo
 * @create 2016-12-29 下午 1:20
 **/
public class DiscountInfoVO {


    /**
     * 计算后得到的页面展示结果，代表具体优惠或者即将享受的优惠描述信息<br/>
     * 如果用在总订单中展示，代表总订单优惠描述<br/>
     * 如果用在单个sku身上，代表该sku优惠描述
     */
    private String discountDesc;

    /**
     * 原始价，不参与优惠时的原始价格<br/>
     * 如果用在总订单中展示，代表总订单价格，包含优惠价格<br/>
     * 如果用在单个sku身上，代表该sku未享受优惠时总价格，（价格乘以数量）
     */
    private String originPrice;

    /**
     * 优惠金额<br/>
     * 如果用在总订单中展示，代表总订单优惠金额<br/>
     * 如果用在单个sku身上，代表该sku享受优惠总金额
     */
    private String discountPrice;

    /**
     * 优惠金额<br/>
     * 只在总订单中有效，代表总订单优惠价格，等于discountPrice<br/>
     */
    private String totalDiscountPrice;

    /**
     * 除去优惠后价格<br/>
     * 如果用在总订单中展示，代表总订单优惠后价格<br/>
     * 如果用在单个sku身上，代表该sku享受优惠后价格
     */
    private String currPrice;

    public String getTotalDiscountPrice() {
        return totalDiscountPrice;
    }

    public void setTotalDiscountPrice(String totalDiscountPrice) {
        this.totalDiscountPrice = totalDiscountPrice;
    }

    public String getDiscountDesc() {
        return discountDesc;
    }

    public void setDiscountDesc(String discountDesc) {
        this.discountDesc = discountDesc;
    }

    public String getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(String originPrice) {
        this.originPrice = originPrice;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getCurrPrice() {
        return currPrice;
    }

    public void setCurrPrice(String currPrice) {
        this.currPrice = currPrice;
    }
}