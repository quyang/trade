package com.xianzaishi.trade.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alipay.util.AlipayCore;
import com.alipay.util.AlipayNotify;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.client.vo.DiscountInfoVO;
import com.xianzaishi.trade.client.vo.OrderItemVO;
import com.xianzaishi.trade.client.vo.OrderVO;
import com.xianzaishi.trade.client.vo.SkuInfoVO;
import com.xianzaishi.trade.core.coupon.CouponManager;
import com.xianzaishi.trade.core.coupon.UserCouponManager;
import com.xianzaishi.trade.core.order.OrdersManager;
import com.xianzaishi.trade.core.order.TradeManager;
import com.xianzaishi.trade.dal.dao.extend.OrderPayLogDAO;
import com.xianzaishi.trade.dal.dao.extend.OrdersDAO;
import com.xianzaishi.trade.dal.model.OrderItem;
import com.xianzaishi.trade.dal.model.OrderPayLog;
import com.xianzaishi.trade.dal.model.OrderPayLogExample;
import com.xianzaishi.trade.dal.model.Orders;
import com.xianzaishi.trade.dal.model.ShoppingCart;
import com.xianzaishi.trade.dal.model.extend.OrderInfo;
import com.xianzaishi.trade.pay.util.PayUtil;
import com.xianzaishi.trade.pay.weixin.CallbackModel;
import com.xianzaishi.trade.pay.weixin.ReturnValue;
import com.xianzaishi.trade.pay.weixin.WeiXinPayConfig;
import com.xianzaishi.trade.utils.JsonConvert;
import com.xianzaishi.trade.utils.PageList;
import com.xianzaishi.trade.utils.Pagination;
import com.xianzaishi.trade.utils.TradeLog;
import com.xianzaishi.trade.utils.enums.OrderStatus;
import com.xianzaishi.trade.utils.enums.PayWay;
import com.xianzaishi.trade.utils.enums.Status;
import com.xianzaishi.trade.web.form.ChargeRecord;
import com.xianzaishi.trade.web.form.OrderForm;
import com.xianzaishi.trade.web.form.OrderFormItem;
import com.xianzaishi.trade.web.model.ResponseModel;
import com.xianzaishi.trade.web.utils.ConstantUtils;
import com.xianzaishi.trade.web.utils.HttpServletUtils;
import com.xianzaishi.trade.web.utils.SecurityUtils;
import com.xianzaishi.trade.web.vo.PageListVO;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.usercenter.client.user.dto.UserDTO;

/**
 * 
 * @author Croky.Zheng 2016年7月20日
 */
@Controller
public class OrderController {
	private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Resource
    protected OrdersDAO ordersDAO;

	@Autowired
	private OrdersManager ordersManager;

	@Autowired
	private UserCouponManager userCouponManager;

	@Autowired
	private CouponManager couponManager;

	@Autowired
	private UserService userService;

	@Autowired
	private TradeManager tradeManager;

	@Resource
	protected OrderPayLogDAO orderPayLogDAO;

	private final static String PAYBACK_KEY = "6c1e86bdc3d6cdd5724c22c20cb85e3b";

	/**
	 * 收银机调用退款，直接退钱
	 * @param orderId
	 * @param amount
	 * @param reason
	 * @param operatorId
	 * @param sign
	 * @return
	 */
	@RequestMapping(value = "/order/payback.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel payback(@RequestParam Long orderId, @RequestParam double amount, @RequestParam String reason,
			@RequestParam Integer operatorId, @RequestParam String sign) {
		try {
			PayBackRequest request = new PayBackRequest(orderId, amount, reason, operatorId);

			if (StringUtils.isBlank(sign) || !sign.equalsIgnoreCase(PayUtil.signByWeiXin(request, PAYBACK_KEY))) {
				return ConstantUtils.errorReponse("签名失败");
			}
			boolean result = ordersManager.payback(orderId, amount, null, "0", null, reason, operatorId);
			if (result) {
				return ConstantUtils.normalResponse();
			} else {
				return ConstantUtils.errorReponse("退款失败");
			}
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 收银机调用创建订单入口
	 * 
	 * @param orderForm
	 *            订单内容JSON格式
	 * @see OrderForm
	 * @return 订单总数
	 */
	@RequestMapping(value = "/order/createOrder.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel createOrder(@RequestBody OrderForm orderForm) {
		if (null != orderForm) {
			List<ShoppingCart> shoppingCarts = new ArrayList<ShoppingCart>(orderForm.getOrderItems().size());
			for (OrderFormItem item : orderForm.getOrderItems()) {
			    log.error("co id is:"+item.getSkuId());
				ShoppingCart cart = this.orderFormItem2VO(item);
				cart.setStatus(Status.VALID.getValue());
				cart.setUserId(orderForm.getUserId());
				cart.setOperator(orderForm.getEmployeeId());
				shoppingCarts.add(cart);
			}
			try {
				long orderId = ordersManager.createOrder(orderForm.getUserId(), orderForm.getShopId(), shoppingCarts, orderForm.getPreferentialId(),
						0, orderForm.getDeviceId(), orderForm.getEmployeeId(), null, 0, "线下购买", orderForm.getOrderType());
				if (orderId > 0) {
					for (ChargeRecord record : orderForm.getRecords()) {
						PayWay payWay = PayWay.from(record.getPayWay());
						if (payWay == PayWay.WEIXIN) {
							payWay = PayWay.WEIXINCODE;
						} else if (payWay == PayWay.ALIPAY) {
							payWay = PayWay.ALIPAYCODE;
						}
						ordersManager.pay(orderId, record.getAmount(), payWay, record.getAccount(), record.getCallbackContent());
					}
					return ConstantUtils.normalResponse(orderId);
				} else {
					return ConstantUtils.errorReponse("创建订单失败");
				}
			} catch (RuntimeException e) {
				return ConstantUtils.errorReponse("创建订单失败:" + e.getMessage());
			}
		}
		return ConstantUtils.errorReponse("参数解析失败");
	}

	/**
	 * app调用对订单发起退款
	 * 
	 * @param uid
	 *            用户ID
	 * @param oid
	 *            订单ID
	 * @param amount
	 *            退款金额
	 * @param cause
	 *            退款原因
	 * @return 订单总数
	 */
	@RequestMapping(value = "/order/refund.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel refund(@RequestParam Long uid, @RequestParam Long oid, @RequestParam Double amount,
			@RequestParam String cause) {
		try {
			boolean result = ordersManager.refund(oid, amount, null, null, null, cause, 0);
			if (result) {
				return ConstantUtils.normalResponse();
			} else {
				return ConstantUtils.errorReponse("退款失败");
			}
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 获取年内订单总数
	 * 
	 * @param uid
	 *            用户ID
	 * @param status[]
	 *            状态数组，可以用status=1&amp;status=2&amp;status=3或者status=1,2,3的方式上传
	 * @return 订单总数
	 */
	@RequestMapping(value = "/order/getOrderCount.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getOrderCount(@RequestParam Long uid, @RequestParam(value = "status", required = false) Short[] status) {
		if (uid <= 0) {
			return ConstantUtils.errorReponse("uid非法");
		}
		OrderStatus[] orderStatus = null;
		if (null != status) {
			int idx = 0;
			orderStatus = new OrderStatus[status.length];
			for (Short state : status) {
				orderStatus[idx++] = OrderStatus.from(state);
			}
		}
		return ConstantUtils.normalResponse(ordersManager.getOrderCountByUserId(uid, orderStatus));
	}

	/**
	 * 获取订单列表
	 * 
	 * @param uid
	 *            用户ID
	 * @param status
	 *            状态数组，可以用status=1&amp;status=2&amp;status=3或者status=1,2,3的方式上传
	 * @param pageSize
	 *            页大小
	 * @param curPage
	 *            当前页
	 * @return 返回订单列表
	 * @see OrderVO
	 */
	@RequestMapping(value = "/order/getOrders.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getOrders(@RequestParam Long uid, @RequestParam Short[] status,
			@RequestParam(value = "pageSize", defaultValue = "99", required = false) Integer pageSize,
			@RequestParam(value = "curPage", defaultValue = "0", required = false) Integer curPage) {
		if (uid <= 0) {
			return ConstantUtils.errorReponse("uid非法");
		}
		PageList<OrderInfo> orderInfoList = null;
		OrderStatus[] orderStatus = null;
		if ((null != status) && (status.length >= 1)) {
			orderStatus = new OrderStatus[status.length];
			for (int i = 0; i < status.length; i++) {
				// 为0则取所有
				if (status[i] == 0) {
					orderStatus = null;
					break;
				}
				OrderStatus state = OrderStatus.from(status[i]);
				if (null == state) {
					return ConstantUtils.errorReponse("不存在的订单状态");
				}
				orderStatus[i] = state;
			}
		}
		try {
			orderInfoList = ordersManager.getOrdersByUserId(uid, orderStatus, curPage, pageSize, true);
			List<OrderVO> voList = null;
			if (CollectionUtils.isNotEmpty(orderInfoList)) {
				voList = new ArrayList<OrderVO>(orderInfoList.size());
				for (OrderInfo orderInfo : orderInfoList) {
					voList.add(info2vo(orderInfo));
				}
				Pagination pagin = orderInfoList.getPagination();
				PageListVO<OrderVO> pageList = new PageListVO<OrderVO>(pagin, voList);
				return ConstantUtils.normalResponse(pageList);
			}
			return ConstantUtils.normalResponse();
		} catch (Exception e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 我们账号被处罚之类，支付宝回调我们系统
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/order/alipaySystemCallback.htm", method = { RequestMethod.POST })
	public void alipaySystemCallback(HttpServletRequest request, HttpServletResponse response) {

		Map<String, String[]> requestParams = request.getParameterMap();
		log.error(String.format("Alipay system notify -> %s", JSON.toJSONString(requestParams)));

		HttpServletUtils.write(response, "success");
	}

	/**
	 * 支付宝回调接口，收钱多少，支付是否成功
	 * 
	 * @param request
	 *            访问对象，由SPRING自动注入
	 * @param response
	 *            回答对象，由SPRING自动注入
	 * @return 支付宝回调接口，成功返回success
	 */
	@RequestMapping(value = "/order/alipayCallBack.htm", method = { RequestMethod.POST })
	public String alipayCallBack(HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> params = new HashMap<String, String>();

		Map<String, String[]> requestParams = request.getParameterMap();

		log.error("alipay origin params -> " + JSON.toJSONString(requestParams));

		TradeLog.info("[AliPay]-[CALLBACK] PARAMS[%s]", JSON.toJSONString(requestParams));

		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
			}
			params.put(name, valueStr);
		}

		// log.error("alipay pretty params -> " + JSON.toJSONString(params));

		String data = AlipayCore.createLinkString(params);
		log.error(data);

		// 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
		// 商户订单号
		try {
			String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
			// 支付宝交易号
			String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

			// 交易状态
			String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");

			// 异步通知ID
			String notify_id = request.getParameter("notify_id");

			String total_fee = request.getParameter("total_fee");

			String buyer_id = request.getParameter("buyer_id");

			// sign
			String sign = URLDecoder.decode(request.getParameter("sign"), "utf-8");

			response.setContentType("text/html; charset=utf-8");
			PrintWriter out = response.getWriter();

			// 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//

			if (notify_id != "" && notify_id != null) {//// 判断接受的post通知中有无notify_id，如果有则是异步通知。
				if (AlipayNotify.verifyResponse(notify_id).equals("true")) // 判断成功之后使用getResponse方法判断是否是支付宝发来的异步通知。
				{
					// if(AlipayNotify.getSignVeryfy(params, sign))//使用支付宝公钥验签
					{
						// 订单号
						long orderId = Long.parseLong(StringUtils.right(out_trade_no, 16));

						double amount = Double.parseDouble(total_fee);

						// ——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
						if (trade_status.equals("TRADE_FINISHED")) {
							// 判断该笔订单是否在商户网站中已经做过处理
							// 如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
							// 如果有做过处理，不执行商户的业务程序
							// 注意：
							// 退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
							// 请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的

							ordersManager.pay(orderId, amount, PayWay.ALIPAY, trade_no, data);
						} else if (trade_status.equals("TRADE_SUCCESS")) {
							// 判断该笔订单是否在商户网站中已经做过处理
							// 如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
							// 如果有做过处理，不执行商户的业务程序
							// 注意：
							// 付款完成后，支付宝系统发送该交易状态通知
							// 请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的
							ordersManager.pay(orderId, amount, PayWay.ALIPAY, trade_no, data);
						}
						// ——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
						out.print("success");// 请不要修改或删除

						// 调试打印log
						// AlipayCore.logResult("notify_url
						// success!","notify_url");
					}
					// else//验证签名失败
					// {
					// out.print("sign fail");
					// }
				} else// 验证是否来自支付宝的通知失败
				{
					out.print("response fail");
				}
			} else {
				out.print("no notify message");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 使用return null来定位信息
		return null;
	}

	/**
	 * 支付宝回调接口，支付宝最新回调接口
	 * 
	 * @param request
	 *            访问对象，由SPRING自动注入
	 * @param response
	 *            回答对象，由SPRING自动注入
	 * @return 支付宝回调接口，成功返回success
	 */
	@RequestMapping(value = "/alipay/qrPayCallback.htm", method = { RequestMethod.POST })
	public String alipayQrPayCallBack(HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> params = new HashMap<String, String>();

		Map<String, String[]> requestParams = request.getParameterMap();

		log.error("alipay origin params -> " + JSON.toJSONString(requestParams));

		TradeLog.info("[AliPay]-[CALLBACK] PARAMS[%s]", JSON.toJSONString(requestParams));

		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
			}
			params.put(name, valueStr);
		}

		// log.error("alipay pretty params -> " + JSON.toJSONString(params));

		String data = AlipayCore.createLinkString(params);
		log.error(data);

		// 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
		// 商户订单号
		try {
			String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
			// 支付宝交易号
			String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

			// 交易状态
			String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");

			// 异步通知ID
			String notify_id = request.getParameter("notify_id");

			String total_fee = request.getParameter("total_amount");

			String buyer_id = request.getParameter("buyer_id");

			// sign
			String sign = URLDecoder.decode(request.getParameter("sign"), "utf-8");

			response.setContentType("text/html; charset=utf-8");
			PrintWriter out = response.getWriter();

			// 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//

			if (notify_id != "" && notify_id != null) {//// 判断接受的post通知中有无notify_id，如果有则是异步通知。
				// 订单号
				long orderId = Long.parseLong(StringUtils.right(out_trade_no, 16));

				double amount = Double.parseDouble(total_fee);

				// ——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				if ("TRADE_SUCCESS".equals(trade_status)) {
					// 判断该笔订单是否在商户网站中已经做过处理
					// 如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
					// 如果有做过处理，不执行商户的业务程序
					// 注意：
					// 付款完成后，支付宝系统发送该交易状态通知
					// 请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的
					ordersManager.pay(orderId, amount, PayWay.ALIPAY_QR, trade_no, data);
				}
				// ——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
				out.print("success");// 请不要修改或删除

				// 调试打印log
				// AlipayCore.logResult("notify_url
				// success!","notify_url");
			} else {
				out.print("no notify message");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 使用return null来定位信息
		return null;
	}

	/**
	 * 支付宝退款后回调，退款多少钱，退款是否成功
	 * @param requestz
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/order/alipayRefundCallBack.htm", method = { RequestMethod.POST })
	public String alipayRefundCallBack(HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> params = new HashMap<String, String>();

		Map<String, String[]> requestParams = request.getParameterMap();

		log.error("alipay origin params -> " + JSON.toJSONString(requestParams));

		TradeLog.info("[AliPay]-[CALLBACK_REFUND] PARAMS[%s]", JSON.toJSONString(requestParams));

		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
			}
			params.put(name, valueStr);
		}

		// log.error("alipay pretty params -> " + JSON.toJSONString(params));

		String data = AlipayCore.createLinkString(params);
		log.error(data);

		// 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
		// 商户订单号

		try {

			response.setContentType("text/html; charset=utf-8");
			PrintWriter out = response.getWriter();

			long num = Long.parseLong(params.get("success_num"));
			if (num == 1) {
				String[] details = params.get("result_details").split("\\^");
				String account = details[0];
				double refundTotal = Double.parseDouble(details[1]);
				String result = details[2];
				if ("success".equalsIgnoreCase(result)) {

					List<OrderPayLog> payLogs = null;
					try {
						OrderPayLogExample example = new OrderPayLogExample();
						example.createCriteria().andAccountEqualTo(account).andPayWayIn(Lists.newArrayList(PayWay.ALIPAY.getValue()));
						payLogs = orderPayLogDAO.selectByExample(example);
					} catch (DataAccessException e) {
						log.error("get orderPayLogs failed", e);
					}

					if (payLogs != null && !payLogs.isEmpty()) {
						long orderId = payLogs.get(0).getOrderId();
						if (ordersManager.paybackDirect(orderId, refundTotal, PayWay.ALIPAY, account, "支付宝退款", 0)) {
							out.println("SUCCESS");
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("Fail to process alipay refund callback.");
		}

		// 使用return null来定位信息
		return null;
	}

	/**
	 * 微信支付成功后回调系统
	 * @param content
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/order/weixinPayCallBack.htm", method = { RequestMethod.POST })
	public void weixinPayCallBack(@RequestBody String content, HttpServletRequest request, HttpServletResponse response) {

		if (log.isDebugEnabled()) {
			log.debug(String.format("weixin callback -> %s", content));
		}

		TradeLog.info("[WeiXin]-[CALLBACK] PARAMS[%s]", content != null ? content.replaceAll("\r|\n", "") : content);

		response.setContentType("text/html; charset=utf-8");
		ReturnValue returnValue = new ReturnValue();

		if (StringUtils.isBlank(content)) {
			returnValue.setReturn_code(WeiXinPayConfig.WEIXIN_CODE_FAIL);
			returnValue.setReturn_msg("无法解析空内容");
			HttpServletUtils.write(response, PayUtil.objectToXML(returnValue));
			return;
		}

		String resultCode = WeiXinPayConfig.WEIXIN_CODE_FAIL;
		try {
			Document document = DocumentHelper.parseText(content);

			Element root = document.getRootElement();

			resultCode = root.element("return_code").getText();

		} catch (Exception e) {
			log.error(String.format("Fail to parse xml content -> ", content), e);
			returnValue.setReturn_code(WeiXinPayConfig.WEIXIN_CODE_FAIL);
			returnValue.setReturn_msg("内容格式错误");
			HttpServletUtils.write(response, PayUtil.objectToXML(returnValue));
			return;
		}

		if (!WeiXinPayConfig.WEIXIN_CODE_SUCCESS.equalsIgnoreCase(resultCode)) {
			log.warn(String.format("weixin callback fail -> %s", content));
			returnValue.setReturn_code(WeiXinPayConfig.WEIXIN_CODE_FAIL);
			returnValue.setReturn_msg("回调通讯失败");
			HttpServletUtils.write(response, PayUtil.objectToXML(returnValue));
			return;
		}

		try {
			CallbackModel callback = PayUtil.xmlToObject(content, CallbackModel.class);

			String sign = PayUtil.signByWeiXin(callback, WeiXinPayConfig.APP_KEY);
			if (!sign.equals(callback.getSign())) {
				log.warn(String.format("weixin callback sign error -> %s", content));
				returnValue.setReturn_code(WeiXinPayConfig.WEIXIN_CODE_FAIL);
				returnValue.setReturn_msg("签名校验失败");
				HttpServletUtils.write(response, PayUtil.objectToXML(returnValue));
				return;
			}

			returnValue.setReturn_code(WeiXinPayConfig.WEIXIN_CODE_SUCCESS);
			returnValue.setReturn_msg("OK");

			if (WeiXinPayConfig.WEIXIN_CODE_SUCCESS.equalsIgnoreCase(callback.getResult_code())) {
				// 支付成功
				long orderId = Long.parseLong(StringUtils.right(callback.getOut_trade_no(), 16));
				double amount = (double) callback.getTotal_fee() / 100.0;
				ordersManager.pay(orderId, amount, PayWay.WEIXIN, callback.getTransaction_id(), content);
			} else {
				// 支付失败
				log.warn(String.format("weixin callback pay fail -> %s", content));
			}

			HttpServletUtils.write(response, PayUtil.objectToXML(returnValue));
		} catch (Exception e) {
			log.warn(String.format("weixin callback internal fail -> %s", content));
			returnValue.setReturn_code(WeiXinPayConfig.WEIXIN_CODE_FAIL);
			returnValue.setReturn_msg("服务器内部错误");
			HttpServletUtils.write(response, PayUtil.objectToXML(returnValue));
		}
	}

	/**
	 * 暂时无用，早餐券使用
	 * @param content
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/order/weixinPayCallBack2.htm", method = { RequestMethod.POST })
	public String weixinPayCallBack2(@RequestBody String content, HttpServletRequest request, HttpServletResponse response) {
		// InputStream is = request.getInputStream();
		// 使用return null来定位信息
		log.error("callback by weixin -> " + content);
		response.setContentType("text/html; charset=utf-8");
		ReturnValue returnValue = new ReturnValue();
		returnValue.setReturn_code("SUCCESS");
		try {
			PrintWriter out = response.getWriter();
			if (StringUtils.isNotEmpty(content)) {
				CallbackModel model = PayUtil.xmlToObject(content, CallbackModel.class);
				if (null != model) {
					if ("SUCCESS".equals(model.getReturn_code())) {
						long userId = Long.parseLong(model.getOut_trade_no());
						String sign = PayUtil.signByWeiXin(model, "d7Da7b62646b2a7dd0d62a2b3d0d123T");
						if (sign.equals(model.getSign()) && "SUCCESS".equals(model.getResult_code())) {
							// 支付成功
							returnValue.setReturn_msg("OK");
							log.warn("user weixin pay success <-> " + userId);
							try {
								Result<? extends BaseUserDTO> userResult = userService.queryUserByUserId(userId, false);
								if (userResult != null && userResult.getSuccess() && userResult.getModule() != null) {
									BaseUserDTO user = userResult.getModule();
									UserDTO dto = new UserDTO();
									dto.setUserId(user.getUserId());
									dto.setUserTag(user.getStatus() | 0x2);
									Result<Boolean> userUpdateResult = userService.updateUserProfileInfo(dto);
									log.warn("user tag update result -> " + JSON.toJSONString(userUpdateResult));
								}
							} catch (Exception e) {
								log.error("fail to update user result -> " + content, e);
							}
							try {
							  //早餐券下线
//								List<Coupon> coupons = couponManager.getCouponsByType((short) 77);
//								List<UserCoupon> userCoupons = userCouponManager.getUserCoupons(userId);
//								Set<Long> exists = new HashSet<>();
//								if (CollectionUtils.isNotEmpty(userCoupons)) {
//									for (UserCoupon coupon : userCoupons) {
//										exists.add(coupon.getCouponId());
//									}
//								}
//								if (CollectionUtils.isNotEmpty(coupons)) {
//									for (Coupon coupon : coupons) {
//										if (!exists.contains(coupon.getId())) {
//											userCouponManager.insert(userId, coupon.getId());
//										}
//									}
//								}
							} catch (Exception e) {
								log.error("fail to add coupon -> " + content, e);
							}
						} else {
							log.warn("user weixin pay fail xml -> " + content);
							returnValue.setReturn_code("FAIL");
							returnValue.setReturn_msg("签名或者支付失败");
						}
					}
				}
			} else {
				returnValue.setReturn_code("FAIL");
				returnValue.setReturn_msg("缺少POST数据");
			}
			out.print(PayUtil.objectToXML(returnValue));
			out.flush();
		} catch (IOException e) {
			log.error("Fail to receive weixin msg -> " + content, e);
		}
		return null;
	}

	/**
	 * 获取alipay支付的签名
	 * 
	 * @param oid
	 *            订单ID
	 * @return 支付宝的完整签名数据
	 */
	@RequestMapping(value = "/order/getAlipaySign.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getAlipaySign(@RequestParam Long oid) {
		if (oid <= 0) {
			return ConstantUtils.errorReponse("获取支付宝支付凭证失败");
		}
		try {
			return ConstantUtils.normalResponse(ordersManager.getAlipaySignature(oid));
		} catch (Exception e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 获取alipay支付的签名，暂时不用，别人扫我们，产生预付订单
	 * 
	 * @param oid
	 *            订单ID
	 * @return 支付宝的完整签名数据
	 */
	@RequestMapping(value = "/order/getAlipayQrSign.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getAlipayQrSign(@RequestParam Long oid) {
		if (oid <= 0) {
			return ConstantUtils.errorReponse("获取支付宝支付凭证失败");
		}
		try {
			return ConstantUtils.normalResponse(ordersManager.getAlipayQrSignature(oid));
		} catch (Exception e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 用户给我们条形码，我们扣钱
	 * @param oid
	 * @param authCode
	 * @return
	 */
	@RequestMapping(value = "/order/getAlipayAuthCodePay.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getAlipayAuthCodePay(@RequestParam Long oid, @RequestParam String authCode) {
		if (oid <= 0) {
			return ConstantUtils.errorReponse("获取支付宝支付凭证失败");
		}
		try {
			return ConstantUtils.normalResponse(ordersManager.alipayByAuthCode(oid, authCode));
		} catch (Exception e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 获取微信统一预支付的签名，产生预付订单
	 * 
	 * @param oid
	 *            订单ID
	 * @return 微信的完整签名数据
	 */
	@RequestMapping(value = "/order/getWeixinpaySign.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getWeixinpaySign(@RequestParam Long oid, HttpServletRequest request) {
		if (oid <= 0) {
			return ConstantUtils.errorReponse("获取微信支付凭证失败");
		}
		try {
			return ConstantUtils.normalResponse(ordersManager.getWeixinpaySignature(oid, SecurityUtils.getClientIp(request)));
		} catch (Exception e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 获取微信统一预支付的签名，已经不用，是1元早餐券
	 * 
	 * @param uid
	 *            用户ID
	 * @return 微信的完整签名数据
	 */
	@RequestMapping(value = "/order/getWeixinpaySign2.json")
	public @ResponseBody JSONPObject getWeixinpaySign2(@RequestParam(required = false) Long userId, @RequestParam(required = true) String token,
			@RequestParam(required = true) String openId, @RequestParam(required = false) String callback) {
		if (StringUtils.isBlank(token) || !CALLBACK_PATTERN.matcher(callback).matches()) {
			return new JSONPObject("error", ConstantUtils.errorReponse("参数错误"));
		}
		try {
			Result<BaseUserDTO> result = userService.queryBaseUserByToken(token);
			if (result == null || result.getModule() == null || result.getModule().getUserId() == null || result.getModule().getUserId() <= 0) {
				return new JSONPObject("error", ConstantUtils.errorReponse("无法查询到登录用户信息"));
			}
			userId = result.getModule().getUserId();

			return new JSONPObject(callback, ConstantUtils.normalResponse(ordersManager.getWeixinGongZongHaoPaySignature(userId, openId)));
		} catch (Exception e) {
			return new JSONPObject("error", ConstantUtils.errorReponse(e.getMessage()));
		}
	}

	private final static Pattern CALLBACK_PATTERN = Pattern.compile("[a-zA-Z]+");

	/**
	 * 获取订单详情
	 * 
	 * @param oid
	 *            订单ID
	 * @return 返回一个订单的所有相关数据
	 * @see OrderVO 订单实体
	 * @see OrderItemVO 订单商品实体
	 */
	@RequestMapping(value = "/order/getOrderDetail.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getOrderDetail(@RequestParam Long oid) {
		if (oid <= 0) {
			return ConstantUtils.errorReponse("获取用户订单详情失败");
		}
		try {
			OrderInfo orderInfo = ordersManager.getOrder(oid);
			if (null != orderInfo) {
				OrderVO vo = info2vo(orderInfo);
				Map<String,DiscountInfoVO> orderDiscount = ordersManager.getOrderDiscount(oid);
				vo.setOrderDiscount(orderDiscount);
				return ConstantUtils.normalResponse(vo);
			} else {
				return ConstantUtils.errorReponse("订单:" + oid + "不存在");
			}
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	@RequestMapping(value = "/order/getOrderDetails.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getOrderDetails(@RequestParam List<Long> orderIds) {
		if (orderIds == null || orderIds.isEmpty()) {
			return ConstantUtils.errorReponse("请求参数错误");
		}
		try {
			List<OrderVO> orders = new ArrayList<>();
			for (long orderId : orderIds) {
				OrderInfo orderInfo = ordersManager.getOrder(orderId);
				if (null != orderInfo) {
					orders.add(info2vo(orderInfo));
				}
			}
			if (!orders.isEmpty()) {
				return ConstantUtils.normalResponse(orders);
			} else {
				return ConstantUtils.errorReponse("系统繁忙,请稍后重试");
			}
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 获取订单状态
	 * 
	 * @param oid
	 *            订单ID
	 * @return 返回一个订单的状态
	 */
	@RequestMapping(value = "/order/getOrderStatus.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getOrderStatus(@RequestParam Long oid) {
		if (oid <= 0) {
			return ConstantUtils.errorReponse("获取用户订单状态失败");
		}

		try {
			OrderInfo orderInfo = ordersManager.getOrder(oid);
			if (null != orderInfo) {
				return ConstantUtils.normalResponse(orderInfo.getStatus());
			} else {
				return ConstantUtils.errorReponse("订单:" + oid + "不存在");
			}
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 关闭交易，app取消订单
	 * 
	 * @param oid
	 *            订单ID
	 * @return ResponseModel
	 * @see ResponseModel
	 */
	@RequestMapping(value = "/order/cancelTrade.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel cancelTrade(@RequestParam Long oid) {
		if (oid <= 0) {
			return ConstantUtils.errorReponse("关闭交易失败");
		}

        Orders info = ordersDAO.selectByPrimaryKey(oid);
        if (info != null && info.getStatus() == 6 ) {
            log.error("order status="+info.getStatus());
            return ConstantUtils.errorReponse("配送中的订单不能取消");
        }

		try {
			boolean result = tradeManager.tryCancelTrade(oid, "APP关闭交易");
			if (result) {
				return ConstantUtils.normalResponse();
			} else {
				return ConstantUtils.errorReponse("关闭交易失败");
			}
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 更好订单状态
	 * 
	 * @param oid
	 *            订单ID
	 * @param status
	 *            新的状态
	 * @return 更改订单状态，不建议使用的接口
	 * @see ResponseModel
	 */
	@Deprecated
	@RequestMapping(value = "/order/changeOrderStatus.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel changeOrderStatus(@RequestParam Long oid, @RequestParam Byte status) {
		if (oid <= 0) {
			return ConstantUtils.errorReponse("更新订单状态失败");
		}
		return ConstantUtils.normalResponse();
	}

	/**
     * 获取用户的支付方式 优惠和积分方式不返回
     * @param oid
     * @return
     */
    @RequestMapping(value = "/order/getOrderPayLog.json", method = { RequestMethod.POST })
    public  @ResponseBody ResponseModel getOrderPayLog(@RequestParam Long oid){
        if (oid <= 0) {
            return ConstantUtils.errorReponse("获取用户订单状态失败");
        }
        try{
            List<OrderPayLog> payLogList =ordersManager.getOrderPayLog(oid);
            if (CollectionUtils.isNotEmpty(payLogList)) {
                for (OrderPayLog payLog : payLogList) {
                    short payWayValue = payLog.getPayWay().shortValue();
                    if ((payWayValue == PayWay.YINTONG.getValue()) || (payWayValue == PayWay.ALIPAY.getValue())
                            || (payWayValue == PayWay.WEIXIN.getValue())
                            || (payWayValue == PayWay.WEIXINCODE.getValue()) || (payWayValue == PayWay.ALIPAYCODE.getValue())) {

                        return   ConstantUtils.normalResponse(payLog.getPayWay()); 
                    }
                }
            }
            return ConstantUtils.errorReponse("无支付方式");

        }catch (RuntimeException e) {
            return ConstantUtils.errorReponse(e.getMessage());
        }

    }
    
	private OrderVO info2vo(OrderInfo orderInfo) {
		OrderVO vo = new OrderVO();
		vo.setCashierId(orderInfo.getCashierId());
		vo.setDeviceId(orderInfo.getDeviceCode());
		vo.setEffeAmount(orderInfo.getEffeAmount() != null ? String.format("%.2f", orderInfo.getEffeAmount()) : null);
		vo.setPayAmount(orderInfo.getPayAmount() != null ? String.format("%.2f", orderInfo.getPayAmount()) : null);
		vo.setDiscountAmount(orderInfo.getDiscountAmount() != null ? String.format("%.2f", orderInfo.getDiscountAmount()) : null);
		vo.setId(orderInfo.getId());
		vo.setUserAddressId(58L);
		vo.setSeq(orderInfo.getSeq());
		vo.setStatus(orderInfo.getStatus());
		vo.setStatusString(OrderStatus.from(orderInfo.getStatus()).getDescription());
		vo.setGmtPay(orderInfo.getGmtPay());
		vo.setGmtEnd(orderInfo.getGmtReceive());
		vo.setGmtCreate(orderInfo.getGmtCreate());
		vo.setUserAddressId(orderInfo.getUserAddressId());
		vo.setUserAddress(orderInfo.getUserAddress());
		vo.setShopId(orderInfo.getShopId());
		vo.setGmtDistribution(orderInfo.getGmtDistribution());
		vo.setCredit(orderInfo.getCredit());
		vo.setCouponId(orderInfo.getCouponId());
		vo.setUserId(orderInfo.getUserId());
		vo.setChannelType(orderInfo.getChannelType());
		vo.setAttribute(orderInfo.getAttribute());
		List<OrderItemVO> items = null;
		if (CollectionUtils.isNotEmpty(orderInfo.getItems())) {
			items = new ArrayList<OrderItemVO>(orderInfo.getItems().size());
			for (OrderItem orderItem : orderInfo.getItems()) {
				items.add(orderItem2VO(orderItem));
			}
		}
		vo.setItems(items);
		return vo;
	}

	private OrderItemVO orderItem2VO(OrderItem item) {
		OrderItemVO vo = new OrderItemVO();
		vo.setAmount(item.getItemBillAmount());
		vo.setCategoryId(item.getItemCategoryId());
		vo.setCount(item.getItemCount());
		vo.setIconUrl(item.getItemIconUrl());
		vo.setId(item.getItemId());
		vo.setName(item.getItemName());
		vo.setPrice(item.getItemBillPrice());
		vo.setSkuId(item.getItemSkuId());
		vo.setSkuInfo(JsonConvert.json2Object(item.getItemSkuInfo(), SkuInfoVO.class));
		vo.setEffeAmount(item.getItemEffeAmount());
		vo.setEffePrice(item.getItemEffePrice());
		return vo;
	}

	private ShoppingCart orderFormItem2VO(OrderFormItem item) {
		ShoppingCart cart = new ShoppingCart();
		cart.setBillAmount(item.getAmount());
		cart.setEffeAmount(item.getEffeAmount());
		cart.setItemCategoryId(item.getCategory_id());
		cart.setItemCount(item.getCount() * 1.0);
		cart.setItemIconUrl(item.getIcon_url());
		cart.setItemId(item.getId());
		cart.setItemName(item.getTitle());
		cart.setItemSkuId(item.getSkuId());
		cart.setItemEffePrice(item.getEffePrice());
		cart.setItemBillPrice(item.getPrice());
		SkuInfoVO info = new SkuInfoVO();
		info.setSpec(item.getSpecification());
		cart.setItemSkuInfo(JsonConvert.object2Json(info));
		cart.setSkuBarCode(item.getSkuBarCode());
		return cart;
	}

	private static class PayBackRequest {

		private Long orderId;

		private Double amount;

		private String cause;

		private Integer operatorId;

		public PayBackRequest(Long orderId, Double amount, String cause, Integer operatorId) {
			this.orderId = orderId;
			this.amount = amount;
			this.cause = cause;
			this.operatorId = operatorId;
		}

		public Long getOrderId() {
			return orderId;
		}

		public void setOrderId(Long orderId) {
			this.orderId = orderId;
		}

		public Double getAmount() {
			return amount;
		}

		public void setAmount(Double amount) {
			this.amount = amount;
		}

		public String getCause() {
			return cause;
		}

		public void setCause(String cause) {
			this.cause = cause;
		}

		public Integer getOperatorId() {
			return operatorId;
		}

		public void setOperatorId(Integer operatorId) {
			this.operatorId = operatorId;
		}

	}
}
