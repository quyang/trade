package com.xianzaishi.trade.facade.order;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.trade.client.PromotionService;
import com.xianzaishi.trade.client.Result;
import com.xianzaishi.trade.client.vo.UserCouponDiscontQueryVO;
import com.xianzaishi.trade.client.vo.UserCouponDiscontResultVO;
import com.xianzaishi.trade.client.vo.UserCouponVO;
import com.xianzaishi.trade.core.coupon.PromotionManager;
import com.xianzaishi.trade.core.coupon.UserCouponManager;
import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.trade.utils.enums.Status;

@Service(value = "promotionService")
public class PromotionServiceImpl implements PromotionService {

  @Autowired
  private PromotionManager promotionManager;

  @Autowired
  private UserCouponManager userCouponManager;

  @Override
  public Result<UserCouponDiscontResultVO> selectMathCoupon(UserCouponDiscontQueryVO query) {
//    UserCouponDiscontResultVO checkResult = promotionManager.selectMathCoupon(query);
//    if (null != checkResult) {
//      return new Result<>(0, "查询成功", checkResult);
//    }
    return new Result<>(-1, "服务器迁移", null);
  }

  @Override
  public Result<UserCouponDiscontResultVO> calculateDiscountPriceWithCoupon(
      UserCouponDiscontQueryVO query) {
//    UserCouponDiscontResultVO checkResult =
//        promotionManager.calculateDiscountPriceWithCoupon(query);
//    if (null != checkResult) {
//      return new Result<>(0, "查询成功", checkResult);
//    }
    return new Result<>(-1, "服务器迁移", null);
  }

  @Override
  public Result<List<UserCouponVO>> selectUserCoupon(UserCouponDiscontQueryVO query) {
//    if (query.getQueryType() == 0) {
//      List<UserCoupon> userCoupons =
//          userCouponManager.getUserCouponsByStatus(query.getUserId(),
//              Lists.newArrayList((short) 1, (short) 11, (short) 12, (short) 77), Status.VALID);
//      if(CollectionUtils.isNotEmpty(userCoupons)){
//        List<UserCouponVO> queryResult = Lists.newArrayList();
//        BeanCopierUtils.copyListBean(userCoupons, queryResult, UserCouponVO.class);
//        return new Result<>(0, "查询成功", queryResult);
//      }else{
//        return new Result<>(0, "查询成功", null);
//      }
//    }else{
//      UserCouponDiscontResultVO checkResult = promotionManager.selectMathCoupon(query);
//      if (null != checkResult) {
//        return new Result<>(0, "查询成功", checkResult.getUsedCouponList());
//      }else{
//        return new Result<>(-1, "服务器内部错误", null);
//      }
//    }
    return new Result<>(-1, "服务器迁移", null);
  }

}
