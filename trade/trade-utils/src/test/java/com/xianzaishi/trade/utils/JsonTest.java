package com.xianzaishi.trade.utils;

import java.util.List;

import org.junit.Test;

public class JsonTest {

	@Test
	public void testPagination() {
		Pagination pagin = Pagination.getPagination(0, 20, 5032);
		PageList<Object> pageList = new PageList<Object>(pagin, null);
		PageListDO pageListDO = new PageListDO();
		pageListDO.setPagin(pagin);
		System.out.println(JsonConvert.object2Json(pageListDO));
		System.out.println(JsonConvert.object2Json(pageList));
	}
	
	class PageListDO {
		private Pagination pagin;
		
		private List objects = null;

		public Pagination getPagin() {
			return pagin;
		}

		public void setPagin(Pagination pagin) {
			this.pagin = pagin;
		}

		public List getObjects() {
			return objects;
		}

		public void setObjects(List objects) {
			this.objects = objects;
		}
		
	}
}
