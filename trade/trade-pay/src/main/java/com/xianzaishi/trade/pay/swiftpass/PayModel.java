package com.xianzaishi.trade.pay.swiftpass;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.croky.util.StringUtils;
import com.xianzaishi.trade.pay.util.PayUtil;

/**
 * 
 * @author Croky.Zheng
 * 2016年9月3日
 */
//将一个Java类映射为一段XML的根节点
@XmlRootElement(name="xml")//(name="pay_model",namespace=null)
@XmlAccessorType 
public class PayModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -398383394142179505L;
	
	/**
	 * 接口类型 ,unified.trade.micropay
	 */
	private String service = "unified.trade.micropay";
	
	private String version = "2.0";
	
	private String charset = "UTF-8";
	
	private String sign_type = "MD5";
	/**
	 * 商户号，威富通分配
	 */
	private String mch_id = "7551000001";
	
	/**
	 * 大商户编号，如果不为空,则用大商户密钥进行签名
	 */
	private String groupno = null;
	
	/**
	 * 商户订单号
	 */
	private String out_trade_no = "11223344556677889900";

	/**
	 * 设备号,威富通分配
	 */
	private String device_info = null;
	
	/**
	 * 商品描述
	 */
	private String body = "二牛大烧饼";
	
	/**
	 * 附加信息
	 */
	private String attach = null;
	
	/**
	 * 总金额，以分为单位
	 */
	private int total_fee = 1;
	
	/**
	 * 终端IP
	 */
	private String mch_create_ip = "127.0.0.1";//NetUtils.getLocalAddress();
	
	/**
	 * 授权码,扫码支付授权码,设备读取用户展示的条码或者二维码信息
	 */
	private String auth_code = "130058611877672470";
	//283977409967500862
	
	/**
	 * 订单生产时间，格式yyyymmddhhmmss
	 */
	private String time_start = "201609061756";
	
	/**
	 * 订单超时时间
	 */
	private String time_expire= "201609091756";
	
	/**
	 * 操作员帐号,默认为商户号(32)
	 */
	private String op_user_id;
	
	/**
	 * 门店编号(32)
	 */
	private String op_shop_id = "100530020296";
	
	/**
	 * 操作设备号(32)
	 */
	private String op_device_id;
	
	/**
	 * 商品标记(32)
	 */
	private String goods_tag;
	
	/**
	 * 随机字符串（32）
	 */
	private String nonce_str = "croky_1982_63";
	
	/**
	 * MD5 签名结果（32）
	 */
	private String sign;

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String getMch_id() {
		return mch_id;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	public String getGroupno() {
		return groupno;
	}

	public void setGroupno(String groupno) {
		this.groupno = groupno;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getDevice_info() {
		return device_info;
	}

	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public int getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(int total_fee) {
		this.total_fee = total_fee;
	}

	public String getMch_create_ip() {
		return mch_create_ip;
	}

	public void setMch_create_ip(String mch_create_ip) {
		this.mch_create_ip = mch_create_ip;
	}

	public String getAuth_code() {
		return auth_code;
	}

	public void setAuth_code(String auth_code) {
		this.auth_code = auth_code;
	}

	public String getTime_start() {
		return time_start;
	}

	public void setTime_start(String time_start) {
		this.time_start = time_start;
	}

	public String getTime_expire() {
		return time_expire;
	}

	public void setTime_expire(String time_expire) {
		this.time_expire = time_expire;
	}

	public String getOp_user_id() {
		return op_user_id;
	}

	public void setOp_user_id(String op_user_id) {
		this.op_user_id = op_user_id;
	}

	public String getOp_shop_id() {
		return op_shop_id;
	}

	public void setOp_shop_id(String op_shop_id) {
		this.op_shop_id = op_shop_id;
	}

	public String getOp_device_id() {
		return op_device_id;
	}

	public void setOp_device_id(String op_device_id) {
		this.op_device_id = op_device_id;
	}

	public String getGoods_tag() {
		return goods_tag;
	}

	public void setGoods_tag(String goods_tag) {
		this.goods_tag = goods_tag;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public String calculSign(String key) {
		String link = PayUtil.obj2Link(this);
		this.sign = StringUtils.md5String(link +"&key=" + key).toUpperCase();
		return this.sign;
	}
	
	
}
