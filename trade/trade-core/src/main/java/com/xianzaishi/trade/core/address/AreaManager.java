package com.xianzaishi.trade.core.address;

import java.util.List;

import com.xianzaishi.trade.dal.model.Area;

public interface AreaManager {

	List<Area> getRootArea();

	List<Area> getChildArea(String code);

	Area getArea(String code);

	String getAreaAddress(String code);

}
