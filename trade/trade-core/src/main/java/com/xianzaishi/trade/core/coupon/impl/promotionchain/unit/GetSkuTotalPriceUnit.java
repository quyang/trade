package com.xianzaishi.trade.core.coupon.impl.promotionchain.unit;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.itemsku.constants.SkuConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.client.vo.BuySkuInfoVO;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.PromotionChainContext;

/**
 * 计算sku所有的价格
 * 
 * @author zhancang
 */
@Component(value = "getSkuTotalPriceUnit")
public class GetSkuTotalPriceUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    String errorInfo = "";
    if (CollectionUtils.isEmpty(context.getSkuMaps().keySet())
        || CollectionUtils.isEmpty(context.getBuyInfoList())) {
      errorInfo = "[PromotionProcessorUnit]Input parameter skuIdList is empty,return null";
      log.error(errorInfo);
      return Result.getErrDataResult(-1, errorInfo);
    }

    Map<String, Integer> skuPriceMaps = Maps.newHashMap();
    Map<Long, ItemCommoditySkuDTO> skuMaps = context.getSkuMaps();
    for (BuySkuInfoVO skuInfo : context.getBuyInfoList()) {
      String buyCountStr = skuInfo.getBuyCount();
      long skuId = skuInfo.getSkuId();
      ItemCommoditySkuDTO sku = skuMaps.get(skuId);
      
      //增加打不支持优惠标商品时，不会计入总价
      if(sku.skuContainTag(SkuConstants.SKU_IGNORE_PROMOTION)){
        addPrice(0, PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0", skuPriceMaps);
        continue;
      }

      Integer skuPrice = sku.getDiscountPrice();
      Integer skuUnitPrice = sku.getDiscountUnitPrice();
      Integer skuTotalPrice = 0;
      if (sku.getIsSteelyardSku() && context.getPayChannel() == 2) {// 称重类型的，线下传来的是称重重量
        skuTotalPrice =
            new BigDecimal(buyCountStr).multiply(new BigDecimal(skuUnitPrice)).intValue();
      } else {
        skuTotalPrice = new BigDecimal(buyCountStr).multiply(new BigDecimal(skuPrice)).intValue();
      }

      addPrice(skuTotalPrice, PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0", skuPriceMaps);

      if (null != sku.getTags() && sku.getTags() > 0) {
        int tag = sku.getTags();
        for (int i = 1; i <= tag; i = i << 1) {
          if ((i & tag) == i) {
            addPrice(skuTotalPrice, PromotionChainContext.TOTAL_PRICE_PRE_KEY
                + i, skuPriceMaps);
          }
        }
      }
    }
    context.setSkuPriceMaps(skuPriceMaps);
    return Result.getSuccDataResult(true);
  }
}
