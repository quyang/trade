package com.xianzaishi.trade.core.coupon.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.croky.util.StringUtils;
import com.google.common.collect.Lists;
import com.xianzaishi.trade.core.coupon.CouponManager;
import com.xianzaishi.trade.dal.dao.extend.CouponDAO;
import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.dal.model.CouponExample;
import com.xianzaishi.trade.dal.model.CouponExample.Criteria;
import com.xianzaishi.trade.utils.PageList;
import com.xianzaishi.trade.utils.Pagination;
import com.xianzaishi.trade.utils.enums.Status;

/**
 * 
 * @author Croky.Zheng 2016年8月26日
 */
@Component(value = "couponManager")
public class CouponManagerImpl implements CouponManager {
	private static final Logger log = LoggerFactory.getLogger(CouponManagerImpl.class);

	private ConcurrentHashMap<Long, Coupon> cache = new ConcurrentHashMap<Long, Coupon>();

	@Resource
	private CouponDAO couponDAO;

	@Override
	public long insert(String title, String rules, Date start, Date end, double amount) {
		Coupon coupon = new Coupon();
		coupon.setAmount(amount);
		coupon.setCrowdId(0L);
		coupon.setGmtEnd(end);
		coupon.setGmtStart(start);
		coupon.setRules(rules);
		coupon.setTitle(title);
		coupon.setType((short) 1);
		return insert(coupon);
	}

	@Override
	public Coupon getCoupon(long id) {
		Coupon coupon = cache.get(id);
		if (null == coupon) {
			coupon = get(id);
			if (null != coupon) {
				cache.put(coupon.getId(), coupon);
			}
		}
		return coupon;
	}

	private Coupon get(long id) {
		try {
			return couponDAO.selectByPrimaryKey(id);
		} catch (DataAccessException e) {
			log.error("select Coupon failed.", e);
		}
		return null;
	}

	@Override
	public List<Coupon> getCouponsByType(short type) {
		CouponExample example = new CouponExample();
		example.createCriteria().andTypeEqualTo(type).andStatusEqualTo((short) 1).andGmtEndGreaterThan(new Date());
		try {
			return couponDAO.selectByExample(example);
		} catch (DataAccessException e) {
			log.error("select Coupons failed.", e);
		}
		return null;
	}

	@Override
	public PageList<Coupon> getConpons(short[] status, int curPage, int pageSize) {
		String statusStr = "";
		if ((null != status) && (status.length > 0)) {
			for (short state : status) {
				statusStr += (String.valueOf(state) + ",");
			}
			statusStr = StringUtils.left(statusStr, statusStr.length() - 1);
		} else {
			statusStr = "-1,0,1";
		}
		try {
			int total = couponDAO.countByStatus(statusStr);
			if (total > 0) {
				Pagination pagination = Pagination.getPagination(curPage, pageSize, total);
				return new PageList<Coupon>(pagination, couponDAO.selectByStatus(statusStr, pagination));
			}
		} catch (DataAccessException e) {
			log.error("select Coupons failed.", e);
		}
		return null;
	}

	@Override
	public boolean delete(long id) {
		return update(id, Status.INVALID);
	}

	private boolean update(long id, Status status) {
		Coupon coupon = this.getCoupon(id);
		if (null != coupon) {
			coupon.setStatus(status.getValue());
			try {
				return couponDAO.updateByPrimaryKey(coupon) > 0;
			} catch (DataAccessException e) {
				log.error("update Coupon failed.", e);
			}
		}
		return false;
	}

	private long insert(Coupon coupon) {
		Date now = new Date();
		coupon.setGmtCreate(now);
		coupon.setGmtModify(now);
		coupon.setStatus(Status.VALID.getValue());
		try {
			if (couponDAO.insert(coupon) > 0) {
				return coupon.getId();
			}
		} catch (DataAccessException e) {
			log.error("insert Coupon failed.", e);
		}
		return 0L;
	}

	@Override
	public List<Coupon> getCouponsByCouponTypes(Collection<Short> couponTypes) {
		CouponExample example = new CouponExample();

		Criteria criteria = example.createCriteria().andStatusEqualTo((short) 1).andGmtEndGreaterThan(new Date());
		if (couponTypes != null && !couponTypes.isEmpty()) {
			criteria.andTypeIn(Lists.newArrayList(couponTypes));
		} else {
			criteria.andTypeNotIn(Lists.newArrayList((short) 77));
		}
		try {
			return couponDAO.selectByExample(example);
		} catch (DataAccessException e) {
			log.error("select Coupons failed.", e);
		}
		return null;
	}
}
