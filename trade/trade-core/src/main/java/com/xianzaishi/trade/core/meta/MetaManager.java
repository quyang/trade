package com.xianzaishi.trade.core.meta;

import java.util.List;

import com.xianzaishi.trade.core.model.Column;
import com.xianzaishi.trade.core.model.Table;


/**
 * 
 * @author Croky.Zheng
 * 2016年8月12日
 */
public interface MetaManager {

	public List<String> getPrimaryKeys(String tableName);

	public List<Column> getColumns(String tableName);

	public List<Table> getTables();
	
	public Object getDataById(String tableName,long id);

}
