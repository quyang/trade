package com.xianzaishi.trade.dal.dao;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import com.xianzaishi.trade.dal.model.OrderItem;
import com.xianzaishi.trade.dal.model.OrderItemExample.Criteria;
import com.xianzaishi.trade.dal.model.OrderItemExample.Criterion;
import com.xianzaishi.trade.dal.model.OrderItemExample;
import java.util.List;
import java.util.Map;

public class OrderItemSqlProvider {

    public String countByExample(OrderItemExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("order_item");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(OrderItemExample example) {
        BEGIN();
        DELETE_FROM("order_item");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(OrderItem record) {
        BEGIN();
        INSERT_INTO("order_item");
        
        if (record.getOrdersId() != null) {
            VALUES("orders_id", "#{ordersId,jdbcType=BIGINT}");
        }
        
        if (record.getItemId() != null) {
            VALUES("item_id", "#{itemId,jdbcType=BIGINT}");
        }
        
        if (record.getItemSkuId() != null) {
            VALUES("item_sku_id", "#{itemSkuId,jdbcType=BIGINT}");
        }
        
        if (record.getItemName() != null) {
            VALUES("item_name", "#{itemName,jdbcType=VARCHAR}");
        }
        
        if (record.getItemBillPrice() != null) {
            VALUES("item_bill_price", "#{itemBillPrice,jdbcType=DOUBLE}");
        }
        
        if (record.getItemEffePrice() != null) {
            VALUES("item_effe_price", "#{itemEffePrice,jdbcType=DOUBLE}");
        }
        
        if (record.getItemCount() != null) {
            VALUES("item_count", "#{itemCount,jdbcType=DOUBLE}");
        }
        
        if (record.getItemSkuInfo() != null) {
            VALUES("item_sku_info", "#{itemSkuInfo,jdbcType=VARCHAR}");
        }
        
        if (record.getItemIconUrl() != null) {
            VALUES("item_icon_url", "#{itemIconUrl,jdbcType=VARCHAR}");
        }
        
        if (record.getItemCategoryId() != null) {
            VALUES("item_category_id", "#{itemCategoryId,jdbcType=INTEGER}");
        }
        
        if (record.getItemBillAmount() != null) {
            VALUES("item_bill_amount", "#{itemBillAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getItemEffeAmount() != null) {
            VALUES("item_effe_amount", "#{itemEffeAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getGmtCreate() != null) {
            VALUES("gmt_create", "#{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            VALUES("gmt_modify", "#{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            VALUES("status", "#{status,jdbcType=SMALLINT}");
        }
        
        return SQL();
    }

    public String selectByExample(OrderItemExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("orders_id");
        SELECT("item_id");
        SELECT("item_sku_id");
        SELECT("item_name");
        SELECT("item_bill_price");
        SELECT("item_effe_price");
        SELECT("item_count");
        SELECT("item_sku_info");
        SELECT("item_icon_url");
        SELECT("item_category_id");
        SELECT("item_bill_amount");
        SELECT("item_effe_amount");
        SELECT("gmt_create");
        SELECT("gmt_modify");
        SELECT("status");
        FROM("order_item");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        OrderItem record = (OrderItem) parameter.get("record");
        OrderItemExample example = (OrderItemExample) parameter.get("example");
        
        BEGIN();
        UPDATE("order_item");
        
        if (record.getId() != null) {
            SET("id = #{record.id,jdbcType=BIGINT}");
        }
        
        if (record.getOrdersId() != null) {
            SET("orders_id = #{record.ordersId,jdbcType=BIGINT}");
        }
        
        if (record.getItemId() != null) {
            SET("item_id = #{record.itemId,jdbcType=BIGINT}");
        }
        
        if (record.getItemSkuId() != null) {
            SET("item_sku_id = #{record.itemSkuId,jdbcType=BIGINT}");
        }
        
        if (record.getItemName() != null) {
            SET("item_name = #{record.itemName,jdbcType=VARCHAR}");
        }
        
        if (record.getItemBillPrice() != null) {
            SET("item_bill_price = #{record.itemBillPrice,jdbcType=DOUBLE}");
        }
        
        if (record.getItemEffePrice() != null) {
            SET("item_effe_price = #{record.itemEffePrice,jdbcType=DOUBLE}");
        }
        
        if (record.getItemCount() != null) {
            SET("item_count = #{record.itemCount,jdbcType=DOUBLE}");
        }
        
        if (record.getItemSkuInfo() != null) {
            SET("item_sku_info = #{record.itemSkuInfo,jdbcType=VARCHAR}");
        }
        
        if (record.getItemIconUrl() != null) {
            SET("item_icon_url = #{record.itemIconUrl,jdbcType=VARCHAR}");
        }
        
        if (record.getItemCategoryId() != null) {
            SET("item_category_id = #{record.itemCategoryId,jdbcType=INTEGER}");
        }
        
        if (record.getItemBillAmount() != null) {
            SET("item_bill_amount = #{record.itemBillAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getItemEffeAmount() != null) {
            SET("item_effe_amount = #{record.itemEffeAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{record.status,jdbcType=SMALLINT}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("order_item");
        
        SET("id = #{record.id,jdbcType=BIGINT}");
        SET("orders_id = #{record.ordersId,jdbcType=BIGINT}");
        SET("item_id = #{record.itemId,jdbcType=BIGINT}");
        SET("item_sku_id = #{record.itemSkuId,jdbcType=BIGINT}");
        SET("item_name = #{record.itemName,jdbcType=VARCHAR}");
        SET("item_bill_price = #{record.itemBillPrice,jdbcType=DOUBLE}");
        SET("item_effe_price = #{record.itemEffePrice,jdbcType=DOUBLE}");
        SET("item_count = #{record.itemCount,jdbcType=DOUBLE}");
        SET("item_sku_info = #{record.itemSkuInfo,jdbcType=VARCHAR}");
        SET("item_icon_url = #{record.itemIconUrl,jdbcType=VARCHAR}");
        SET("item_category_id = #{record.itemCategoryId,jdbcType=INTEGER}");
        SET("item_bill_amount = #{record.itemBillAmount,jdbcType=DOUBLE}");
        SET("item_effe_amount = #{record.itemEffeAmount,jdbcType=DOUBLE}");
        SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        SET("status = #{record.status,jdbcType=SMALLINT}");
        
        OrderItemExample example = (OrderItemExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(OrderItem record) {
        BEGIN();
        UPDATE("order_item");
        
        if (record.getOrdersId() != null) {
            SET("orders_id = #{ordersId,jdbcType=BIGINT}");
        }
        
        if (record.getItemId() != null) {
            SET("item_id = #{itemId,jdbcType=BIGINT}");
        }
        
        if (record.getItemSkuId() != null) {
            SET("item_sku_id = #{itemSkuId,jdbcType=BIGINT}");
        }
        
        if (record.getItemName() != null) {
            SET("item_name = #{itemName,jdbcType=VARCHAR}");
        }
        
        if (record.getItemBillPrice() != null) {
            SET("item_bill_price = #{itemBillPrice,jdbcType=DOUBLE}");
        }
        
        if (record.getItemEffePrice() != null) {
            SET("item_effe_price = #{itemEffePrice,jdbcType=DOUBLE}");
        }
        
        if (record.getItemCount() != null) {
            SET("item_count = #{itemCount,jdbcType=DOUBLE}");
        }
        
        if (record.getItemSkuInfo() != null) {
            SET("item_sku_info = #{itemSkuInfo,jdbcType=VARCHAR}");
        }
        
        if (record.getItemIconUrl() != null) {
            SET("item_icon_url = #{itemIconUrl,jdbcType=VARCHAR}");
        }
        
        if (record.getItemCategoryId() != null) {
            SET("item_category_id = #{itemCategoryId,jdbcType=INTEGER}");
        }
        
        if (record.getItemBillAmount() != null) {
            SET("item_bill_amount = #{itemBillAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getItemEffeAmount() != null) {
            SET("item_effe_amount = #{itemEffeAmount,jdbcType=DOUBLE}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{status,jdbcType=SMALLINT}");
        }
        
        WHERE("id = #{id,jdbcType=BIGINT}");
        
        return SQL();
    }

    protected void applyWhere(OrderItemExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}