package com.xianzaishi.trade.client.vo;

import java.io.Serializable;

/**
 * 配送用户
 * 
 * @author quyang
 */
public class DeliveryUserVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long uid;

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}
	
	
}
