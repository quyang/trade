package com.xianzaishi.trade.client;

/**
 * 
 * @author quyang 2017-2-8 17:38:30
 */
public interface CreditService {

	/**
	 * 用户id
	 */
	Result<Integer> getUserCredit(Long userId);
	

}
