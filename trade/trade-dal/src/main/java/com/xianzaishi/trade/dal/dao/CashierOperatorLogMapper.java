package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.CashierOperatorLog;
import com.xianzaishi.trade.dal.model.CashierOperatorLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface CashierOperatorLogMapper {
    @SelectProvider(type=CashierOperatorLogSqlProvider.class, method="countByExample")
    int countByExample(CashierOperatorLogExample example);

    @DeleteProvider(type=CashierOperatorLogSqlProvider.class, method="deleteByExample")
    int deleteByExample(CashierOperatorLogExample example);

    @Delete({
        "delete from cashier_operator_log",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into cashier_operator_log (member_id, device_id, ",
        "device_code, operate_code, ",
        "gmt_create, gmt_modify)",
        "values (#{memberId,jdbcType=INTEGER}, #{deviceId,jdbcType=INTEGER}, ",
        "#{deviceCode,jdbcType=VARCHAR}, #{operateCode,jdbcType=SMALLINT}, ",
        "#{gmtCreate,jdbcType=TIMESTAMP}, #{gmtModify,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insert(CashierOperatorLog record);

    @InsertProvider(type=CashierOperatorLogSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insertSelective(CashierOperatorLog record);

    @SelectProvider(type=CashierOperatorLogSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.INTEGER),
        @Result(column="device_id", property="deviceId", jdbcType=JdbcType.INTEGER),
        @Result(column="device_code", property="deviceCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="operate_code", property="operateCode", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    List<CashierOperatorLog> selectByExample(CashierOperatorLogExample example);

    @Select({
        "select",
        "id, member_id, device_id, device_code, operate_code, gmt_create, gmt_modify",
        "from cashier_operator_log",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.INTEGER),
        @Result(column="device_id", property="deviceId", jdbcType=JdbcType.INTEGER),
        @Result(column="device_code", property="deviceCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="operate_code", property="operateCode", jdbcType=JdbcType.SMALLINT),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    CashierOperatorLog selectByPrimaryKey(Integer id);

    @UpdateProvider(type=CashierOperatorLogSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") CashierOperatorLog record, @Param("example") CashierOperatorLogExample example);

    @UpdateProvider(type=CashierOperatorLogSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") CashierOperatorLog record, @Param("example") CashierOperatorLogExample example);

    @UpdateProvider(type=CashierOperatorLogSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(CashierOperatorLog record);

    @Update({
        "update cashier_operator_log",
        "set member_id = #{memberId,jdbcType=INTEGER},",
          "device_id = #{deviceId,jdbcType=INTEGER},",
          "device_code = #{deviceCode,jdbcType=VARCHAR},",
          "operate_code = #{operateCode,jdbcType=SMALLINT},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(CashierOperatorLog record);
}