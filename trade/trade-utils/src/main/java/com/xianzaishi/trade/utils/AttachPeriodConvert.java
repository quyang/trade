package com.xianzaishi.trade.utils;

import java.util.Calendar;
import java.util.Date;

import com.croky.util.DateUtils;
import com.xianzaishi.trade.utils.enums.DatePeriod;

/**
 * 
 * @author Croky.Zheng
 * 2016年9月17日
 */
public class AttachPeriodConvert {

	/**
	 * 将字符串类型的归属日期转化为归属周期
	 * 归属周期分为实时,日,周,月,季,年
	 * 实时和日则直接转为20160407
	 * 周则转为20162**0，周的周期为当天离2016-01-01的天数/7
	 * 月则转为20160400
	 * 季则转为20163100,20163200,20163300,20163400
	 * 年则转为20160000等
	 * @param gmt_attach 类似于2016-04-07的时间格式转为归属周期
	 * @return
	 */
	public static synchronized int convert(String gmt_attach,DatePeriod datePeriod) {
		Date date = DateUtils.yyyy_MM_dd_Date(gmt_attach);
		return convert(date,datePeriod);
	}
	

	public static synchronized int convert(Date date,DatePeriod datePeriod) {

		//时间格式不对
		if (null == date) {
			return 0;
		}
		if (datePeriod.equals(DatePeriod.DAY) || datePeriod.equals(DatePeriod.REALTIME)) {
			
		}
		switch (datePeriod) {
		case REALTIME:
		case DAY:
			return Integer.parseInt(DateUtils.yyyyMMdd(date));
		case WEEK:
		{
			Calendar calendar = Calendar.getInstance();
			//int year = date.getYear();
			calendar.setTime(date);
			int year = calendar.get(Calendar.YEAR);
			int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
			return Integer.parseInt(year + "2" + weekOfYear) * 10;
		}
		case MONTH:
		{
			Calendar calendar = Calendar.getInstance();
			//int year = date.getYear();
			calendar.setTime(date);
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			if (month < 10) {
				return Integer.parseInt(year + "0" + month) * 100;
			} else {
				return Integer.parseInt(year + "" + month) * 100;
			}
		}
		case QUARTER:
		{//加上大括号,避免相同的
			Calendar calendar = Calendar.getInstance();
			//int year = date.getYear();
			calendar.setTime(date);
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH);
			int quarter = (month + 1 + 2) / 3;
			return Integer.parseInt(year + "3" + quarter) * 100 + ((month+1)%3) * 10;
		}
		case YEAR:
		{
			Calendar calendar = Calendar.getInstance();
			//int year = date.getYear();
			calendar.setTime(date);
			int year = calendar.get(Calendar.YEAR);
			return year * 10000;
		}
		default:
			return 0;
		}
	}
}
