package com.xianzaishi.trade.dal.model;

import java.io.Serializable;
import java.util.Date;

public class OrderItem implements Serializable {
    private Long id;

    private Long ordersId;

    private Long itemId;

    private Long itemSkuId;

    private String itemName;

    private Double itemBillPrice;

    private Double itemEffePrice;

    private Double itemCount;

    private String itemSkuInfo;

    private String itemIconUrl;

    private Integer itemCategoryId;

    private Double itemBillAmount;

    private Double itemEffeAmount;

    private Date gmtCreate;

    private Date gmtModify;

    private Short status;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(Long ordersId) {
        this.ordersId = ordersId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getItemSkuId() {
        return itemSkuId;
    }

    public void setItemSkuId(Long itemSkuId) {
        this.itemSkuId = itemSkuId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName == null ? null : itemName.trim();
    }

    public Double getItemBillPrice() {
        return itemBillPrice;
    }

    public void setItemBillPrice(Double itemBillPrice) {
        this.itemBillPrice = itemBillPrice;
    }

    public Double getItemEffePrice() {
        return itemEffePrice;
    }

    public void setItemEffePrice(Double itemEffePrice) {
        this.itemEffePrice = itemEffePrice;
    }

    public Double getItemCount() {
        return itemCount;
    }

    public void setItemCount(Double itemCount) {
        this.itemCount = itemCount;
    }

    public String getItemSkuInfo() {
        return itemSkuInfo;
    }

    public void setItemSkuInfo(String itemSkuInfo) {
        this.itemSkuInfo = itemSkuInfo == null ? null : itemSkuInfo.trim();
    }

    public String getItemIconUrl() {
        return itemIconUrl;
    }

    public void setItemIconUrl(String itemIconUrl) {
        this.itemIconUrl = itemIconUrl == null ? null : itemIconUrl.trim();
    }

    public Integer getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(Integer itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public Double getItemBillAmount() {
        return itemBillAmount;
    }

    public void setItemBillAmount(Double itemBillAmount) {
        this.itemBillAmount = itemBillAmount;
    }

    public Double getItemEffeAmount() {
        return itemEffeAmount;
    }

    public void setItemEffeAmount(Double itemEffeAmount) {
        this.itemEffeAmount = itemEffeAmount;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        OrderItem other = (OrderItem) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getOrdersId() == null ? other.getOrdersId() == null : this.getOrdersId().equals(other.getOrdersId()))
            && (this.getItemId() == null ? other.getItemId() == null : this.getItemId().equals(other.getItemId()))
            && (this.getItemSkuId() == null ? other.getItemSkuId() == null : this.getItemSkuId().equals(other.getItemSkuId()))
            && (this.getItemName() == null ? other.getItemName() == null : this.getItemName().equals(other.getItemName()))
            && (this.getItemBillPrice() == null ? other.getItemBillPrice() == null : this.getItemBillPrice().equals(other.getItemBillPrice()))
            && (this.getItemEffePrice() == null ? other.getItemEffePrice() == null : this.getItemEffePrice().equals(other.getItemEffePrice()))
            && (this.getItemCount() == null ? other.getItemCount() == null : this.getItemCount().equals(other.getItemCount()))
            && (this.getItemSkuInfo() == null ? other.getItemSkuInfo() == null : this.getItemSkuInfo().equals(other.getItemSkuInfo()))
            && (this.getItemIconUrl() == null ? other.getItemIconUrl() == null : this.getItemIconUrl().equals(other.getItemIconUrl()))
            && (this.getItemCategoryId() == null ? other.getItemCategoryId() == null : this.getItemCategoryId().equals(other.getItemCategoryId()))
            && (this.getItemBillAmount() == null ? other.getItemBillAmount() == null : this.getItemBillAmount().equals(other.getItemBillAmount()))
            && (this.getItemEffeAmount() == null ? other.getItemEffeAmount() == null : this.getItemEffeAmount().equals(other.getItemEffeAmount()))
            && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
            && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify().equals(other.getGmtModify()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getOrdersId() == null) ? 0 : getOrdersId().hashCode());
        result = prime * result + ((getItemId() == null) ? 0 : getItemId().hashCode());
        result = prime * result + ((getItemSkuId() == null) ? 0 : getItemSkuId().hashCode());
        result = prime * result + ((getItemName() == null) ? 0 : getItemName().hashCode());
        result = prime * result + ((getItemBillPrice() == null) ? 0 : getItemBillPrice().hashCode());
        result = prime * result + ((getItemEffePrice() == null) ? 0 : getItemEffePrice().hashCode());
        result = prime * result + ((getItemCount() == null) ? 0 : getItemCount().hashCode());
        result = prime * result + ((getItemSkuInfo() == null) ? 0 : getItemSkuInfo().hashCode());
        result = prime * result + ((getItemIconUrl() == null) ? 0 : getItemIconUrl().hashCode());
        result = prime * result + ((getItemCategoryId() == null) ? 0 : getItemCategoryId().hashCode());
        result = prime * result + ((getItemBillAmount() == null) ? 0 : getItemBillAmount().hashCode());
        result = prime * result + ((getItemEffeAmount() == null) ? 0 : getItemEffeAmount().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        return result;
    }
}