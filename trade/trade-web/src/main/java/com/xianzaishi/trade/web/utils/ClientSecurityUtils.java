package com.xianzaishi.trade.web.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.xianzaishi.trade.utils.Base64;
import com.xianzaishi.trade.utils.DESCoder;
import com.xianzaishi.trade.utils.RSACoder;

public class ClientSecurityUtils {

	/**
	 * 服务端公钥
	 */
	private final static String RSA_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDNqSm/hohelD9Nj/wvwaRscW+J8QDB1i6Gj/wFkTErRHaU9huUgN+8CMxfx7yGn2bVIjx+ncNphpcVX0q+oLuvhu5ux3Dnffuk0OEMYeI8IxW3s0OgS/VirWvHX8g9FFi9h1+5JxEgSV7L2yoKMbijP1W3FfVq0YOVh8PB8z7LdQIDAQAB";

	/**
	 * 客户端私钥
	 */
	private final static String CLIENT_RSA_PRIVATE_KEY = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALfDjkvPMyG0HT/KsDEgPMTtU1j7zTIdYqtxDI9ExV4e58Tr87HzmCFNCyjhlhRU8bi2E24YdYS/71bjSVtLovAAWY7cyIfO65u2PDj1s1K7wj8c/5dxmmgwdIQ/HIqGcCg2W00pfastiphqy97cDfAVvxS1YlQuDRuIVz+T8pqDAgMBAAECgYEAt1FsSJAgsASgER0/+yHvvXwLY39+SfoqjmCuaNXwFl66UyYW63TYVJApC9VPBGVg5iUs6O2Zfp6CQqlmap7foN74whgku9CwGaHWK/XHvKfV3MFlAUfyDuY1NLP4FMfQxqJWkry9hg5X7TTzN5jh808d/2yre92DXCNCLEF7y7ECQQDnKyhlK3iti/jY+w6OFuhTA5Hh8MuYc6PI9Tz9M6/kqF7fNwQDLHbD3PO7n8sfPmfrOOdpHvkQY/tpvbpTdVJlAkEAy4DURnvotj2ZiY+1FT/i3YQ6kQgZCdd6n7HbkERuZxYBKnuD6UVEP5WuWEICujZhc4/yyXu7YBLjvrgIkeF2xwJAcl2Yq4y5NGrmXzq9tbBBmihO+Q/vOeokIpypYr86iw59eUM3mfZaf4YFk1J35Go0cVwCqLApGmHMlUDHONj3PQJAN6j75m9bngTJKtPQfohjv5y7/BEP8B4snuMhn5rn63AcT4dK9BGK8i05a1BauJ3JF7eGe09JV6Sy/+YMCgJ13wJAHPdYkbSxnnXTwAB9FRbtQBHJPzfK6ChGUPruheIM0TCvDbuZXhPqpqi4UtJ1ch+NlagQ6NjT91Bv2KKmM2Wyig==";

	/**
	 * DES密钥
	 */
	private final static String DES_SEDE_KEY = "s4mzyJSPI/vjm5hXuguGzSXfBA7+9yzV";

	public static String sign(Object params) throws Exception {
		return RSACoder.sign(toJson(params).getBytes(), CLIENT_RSA_PRIVATE_KEY);
	}

	public static String sign(String params) throws Exception {
		return RSACoder.sign(params.getBytes(), CLIENT_RSA_PRIVATE_KEY);
	}

	public static boolean verify(Object params, String sign) throws Exception {
		if (params != null && StringUtils.isNotBlank(sign)) {
			return RSACoder.verify(toJson(params).getBytes(), RSA_PUBLIC_KEY, sign);
		}
		return false;
	}

	public static String encode(String json) throws Exception {
		if (StringUtils.isNotBlank(json)) {
			byte[] encode = DESCoder.encrypt(json.getBytes(), DES_SEDE_KEY);
			return Base64.encode(encode);
		}
		return "";
	}

	public static String decode(String data) throws Exception {
		if (StringUtils.isNotBlank(data)) {
			byte[] encode = Base64.decode(data);
			return new String(DESCoder.decrypt(encode, DES_SEDE_KEY));
		}
		return "";
	}

	public static String toJson(Object params) throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = params.getClass().getDeclaredFields();
		Map<String, Object> wrapper = new HashMap<>();
		for (Field field : fields) {
			if ("sign".equals(field.getName())) {
				continue;
			}
			field.setAccessible(true);
			wrapper.put(field.getName(), field.get(params));
		}
		List<String> keys = new ArrayList<String>(wrapper.keySet());
		Collections.sort(keys);
		JSONObject json = new JSONObject();
		for (String key : keys) {
			json.put(key, wrapper.get(key));
		}

		return json.toJSONString();
	}

}
