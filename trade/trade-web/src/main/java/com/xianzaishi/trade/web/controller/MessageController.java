package com.xianzaishi.trade.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.trade.client.MessageService;
import com.xianzaishi.trade.web.model.ResponseModel;
import com.xianzaishi.trade.web.utils.ConstantUtils;

/**
 * 
 * @author Croky.Zheng
 * 2016年8月16日
 */
@Controller
public class MessageController {

	@Autowired
	private MessageService messageService;
	
	@RequestMapping(value = "/msg/send.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel send(
			@RequestParam(value="mobile",defaultValue="",required = false) String mobile,
			@RequestParam(value="content",defaultValue="",required = false) String content
			) {
		if (messageService.send("010101", mobile, content, true)) {
			return ConstantUtils.normalResponse();
		} else {
			return ConstantUtils.errorReponse("短信通知失败!");
		}
	}
}
