package com.xianzaishi.trade.pay;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import com.croky.util.DateUtils;
import com.croky.util.StringUtils;
import com.xianzaishi.trade.pay.util.PayUtil;
import com.xianzaishi.trade.pay.weixin.CallbackModel;
import com.xianzaishi.trade.pay.weixin.MobilePayModel;
import com.xianzaishi.trade.pay.weixin.UnifiedorderModel;
import com.xianzaishi.trade.pay.weixin.UnifiedorderReturnValue;
import com.xianzaishi.trade.pay.weixin.WeiXinPayHttpsRequest;

public class WeiXinPayTest {

	//HttpClientUtils httpClient = HttpClientUtils.getInstance();

	@Test
	public void testWeixinPay() {
		UnifiedorderModel model = new UnifiedorderModel();
		model.setAttach(String.valueOf(1000001));
		//model.setNonce_str(StringUtils.rightPad(String.valueOf(order.getSeq()), 16));
		model.setOut_trade_no(this.getOutTradeNo());
		Date now = new Date();
		model.setTime_start(DateUtils.yyyyMMddhhmmss(now));
		model.setTime_expire(DateUtils.yyyyMMddhhmmss(DateUtils.dateAddDays(now, 1)));
		model.setTotal_fee(1);
		String link = PayUtil.obj2Link(model);
		String sign = StringUtils.md5String(link+"&key=d7Da7b62646b2a7dd0d62a2b3d0d123T").toUpperCase();
		model.setSign(sign);
		String xml = PayUtil.objectToXML(model, "utf-8");
		String result;
		try {
			//result = postDataToWeiXin(xml);
			//System.out.println("result:" + result);
			result = WeiXinPayHttpsRequest.unifiedOrder(xml);
			if (StringUtils.isNotEmpty(result)) {
				UnifiedorderReturnValue returnValue = (UnifiedorderReturnValue)PayUtil.xmlToObject(result, "utf-8", UnifiedorderReturnValue.class);
				System.out.println(returnValue.getPrepay_id());
				MobilePayModel mobileModel = new MobilePayModel();
				mobileModel.setPrepayid(returnValue.getPrepay_id());
				link = PayUtil.obj2Link(mobileModel);
				System.out.println(link);
				mobileModel.setSign(StringUtils.md5String(link+"&key=d7Da7b62646b2a7dd0d62a2b3d0d123T").toUpperCase());
				System.out.println(PayUtil.objectToXML(mobileModel, "utf-8"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Test
	public void testReadKey() {
        InputStream is = WeiXinPayHttpsRequest.class.getResourceAsStream("/apiclient_cert.p12");
        //FileInputStream is = null;
        /*
		try {
			is = new FileInputStream("/Users/croky/workspaces/trade/trade-pay/src/main/resources/apiclient_cert.p12");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}//加载本地的证书进行https加密传输
		*/
        int b = -1;
        try {
        	int pos = 0;
			while((b = is.read()) != -1) {
				System.out.print((byte)(b&0xff) + ",");
				if (++pos % 20 == 0) {
					System.out.println();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRandomString() {
		PayUtil.genRandomString();
	}
	
	@Test
	public void testCallBackWeixin() {
		String xml = "<xml><appid><![CDATA[wxdf3989b2edb80672]]></appid><attach><![CDATA[2]]></attach><bank_type><![CDATA[CFT]]></bank_type><cash_fee><![CDATA[1]]></cash_fee><device_info><![CDATA[online_iphone_android]]></device_info><fee_type><![CDATA[CNY]]></fee_type><is_subscribe><![CDATA[N]]></is_subscribe><mch_id><![CDATA[1385693802]]></mch_id><nonce_str><![CDATA[6D156B98911CCB04]]></nonce_str><openid><![CDATA[oTkv5wI9Rv3YYNB7RVU4_i7o3yP0]]></openid><out_trade_no><![CDATA[200000000000000000010134]]></out_trade_no><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><sign><![CDATA[CA07DD8BA3E88D744D40B03FEE2F5F73]]></sign><time_end><![CDATA[20160919210734]]></time_end><total_fee>1</total_fee><trade_type><![CDATA[APP]]></trade_type><transaction_id><![CDATA[4005612001201609194394794748]]></transaction_id></xml>";
		CallbackModel model = PayUtil.xmlToObject(xml, CallbackModel.class);
		System.out.println(PayUtil.obj2Link(model));
		System.out.println(PayUtil.signByWeiXin(model));
	}
	private String getOutTradeNo() {
		String orderIdStr = StringUtils.leftPad(String.valueOf(1000015), 16, '0');
		String seqStr = StringUtils.rightPad(String.valueOf(1000001), 8, '0');
		return seqStr + orderIdStr;
	}
	
	private String postDataToWeiXin(String content) throws Exception {
		PrintWriter writer = null;
		BufferedReader reader = null;
			URI uri = URI.create("https://api.mch.weixin.qq.com/pay/unifiedorder");
			URL url = uri.toURL();
			HttpURLConnection http;
			if (url.getProtocol().equals("https")) {
				HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
				HostnameVerifier hostNameVerify = new HostnameVerifier()
				{
					/**
					 * Always return true
					 */
					public boolean verify(String urlHostName, SSLSession session)
					{
						return true;
					}
				};
				HttpsURLConnection.setDefaultHostnameVerifier(hostNameVerify);
				https.setSSLSocketFactory(createSSLSocketFactory());
				http = (HttpURLConnection) https;
			} else {
				http = (HttpURLConnection) url.openConnection();
			}
			http.setDoInput(true);
			http.setDoOutput(true);
			http.setUseCaches(false);
			http.setConnectTimeout(3000);
			http.setReadTimeout(3000);
			http.setRequestMethod("POST");
			http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");//text/plain
			http.setInstanceFollowRedirects(true);
			//http.setChunkedStreamingMode(5);
			http.connect();
			///*
			writer = new PrintWriter(http.getOutputStream());
			if (http.getDoOutput()) {
				writer.write(content);
				writer.flush();
				writer.close();
			}
			//*/
			if (http.getResponseCode() == 200 && http.getDoInput()) {
				reader = new BufferedReader(new InputStreamReader(http.getInputStream(), getHttpEncoding(http)));
				int len = http.getContentLength();
				if (len < 0 || len > 10240) {
					len = 1024;
				}
				char[] cbuf = new char[len];
				StringBuffer buf = new StringBuffer();
				while ((len = reader.read(cbuf)) > 0) {
					buf.append(new String(cbuf,0,len));
				}
				reader.close();
				return buf.toString();
			}
			http.disconnect();
		return null;
	}


	private static String getHttpEncoding(HttpURLConnection http) {
		String encoding = http.getContentEncoding();
		if (StringUtils.isEmpty(encoding)) {
			String contentType = http.getHeaderField("Content-Type");
			int index = contentType.indexOf("charset=");
			if (-1 != index) {
				encoding = contentType.substring(index + 8,
						contentType.length());
			}
		}
		if (StringUtils.isEmpty(encoding)) {
			encoding = System.getProperty("file.encoding");
		}
		return encoding;
	}
	
	public static SSLSocketFactory createSSLSocketFactory() throws Exception {

		class MyX509TrustManager implements X509TrustManager {
			
			public MyX509TrustManager() throws Exception {
				// do nothing
			}

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate,
					String paramString) throws CertificateException {
				
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate,
					String paramString) throws CertificateException {
				
			}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[] {};
			}
		}
		TrustManager[] tm = { new MyX509TrustManager() };
		
		System.setProperty("https.protocols", "TLSv1");
		SSLContext sslContext = SSLContext.getInstance("TLSv1","SunJSSE");
		sslContext.init(null, tm, new java.security.SecureRandom());
		SSLSocketFactory ssf = sslContext.getSocketFactory();
		
		return ssf;
	}
	
	
}
