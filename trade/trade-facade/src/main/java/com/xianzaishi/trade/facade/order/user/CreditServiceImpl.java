package com.xianzaishi.trade.facade.order.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.trade.client.CreditService;
import com.xianzaishi.trade.client.Result;
import com.xianzaishi.trade.core.credit.UserCreditManager;

@Service(value = "creditService")
public class CreditServiceImpl implements CreditService {

  @Autowired
  private UserCreditManager userCreditManager;

  private Logger logger = LoggerFactory.getLogger(CreditServiceImpl.class);

  @Override
  public Result<Integer> getUserCredit(Long userId) {
    if (null == userId || userId < 0) {
      return new Result<>(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询用户积分失败");
    }
    return new Result<>(ServerResultCode.SUCCESS_CODE, userCreditManager.getCreditByUserId(userId));
  }
}
