package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.MessageReport;
import com.xianzaishi.trade.dal.model.MessageReportExample;
import java.util.List;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface MessageReportMapper {
    @SelectProvider(type=MessageReportSqlProvider.class, method="countByExample")
    int countByExample(MessageReportExample example);

    @DeleteProvider(type=MessageReportSqlProvider.class, method="deleteByExample")
    int deleteByExample(MessageReportExample example);

    @Insert({
        "insert into message_report (receiver, pwd, ",
        "msgid, reportTime, ",
        "mobile, status, gmt_create, ",
        "gmt_modify)",
        "values (#{receiver,jdbcType=VARCHAR}, #{pwd,jdbcType=VARCHAR}, ",
        "#{msgid,jdbcType=VARCHAR}, #{reporttime,jdbcType=VARCHAR}, ",
        "#{mobile,jdbcType=CHAR}, #{status,jdbcType=VARCHAR}, #{gmtCreate,jdbcType=TIMESTAMP}, ",
        "#{gmtModify,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(MessageReport record);

    @InsertProvider(type=MessageReportSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insertSelective(MessageReport record);

    @SelectProvider(type=MessageReportSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT),
        @Result(column="receiver", property="receiver", jdbcType=JdbcType.VARCHAR),
        @Result(column="pwd", property="pwd", jdbcType=JdbcType.VARCHAR),
        @Result(column="msgid", property="msgid", jdbcType=JdbcType.VARCHAR),
        @Result(column="reportTime", property="reporttime", jdbcType=JdbcType.VARCHAR),
        @Result(column="mobile", property="mobile", jdbcType=JdbcType.CHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    List<MessageReport> selectByExample(MessageReportExample example);

    @UpdateProvider(type=MessageReportSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") MessageReport record, @Param("example") MessageReportExample example);

    @UpdateProvider(type=MessageReportSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") MessageReport record, @Param("example") MessageReportExample example);
}