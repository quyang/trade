package com.xianzaishi.trade.client.query;

/**
 * 默认分页条件
 * 
 * @author zhancang
 */
public class OrderBaseQuery {

  public static final int DEFAULT_PAGE_NUM = 0;// 默认页码
  public static final int DEFAULT_PAGE_SIZE = 20;// 默认页大小

  protected Integer pageNum = DEFAULT_PAGE_NUM;

  protected Integer pageSize = DEFAULT_PAGE_SIZE;

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    this.pageNum = pageNum;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  /**
   * 数据库分页用
   * 
   * @return
   */
  public Integer getStart() {
    return pageNum * pageSize;
  }

  /**
   * 数据库页大小
   * 
   * @return
   */
  public Integer getNum() {
    return pageSize;
  }
}
