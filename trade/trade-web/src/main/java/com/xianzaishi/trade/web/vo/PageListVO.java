package com.xianzaishi.trade.web.vo;

import java.io.Serializable;
import java.util.List;
import com.xianzaishi.trade.utils.Pagination;

public class PageListVO<E> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4409024886007080222L;

    private Pagination pagination = null;
    
    private List<E> objects = null;

    private DiscountInfoVO discountInfo;

    private String freightDesc;

    
    public PageListVO(){}
    public PageListVO(Pagination pagination,List<E> objects) {
        this.objects = objects;
        this.pagination = pagination;
    }

    public String getFreightDesc() {
        return freightDesc;
    }

    public void setFreightDesc(String freightDesc) {
        this.freightDesc = freightDesc;
    }

    public DiscountInfoVO getDiscountInfo() {
        return discountInfo;
    }

    public void setDiscountInfo(DiscountInfoVO discountInfo) {
        this.discountInfo = discountInfo;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
    
    public List<E> getObjects() {
        return objects;
    }
    
    public void setObjects(List<E> objects) {
        this.objects = objects;
    }
}