package com.xianzaishi.trade.utils;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class FelTest {
	
	@Test
	public void testCalcul() {
		Map<String,Double> contextMap = new HashMap<String,Double>();
		contextMap.put("金额", 50.0);
		contextMap.put("优惠额", 250.0);
		contextMap.put("界限额", 250.0);
		double value = (Double)ExpressionCalculator.calcul("金额>300.0?优惠额:0.0", contextMap);
		System.out.println(value);
	}

}
