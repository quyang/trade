package com.xianzaishi.trade.core.coupon.impl.promotionchain.unit;

import java.util.Arrays;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.PromotionChainContext;
import com.xianzaishi.trade.dal.model.UserCoupon;

/**
 * 查询用户符合条件的第一个优惠券
 * @author zhancang
 */
@Component(value = "getSkuPriceMatchFirstCouponUnit")
public class GetSkuPriceMatchFirstCouponUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getUserCouponList())) {
      return Result.getSuccDataResult(true);
    }

    for (UserCoupon coupon : context.getUserCouponList()) {
      int discountpirce = calculate(coupon, context.getSkuPriceMaps());
      if(discountpirce > 0){
        context.setPrice(discountpirce);
        context.setCanUseCouponList(Arrays.asList(coupon));
        return Result.getSuccDataResult(true);
      }
    }
    return Result.getSuccDataResult(true);
  }
}
