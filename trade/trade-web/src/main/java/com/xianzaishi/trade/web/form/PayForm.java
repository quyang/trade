package com.xianzaishi.trade.web.form;

/**
 * 收银机支付
 *
 * @author nianyue.hyj
 * @since 2016.10.19
 */
public class PayForm {

	/**
	 * 订单ID
	 */
	private Long orderId;

	/**
	 * 支付金额
	 */
	private Long payAmount;

	/**
	 * 支付类型：1银通; 2现金
	 */
	private Short payType;

	/**
	 * 客户端私钥签名
	 */
	private String sign;

	/**
	 * 支付账号
	 */
	private String account;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(Long payAmount) {
		this.payAmount = payAmount;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public Short getPayType() {
		return payType;
	}

	public void setPayType(Short payType) {
		this.payType = payType;
	}
}
