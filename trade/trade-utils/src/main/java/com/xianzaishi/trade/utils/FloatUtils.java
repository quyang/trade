package com.xianzaishi.trade.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * 
 * @author Croky.Zheng
 * 2016年7月16日
 */
public class FloatUtils {
	private FloatUtils(){}
	private static final String DEFAULT_ZERO = "00000000000000000000000000000000";
	
	public static double getFixedScaleDouble(int scale,double value) {
		BigDecimal	bd	= new	BigDecimal(value);  
		return bd.setScale(scale,BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	

	public static String getFixedScaleString(int scale,double value) {
		int len = scale % 32;
		len = (len < 0) ? 0 : len;
		String format = "#." + DEFAULT_ZERO.substring(0, len);
		DecimalFormat df =new DecimalFormat(format);
		return df.format(value);
	}
}
