package com.xianzaishi.trade.core.coupon.impl.promotionchain.unit;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.constants.SkuConstants;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.PromotionChainContext;
import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.trade.utils.ExpressionCalculator;

public abstract class ProcessorUnit {

  protected static final Logger log = LoggerFactory.getLogger(ProcessorUnit.class);

  @Autowired
  protected SkuService skuService;
  
  public abstract Result<Boolean> process(PromotionChainContext context);
  
  /**
   * 添加价格
   * 
   * @param price
   * @param key
   * @param skuPriceMaps
   */
  protected void addPrice(int price, String key, Map<String, Integer> skuPriceMaps) {
    if (null == skuPriceMaps) {
      skuPriceMaps = Maps.newHashMap();
    }
    Integer addPrice = skuPriceMaps.get(key);
    if (null == addPrice || addPrice == 0) {
      addPrice = price;
    } else {
      addPrice = addPrice + price;
    }
    skuPriceMaps.put(key, addPrice);
  }

  /**
   * 规则校验入口
   * @param rule
   * @param parameterM
   * @return
   */
  public synchronized String calculate(String rule, Map<String, Double> parameterM) {
    Object result = ExpressionCalculator.calcul(rule, parameterM);
    if (null != result) {
      return result.toString();
    }
    return "0";
  }
  
  /**
   * 根据价格参数和优惠价计算抵扣值，分为单位
   * @param coupon
   * @param priceM
   * @return
   */
  protected int calculate(UserCoupon coupon, Map<String, Integer> priceM){
    String rule = coupon.getCouponRules();
    if(StringUtils.isEmpty(rule)){
      return 0;
    }
    int amount = priceM.get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0");
    String key = "金额";
    Map<String, Double> parameterM = Maps.newHashMap();
    if (coupon.getCouponType()== 12) {
      int couponTag = getCouponTag(coupon);
      if(couponTag > 0){
        key = "金额_tag_" + couponTag;
        Integer tagAmount = priceM.get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + couponTag);
        if(null != tagAmount){
          amount = tagAmount;
        }else{
          amount = 0;
        }
      }
    }
    
    double cmpAmount = amount;
    if(!coupon.isFenPriceTypeUserCoupon()){
      cmpAmount = new BigDecimal(amount).divide(new BigDecimal(100),2,BigDecimal.ROUND_DOWN).doubleValue();
    }
    
    parameterM.put(key, cmpAmount);
    String discountpirce = calculate(rule, parameterM);
    int discount = 0;
    if(!coupon.isFenPriceTypeUserCoupon()){
      discount = new BigDecimal(discountpirce).multiply(new BigDecimal(100)).intValue();
    }else{
      discount = new BigDecimal(discountpirce).intValue();
    }
    return discount;
  }
  
  /**
   * 校验优惠券是否可用
   * @param coupon
   * @return
   */
  protected boolean checkCouponAvailable(UserCoupon coupon, PromotionChainContext context){
    if ((coupon.getChannelType() == 2 && context.getPayChannel() == 1)
        || (coupon.getChannelType() == 1 && context.getPayChannel() == 2)) {
      return false;
    }
    Date now = new Date();
    if (coupon.getCouponStartTime().after(now) || coupon.getCouponEndTime().before(now)) {
      return false;
    }
    if (coupon.getStatus() == 0) {
      return false;
    }
    if (!coupon.getUserId().equals(context.getUserId())) {
      return false;
    }
    return true;
  }
  
  public static class CompratorByMoney implements Comparator<UserCoupon> {
    public int compare(UserCoupon f1, UserCoupon f2) {
      Double amont1 = f1.getAmount() == null ? 0 : f1.getAmount();
      Double amont2 = f2.getAmount() == null ? 0 : f2.getAmount();
      if(!f1.isFenPriceTypeUserCoupon()){
        amont1 = new BigDecimal(amont1.toString()).multiply(new BigDecimal(100)).doubleValue();
      }
      if(!f2.isFenPriceTypeUserCoupon()){
        amont2 = new BigDecimal(amont2.toString()).multiply(new BigDecimal(100)).doubleValue();
      }
      if (amont1 > amont2) {
        return -1;
      } else {
        return 1;
      }
    }
  }
  
  private int getCouponTag(UserCoupon coupon){
    String rule = coupon.getCouponRules();
    String tmpKey = rule.substring(0, rule.indexOf(">"));
    tmpKey = tmpKey.replace("金额_tag_", "");
    if(StringUtils.isNumeric(tmpKey)){
      return Integer.valueOf(tmpKey);
    }
    return 0;
  }
}
