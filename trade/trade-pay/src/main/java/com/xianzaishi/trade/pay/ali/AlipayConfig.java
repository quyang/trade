package com.xianzaishi.trade.pay.ali;

public class AlipayConfig {

	public final static String V1_APP_RSA_PUBLIC = "MIICXQIBAAKBgQCwcTbif+c+wzbd9laG5nCFHR+FD9yY1bew9ZW6r4j/s9Mh4JByfPOtiLvi08avZi3jbC6zxGE4aQDjrLpPQOH9hq53h8mpUGwcuHiZsSOgiAXdYy1tpeG/N6nALK/fvzURKAcoQUnKeEgLNs1iOTMMDn9CqsuXyNjfx1BfvlAQgwIDAQABAoGBAKUpZSoCdlFe5SZDKF3gqeVBDLc+0M1UCT4htQXquMA68WEd17kD1ApWGyJKAQtBB6WCJ/lo02S9jfKRRllXr/JWzrLq1Debe0P39D7cWxmDWt4x6MhOPC5aINfrb72zgSnNfUqH9dZgrn6g4MXPItzlNbIbTajoLbkm8lF2P6LpAkEA36U070jyhnPZ7gEadYzeO7JSM9HjClZTHzHcxB7fHH/GmcbeOX6zHRPX87nvseSt5VSwoxPFcYMuAxA3RupbdQJBAMn30og/mvXoWxS2K/vBQ/nTKvxzkp+y6bSLoFbP0VztmJRSiq5xrOFkETaNGXgXwX+VBq10BsePcWQw1UrUVRcCQE8echCpHOuF9rYle8fUUxaJal1cxlZ03akuiax0Q3ggmBD08s8iTJlf2MknoW2sufxkrqyypOoYf2GkDfovlLUCQCoIKElwq0g4BhSGYRrwurvYRZ7qUn5n1plbYZAPievECrf7gZ8SSz9Q+wAWV1GV6BAsLIqWlf6cDKYkP49mJxsCQQDNYmbNi8sASVsqBbcHJEF+32Sr8fGtLjzrt0HnldKuc6n/cALEzzgXbfwwZ3UIej/j6plxtZL9V6afIOLs8Klw/6FTFY99uhpiq0qadD/uSzQsefWo0aTvP/65zi3eof7TcZ32oWpwIDAQAB";
	public final static String V1_APP_RSA_PRIVATE = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBANXenWCCjvXwdDKEQNsfJgouLggj/Cx87LfSJ0MyL7UQH+atx8ShXgRVj4OZL6I/QYynzS5iihrCD5jo0dN84FG+SlN2/g7x8iaYJ+kqq4Gf6BWs2I7IZgRkzRaq1nhB1+nFtWDU/NbgJYFkgoKEijVl0hox47ksWk0LVeWdLvtjAgMBAAECgYEAzRTXXMBVTkjxY6+mcXiBBaoawfufyRlR9UM0Gx941+tKa+2gblE+0nEpWUv/fVmjBbmy6xPa0qXcRwiajG5murqMpbUkHdkq6zkNDmxSV1JrzwuYHAG3CCgDZT2kdNMEDF1njscQNx73YHW4pTafGryabwVdNhTytriw3J98NOECQQD3LsPjiLzCcvC5oM6VHYXHdYTC2udi/Dk1Ly8WftxfWpSE1vyvs2r4qWl+EZ1Lif9Hi1MXYHKjWVSyMyoLrjYdAkEA3X+jtWAoqZIgZLoCagNaaiN3rTM/XaUQll7fgnGzzpMKIXQecbYUU1v2mzz8bcKx3/Ips4giLHs+dw3BrWU/fwJBAJZ3Wzsow27CtRLqdpaC8CqouPY8dtnkm5ZqcImLE+7fnsT2cb8qwpU320Wox01yZXlRsHTsexxAhrQrPQ77L2kCQDIQcJNFactyIOpDdNo7actFuv4l8DOdZJNoXEKiqo6Ng6OuGFeBXTS+O445CaFReVzx4mUW5wqAzMyiCl3D3ccCQQDrfk2sUPU1KvjdpKhluFDws8tCvfGrhDDVM+FtWYFpnEW0MibOjEh+N/c+Bveyjs4bK3CVySPxbY6H5NGOQrmY";
	public final static String V1_ALIPAY_RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";
	
	/*
	 	public final static String V1_APP_RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDFioqJwfsDXKNz5kTCfozOvn0l2cdA9Uq2JnlAmTZcR0m4chZeCGWdVOlZ7+As0evlXLN5qTAGY0EJmFaLg+X7UDj5PHU01itGHmEvPgGa1P9DUpXZ7YSQypog6+cqP2nbzjiOr0elk/4ItLFLcn06UKzqaTa/TEcKJNfNnnZlsQIDAQAB";
	public final static String V1_APP_RSA_PRIVATE = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMWKionB+wNco3PmRMJ+jM6+fSXZx0D1SrYmeUCZNlxHSbhyFl4IZZ1U6Vnv4CzR6+Vcs3mpMAZjQQmYVouD5ftQOPk8dTTWK0YeYS8+AZrU/0NSldnthJDKmiDr5yo/advOOI6vR6WT/gi0sUtyfTpQrOppNr9MRwok182edmWxAgMBAAECgYEAnYJ6NJ2XqHaVXjSdXvfJDpCU+TlMx17O066ZwAhqb+nnvko5y4CmhWzPJAdAmHxJu/jOopNk17MMiLoPR+9D0Fja4p5KdRygFP675nwvZxvbn3OEx9GUAf1QGVcUrGMimhJqK+G4u/nua8vM/7+22GVsLs6ioeLpmtBaBzDGCAECQQDkv8Zoo06jfJSLxeVlRKVL5aY0GtVdHsIjxNhBMXwczNnff4USzg8G+wo3IWuccGkguxf0b41QDSSfrMubEWHZAkEA3RMCQ1+tkgyq1mdfhOrgKP0a7bSS9JeQLHdtKIZR1vp0ROx3IgR+oX9IMvny5j48v10bsQw/mMef8gTMWJxjmQJAZlk0Ih9nkRitsjhSYlQrjWDsU4XqD2cywCRUCh9lA9HYbfq1hOtzp9DtCivG5VEaiI2Ns02yMa4fw93hn7lQSQJAM2G3pPEX5loLWsmr6bitYNly6MyPW0neuHcJZ+HQI6cSXvKWvdFNnTt4DGELk3fjppZ9WSj/nwLGqflzhSTUAQJAS8X14aXtwYmSJ6IoyoWqeaIGN+mkMYxfRC3iPag3m0nWcK6A+WELmsMnR4vsWqMwyNUjJyIt0+L/eZbTMkJUfg==";
	public final static String V1_ALIPAY_RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";
	 
	 */
	
	public final static String APP_ID = "2016091401905282";
	
	/**
	 * 接口名称 固定值
	 */
	public final static String V1_PAY_SERVICE = "mobile.securitypay.pay";

	/**
	 * 签约的支付宝账号对应的支付宝唯一用户号。以2088开头的16位纯数字组成。
	 */
	public final static String V1_PARTNER = "2088421284384567";

	/**
	 * 商户网站使用的编码格式，固定为UTF-8
	 */
	public final static String V1_INPUT_CHARSET = "utf-8";

	/**
	 * 签名类型，目前仅支持RSA
	 */
	public final static String V1_SIGN_TYPE = "RSA";

	/**
	 * 支付宝服务器主动通知商户网站制定的页面http路径
	 */
	public final static String V1_NOTIFY_URL = "http://trade.xianzaishi.com/order/alipayCallBack.htm";
	
	/**
     * 支付宝服务器主动通知商户网站制定的页面http路径
     */
    public final static String V1_NOTIFY_URL_DAILY = "http://trade.xianzaishi.net/order/alipayCallBack.htm";
    
	/**
	 * 支付类型。默认值为：1（商品购买）。
	 */
	public final static String V1_PARMENT_TYPE = "1";

	/**
	 * 卖家支付宝账号(邮箱或者手机号码)或者对应的支付宝唯一用户号(以2088开头的纯16位数字)
	 */
	public final static String V1_SELLER_ID = "pay@xianzaishi.com";

	/**
	 * 设置未付款交易的超时时间，一旦超时，该笔交易就会自动被关闭。当用户输入支付密码、点击确认付款后（即创建支付宝交易后）开始计时。取值范围：1m～
	 * 15d，或者使用绝对时间（示例格式：2014-06-13
	 * 16:00:00）。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。该参数数值不接受小数点，如1.
	 * 5h，可转换为90m
	 */
	public final static String V1_IT_B_PAY = "1d";
	
	/**
	 * APP_ID
	 */
	public final static String V2_APP_ID = "2016091401905282";

}
