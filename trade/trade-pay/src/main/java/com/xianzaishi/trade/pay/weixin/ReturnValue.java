package com.xianzaishi.trade.pay.weixin;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 返回值的基类
 * @author Croky.Zheng
 * 2016年9月16日
 */
@XmlRootElement(name="xml")
@XmlAccessorType
public class ReturnValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3147392322507076141L;

	private String return_code;
	
	private String return_msg;

	public String getReturn_code() {
		return return_code;
	}

	public void setReturn_code(String return_code) {
		this.return_code = return_code;
	}

	public String getReturn_msg() {
		return return_msg;
	}

	public void setReturn_msg(String return_msg) {
		this.return_msg = return_msg;
	}
}
