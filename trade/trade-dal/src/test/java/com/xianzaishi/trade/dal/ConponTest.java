package com.xianzaishi.trade.dal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.croky.util.DateUtils;
import com.xianzaishi.trade.dal.dao.extend.CouponDAO;
import com.xianzaishi.trade.dal.dao.extend.ShoppingCartDAO;
import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.utils.Pagination;


public class ConponTest extends BaseDalTest {

    @Resource
    @Autowired
    private CouponDAO couponDAO;
    
    @Test
    public void insertCoupon(){
    	Coupon coupon = new Coupon();
    	coupon.setAmount(25.0);
    	coupon.setCrowdId(0L);
    	coupon.setGmtCreate(new Date());
    	coupon.setGmtEnd(DateUtils.yyyyMMddhhmmss("20170926154433"));
    	coupon.setGmtModify(new Date());
    	coupon.setGmtStart(new Date());
    	coupon.setRules("金额>50.0?25.0:0.0");
    	coupon.setStatus((short)-1);
    	coupon.setTitle("优惠券25元");
    	coupon.setType((short)1);
    	couponDAO.insert(coupon);
    }
    
    @Test
    public void testCoupon(){ 
    	List<Short> status = new ArrayList<Short>();
    	status.add((short)-1);
    	status.add((short)0);
    	status.add((short)1);
    	
    	short[] statusArray = new short[]{-1,0,1};
    	
    	int total = couponDAO.countByStatus("-1,0,1");
    	if (total > 0) {
    		Pagination pagination = Pagination.getPagination(0, 20, total);
    		List<Coupon> coupons = couponDAO.selectByStatus("-1,0,1", pagination);
    		for (Coupon coupon : coupons) {
    			System.out.println(coupon.getId() + " + " + coupon.getTitle() + " + " + coupon.getRules());
    		}
    	}
    }
}
