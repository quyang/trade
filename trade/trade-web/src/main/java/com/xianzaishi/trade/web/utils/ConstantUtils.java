package com.xianzaishi.trade.web.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.xianzaishi.trade.web.model.DataTableModel;
import com.xianzaishi.trade.web.model.DataTableParam;
import com.xianzaishi.trade.web.model.ResponseModel;

/**
 * 
 * @author Croky.Zheng
 * 2016年3月28日
 */
public class ConstantUtils {

	/**
	 * 获取登录的用户
	 * @param request
	 * @return
	 */
	/*
	public static Member getLoginMember(HttpServletRequest request) {
		return (Member) request.getSession().getAttribute(Constants.LOGIN_MEMBER_KEY);
	}
	*/
	
	/**
	 * 添加错误信息到model
	 * @param modelAndView
	 * @param message
	 */
	@Deprecated
	public static void addErrorMessage(ModelAndView modelAndView,String message) {
		modelAndView.addObject(Constants.ERROR_MESSAGE_KEY, message);
	}
	@Deprecated
	public static void addErrorMessage(Model model,String message) {
		model.addAttribute(Constants.ERROR_MESSAGE_KEY, message);
	}
	
	@Deprecated
	public static ResponseModel errorReponse(int code,String message) {
		ResponseModel model = new ResponseModel(code,message);
		return model;
	}
	
	public static ResponseModel errorReponse(String message) {
		return errorReponse(ResponseModel.JSON_FAILED_RESPONSE,message);
	}
	
	public static ResponseModel normalResponse(Object value) {
		ResponseModel model = new ResponseModel(value);
		return model;
	}

	public static ResponseModel normalResponse(int code, String message, Object value) {
		ResponseModel model = new ResponseModel(code,message,value);
		return model;
	}
	
	public static DataTableModel dataTableResponse(DataTableParam dataTableParam,Object value) {
		DataTableModel model = new DataTableModel(dataTableParam,value);
		return model;
	}
	
	public static ResponseModel normalResponse() {
		ResponseModel model = new ResponseModel();
		return model;
	}
	
	public static DataTableParam getRequestDataTableParam(HttpServletRequest request) {
		String aoData = request.getParameter("aoData");
		DataTableParam dataTableParam = null;
		if(StringUtils.isNotEmpty(aoData)) {
			try {
				dataTableParam = DataTableParam.json2Object(aoData);
			} catch (Exception e) {
			}
		}
		if (null == dataTableParam) {
			dataTableParam = new DataTableParam();
		}
		return dataTableParam;
	}
	
	
}
