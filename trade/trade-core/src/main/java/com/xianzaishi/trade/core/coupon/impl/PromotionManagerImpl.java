package com.xianzaishi.trade.core.coupon.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.trade.client.vo.UserCouponDiscontQueryVO;
import com.xianzaishi.trade.client.vo.UserCouponDiscontResultVO;
import com.xianzaishi.trade.client.vo.UserCouponVO;
import com.xianzaishi.trade.core.coupon.PromotionManager;
import com.xianzaishi.trade.core.coupon.UserCouponManager;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.PromotionChainContext;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.PromotionProcessor;
import com.xianzaishi.trade.dal.dao.extend.UserCouponDAO;
import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.trade.utils.enums.Status;

@Component(value = "promotionManager")
public class PromotionManagerImpl implements PromotionManager {

  protected static final Logger log = LoggerFactory.getLogger(PromotionManagerImpl.class);

  @Autowired
  private PromotionProcessor promotionProcessor;

  @Autowired
  private UserCouponManager userCouponManager;

  @Resource
  private UserCouponDAO userCouponDAO;

  @Override
  public UserCouponDiscontResultVO selectMathCoupon(UserCouponDiscontQueryVO query) {

//    PromotionChainContext context = new PromotionChainContext();
//    context.setBuyInfoList(query.getBuySkuDetail());
//    context.setPayChannel(query.getPayType());
//    context.setUserId(query.getUserId());
//    context.setUserCouponList(userCouponManager.getUserCoupons(query.getUserId()));
//
//    PromotionChainContext contextResult = null;
//    if (null != query.getQueryType() && query.getQueryType() == 1) {
//      contextResult = promotionProcessor.selectMathAllCoupon(context);
//    } else {
//      contextResult = promotionProcessor.selectMathFirstCoupon(context);
//    }
//    if (null != contextResult) {
//      UserCouponDiscontResultVO discountResult = new UserCouponDiscontResultVO();
//      discountResult.setOriginalPrice(contextResult.getSkuPriceMaps().get(
//          PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0"));
//      discountResult.setDiscountPrice(contextResult.getPrice());
//      discountResult.setActualPayPrice(discountResult.getOriginalPrice()
//          - discountResult.getDiscountPrice());
//
//      if (CollectionUtils.isNotEmpty(contextResult.getCanUseCouponList())) {
//        List<UserCouponVO> couponVOList = new ArrayList<UserCouponVO>();
//        BeanCopierUtils.copyListBean(contextResult.getCanUseCouponList(), couponVOList,
//            UserCouponVO.class);
//        discountResult.setUsedCouponList(couponVOList);
//      }
//      return discountResult;
//    }
    return null;
  }

  @Override
  public UserCouponDiscontResultVO calculateDiscountPriceWithCoupon(UserCouponDiscontQueryVO query) {
//    PromotionChainContext context = new PromotionChainContext();
//    context.setBuyInfoList(query.getBuySkuDetail());
//    context.setPayChannel(query.getPayType());
//    context.setUserId(query.getUserId());
//    if (null != query.getCouponId() && query.getCouponId() > 0) {
//      UserCoupon inputCoupon = get(query.getCouponId());
//      if (null != inputCoupon) {
//        context.setUserSelectedCouponList(Arrays.asList(inputCoupon));
//      }
//    }
//
//    PromotionChainContext contextResult =
//        promotionProcessor.calculateDiscountPriceWithCoupon(context);
//    if (null != contextResult) {
//      UserCouponDiscontResultVO discountResult = new UserCouponDiscontResultVO();
//      discountResult.setOriginalPrice(contextResult.getSkuPriceMaps().get(
//          PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0"));
//      discountResult.setDiscountPrice(contextResult.getPrice());
//      discountResult.setActualPayPrice(discountResult.getOriginalPrice()
//          - discountResult.getDiscountPrice());
//
//      if (CollectionUtils.isNotEmpty(contextResult.getCanUseCouponList())) {
//        List<UserCouponVO> couponVOList = new ArrayList<UserCouponVO>();
//        BeanCopierUtils.copyListBean(contextResult.getCanUseCouponList(), couponVOList,
//            UserCouponVO.class);
//        discountResult.setUsedCouponList(couponVOList);
//        if (query.isCalculateAndUse()) {
//          try {
//            for (UserCoupon coupon : contextResult.getCanUseCouponList()) {
//              coupon.setStatus(Status.INIT.getValue());
//              coupon.setOrderId(0L);
//              coupon.setGmtModify(new Date());
//              if (!(userCouponDAO.updateByPrimaryKey(coupon) > 0)) {
//                log.error("[PromotionManagerImpl]update UserCoupon failed!", coupon.toString());
//              }
//            }
//          } catch (DataAccessException e) {
//            log.error("[PromotionManagerImpl]select UserCoupon failed!", e);
//          }
//        }
//      }
//      return discountResult;
//    }
    return null;
  }

//  private UserCoupon get(long userCouponId) {
//    try {
//      return userCouponDAO.selectByPrimaryKey(userCouponId);
//    } catch (DataAccessException e) {
//      log.error("select UserCoupon failed!", e);
//    }
//    return null;
//  }
}
