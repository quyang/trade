package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.OrderPayLog;
import com.xianzaishi.trade.dal.model.OrderPayLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface OrderPayLogMapper {
    @SelectProvider(type=OrderPayLogSqlProvider.class, method="countByExample")
    int countByExample(OrderPayLogExample example);

    @DeleteProvider(type=OrderPayLogSqlProvider.class, method="deleteByExample")
    int deleteByExample(OrderPayLogExample example);

    @Delete({
        "delete from order_pay_log",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into order_pay_log (order_id, pay_way, ",
        "pay_amount, gmt_create, ",
        "gmt_modify, account, ",
        "callbackContent, status)",
        "values (#{orderId,jdbcType=BIGINT}, #{payWay,jdbcType=SMALLINT}, ",
        "#{payAmount,jdbcType=DOUBLE}, #{gmtCreate,jdbcType=TIMESTAMP}, ",
        "#{gmtModify,jdbcType=TIMESTAMP}, #{account,jdbcType=VARCHAR}, ",
        "#{callbackcontent,jdbcType=VARCHAR}, #{status,jdbcType=SMALLINT})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(OrderPayLog record);

    @InsertProvider(type=OrderPayLogSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insertSelective(OrderPayLog record);

    @SelectProvider(type=OrderPayLogSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="order_id", property="orderId", jdbcType=JdbcType.BIGINT),
        @Result(column="pay_way", property="payWay", jdbcType=JdbcType.SMALLINT),
        @Result(column="pay_amount", property="payAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="account", property="account", jdbcType=JdbcType.VARCHAR),
        @Result(column="callbackContent", property="callbackcontent", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT)
    })
    List<OrderPayLog> selectByExample(OrderPayLogExample example);

    @Select({
        "select",
        "id, order_id, pay_way, pay_amount, gmt_create, gmt_modify, account, callbackContent, ",
        "status",
        "from order_pay_log",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="order_id", property="orderId", jdbcType=JdbcType.BIGINT),
        @Result(column="pay_way", property="payWay", jdbcType=JdbcType.SMALLINT),
        @Result(column="pay_amount", property="payAmount", jdbcType=JdbcType.DOUBLE),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="account", property="account", jdbcType=JdbcType.VARCHAR),
        @Result(column="callbackContent", property="callbackcontent", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT)
    })
    OrderPayLog selectByPrimaryKey(Long id);

    @UpdateProvider(type=OrderPayLogSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") OrderPayLog record, @Param("example") OrderPayLogExample example);

    @UpdateProvider(type=OrderPayLogSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") OrderPayLog record, @Param("example") OrderPayLogExample example);

    @UpdateProvider(type=OrderPayLogSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(OrderPayLog record);

    @Update({
        "update order_pay_log",
        "set order_id = #{orderId,jdbcType=BIGINT},",
          "pay_way = #{payWay,jdbcType=SMALLINT},",
          "pay_amount = #{payAmount,jdbcType=DOUBLE},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP},",
          "account = #{account,jdbcType=VARCHAR},",
          "callbackContent = #{callbackcontent,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=SMALLINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(OrderPayLog record);
}