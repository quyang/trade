package com.xianzaishi.trade.core.coupon.impl.promotionchain.unit;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.PromotionChainContext;
import com.xianzaishi.trade.dal.model.UserCoupon;

/**
 * 检查过滤用户所有优惠券，并且按照优惠价格倒序排序
 * @author zhancang
 */
@Component(value = "checkUserCouponUnit")
public class CheckUserCouponUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getUserCouponList())) {
      return Result.getSuccDataResult(true);
    }

    List<UserCoupon> userCouponList = context.getUserCouponList();
    List<UserCoupon> cleanCouponList = Lists.newArrayList();
    for (UserCoupon coupon : userCouponList) {
      if(checkCouponAvailable(coupon, context)){
        cleanCouponList.add(coupon);
      }
    }
    Collections.sort(cleanCouponList, new CompratorByMoney());
    context.setUserCouponList(cleanCouponList);
    return Result.getSuccDataResult(true);
  }
}
