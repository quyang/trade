package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.trade.dal.model.UserCouponExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface UserCouponMapper {
    @SelectProvider(type=UserCouponSqlProvider.class, method="countByExample")
    int countByExample(UserCouponExample example);

    @DeleteProvider(type=UserCouponSqlProvider.class, method="deleteByExample")
    int deleteByExample(UserCouponExample example);

    @Delete({
        "delete from user_coupon",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into user_coupon (user_id, coupon_id, ",
        "coupon_title, coupon_start_time, ",
        "coupon_end_time, order_id, ",
        "coupon_rules, gmt_create, ",
        "gmt_modify, status, ",
        "amount, coupon_type, ",
        "channel_type, amount_limit)",
        "values (#{userId,jdbcType=BIGINT}, #{couponId,jdbcType=BIGINT}, ",
        "#{couponTitle,jdbcType=VARCHAR}, #{couponStartTime,jdbcType=TIMESTAMP}, ",
        "#{couponEndTime,jdbcType=TIMESTAMP}, #{orderId,jdbcType=BIGINT}, ",
        "#{couponRules,jdbcType=VARCHAR}, #{gmtCreate,jdbcType=TIMESTAMP}, ",
        "#{gmtModify,jdbcType=TIMESTAMP}, #{status,jdbcType=SMALLINT}, ",
        "#{amount,jdbcType=DOUBLE}, #{couponType,jdbcType=SMALLINT}, ",
        "#{channelType,jdbcType=SMALLINT}, #{amountLimit,jdbcType=DOUBLE})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(UserCoupon record);

    @InsertProvider(type=UserCouponSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insertSelective(UserCoupon record);

    @SelectProvider(type=UserCouponSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="coupon_id", property="couponId", jdbcType=JdbcType.BIGINT),
        @Result(column="coupon_title", property="couponTitle", jdbcType=JdbcType.VARCHAR),
        @Result(column="coupon_start_time", property="couponStartTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="coupon_end_time", property="couponEndTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="order_id", property="orderId", jdbcType=JdbcType.BIGINT),
        @Result(column="coupon_rules", property="couponRules", jdbcType=JdbcType.VARCHAR),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount", property="amount", jdbcType=JdbcType.DOUBLE),
        @Result(column="coupon_type", property="couponType", jdbcType=JdbcType.SMALLINT),
        @Result(column="channel_type", property="channelType", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount_limit", property="amountLimit", jdbcType=JdbcType.DOUBLE)
    })
    List<UserCoupon> selectByExample(UserCouponExample example);

    @Select({
        "select",
        "id, user_id, coupon_id, coupon_title, coupon_start_time, coupon_end_time, order_id, ",
        "coupon_rules, gmt_create, gmt_modify, status, amount, coupon_type, channel_type, ",
        "amount_limit",
        "from user_coupon",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT),
        @Result(column="coupon_id", property="couponId", jdbcType=JdbcType.BIGINT),
        @Result(column="coupon_title", property="couponTitle", jdbcType=JdbcType.VARCHAR),
        @Result(column="coupon_start_time", property="couponStartTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="coupon_end_time", property="couponEndTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="order_id", property="orderId", jdbcType=JdbcType.BIGINT),
        @Result(column="coupon_rules", property="couponRules", jdbcType=JdbcType.VARCHAR),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="status", property="status", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount", property="amount", jdbcType=JdbcType.DOUBLE),
        @Result(column="coupon_type", property="couponType", jdbcType=JdbcType.SMALLINT),
        @Result(column="channel_type", property="channelType", jdbcType=JdbcType.SMALLINT),
        @Result(column="amount_limit", property="amountLimit", jdbcType=JdbcType.DOUBLE)
    })
    UserCoupon selectByPrimaryKey(Long id);

    @UpdateProvider(type=UserCouponSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") UserCoupon record, @Param("example") UserCouponExample example);

    @UpdateProvider(type=UserCouponSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") UserCoupon record, @Param("example") UserCouponExample example);

    @UpdateProvider(type=UserCouponSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(UserCoupon record);

    @Update({
        "update user_coupon",
        "set user_id = #{userId,jdbcType=BIGINT},",
          "coupon_id = #{couponId,jdbcType=BIGINT},",
          "coupon_title = #{couponTitle,jdbcType=VARCHAR},",
          "coupon_start_time = #{couponStartTime,jdbcType=TIMESTAMP},",
          "coupon_end_time = #{couponEndTime,jdbcType=TIMESTAMP},",
          "order_id = #{orderId,jdbcType=BIGINT},",
          "coupon_rules = #{couponRules,jdbcType=VARCHAR},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP},",
          "status = #{status,jdbcType=SMALLINT},",
          "amount = #{amount,jdbcType=DOUBLE},",
          "coupon_type = #{couponType,jdbcType=SMALLINT},",
          "channel_type = #{channelType,jdbcType=SMALLINT},",
          "amount_limit = #{amountLimit,jdbcType=DOUBLE}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(UserCoupon record);
}