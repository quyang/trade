package com.xianzaishi.trade.facade.order;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.client.Result;
import com.xianzaishi.trade.client.vo.OrderItemVO;
import com.xianzaishi.trade.client.vo.OrderVO;
import com.xianzaishi.trade.client.vo.SkuInfoVO;
import com.xianzaishi.trade.core.order.OrdersManager;
import com.xianzaishi.trade.core.order.impl.OrdersManagerImpl;
import com.xianzaishi.trade.dal.model.OrderItem;
import com.xianzaishi.trade.dal.model.extend.OrderInfo;
import com.xianzaishi.trade.utils.JsonConvert;
import com.xianzaishi.trade.utils.PageList;
import com.xianzaishi.trade.utils.enums.OrderStatus;

/**
 * 
 * @author Croky.Zheng 2016年8月27日
 */
@Service(value = "orderService")
public class OrderServiceImpl implements OrderService {
	protected static final Logger log = LoggerFactory.getLogger(OrdersManagerImpl.class);

	@Autowired
	private OrdersManager ordersManager;

	@Override
	public Result<OrderVO> getOrder(Long orderId) {
		try {
			OrderInfo orderInfo = ordersManager.getOrder(orderId);
			OrderVO order = null;
			if (null != orderInfo) {
				order = this.info2vo(orderInfo);
			}
			return new Result<OrderVO>(order);
		} catch (RuntimeException e) {
			log.error("Fail to get order result.", e);
			return new Result<OrderVO>(-1, e.getMessage());
		}
	}

	@Override
	public Result<Boolean> updateOrderStatus(long orderId, short status) {
		OrderStatus orderStatus = OrderStatus.from(status);
		if (null != orderStatus) {
			try {
				boolean result = ordersManager.updateStatus(orderId, orderStatus);
				return new Result<Boolean>(result);
			} catch (RuntimeException e) {
				return new Result<Boolean>(-2, e.getMessage());
			}
		} else {
			return new Result<Boolean>(-1, "状态值不合法");
		}
	}

	@Override
	public Result<Boolean> refund(long orderId, double amount, String cause, int memberId) {
		try {
			boolean result = ordersManager.payback(orderId, amount, null, null, null, cause, memberId);
			return new Result<Boolean>(result);
		} catch (RuntimeException e) {
			return new Result<Boolean>(-2, e.getMessage());
		}
	}
	
  @Override
  public Result<List<OrderVO>> getOrdersByUserId(long uid, short[] status, int pageNo,
      int pageSize, boolean isOrderByDesc) {
    PageList<OrderInfo> orderResult =
        ordersManager.getOrdersByUserId(uid, getStatus(status), pageNo, pageSize, isOrderByDesc);
    List<OrderVO> resultList = info2voList(orderResult);
    if (CollectionUtils.isEmpty(resultList)) {
      return new Result<List<OrderVO>>(null);
    }
    return new Result<List<OrderVO>>(resultList);
  }

  @Override
  public Result<List<OrderVO>> getOrdersByTime(Date begin, Date end, short[] status, int pageNo,
      int pageSize) {
    PageList<OrderInfo> orderResult = ordersManager.getOrdersByTime(begin, end, getStatus(status), pageNo, pageSize);
    List<OrderVO> resultList = info2voList(orderResult);
    if (CollectionUtils.isEmpty(resultList)) {
      return new Result<List<OrderVO>>(null);
    }
    return new Result<List<OrderVO>>(resultList);
  }
  
  @Override
  public Result<Integer> getOrderCountByUserId(long uid, short[] status) {
    return new Result<Integer>(ordersManager.getOrderCountByUserId(uid, getStatus(status)));
  }

  @Override
  public Result<Integer> getOrderCountByTime(Date begin, Date end, short[] status) {
    return new Result<Integer>(ordersManager.getOrderCountByTime(begin, end, getStatus(status)));
  }

  /**
   * 转化状态逻辑
   * @param status
   * @return
   */
  private OrderStatus[] getStatus(short[] status) {
    OrderStatus[] orderStatus = null;
    if ((null != status) && (status.length >= 1)) {
      orderStatus = new OrderStatus[status.length];
      for (int i = 0; i < status.length; i++) {
        // 为0则取所有
        if (status[i] == 0) {
          orderStatus = null;
          break;
        }
        OrderStatus state = OrderStatus.from(status[i]);
        if (null == state) {
          continue;
        }
        orderStatus[i] = state;
      }
    }
    return orderStatus;
  }
  
  private List<OrderVO> info2voList(PageList<OrderInfo> infoList){
    if(null != infoList && (CollectionUtils.isNotEmpty(infoList))){
      List<OrderVO> resultList = Lists.newArrayList();
      for(OrderInfo order:infoList){
        resultList.add(info2vo(order));
      }
      return resultList;
    }
    return Collections.emptyList();
  }
  private OrderVO info2vo(OrderInfo orderInfo) {
		OrderVO vo = new OrderVO();
		vo.setCashierId(orderInfo.getCashierId());
		vo.setDeviceId(orderInfo.getDeviceCode());
		vo.setEffeAmount(orderInfo.getEffeAmount() != null ? String.format("%.2f", orderInfo.getEffeAmount()) : null);
		vo.setPayAmount(orderInfo.getPayAmount() != null ? String.format("%.2f", orderInfo.getPayAmount()) : null);
		vo.setId(orderInfo.getId());
		vo.setUserAddressId(58L);
		vo.setAttribute(orderInfo.getAttribute());
		vo.setSeq(orderInfo.getSeq());
		vo.setStatus(orderInfo.getStatus());
		vo.setStatusString(OrderStatus.from(orderInfo.getStatus()).getDescription());
		vo.setGmtPay(orderInfo.getGmtPay());
		vo.setGmtEnd(orderInfo.getGmtReceive());
		vo.setGmtCreate(orderInfo.getGmtCreate());
		vo.setUserAddressId(orderInfo.getUserAddressId());
		vo.setUserAddress(orderInfo.getUserAddress());
		vo.setShopId(orderInfo.getShopId());
		vo.setGmtDistribution(orderInfo.getGmtDistribution());
		vo.setCredit(orderInfo.getCredit());
		vo.setUserId(orderInfo.getUserId());
		vo.setOrderType(orderInfo.getOrderType());
		vo.setChannelType(orderInfo.getChannelType());
		vo.setCouponId(orderInfo.getCouponId());
		List<OrderItemVO> items = null;
		if (CollectionUtils.isNotEmpty(orderInfo.getItems())) {
			items = new ArrayList<OrderItemVO>(orderInfo.getItems().size());
			for (OrderItem orderItem : orderInfo.getItems()) {
				items.add(orderItem2VO(orderItem));
			}
		}
		vo.setItems(items);
		return vo;
	}

	private OrderItemVO orderItem2VO(OrderItem item) {
		OrderItemVO vo = new OrderItemVO();
		vo.setAmount(item.getItemBillAmount());
		vo.setCategoryId(item.getItemCategoryId());
		vo.setCount(item.getItemCount());
		vo.setIconUrl(item.getItemIconUrl());
		vo.setId(item.getItemId());
		vo.setName(item.getItemName());
		vo.setPrice(item.getItemBillPrice());
		vo.setSkuId(item.getItemSkuId());
		vo.setSkuInfo(JsonConvert.json2Object(item.getItemSkuInfo(), SkuInfoVO.class));
		vo.setEffeAmount(item.getItemEffeAmount());
		vo.setEffePrice(item.getItemEffePrice());
		return vo;
	}

}
