package com.xianzaishi.trade.core.coupon.impl.promotionchain.unit;

import java.util.Arrays;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.PromotionChainContext;
import com.xianzaishi.trade.dal.model.UserCoupon;

/**
 * 根据用户选择的优惠券给用户打折价格
 * @author zhancang
 */
@Component(value = "getSkuCouponDiscountPriceUnit")
public class GetSkuCouponDiscountPriceUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getUserSelectedCouponList())) {
      return Result.getSuccDataResult(true);
    }

    for (UserCoupon coupon : context.getUserSelectedCouponList()) {
      int discountpirce = calculate(coupon, context.getSkuPriceMaps());
      if(discountpirce > 0){
        context.setPrice(discountpirce);
        context.setCanUseCouponList(Arrays.asList(coupon));
        return Result.getSuccDataResult(true);
      }
    }
    return Result.getSuccDataResult(true);
  }
}
