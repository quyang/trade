package com.xianzaishi.trade.dal.model;

import java.io.Serializable;
import java.util.Date;

public class OrderDiscount implements Serializable {

  private static final long serialVersionUID = 4763335428495884411L;

  private Long id;

  private Long orderId;

  private Long skuId;

  private Long userId;

  private Long discountId;

  private Short status;

  /**
   * 优惠后付款金额
   */
  private Integer price;
  
  /**
   * 优惠前符合条件金额
   */
  private Integer originPrice;
  
  /**
   * 不包含优惠时的金额
   */
  private Integer ignorePrice;

  private String disname;
  
  /**
   * 记录详细优惠信息，有时候的一个优惠记录对应多个优惠id
   */
  private String discountdetail;
  
  private Short type;

  private Date gmtCreate;

  private Date gmtModified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getDiscountId() {
    return discountId;
  }

  public void setDiscountId(Long discountId) {
    this.discountId = discountId;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }
  
  public Integer getOriginPrice() {
    return originPrice;
  }

  public void setOriginPrice(Integer originPrice) {
    this.originPrice = originPrice;
  }

  public Integer getIgnorePrice() {
    return ignorePrice;
  }

  public void setIgnorePrice(Integer ignorePrice) {
    this.ignorePrice = ignorePrice;
  }

  public String getDisname() {
    return disname;
  }

  public void setDisname(String disname) {
    this.disname = disname;
  }
  
  public String getDiscountdetail() {
    return discountdetail;
  }

  public void setDiscountdetail(String discountdetail) {
    this.discountdetail = discountdetail;
  }



  public static class OrderDiscountStatus {
    public static final Short INIT = 0;//初始状态
    public static final Short USED = 1;//优惠已经使用
    public static final Short REFUND = 2;//优惠已经退款
  }
}
