package com.xianzaishi.trade.client;

import com.xianzaishi.trade.client.vo.DeliveryAddressVO;

/**
 * 
 * 地理位置、地址服务
 *
 * @author nianyue.hyj
 * @since 2016.10.19
 */
public interface LocationService {

	/**
	 * 获取送货地址
	 * 
	 * @param addressId
	 * @return
	 */
	public Result<DeliveryAddressVO> getDeliveryAddressById(long addressId);

}
