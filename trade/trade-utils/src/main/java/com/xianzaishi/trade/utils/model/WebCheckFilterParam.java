package com.xianzaishi.trade.utils.model;

import java.util.Set;

public class WebCheckFilterParam {

	private String loginUrl;
	
	private Set<String> allowUrlSet;

	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	public Set<String> getAllowUrlSet() {
		return allowUrlSet;
	}

	public void setAllowUrlSet(Set<String> allowUrlSet) {
		this.allowUrlSet = allowUrlSet;
	}
	
	
}
