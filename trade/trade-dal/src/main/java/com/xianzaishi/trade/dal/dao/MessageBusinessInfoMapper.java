package com.xianzaishi.trade.dal.dao;

import com.xianzaishi.trade.dal.model.MessageBusinessInfo;
import com.xianzaishi.trade.dal.model.MessageBusinessInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface MessageBusinessInfoMapper {
    @SelectProvider(type=MessageBusinessInfoSqlProvider.class, method="countByExample")
    int countByExample(MessageBusinessInfoExample example);

    @DeleteProvider(type=MessageBusinessInfoSqlProvider.class, method="deleteByExample")
    int deleteByExample(MessageBusinessInfoExample example);

    @Delete({
        "delete from message_business_info",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into message_business_info (app_id, app_name, ",
        "business_code, app_owner_id, ",
        "app_owner_name, template, ",
        "gmt_create, gmt_modify)",
        "values (#{appId,jdbcType=INTEGER}, #{appName,jdbcType=VARCHAR}, ",
        "#{businessCode,jdbcType=VARCHAR}, #{appOwnerId,jdbcType=INTEGER}, ",
        "#{appOwnerName,jdbcType=VARCHAR}, #{template,jdbcType=VARCHAR}, ",
        "#{gmtCreate,jdbcType=TIMESTAMP}, #{gmtModify,jdbcType=TIMESTAMP})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insert(MessageBusinessInfo record);

    @InsertProvider(type=MessageBusinessInfoSqlProvider.class, method="insertSelective")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Integer.class)
    int insertSelective(MessageBusinessInfo record);

    @SelectProvider(type=MessageBusinessInfoSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="app_id", property="appId", jdbcType=JdbcType.INTEGER),
        @Result(column="app_name", property="appName", jdbcType=JdbcType.VARCHAR),
        @Result(column="business_code", property="businessCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="app_owner_id", property="appOwnerId", jdbcType=JdbcType.INTEGER),
        @Result(column="app_owner_name", property="appOwnerName", jdbcType=JdbcType.VARCHAR),
        @Result(column="template", property="template", jdbcType=JdbcType.VARCHAR),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    List<MessageBusinessInfo> selectByExample(MessageBusinessInfoExample example);

    @Select({
        "select",
        "id, app_id, app_name, business_code, app_owner_id, app_owner_name, template, ",
        "gmt_create, gmt_modify",
        "from message_business_info",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="app_id", property="appId", jdbcType=JdbcType.INTEGER),
        @Result(column="app_name", property="appName", jdbcType=JdbcType.VARCHAR),
        @Result(column="business_code", property="businessCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="app_owner_id", property="appOwnerId", jdbcType=JdbcType.INTEGER),
        @Result(column="app_owner_name", property="appOwnerName", jdbcType=JdbcType.VARCHAR),
        @Result(column="template", property="template", jdbcType=JdbcType.VARCHAR),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modify", property="gmtModify", jdbcType=JdbcType.TIMESTAMP)
    })
    MessageBusinessInfo selectByPrimaryKey(Integer id);

    @UpdateProvider(type=MessageBusinessInfoSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") MessageBusinessInfo record, @Param("example") MessageBusinessInfoExample example);

    @UpdateProvider(type=MessageBusinessInfoSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") MessageBusinessInfo record, @Param("example") MessageBusinessInfoExample example);

    @UpdateProvider(type=MessageBusinessInfoSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(MessageBusinessInfo record);

    @Update({
        "update message_business_info",
        "set app_id = #{appId,jdbcType=INTEGER},",
          "app_name = #{appName,jdbcType=VARCHAR},",
          "business_code = #{businessCode,jdbcType=VARCHAR},",
          "app_owner_id = #{appOwnerId,jdbcType=INTEGER},",
          "app_owner_name = #{appOwnerName,jdbcType=VARCHAR},",
          "template = #{template,jdbcType=VARCHAR},",
          "gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},",
          "gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MessageBusinessInfo record);
}