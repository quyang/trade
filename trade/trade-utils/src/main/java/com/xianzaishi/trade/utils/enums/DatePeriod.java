package com.xianzaishi.trade.utils.enums;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author Croky.Zheng
 * 2016年7月16日
 */
public enum DatePeriod {
	//周期类型(1=实时,2=日,3=周,4=月,5=2周,6=3周,4=月,5=2月,6=季,7=2季,8=3季,9=年)
	REALTIME("实时",(short)0xFFFF),
	DAY("日",(short)0x1),
	//DAY2("两日",(short)0x2),
	//DAY3("三日",(short)0x4),
	//DAY4("四日",(short)0x8),
	//DAY5("五日",(short)0xF),
	//DAY6("六日",(short)0x10),
	WEEK("周",(short)0x20),
	//WEEK2("两周",(short)0x40),
	//WEEK3("三周",(short)0x80),
	MONTH("月",(short)0xF0),
	//MONTH2("两月",(short)0x100),
	QUARTER("季",(short)0x200),
	//QUARTER2("两季",(short)0x400),
	//QUARTER3("三季",(short)0x800),
	YEAR("年",(short)0xF00),
	
	YEARMONTH("年月",(short)0x1000);
	
	

	private String description;
	private short value;
	
	private DatePeriod(String description,short value) {
		this.description = description;
		this.value = value;
	}
	
	public static DatePeriod from(int value) {
		for (DatePeriod tmp : DatePeriod.values()) {
			if (tmp.getValue() == value) {
				return tmp;
			}
		}
		return null;
	}
	
	public static Map<Short,String> toMap() {
		Map<Short,String> payMentPeriodMap = new LinkedHashMap<Short,String>();
		for (DatePeriod tmp : DatePeriod.values()) {
			payMentPeriodMap.put(tmp.value, tmp.description);
		}
		return payMentPeriodMap;
	}

	public String getDescription() {
		return description;
	}

	public short getValue() {
		return value;
	}

}
