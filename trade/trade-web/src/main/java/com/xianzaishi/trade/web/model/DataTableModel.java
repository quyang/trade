package com.xianzaishi.trade.web.model;

import java.util.List;

import com.xianzaishi.trade.utils.PageList;


/**
 * datatable前端插件返回的JSON数据格式
 * @author Croky.Zheng
 * 2016年4月8日
 */
public class DataTableModel extends ResponseModel {

	private int draw = 1;
	
	private int sEcho = 0;
	
	/**
	 * 开始显示的索引号
	 */
	private int iDisplayStart = 0;
	/**
	 * 需要显示的数量
	 */
	private int iDisplayLength = 0;
	/**
	 * 总记录个数
	 */
	private int iTotalRecords = 0;
	/**
	 * 需要显示的记录条数
	 */
	private int iTotalDisplayRecords = 0;
	/**
	 * 页码总数
	 */
	private int iTotalPages = 0;
	/**
	 * 当前页码
	 */
	private int iCurPage = 0;
	private Object aaData;

	@SuppressWarnings("rawtypes")
	public DataTableModel(DataTableParam dataTableParam, Object value) {
		if (null == dataTableParam) {
			this.sEcho = 0;
		} else {
			this.sEcho = dataTableParam.getsEcho() + 1;
		}
		this.aaData = value;
		if ((null != aaData) && (aaData instanceof PageList)) {
			this.iTotalDisplayRecords = (int) ((PageList)aaData).getTotal();
			this.iTotalRecords = (int)((PageList)aaData).getTotal();
			this.iDisplayLength = (int) ((PageList)aaData).getPageSize();
			this.iTotalPages = (int) ((PageList)aaData).getTotalPage();
			this.iCurPage = (int) ((PageList)aaData).getCurrentPage();
		} else if ((null != aaData) && (aaData instanceof List)) {
			this.iTotalDisplayRecords = ((List)value).size();
			this.iTotalRecords = this.iTotalDisplayRecords;
			this.iDisplayLength = this.iTotalDisplayRecords;
		}
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getsEcho() {
		return sEcho;
	}

	public void setsEcho(int sEcho) {
		this.sEcho = sEcho;
	}

	public int getiDisplayStart() {
		return iDisplayStart;
	}

	public void setiDisplayStart(int iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}

	public int getiDisplayLength() {
		return iDisplayLength;
	}

	public void setiDisplayLength(int iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Object getAaData() {
		return aaData;
	}

	public void setAaData(Object aaData) {
		this.aaData = aaData;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public int getiTotalPages() {
		return iTotalPages;
	}

	public void setiTotalPages(int iTotalPages) {
		this.iTotalPages = iTotalPages;
	}

	public int getiCurPage() {
		return iCurPage;
	}

	public void setiCurPage(int iCurPage) {
		this.iCurPage = iCurPage;
	}
}
