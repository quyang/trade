package com.xianzaishi.trade.dal.dao;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import com.xianzaishi.trade.dal.model.MessageBusinessInfo;
import com.xianzaishi.trade.dal.model.MessageBusinessInfoExample.Criteria;
import com.xianzaishi.trade.dal.model.MessageBusinessInfoExample.Criterion;
import com.xianzaishi.trade.dal.model.MessageBusinessInfoExample;
import java.util.List;
import java.util.Map;

public class MessageBusinessInfoSqlProvider {

    public String countByExample(MessageBusinessInfoExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("message_business_info");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(MessageBusinessInfoExample example) {
        BEGIN();
        DELETE_FROM("message_business_info");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(MessageBusinessInfo record) {
        BEGIN();
        INSERT_INTO("message_business_info");
        
        if (record.getAppId() != null) {
            VALUES("app_id", "#{appId,jdbcType=INTEGER}");
        }
        
        if (record.getAppName() != null) {
            VALUES("app_name", "#{appName,jdbcType=VARCHAR}");
        }
        
        if (record.getBusinessCode() != null) {
            VALUES("business_code", "#{businessCode,jdbcType=VARCHAR}");
        }
        
        if (record.getAppOwnerId() != null) {
            VALUES("app_owner_id", "#{appOwnerId,jdbcType=INTEGER}");
        }
        
        if (record.getAppOwnerName() != null) {
            VALUES("app_owner_name", "#{appOwnerName,jdbcType=VARCHAR}");
        }
        
        if (record.getTemplate() != null) {
            VALUES("template", "#{template,jdbcType=VARCHAR}");
        }
        
        if (record.getGmtCreate() != null) {
            VALUES("gmt_create", "#{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            VALUES("gmt_modify", "#{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        return SQL();
    }

    public String selectByExample(MessageBusinessInfoExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("app_id");
        SELECT("app_name");
        SELECT("business_code");
        SELECT("app_owner_id");
        SELECT("app_owner_name");
        SELECT("template");
        SELECT("gmt_create");
        SELECT("gmt_modify");
        FROM("message_business_info");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        MessageBusinessInfo record = (MessageBusinessInfo) parameter.get("record");
        MessageBusinessInfoExample example = (MessageBusinessInfoExample) parameter.get("example");
        
        BEGIN();
        UPDATE("message_business_info");
        
        if (record.getId() != null) {
            SET("id = #{record.id,jdbcType=INTEGER}");
        }
        
        if (record.getAppId() != null) {
            SET("app_id = #{record.appId,jdbcType=INTEGER}");
        }
        
        if (record.getAppName() != null) {
            SET("app_name = #{record.appName,jdbcType=VARCHAR}");
        }
        
        if (record.getBusinessCode() != null) {
            SET("business_code = #{record.businessCode,jdbcType=VARCHAR}");
        }
        
        if (record.getAppOwnerId() != null) {
            SET("app_owner_id = #{record.appOwnerId,jdbcType=INTEGER}");
        }
        
        if (record.getAppOwnerName() != null) {
            SET("app_owner_name = #{record.appOwnerName,jdbcType=VARCHAR}");
        }
        
        if (record.getTemplate() != null) {
            SET("template = #{record.template,jdbcType=VARCHAR}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("message_business_info");
        
        SET("id = #{record.id,jdbcType=INTEGER}");
        SET("app_id = #{record.appId,jdbcType=INTEGER}");
        SET("app_name = #{record.appName,jdbcType=VARCHAR}");
        SET("business_code = #{record.businessCode,jdbcType=VARCHAR}");
        SET("app_owner_id = #{record.appOwnerId,jdbcType=INTEGER}");
        SET("app_owner_name = #{record.appOwnerName,jdbcType=VARCHAR}");
        SET("template = #{record.template,jdbcType=VARCHAR}");
        SET("gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP}");
        SET("gmt_modify = #{record.gmtModify,jdbcType=TIMESTAMP}");
        
        MessageBusinessInfoExample example = (MessageBusinessInfoExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(MessageBusinessInfo record) {
        BEGIN();
        UPDATE("message_business_info");
        
        if (record.getAppId() != null) {
            SET("app_id = #{appId,jdbcType=INTEGER}");
        }
        
        if (record.getAppName() != null) {
            SET("app_name = #{appName,jdbcType=VARCHAR}");
        }
        
        if (record.getBusinessCode() != null) {
            SET("business_code = #{businessCode,jdbcType=VARCHAR}");
        }
        
        if (record.getAppOwnerId() != null) {
            SET("app_owner_id = #{appOwnerId,jdbcType=INTEGER}");
        }
        
        if (record.getAppOwnerName() != null) {
            SET("app_owner_name = #{appOwnerName,jdbcType=VARCHAR}");
        }
        
        if (record.getTemplate() != null) {
            SET("template = #{template,jdbcType=VARCHAR}");
        }
        
        if (record.getGmtCreate() != null) {
            SET("gmt_create = #{gmtCreate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getGmtModify() != null) {
            SET("gmt_modify = #{gmtModify,jdbcType=TIMESTAMP}");
        }
        
        WHERE("id = #{id,jdbcType=INTEGER}");
        
        return SQL();
    }

    protected void applyWhere(MessageBusinessInfoExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}