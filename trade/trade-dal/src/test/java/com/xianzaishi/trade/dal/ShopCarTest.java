package com.xianzaishi.trade.dal;

import javax.annotation.Resource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.trade.dal.dao.extend.ShoppingCartDAO;

public class ShopCarTest extends BaseDalTest {

	private static final Logger log = LoggerFactory.getLogger(ShopCarTest.class);
    
    @Resource
    @Autowired
    private ShoppingCartDAO shoppingCartDAO;
    
    @Test
    public void countTest() {
    	int count = shoppingCartDAO.countByExample(null);
    	log.debug("count=" + count);
    	log.debug("sumAllCount=" + shoppingCartDAO.getSumItemCount(500000L));
    }
    
}
