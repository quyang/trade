package com.xianzaishi.trade.client.vo;

import java.io.Serializable;

/**
 * 
 * @author Croky.Zheng
 * 2016年9月14日
 */
public class SkuInfoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7692688137214718318L;

	private String spec;

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}
}
