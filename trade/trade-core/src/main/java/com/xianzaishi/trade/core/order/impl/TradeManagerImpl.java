package com.xianzaishi.trade.core.order.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.core.coupon.UserCouponManager;
import com.xianzaishi.trade.core.credit.UserCreditManager;
import com.xianzaishi.trade.core.order.TradeManager;
import com.xianzaishi.trade.dal.dao.extend.OrdersDAO;
import com.xianzaishi.trade.dal.model.Orders;
import com.xianzaishi.trade.utils.TradeLog;
import com.xianzaishi.trade.utils.enums.OrderStatus;
import com.xianzaishi.trade.utils.enums.Status;

/**
 * 交易管理
 *
 * @author nianyue.hyj
 * @since 2016.10.20
 */
@Component(value = "tradeManager")
public class TradeManagerImpl implements TradeManager {

	@Resource
	protected OrdersDAO ordersDAO;

	@Autowired
	private UserCreditManager userCreditManager;

	@Autowired
	private UserCouponManager userCouponManager;

	@Autowired
	private UserCouponService newUserCouponService;
	
	private Logger logger = LoggerFactory.getLogger(TradeManagerImpl.class);

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public boolean tryCancelTrade(long orderId, String reason) {

		Orders order = null;
		try {
			order = ordersDAO.selectByPrimaryKey(orderId);
		} catch (Exception e) {
			logger.error(String.format("Fail to get order by orderId[%s]", orderId), e);
		}
		if (order != null && order.getStatus().shortValue() == OrderStatus.CLOSED.getValue()) {
			return true;
		}
		if (order == null || order.getStatus().shortValue() != OrderStatus.INIT.getValue()) {
			return false;
		}
		order.setGmtModify(new Date());
		order.setStatus(OrderStatus.CLOSED.getValue());

		boolean success = false;
		try {
			success = ordersDAO.updateByPrimaryKey(order) > 0;
		} catch (Exception e) {
			logger.error(String.format("Fail to update order status for orderId[%s]", orderId), e);
		}
		if (success && order.getCredit() != null && order.getCredit() > 0) {
			try {
				userCreditManager.add(order.getUserId(), 10, order.getCredit(), "取消交易返还");
			} catch (Exception e) {
				TradeLog.error("Fail to return user credit when cancal trade for userId[%s], orderId[%s], credit[%s]", order.getUserId(),
						order.getId(), order.getCredit());
			}
		}
		if (success && order.getCouponId() != null && order.getCouponId() > 0) {
			try {
			  Result<Boolean> updateResult = newUserCouponService.updateCouponStatus(order.getCouponId(), Status.VALID.getValue());
              if (null == updateResult || !updateResult.getSuccess() || !updateResult.getModule()) {
					TradeLog.error("Fail to return user credit when cancal trade for userId[%s], orderId[%s], couponId[%s]", order.getUserId(),
							order.getId(), order.getCouponId());
				}
			} catch (Exception e) {
				TradeLog.error("Fail to return user credit when cancal trade for userId[%s], orderId[%s], couponId[%s]", order.getUserId(),
						order.getId(), order.getCouponId());
			}
		}
		return success;
	}

	@Override
	public boolean forceCancelTrade(long orderId, String reason) {
		// TODO Auto-generated method stub
		return false;
	}

}
