package com.xianzaishi.trade.web.form;

/**
 * author :LingHuChong
 */
public   class OrderFormItem {
	private long id;// 10201622,
	private double price;// 1,商品价格
	private double effePrice;//实际价格
	private long skuId;// 22,
	private String specification;// 1盒,规格
	private String title;// 浩烨气调崇明白山羊肉气调带皮羊前腿400g
	private double count;// 个数
	private double amount;//总金额
	private double effeAmount;//实际总金额(扣除折扣优惠)
	private String icon_url;//商品图片URL
	private int category_id;//类目ID
	private String skuBarCode;//商品条形码(处理临期商品或者批次商品时，用条形码确定某个确定的sku)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getEffePrice() {
		return effePrice;
	}
	public void setEffePrice(double effePrice) {
		this.effePrice = effePrice;
	}
	public long getSkuId() {
		return skuId;
	}
	public void setSkuId(long skuId) {
		this.skuId = skuId;
	}
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public double getCount() {
		return count;
	}
	public void setCount(double count) {
		this.count = count;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getEffeAmount() {
		return effeAmount;
	}
	public void setEffeAmount(double effeAmount) {
		this.effeAmount = effeAmount;
	}
	public String getIcon_url() {
		return icon_url;
	}
	public void setIcon_url(String icon_url) {
		this.icon_url = icon_url;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
    public String getSkuBarCode() {
      return skuBarCode;
    }
    public void setSkuBarCode(String skuBarCode) {
      this.skuBarCode = skuBarCode;
    }
}