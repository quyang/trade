package com.xianzaishi.trade.pay.weixin;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.xianzaishi.trade.pay.util.PayUtil;

/**
 * 统一下单返回值
 * @author Croky.Zheng
 * 2016年9月17日
 */
@XmlRootElement(name="xml")//(name="pay_model",namespace=null)
@XmlAccessorType 
public class UnifiedorderReturnValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3742495107555714175L;

	/**
	 * 应用ID
	 */
	private String appid = "wxdf3989b2edb80672";
	
	/**
	 * 商户号
	 */
	private String mch_id = "1385693802";
	
	/**
	 * 设备信息
	 */
	private String device_info = "online_iphone_android";
	
	/**
	 * 随机字符串
	 */
	private String nonce_str = PayUtil.genRandomString();
	
	/**
	 * 签名
	 */
	private String sign;
	
	private String result_code;
	
	private String prepay_id;
	
	private String trade_type;

	private String return_code;
	
	private String return_msg;

	public String getReturn_code() {
		return return_code;
	}

	public void setReturn_code(String return_code) {
		this.return_code = return_code;
	}

	public String getReturn_msg() {
		return return_msg;
	}

	public void setReturn_msg(String return_msg) {
		this.return_msg = return_msg;
	}

	public String getResult_code() {
		return result_code;
	}

	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

	public String getPrepay_id() {
		return prepay_id;
	}

	public void setPrepay_id(String prepay_id) {
		this.prepay_id = prepay_id;
	}

	public String getTrade_type() {
		return trade_type;
	}

	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getMch_id() {
		return mch_id;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	public String getDevice_info() {
		return device_info;
	}

	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
}
