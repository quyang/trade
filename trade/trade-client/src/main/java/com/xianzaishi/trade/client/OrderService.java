package com.xianzaishi.trade.client;

import java.util.Date;
import java.util.List;

import com.xianzaishi.trade.client.vo.OrderVO;

/**
 * 
 * @author Croky.Zheng 2016年8月26日
 */
public interface OrderService {

	/**
	 * 获取订单信息
	 * 
	 * @param orderId
	 *            订单ID
	 * @return
	 */
	public Result<OrderVO> getOrder(Long orderId);

	/**
	 * 更新订单状态
	 * 
	 * @param orderId
	 *            订单ID
	 * @param status
	 *            订单状态4拣货中,5待配送,6配送中,7确认收货,9退款中
	 * @return
	 */
	public Result<Boolean> updateOrderStatus(long orderId, short status);

	/**
	 * 退款确认,一旦确认直接退款
	 * 
	 * @param orderId
	 *            订单ID
	 * @param amount
	 *            退款金额
	 * @param cause
	 *            退款原因
	 * @param memberId
	 *            操作员工ID
	 * @return
	 */
	public Result<Boolean> refund(long orderId, double amount, String cause, int memberId);
	
	/**
	 * 根据用户信息分页查询用户订单
	 * @param uid
	 * @param status
	 * @param pageNo
	 * @param pageSize
	 * @param isOrderByDesc
	 * @return
	 */
	Result<List<OrderVO>> getOrdersByUserId(long uid, short[] status, int pageNo, int pageSize, boolean isOrderByDesc);

	/**
     * 根据用户信息用户订单总数
     * @param uid
     * @param status
     * @return
     */
	Result<Integer> getOrderCountByUserId(long uid, short[] status);
	
	/**
     * 根据时间信息取订单列表
     * @param uid
     * @param status
     * @param pageNo
     * @param pageSize
     * @param isOrderByDesc
     * @return
     */
    Result<List<OrderVO>> getOrdersByTime(Date begin, Date end, short[] status, int pageNo, int pageSize);
    
    /**
     * 根据时间信息取订单数量
     * @param begin
     * @param end
     * @param status
     * @return
     */
    Result<Integer> getOrderCountByTime(Date begin, Date end, short[] status);
}
