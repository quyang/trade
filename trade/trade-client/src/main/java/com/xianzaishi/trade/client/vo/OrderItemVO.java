package com.xianzaishi.trade.client.vo;

import java.io.Serializable;


/**
 * 订单商品实例
 * @author Croky.Zheng
 * 2016年8月3日
 */
public class OrderItemVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 109494072266472818L;

	/**
	 * 商品ID
	 */
	private Long id;
	
	/**
	 * 商品名称
	 */
	private String name;
	
	/**
	 * 商品图标URL
	 */
	private String iconUrl;
	
	/**
	 * 商品价格
	 */
	private Double price;	
	
	/**
	 * 商品实际价格
	 */
	private Double effePrice;
	
	/**
	 * 商品总值
	 */
	private Double amount; 
	
	/**
	 * 商品实际总值
	 */
	private Double effeAmount;
	
	/**
	 * 购买数量
	 */
	private Double count;		
	
	/**
	 * 商品SKU信息
	 * @see SkuVO
	 */
	private SkuInfoVO skuInfo;
	
	/**
	 * 类目ID
	 */
	private Integer categoryId;
	
	
	public OrderItemVO(){}
	
	/**
	 * SKU ID
	 */
	private Long skuId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCount() {
		return count;
	}

	public void setCount(Double count) {
		this.count = count;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getEffePrice() {
		return effePrice;
	}

	public void setEffePrice(Double effePrice) {
		this.effePrice = effePrice;
	}

	public Double getEffeAmount() {
		return effeAmount;
	}

	public void setEffeAmount(Double effeAmount) {
		this.effeAmount = effeAmount;
	}

	public SkuInfoVO getSkuInfo() {
		return skuInfo;
	}

	public void setSkuInfo(SkuInfoVO skuInfo) {
		this.skuInfo = skuInfo;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}
}
