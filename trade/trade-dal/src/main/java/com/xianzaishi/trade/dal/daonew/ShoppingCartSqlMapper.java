package com.xianzaishi.trade.dal.daonew;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.trade.dal.model.ShoppingCart;
import com.xianzaishi.trade.dal.model.query.ShoppingCartQuery;

public interface ShoppingCartSqlMapper {

  /**
   * 插入一个物车商品对象
   */
  int insert(ShoppingCart shoppingCart);

  /**
   * 批量插入购物车商品对象
   */
  int batchInsert(List<ShoppingCart> shoppingCart);

  /**
   * 插入一个物车商品对象
   */
  int update(ShoppingCart shoppingCart);

  /**
   * 分页取购物车商品信息
   * 
   * @param userId
   * @param page
   * @return
   */
  List<ShoppingCart> select(@Param("query") ShoppingCartQuery query);

  /**
   * 查询指定商品
   * 
   * @param id
   * @return
   */
  ShoppingCart selectCart(@Param("id") Long id);

  /**
   * 取用户购物车商品数量
   * 
   * @param userId
   * @param itemstatus
   * @return
   */
  Integer count(@Param("userId") Long userId);

  /**
   * 删除数据
   * 
   * @param query
   * @return
   */
  Integer delete(@Param("query") ShoppingCartQuery query);
}
