package com.xianzaishi.trade.web.utils;

public class Constants {

	public static final String LOGIN_MEMBER_KEY = "memberKey";
	

	public static final String ERROR_MESSAGE_KEY = "errorMessageKey";
	
	public static final int JSON_SUCCESSED_RESPONSE = 1;
	
	public static final int JSON_FAILED_RESPONSE = -1;
}
