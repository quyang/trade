package com.xianzaishi.trade.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;

import org.junit.Test;


/**
 * 
 * @author Croky.Zheng
 * 2016年9月21日
 */
public class HttpTest {

	@Test
	public void postDataTest() {
		//HttpUtils.getContent("trade.xianzaishi.net/order/getOrders.json");
		HttpClientUtils client = HttpClientUtils.getInstance();
		/*
		String params = "uid=10005&status=2,3,4";//uid=10005&status=2&status=3&status=4
		String result = client.sendHttpPost("http://trade.xianzaishi.net/order/getOrders.json", params);
		System.out.println(result);
		try {
			System.out.println(postDate("http://trade.xianzaishi.net/order/getOrders.json",params.getBytes()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	
		//String params = "uid=10005&id=100&name=郑东旭&phone=15657189965&code=101100&address=普陀东路1101号";
		//String result = client.sendHttpPost("http://trade.xianzaishi.net/da/modifyLinkman.json", params);
		//String params = "uid=10005&status=2&status=3&status=4";
		//String result = client.sendHttpPost("http://trade.xianzaishi.net/order/getOrders.json", params);
		//System.out.println(result);
		//params = "uid=10000&sciId=5&sciId=4";
		//result = client.sendHttpPost("http://trade.xianzaishi.net/sc/toOrder.json", params);
		//System.out.println(result);
//		for (int i=10000;i<10010; i++) {
//			for (int j=0;j<20;j++) {
//				for (int k=1;k<=4;k++) {
//					String params = "uid=" + i + "&cid=" + k;
//					String result = client.sendHttpPost("http://trade.xianzaishi.net/user/addCoupon.json", params);
//					System.out.println(result);
//				}
//			}
//		}
		String params = "uid=10000";
		String result = client.sendHttpPost("http://trade.xianzaishi.net/user/getCoupons.json", params);
		System.out.println(result);
	}
	
	public String postDate(String urlString,byte[] data) throws Exception {
		URI uri = URI.create(urlString);
		HttpURLConnection http = (HttpURLConnection) uri.toURL()
				.openConnection();
		http.setDoInput(true);
		http.setDoOutput(true);
		http.setUseCaches(false);
		http.setConnectTimeout(3000);
		http.setReadTimeout(3000);
		http.setRequestMethod("POST");
		http.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");
		http.setInstanceFollowRedirects(true);
		http.setChunkedStreamingMode(5);
		http.connect();
		OutputStream os = http.getOutputStream();
		if ((null != os) && (http.getDoOutput())) {
			os.write(data);
			os.flush();
			os.close();
		}

		StringBuffer buffer = new StringBuffer();
		if (http.getResponseCode() == 200) {
			if (null != http.getInputStream()) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
				String line = null;
				while ((line = reader.readLine()) != null) {
					buffer.append(line);
				}
			}
		}
		http.disconnect();
		return buffer.toString();
	}
}
