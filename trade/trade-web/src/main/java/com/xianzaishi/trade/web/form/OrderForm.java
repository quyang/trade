package com.xianzaishi.trade.web.form;

import java.util.List;

/**
 *  订单信息
 * author :LingHuChong
 */
public class OrderForm {
    //商品集合
	private List<OrderFormItem> orderItems;
	private long userId;//用户Id
	private int employeeId;//员工Id
	private long preferentialId;//优惠卷Id
	private double orderPrice;//订单总金额
    private double actualPrice;//实收金额
    private String deviceId;//设备Id
    private int shopId;//店铺Id
    private short orderType; //订单类型 0普通订单 1加工订单
    private List<ChargeRecord> records;//支付记录
	public List<OrderFormItem> getOrderItems() {
		return orderItems;
	}
	public void setOrderItems(List<OrderFormItem> orderItems) {
		this.orderItems = orderItems;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public long getPreferentialId() {
		return preferentialId;
	}
	public void setPreferentialId(long preferentialId) {
		this.preferentialId = preferentialId;
	}
	public double getOrderPrice() {
		return orderPrice;
	}
	public void setOrderPrice(double orderPrice) {
		this.orderPrice = orderPrice;
	}
	public double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(double actualPrice) {
		this.actualPrice = actualPrice;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public List<ChargeRecord> getRecords() {
		return records;
	}
	public void setRecords(List<ChargeRecord> records) {
		this.records = records;
	}
	public short getOrderType() {
		return orderType;
	}
	public void setOrderType(short orderType) {
		this.orderType = orderType;
	}
	
}
