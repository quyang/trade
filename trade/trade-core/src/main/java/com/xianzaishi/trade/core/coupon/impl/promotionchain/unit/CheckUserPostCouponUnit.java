package com.xianzaishi.trade.core.coupon.impl.promotionchain.unit;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.PromotionChainContext;
import com.xianzaishi.trade.dal.model.UserCoupon;

/**
 * 检查过滤用户提交的即将使用的优惠券，并且过滤无效优惠券，并且按照优惠价格倒序排序
 * @author zhancang
 */
@Component(value = "checkUserPostCouponUnit")
public class CheckUserPostCouponUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getUserSelectedCouponList())) {
      return Result.getSuccDataResult(true);
    }

    List<UserCoupon> userSelect = context.getUserSelectedCouponList();
    List<UserCoupon> cleanCouponList = Lists.newArrayList();
    for (UserCoupon coupon : userSelect) {
      if(checkCouponAvailable(coupon, context)){
        cleanCouponList.add(coupon);
      }
    }
    Collections.sort(cleanCouponList, new CompratorByMoney());
    context.setUserSelectedCouponList(cleanCouponList);
    return Result.getSuccDataResult(true);
  }
}
