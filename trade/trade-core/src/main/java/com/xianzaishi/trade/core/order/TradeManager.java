package com.xianzaishi.trade.core.order;

/**
 * 交易管理
 *
 * @author nianyue.hyj
 * @since 2016.10.20
 */
public interface TradeManager {

  /**
   * 给收银机取消未支付订单
   * @param orderId
   * @param reason
   * @return
   */
	public boolean tryCancelTrade(long orderId, String reason);

	/**
	 * 未使用
	 * @param orderId
	 * @param reason
	 * @return
	 */
	public boolean forceCancelTrade(long orderId, String reason);

}
