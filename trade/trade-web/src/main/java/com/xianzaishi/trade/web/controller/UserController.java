package com.xianzaishi.trade.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.xianzaishi.trade.core.coupon.UserCouponManager;
import com.xianzaishi.trade.core.credit.UserCreditManager;
import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.trade.utils.enums.Status;
import com.xianzaishi.trade.web.model.ResponseModel;
import com.xianzaishi.trade.web.utils.ConstantUtils;

/**
 * 
 * @author Croky.Zheng 2016年9月20日
 */
@Controller
public class UserController {

	@Autowired
	private UserCreditManager userCreditManager;

	@Autowired
	private UserCouponManager userCouponManager;

	/**
	 * 获取用户积分
	 * 
	 * @param uid
	 *            用户ID
	 * @return ReponseModel 用户积分
	 */
	@RequestMapping(value = "/user/getCredit.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getCredit(@RequestParam Long uid) {
		try {
			return ConstantUtils.normalResponse(userCreditManager.getCreditByUserId(uid));
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 获取用户积分增减记录
	 * 
	 * @param uid
	 *            用户ID
	 * @return ReponseModel 用户积分
	 */
	@RequestMapping(value = "/user/getCreditLogs.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getCreditLogs(@RequestParam Long uid) {
		try {
			return ConstantUtils.normalResponse(userCreditManager.getUserCreditLogs(uid));
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 获取用户所有的未使用的优惠券,包括已经过期的优惠券
	 * 
	 * @param uid
	 *            用户ID
	 * @param amount
	 *            订单金额 可惜，不带订单金额则默认返回所有有效订单
	 * @return UserCoupon
	 */
	@RequestMapping(value = "/user/getCoupons.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getCoupons(@RequestParam Long uid,
			@RequestParam(value = "amount", defaultValue = "0.0", required = false) Double amount) {
		try {
			List<UserCoupon> userCoupons = userCouponManager.getUserCouponsByStatus(uid, Lists.newArrayList((short) 1, (short) 11, (short) 12,(short) 77),
					Status.VALID);
			if (amount > 0.0) {
				List<UserCoupon> needDelList = new ArrayList<UserCoupon>(userCoupons.size());
				long now = System.currentTimeMillis();
				// 需要被删除的优惠券
				for (UserCoupon userCoupon : userCoupons) {
					if (userCouponManager.calculate(userCoupon, amount) <= 0.00999999
							|| userCoupon.getCouponStartTime().getTime() > now || userCoupon.getCouponEndTime().getTime() < now) {
						needDelList.add(userCoupon);
					}
					//查询可用优惠券时，老的接口判断逻辑有问题，不能展示这种优惠券
					if(userCoupon.getCouponType() == 12){
					  needDelList.add(userCoupon);
					}
				}
				for (UserCoupon userCoupon : needDelList) {
					userCoupons.remove(userCoupon);
				}
			}
			for(UserCoupon coupon:userCoupons){
			  if(coupon.isFenPriceTypeUserCoupon()){
			    Double amountInfo = coupon.getAmount();
			    Double amountLimitInfo = coupon.getAmountLimit();
			    amountInfo = new BigDecimal(amountInfo.toString()).divide(new BigDecimal(100),2,BigDecimal.ROUND_DOWN).doubleValue();
			    amountLimitInfo = new BigDecimal(amountLimitInfo.toString()).divide(new BigDecimal(100),2,BigDecimal.ROUND_DOWN).doubleValue();
			    coupon.setAmount(amountInfo);
			    coupon.setAmountLimit(amountLimitInfo);
			  }
			}
			return ConstantUtils.normalResponse(userCoupons);
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 获取用户优惠券详情，订单详情页面调用
	 * 
	 * @param uid
	 *            用户ID
	 * @param couponId
	 *            优惠券ID
	 * @return UserCoupon
	 */
	@RequestMapping(value = "/user/getCouponDetail.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel getUserOrderCouponDetail(@RequestParam(required = true) Long uid, @RequestParam(required = true) Long oid) {
		try {
			List<UserCoupon> userCoupons = new ArrayList<>();
			if (uid > 0 && oid > 0) {
				List<UserCoupon> results = userCouponManager.getUserCouponByOrderId(uid, oid);
				if (results != null) {
					userCoupons.addAll(results);
				}
			}
			return ConstantUtils.normalResponse(userCoupons);
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 操作用户积分
	 * 
	 * @param uid
	 *            用户ID
	 * @param amount
	 *            积分数量
	 * @param appId
	 *            APP ID，默认为0
	 * @param description
	 *            原因
	 * @return ReponseModel 用户积分
	 */
	@RequestMapping(value = "/user/addCredit.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel addCredit(@RequestParam Long uid, @RequestParam int amount,
			@RequestParam(value = "appId", defaultValue = "0", required = false) int appId,
			@RequestParam(value = "description", defaultValue = "外部添加", required = false) String description) {
		try {
			return ConstantUtils.normalResponse(userCreditManager.add(uid, appId, amount, description));
		} catch (RuntimeException e) {
			return ConstantUtils.errorReponse(e.getMessage());
		}
	}

	/**
	 * 操作用户积分
	 * 
	 * @param uid
	 *            用户ID
	 * @param cid
	 *            优惠券ID
	 * @return ReponseModel 用户积分
	 */
	@RequestMapping(value = "/user/addCoupon.json", method = { RequestMethod.POST })
	public @ResponseBody ResponseModel addCoupon(@RequestParam Long uid, @RequestParam Long cid) {
//		try {
//			return ConstantUtils.normalResponse(userCouponManager.insert(uid, cid));
//		} catch (RuntimeException e) {
//			return ConstantUtils.errorReponse(e.getMessage());
//		}
	  return ConstantUtils.errorReponse("发送优惠券接口已经迁移");
	}
}
