package com.xianzaishi.trade.utils;


/**
 * 
 * @author Croky.Zheng
 * 2016年9月17日
 */
public class Pagination {


	/**
	 * 
	 */
	//private static final long serialVersionUID = 2852715277256521582L;

	public Pagination(int currentPage,int pageSize,long total) {
		//super(pageSize);
		this.currentPage = currentPage;
		this.pageSize = pageSize;
		this.total = total;
		this.check();
	}
	
	//synchronized
	public static Pagination getPagination(int currentPage,int pageSize,long total) {
		Pagination pagination = new Pagination(currentPage,pageSize,total);
		pagination.check();
		return pagination;
	}
	
	/**
	 * 每页大小
	 */
	private long pageSize = 0;
	

	/**
	 * 传入SQL参数的页面大小
	 */
	private long realSize = 0;
	/**
	 * 总量
	 */
	private long total = 0;
	
	/**
	 * 当前页码
	 */
	private long currentPage = 0;
	
	
	/**
	 * 总页码
	 */
	private long totalPage = 0;

	public long getPageSize() {
		return pageSize;
	}

	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
		this.check();
	}

	public long getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(long currentPage) {
		this.currentPage = currentPage;
	}

	public long getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(long totalPage) {
		this.totalPage = totalPage;
	}
	
	private void check() {

        //分页计算
        if(total > 0) {
        	realSize = pageSize;
        	//页大小 <= 0
        	if (realSize <= 0) {
        		realSize = 20;
        	}
        	
        	//总量小于页大小
        	if (total < realSize) {
        		realSize = total;
        	}
        	//总页数
        	totalPage = total / realSize;
        	//不被整除则加上尾页
        	if (total % realSize  > 0) {
        		totalPage += 1;
        	}
        	//最小当前页为第一页
        	if (currentPage < 1) {
        		currentPage = 1;
        	}
        	//当前页大于总页数
        	if (totalPage < currentPage) {
        		currentPage = totalPage;
        	}
        }
	}
	
	public long limit() {
		return realSize;
	}
	
	public long getLimit() {
		return limit();
	}
	
	public long offset() {
		return (currentPage - 1) * realSize;
	}
	
	public long getOffset() {
		return offset();
	}
}
