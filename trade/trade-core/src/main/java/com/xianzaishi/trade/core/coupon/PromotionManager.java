package com.xianzaishi.trade.core.coupon;

import com.xianzaishi.trade.client.vo.UserCouponDiscontQueryVO;
import com.xianzaishi.trade.client.vo.UserCouponDiscontResultVO;

/**
 * 很快将迁移掉的优惠的新接口
 * 
 * @author zhancang
 */
public interface PromotionManager {

  UserCouponDiscontResultVO selectMathCoupon(UserCouponDiscontQueryVO query);

  UserCouponDiscontResultVO calculateDiscountPriceWithCoupon(UserCouponDiscontQueryVO query);

}
