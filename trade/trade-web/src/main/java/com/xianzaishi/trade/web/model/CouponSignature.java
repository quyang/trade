package com.xianzaishi.trade.web.model;

import java.util.Date;

public class CouponSignature {

	/**
	 * 优惠券名称
	 */
	private String couponTitle;

	/**
	 * 优惠券ID
	 */
	private Long couponId;

	/**
	 * 优惠券开始时间
	 */
	private Date couponStartTime;

	/**
	 * 优惠券结束时间
	 */
	private Date couponEndTime;

	/**
	 * 优惠券加密参数,5分钟内有效
	 */
	private String security;

	/**
	 * RSA私钥签名
	 */
	private String sign;

	public String getCouponTitle() {
		return couponTitle;
	}

	public void setCouponTitle(String couponTitle) {
		this.couponTitle = couponTitle;
	}

	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public Date getCouponStartTime() {
		return couponStartTime;
	}

	public void setCouponStartTime(Date couponStartTime) {
		this.couponStartTime = couponStartTime;
	}

	public Date getCouponEndTime() {
		return couponEndTime;
	}

	public void setCouponEndTime(Date couponEndTime) {
		this.couponEndTime = couponEndTime;
	}

	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
