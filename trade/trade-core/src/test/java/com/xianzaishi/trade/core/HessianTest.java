package com.xianzaishi.trade.core;

import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.google.common.collect.Lists;
import com.xianzaishi.common.domain.RpcResult;
import com.xianzaishi.trade.account.api.AliPayService;
import com.xianzaishi.trade.account.api.WeiXinPayService;
import com.xianzaishi.trade.client.LocationService;
import com.xianzaishi.trade.client.MessageService;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.client.UserCouponService;
import com.xianzaishi.trade.pay.ali.AlipayConfig;
import com.xianzaishi.trade.pay.weixin.WeiXinPayConfig;
import com.xianzaishi.trade.utils.MD5Utils;
import com.xianzaishi.weixin.sdk.domain.WeiXinRefundRequest;
import com.xianzaisih.alipay.sdk.domain.AliPayQueryRequest;
import com.xianzaisih.alipay.sdk.domain.AliPayRefundRequest;
import com.xianzaisih.alipay.sdk.domain.AliPayRefundResponse;

public class HessianTest {

	@Test
	public void test01() throws MalformedURLException {
		String url = "http://trade.xianzaishi.com/hessian/orderService";
		HessianProxyFactory factory = new HessianProxyFactory();
		OrderService service = (OrderService) factory.create(OrderService.class, url);

		System.out.println(JSONObject.toJSONString(service.getOrder(100065L).getModel()));
	}

	@Test
	public void test02() throws MalformedURLException {
		String url = "http://localhost:8080/hessian/messageService";
		// String url = "http://trade.xianzaishi.com/hessian/messageService";
		HessianProxyFactory factory = new HessianProxyFactory();
		factory.setOverloadEnabled(true);
		MessageService service = (MessageService) factory.create(MessageService.class, url);

		System.out.println(JSONObject.toJSONString(service.systemSend("13757132072", "验证码1111，有效期10分钟，感谢您的注册。请不要把验证码告诉其他人，谨防诈骗。")));
	}

	@Test
	public void test03() throws MalformedURLException {
		String url = "http://trade.xianzaishi.net/hessian/locationService";
		HessianProxyFactory factory = new HessianProxyFactory();
		LocationService service = (LocationService) factory.create(LocationService.class, url);

		System.out.println(JSONObject.toJSONString(service.getDeliveryAddressById(179L)));
	}

	@Test
	public void test04() throws MalformedURLException {
		String url = "http://trade.xianzaishi.com/hessian/userCouponService";
		HessianProxyFactory factory = new HessianProxyFactory();
		UserCouponService service = (UserCouponService) factory.create(UserCouponService.class, url);

		System.out.println(JSONObject.toJSONString(service.sendCouponPackage(10031, (short) 11, 15 * 24 * 60 * 60 * 1000)));
	}

	@Test
	public void test05() throws MalformedURLException {
		String url = "http://localhost:9000/hessian/weiXinPayService";
		HessianProxyFactory factory = new HessianProxyFactory();
		WeiXinPayService service = (WeiXinPayService) factory.create(WeiXinPayService.class, url);

		WeiXinRefundRequest request = new WeiXinRefundRequest();
		request.setAppId(WeiXinPayConfig.APPID);
		request.setMchId(WeiXinPayConfig.MCHID);
		request.setNonceStr(MD5Utils.randString());
		request.setTransactionId("4009832001201610217320363032");
		request.setOutRefundNo("11182");
		request.setTotalFee(1);
		request.setRefundFee(1);
		request.setOpUserId(WeiXinPayConfig.APPID);

		System.out.println(JSONObject.toJSONString(service.weixinRefund(request)));
	}

	@Test
	public void test06() throws MalformedURLException {
		String url = "http://localhost:9000/hessian/aliPayService";
		HessianProxyFactory factory = new HessianProxyFactory();
		AliPayService service = (AliPayService) factory.create(AliPayService.class, url);

		AliPayRefundRequest request = new AliPayRefundRequest();
		request.setAppId(AlipayConfig.APP_ID);
		request.setMethod("alipay.trade.refund");
		request.setCharset(AlipayConfig.V1_INPUT_CHARSET);
		request.setSignType(AlipayConfig.V1_SIGN_TYPE);
		request.setTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		request.setVersion("1.0");
		AliPayRefundRequest.BizContent content = request.createBizContent();
		content.setOutTradeNo("100034");
		content.setTradeNo("2016110321001004250234329306");
		content.setRefundAmount("3.60");
		content.setRefundReason("鲜在时-取消订单交易退款");
		content.setOutRequestNo("100034");

		System.out.println(JSONObject.toJSONString(service.aliPayRefundV1(request)));
	}

	@Test
	public void test07() throws MalformedURLException {
		String url = "http://localhost:9000/hessian/aliPayService";
		HessianProxyFactory factory = new HessianProxyFactory();
		AliPayService service = (AliPayService) factory.create(AliPayService.class, url);

		AliPayQueryRequest request = new AliPayQueryRequest();
		request.setAppId(AlipayConfig.APP_ID);
		request.setMethod("alipay.trade.query");
		request.setCharset(AlipayConfig.V1_INPUT_CHARSET);
		request.setSignType(AlipayConfig.V1_SIGN_TYPE);
		request.setTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		request.setVersion("1.0");
		AliPayQueryRequest.BizContent content = request.createBizContent();
		content.setOutTradeNo("11215");
		// content.setRefundAmount("0.01");
		// content.setRefundReason("鲜在时-取消订单交易退款");
		// content.setOutRequestNo("11215");

		System.out.println(JSONObject.toJSONString(service.aliPayQuery(request)));
	}

	@Test
	public void test08() throws MalformedURLException {
		String url = "http://trade.xianzaishi.net/hessian/orderService";
		HessianProxyFactory factory = new HessianProxyFactory();
		OrderService service = (OrderService) factory.create(OrderService.class, url);

		System.out.println(JSONObject.toJSONString(service.refund(11984, 0.30, "测试退款", 0)));
	}

	@Test
	public void test09() throws MalformedURLException {
		String url = "http://trade.xianzaishi.com/hessian/orderService";
		HessianProxyFactory factory = new HessianProxyFactory();
		OrderService service = (OrderService) factory.create(OrderService.class, url);

		System.out.println(JSONObject.toJSONString(service.updateOrderStatus(100035L, (short) 4)));
	}

	@Test
	public void test10() throws MalformedURLException {
		String url = "http://trade.xianzaishi.net/hessian/userCouponService";
		HessianProxyFactory factory = new HessianProxyFactory();
		UserCouponService service = (UserCouponService) factory.create(UserCouponService.class, url);

		System.out.println(JSONObject.toJSONString(service.getUserCouponByTypes(10032, Lists.newArrayList((short) 11))));
	}

	@Test
	public void test11() throws MalformedURLException {
		String url = "http://trade.xianzaishi.net/hessian/userCouponService";
		HessianProxyFactory factory = new HessianProxyFactory();
		UserCouponService service = (UserCouponService) factory.create(UserCouponService.class, url);

		// 17 and 16
		for (int i = 0; i < 10; i++) {
			System.out.println(JSONObject.toJSONString(service.sendCouponByCouponId(10031, 16, 15 * 24 * 60 * 60 * 1000)));
		}
	}
}
