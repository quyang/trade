package com.xianzaishi.trade.facade.delivery;

import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xianzaishi.trade.client.DeliveryService;
import com.xianzaishi.trade.client.Result;
import com.xianzaishi.trade.client.vo.DeliveryVO;
import com.xianzaishi.trade.core.address.DeliveryAddressManager;
import com.xianzaishi.trade.dal.model.DeliveryAddress;

/**
 * quyang 2017-2-9 15:00:32
 *
 * @author quyang
 */
@Service(value = "deliveryService")
public class DeliveryServiceIml implements DeliveryService {

  @Autowired
  private DeliveryAddressManager deliveryAddressManager;

  @Override
  public Result<List<DeliveryVO>> getUseralwaysAddreses(Long userId) {

    if (null == userId || userId <= 0) {
      return new Result<>(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "userId wrong", null);
    }

    List<DeliveryAddress> allLinkman = deliveryAddressManager.getAllLinkman(userId);
    if (null != allLinkman) {

      List<DeliveryVO> allLink = new ArrayList<DeliveryVO>();

      for (DeliveryAddress deliveryAddress : allLinkman) {

        if (null == deliveryAddress) {
          continue;
        }

        DeliveryVO deliveryVO = new DeliveryVO();

        deliveryVO.setName(deliveryAddress.getName());
        deliveryVO.setUserId(deliveryAddress.getUserId());
        deliveryVO.setAddress(deliveryAddress.getAddress());
        deliveryVO.setCodeAddress(deliveryAddress.getCodeAddress());
        deliveryVO.setPhone(deliveryAddress.getPhone());

        allLink.add(deliveryVO);
      }

      return new Result<>(ServerResultCode.SUCCESS_CODE, allLink);

    } else {
      return new Result<>(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询常用地址失败");
    }
  }

}
