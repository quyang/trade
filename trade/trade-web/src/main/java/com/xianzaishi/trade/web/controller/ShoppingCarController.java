package com.xianzaishi.trade.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.croky.util.DateUtils;
import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.discount.DiscountInfoService;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountDetailDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.client.vo.SkuInfoVO;
import com.xianzaishi.trade.core.cart.ShoppingCartManager;
import com.xianzaishi.trade.dal.model.ShoppingCart;
import com.xianzaishi.trade.utils.JsonConvert;
import com.xianzaishi.trade.utils.PageList;
import com.xianzaishi.trade.utils.Pagination;
import com.xianzaishi.trade.web.model.ResponseModel;
import com.xianzaishi.trade.web.utils.ConstantUtils;
import com.xianzaishi.trade.web.vo.DiscountInfoVO;
import com.xianzaishi.trade.web.vo.PageListVO;
import com.xianzaishi.trade.web.vo.ShoppingCarItemVO;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;

/**
 * 
 * @author Croky.Zheng 2016年8月19日
 */
@Controller
public class ShoppingCarController {
    protected static final Logger log = LoggerFactory.getLogger(ShoppingCarController.class);

    @Autowired
    private ShoppingCartManager shoppingCartManager;

    @Autowired
    private IInventory2CDomainClient iInventory2CDomainClient;

    @Autowired
    private DiscountInfoService discountInfoService;

    @Autowired
    private UserService userService;

    /**
     * 添加商品到购物车
     * 
     * @param uid
     *            用户ID
     * @param itemId
     *            商品ID
     * @param itemCount
     *            商品数量
     * @param skuId
     *            SKU ID
     * @return 购物车ITEMID
     */
    @RequestMapping(value = "/sc/appendItem.json", method = { RequestMethod.POST })
    public @ResponseBody ResponseModel appendItem(@RequestParam Long uid, @RequestParam Long itemId, @RequestParam Long skuId,
            @RequestParam Integer itemCount) {
        if (itemCount <= 0) {
            return ConstantUtils.errorReponse("商品数量必须大于0");
        }
        long id = shoppingCartManager.insert(uid, itemId, skuId, itemCount * 1.0);
        if (id > 0) {
            return ConstantUtils.normalResponse(id);
        } else {
            return ConstantUtils.errorReponse("添加商品到购物车失败");
        }
    }

    // logisticalId
    /**
     * 购物车结算，通过购物车生成订单
     * 购物车产生订单
     * 
     * @param uid
     *            用户ID 必选
     * @param shopId
     *            店铺ID 可选
     * @param sciId
     *            购物车商品的条目ID 必选
     * @param couponId
     *            优惠券ID 可选
     * @param adderssId
     *            收货地址ID 可选
     * @param distribution
     *            定时送达时间，格式yyyyMMddhhmmss 可选
     * @param credit
     *            积分 可选
     * @param description
     *            备注 可选
     * @return 包含数据:订单ID
     */
    @RequestMapping(value = "/sc/toOrder.json", method = { RequestMethod.POST })
    public @ResponseBody ResponseModel toOrder(@RequestParam Long uid,
            @RequestParam(value = "shopId", defaultValue = "1", required = false) Integer shopId, @RequestParam Long[] sciId,
            @RequestParam(value = "couponId", defaultValue = "0", required = false) Long couponId,
            @RequestParam(value = "adderssId", defaultValue = "0", required = false) Long adderssId,
            @RequestParam(value = "distribution", defaultValue = "", required = false) String distribution,
            @RequestParam(value = "credit", defaultValue = "0", required = false) int credit,
            @RequestParam(value = "description", defaultValue = "", required = false) String description) {
        if (uid <= 0L) {
            return ConstantUtils.errorReponse("生成订单失败");
        }
        if ((null == sciId) || (sciId.length == 0)) {
            return ConstantUtils.errorReponse("生成订单失败:" + "最少需要选择购物车一个商品");
        }
        try {
            Date date = DateUtils.yyyyMMddhhmmss(distribution);
            long orderId = shoppingCartManager.toOrder(uid, shopId, sciId, couponId, adderssId, date, credit, description);
            if (orderId > 0) {
                return ConstantUtils.normalResponse(orderId);
            } else {
                return ConstantUtils.errorReponse("提示:生成订单失败");
            }
        } catch (Exception e) {
            return ConstantUtils.errorReponse("提示:" + e.getMessage());
        }
    }

    /**
     * 清空购物车 清空购物车
     * 
     * @param uid
     *            用户ID
     * @return ""
     * @see ResponseModel
     */
    @RequestMapping(value = "/sc/clean.json", method = { RequestMethod.POST })
    public @ResponseBody ResponseModel clean(@RequestParam Long uid) {
        if (shoppingCartManager.clean(uid)) {
            return ConstantUtils.normalResponse();
        } else {
            return ConstantUtils.errorReponse("清空购物车失败");
        }
    }

    /**
     * 更新购物车中商品的数量
     * 
     * @param uid
     *            用户ID
     * @param itemId
     *            记录ID
     * @param skuId
     *            SKU ID
     * @param itemCount
     *            数量
     * @return ""
     * @see ResponseModel
     */
    @RequestMapping(value = "/sc/changeItemCount.json", method = { RequestMethod.POST })
    public @ResponseBody ResponseModel changeItemCount(@RequestParam Long uid, @RequestParam Long itemId, @RequestParam Long skuId,
            @RequestParam Integer itemCount) {
        //移除优惠上下文
//        discountInfoService.removePromotionChainContext(uid);
        boolean result = shoppingCartManager.updateItemCount(uid, itemId, skuId, itemCount);

        return (result) ? ConstantUtils.normalResponse() : ConstantUtils.errorReponse("更新商品数量失败");
    }

    /**
     * 获取购物车条目数量
     * 
     * @param uid
     *            用户ID
     * @return 用户购物车的条目数量
     * @see ResponseModel
     */
    @RequestMapping(value = "/sc/getItemCount.json", method = { RequestMethod.POST })
    public @ResponseBody ResponseModel getItemCount(@RequestParam Long uid) {
        if (uid <= 0) {
            return ConstantUtils.errorReponse("获取购物车商品数量失败");
        }
        return ConstantUtils.normalResponse(shoppingCartManager.getAllItemCount(uid));
    }

    /**
     * 获取用户的购物车中所有商品的总量
     * 
     * @param uid
     *            用户ID
     * @return 购物车中商品的量和
     */
    @RequestMapping(value = "/sc/getSumItemCount.json", method = { RequestMethod.POST })
    public @ResponseBody ResponseModel getSumItemCount(@RequestParam Long uid) {
        if (null == uid || uid <= 0) {
            return ConstantUtils.errorReponse("获取购物车商品数量失败");
        }
        return ConstantUtils.normalResponse(shoppingCartManager.getSumItemCount(uid));
    }

    /**
     * 获取购物车所有商品
     *
     * @param uid
     *            用户ID
     * @param pageSize
     *            每页的条目数量
     * @param curPage
     *            当前页码
     * @return 购物车清单
     * @see ResponseModel
     * @see ShoppingCarItemVO
     */
    @RequestMapping(value = "/sc/getItems.json", method = { RequestMethod.POST })
    public @ResponseBody ResponseModel getItems(@RequestParam Long uid,
                                                @RequestParam(value = "pageSize", defaultValue = "99", required = false) Integer pageSize,
                                                @RequestParam(value = "curPage", defaultValue = "0", required = false) Integer curPage) {
        if (uid <= 0) {
            return ConstantUtils.errorReponse("获取购物车条目列表失败");
        }
        //移除优惠上下文
        discountInfoService.removePromotionChainContext(uid);
        try {
            PageList<ShoppingCart> cartList = shoppingCartManager.getItemsByUserId(uid, curPage, pageSize);
            List<ShoppingCarItemVO> voList = new ArrayList<ShoppingCarItemVO>();// cartList.size()
            if (CollectionUtils.isNotEmpty(cartList)) {
                for (ShoppingCart cart : cartList) {
                    ShoppingCarItemVO vo = shoppingCarItem2VO(cart);
                    try {
                        SimpleResultVO<InventoryVO> storeResult = iInventory2CDomainClient.getInventoryByGovAndSKU(1L, cart.getItemSkuId());
                        vo.setNumber(Integer.toString(storeResult != null && storeResult.isSuccess() ? storeResult.getTarget().getNumber() : 0));
                    } catch (Exception e) {
                        // ignore
                    }
                    voList.add(vo);
                }
                Pagination pagin = cartList.getPagination();
                PageListVO<ShoppingCarItemVO> pageList = new PageListVO<ShoppingCarItemVO>(pagin, voList);
                return ConstantUtils.normalResponse(pageList);
            }
            return ConstantUtils.normalResponse();
        } catch (RuntimeException e) {
            return ConstantUtils.errorReponse(e.getMessage());
        }
    }

    private static final String[] CART_RESPONSE_PROPERTIES_FILTER = {"code","data","discountInfo","currPrice","discountPrice",
            "originPrice","objects","discountDesc","effeAmount","id","itemCount","itemEffePrice","itemIconUrl","itemId","itemName","itemPayPrice"
            ,"itemTags","number","payAmount","skuId"
            ,"skuInfo","spec","error","message","success","getDiscount","freightDesc","totalDiscountPrice"
    };


    /**
     * 获取购物车所有商品
     * 
     * @param
     *
     * @param pageSize
     *            每页的条目数量
     * @param curPage
     *            当前页码
     * @return 购物车清单
     * @see ResponseModel
     * @see ShoppingCarItemVO
     */
    @RequestMapping(value = "/sc/getCarts.json", method = { RequestMethod.POST })
    public @ResponseBody String getCarts(@RequestParam String token,
            @RequestParam(value = "pageSize", defaultValue = "99", required = false) Integer pageSize,
            @RequestParam(value = "curPage", defaultValue = "0", required = false) Integer curPage) {
        if (StringUtils.isBlank(token)) {
            return JSON.toJSONString(ConstantUtils.errorReponse("获取购物车条目列表失败"),new SimplePropertyPreFilter(CART_RESPONSE_PROPERTIES_FILTER));
        }

        try {
            Result<BaseUserDTO> userResult = userService.queryBaseUserByToken(token);

            if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
                return  JSON.toJSONString(ConstantUtils.errorReponse("未登录"));

            }
            BaseUserDTO user = userResult.getModule();
            long uid = user.getUserId();
            
            PageList<ShoppingCart> cartList = shoppingCartManager.getItemsByUserId(uid, curPage, pageSize);
            if(null == cartList || CollectionUtils.isEmpty(cartList)){
              return JSON.toJSONString(ConstantUtils.normalResponse());
            }
            //取优惠
            DiscountResultDTO disResult = shoppingCartManager.getShoppingCartPromotionInfo(cartList, uid);
            //更新格式到前台可用vo对象
            PageListVO<ShoppingCarItemVO> returnCartList = changeResultDOToVO(cartList);
            //合并优惠
            appendPromotionInfoToShoppingCarInfo(returnCartList,disResult,false);
            DiscountInfoVO discountInfo = returnCartList.getDiscountInfo();
            if(null == discountInfo){//异常情况处理
              setDiscountInfoVOByCar(returnCartList);
              discountInfo = returnCartList.getDiscountInfo();
            }
            
            returnCartList.setFreightDesc(getFreightInfo(discountInfo.getCurrPrice()));
            return JSON.toJSONString(ConstantUtils.normalResponse(returnCartList),new SimplePropertyPreFilter(CART_RESPONSE_PROPERTIES_FILTER));
           
//            boolean isNeedDistributionPrice = isNeedDistributionPrice(discountInfo.getCurrPrice());
//            if(isNeedDistributionPrice){
//              returnCartList.setFreightDesc("再满59元，可免配送费");
//            }else{
//              returnCartList.setFreightDesc("您已享受，满59元，免配送费");
//            }
//            
//            List<ShoppingCarItemVO> voList = new ArrayList<>();// cartList.size()
//            List<BuySkuInfoDTO> buySkuDetail = new ArrayList<>();
//            if (CollectionUtils.isNotEmpty(cartList)) {
//                for (ShoppingCart cart : cartList) {
//                    ShoppingCarItemVO vo = shoppingCarItem2VO(cart);
//                    BuySkuInfoDTO buySkuInfoDTO = new BuySkuInfoDTO();
//                    buySkuInfoDTO.setSkuId(cart.getItemSkuId());
//                    buySkuInfoDTO.setBuyCount(String.valueOf(cart.getItemCount()));
//                    buySkuDetail.add(buySkuInfoDTO);
//                    try {
//                        SimpleResultVO<InventoryVO> storeResult = iInventory2CDomainClient.getInventoryByGovAndSKU(1L, cart.getItemSkuId());
//                        vo.setNumber(Integer.toString(storeResult != null && storeResult.isSuccess() ? storeResult.getTarget().getNumber() : 0));
//                    } catch (Exception e) {
//                        // ignore
//                    }
//                    voList.add(vo);
//                }
//
//                if(CollectionUtils.isEmpty(buySkuDetail)){
//                    return JSON.toJSONString(ConstantUtils.errorReponse("参数出错"));
//                }
//                //获取优惠信息
//                DiscountQueryDTO query = new DiscountQueryDTO();
//                query.setUserId(uid);
//                query.setBuySkuDetail(buySkuDetail);
//                Result<Map<String,DiscountCalResultDTO>>  discountResult =  null;
//                try{
//                    discountResult = discountInfoService.cal(query);
//                }catch (Exception e){
//                    log.error("优惠计算异常"+e.getMessage());
//                }
//                if(null == discountResult||!discountResult.getSuccess()||null==discountResult.getModule()){
//                    Pagination pagin = cartList.getPagination();
//                    PageListVO<ShoppingCarItemVO> pageList = new PageListVO<ShoppingCarItemVO>(pagin, voList);
//                    pageList.setFreightDesc("再满59元，可免配送费");
////                    resultJson.put("freightDesc","再满59元，可免配送费");
//                    return JSON.toJSONString(ConstantUtils.normalResponse(pageList),new SimplePropertyPreFilter(CART_RESPONSE_PROPERTIES_FILTER));
//                }
//                final Map<String,DiscountCalResultDTO> discountCalResultDTOMap =   discountResult.getModule();
//                CollectionUtils.forAllDo(voList,new Closure(){
//                    @Override
//                    public void execute(Object o) {
//                        ShoppingCarItemVO personInfo1 = (ShoppingCarItemVO)o;
//                        DiscountCalResultDTO fullNUseM = discountCalResultDTOMap.get(DiscountTypeEnum.FULL_N_USE_M.getCode()+DiscountTypeEnum.FULL_N_USE_M.getValue()+"_"+personInfo1.getSkuId());
//                        if(null != fullNUseM){
//                        //设置单个值
//                          DiscountInfoVO discountInfoVo = handleDisountInfo(fullNUseM);
//                          personInfo1.setDiscountInfo(discountInfoVo);
//                        }
//                        
//                        fullNUseM = discountCalResultDTOMap.get(DiscountTypeEnum.SKU_DISCOUNT.getCode()+DiscountTypeEnum.SKU_DISCOUNT.getValue()+"_"+personInfo1.getSkuId());
//                        if(null != fullNUseM){
//                          //设置单个值
//                            DiscountInfoVO discountInfoVo = handleDisountToSpecialPrice(fullNUseM);
//                            personInfo1.setDiscountInfo(discountInfoVo);
//                          }
//                    }
//                });
//                Pagination pagin = cartList.getPagination();
//                PageListVO<ShoppingCarItemVO> pageList = new PageListVO<ShoppingCarItemVO>(pagin, voList);
//                DiscountCalResultDTO fullReduce = discountCalResultDTOMap.get(DiscountTypeEnum.FULL_REDUCE.getCode()+DiscountTypeEnum.FULL_REDUCE.getValue());
//                DiscountInfoVO discountInfoVo = handleDisountInfo(fullReduce);
//                pageList.setDiscountInfo(discountInfoVo);
//                pageList.setFreightDesc(getFreightInfo(discountInfoVo.getCurrPrice()));
//                
//                if(StringUtils.isEmpty(freight))
//                    pageList.setFreightDesc("您已享受，满59元，免配送费");
//                else
//                {
//                    BigDecimal freghtDesc = new BigDecimal("59").subtract(new BigDecimal(discountInfoVo.getCurrPrice()));
//                    pageList.setFreightDesc("再满"+freghtDesc.toString()+"元，可免配送费");
//                }
//                return JSON.toJSONString(ConstantUtils.normalResponse(pageList),new SimplePropertyPreFilter(CART_RESPONSE_PROPERTIES_FILTER));
//            }
//            return JSON.toJSONString(ConstantUtils.normalResponse());
        } catch (RuntimeException e) {
            return JSON.toJSONString(ConstantUtils.errorReponse(e.getMessage()));
        }
    }
    
    /**
     * 更新购物车中商品的数量
     * @param
     * @param itemId
     * @param skuId 当前操作的skuid
     * @param itemCount 当前操作的件数
     * @param checkList 选中的skuId列表
     * @return
     */
    @RequestMapping(value = "/sc/changeCartCount.json", method = { RequestMethod.POST })
    public @ResponseBody String changeCartCount(@RequestParam String token, @RequestParam Long itemId, @RequestParam Long skuId,
                                                       @RequestParam Integer itemCount,@RequestParam(value = "checkList") String checkList) {
        Result<BaseUserDTO> userResult = null;
        try {
            userResult = userService.queryBaseUserByToken(token);
        }catch (Exception e){
            log.error("查询用户异常"+e.getMessage());
        }

        if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
            return JSON.toJSONString(ConstantUtils.errorReponse("未登录"));

        }
        BaseUserDTO user = userResult.getModule();
        long uid = user.getUserId();
//        try{
//            //移除优惠上下文
//            discountInfoService.removePromotionChainContext(uid);
//        }catch (Exception e){
//            log.error("优惠清除context异常"+e.getMessage());
//        }

        //1、先更新购物车
        boolean result = shoppingCartManager.updateItemCount(uid, itemId, skuId, itemCount);
        Map<String,Object> resultJson = new HashMap<>();
        if(result){
            //2、从购物车获取选中商品的件数等信息
            //先获取所有
            PageList<ShoppingCart> cartList = shoppingCartManager.getItemsByUserId(uid, 0, 99);
            checkList = StringUtils.isNotEmpty(checkList)?checkList:"[]";
            final List<Long> checkedSkuList  = JSON.parseArray(checkList, Long.class);
            if(CollectionUtils.isEmpty(checkedSkuList) || CollectionUtils.isEmpty(cartList)){
                resultJson.put("freightDesc","再满59元，可免配送费");
                return JSON.toJSONString(ConstantUtils.normalResponse(resultJson));
            }
            //如果是删除操作，将删除的skuId给去掉
            if(skuId!=null&&itemCount==0&&checkedSkuList.contains(skuId)){
                checkedSkuList.remove(skuId);
            }
            //注意去重
            CollectionUtils.filter(cartList,new Predicate(){
                @Override
                public boolean evaluate(Object object){
                    if(checkedSkuList.contains(((ShoppingCart)object).getItemSkuId())){
                        return true;
                    }
                        return false;
                }
            });
            if(CollectionUtils.isEmpty(cartList)){
                resultJson.put("freightDesc","再满59元，可免配送费");
                return JSON.toJSONString(ConstantUtils.normalResponse(resultJson));
            }
            
            
            //取优惠
            DiscountResultDTO disResult = shoppingCartManager.getShoppingCartPromotionInfo(cartList, uid);
            //更新格式到前台可用vo对象
            PageListVO<ShoppingCarItemVO> returnCartList = changeResultDOToVO(cartList);
            //合并优惠
            appendPromotionInfoToShoppingCarInfo(returnCartList,disResult,false);
            DiscountInfoVO discountInfo = returnCartList.getDiscountInfo();
            if(null == discountInfo){//异常情况处理
              setDiscountInfoVOByCar(returnCartList);
              discountInfo = returnCartList.getDiscountInfo();
            }
            returnCartList.setFreightDesc(getFreightInfo(discountInfo.getCurrPrice()));
            returnCartList.setObjects(null);//购物车变更数据时，不用商品优惠
            
            return JSON.toJSONString(ConstantUtils.normalResponse(returnCartList),new SimplePropertyPreFilter(CART_RESPONSE_PROPERTIES_FILTER));
            
//            List<BuySkuInfoDTO> buySkuDetail = new ArrayList<>();
//            if (CollectionUtils.isNotEmpty(cartList)) {
//                for (ShoppingCart cart : cartList) {
////                  ShoppingCarItemVO vo = shoppingCarItem2VO(cart);
//                    BuySkuInfoDTO buySkuInfoDTO = new BuySkuInfoDTO();
//                    buySkuInfoDTO.setSkuId(cart.getItemSkuId());
//                    buySkuInfoDTO.setBuyCount(String.valueOf(cart.getItemCount()));
//                    buySkuDetail.add(buySkuInfoDTO);
//                }
//            }
//
//            if(CollectionUtils.isEmpty(buySkuDetail)){
//                resultJson.put("freightDesc","再满59元，可免配送费");
//                return JSON.toJSONString(ConstantUtils.normalResponse(resultJson));
//            }
//            //3、计算优惠信息
//            //获取优惠信息
//            DiscountQueryDTO query = new DiscountQueryDTO();
//            query.setUserId(uid);
//            query.setBuySkuDetail(buySkuDetail);
//            Result<Map<String,DiscountCalResultDTO>>  discountResult = null;
//            try{
//                discountResult = discountInfoService.cal(query);
//            }catch (Exception e){
//                log.error("优惠计算异常"+e.getMessage());
//            }
//            if(null == discountResult||!discountResult.getSuccess()||null==discountResult.getModule()){
//                resultJson.put("freightDesc","再满59元，可免配送费");
//                return JSON.toJSONString(ConstantUtils.normalResponse(resultJson));
//            }
//            final Map<String,DiscountCalResultDTO> discountCalResultDTOMap =   discountResult.getModule();
//            DiscountCalResultDTO fullNUseM = discountCalResultDTOMap.get(DiscountTypeEnum.FULL_N_USE_M.getCode()+DiscountTypeEnum.FULL_N_USE_M.getValue()+"_"+skuId);
//            DiscountCalResultDTO fullReduce = discountCalResultDTOMap.get(DiscountTypeEnum.FULL_REDUCE.getCode()+DiscountTypeEnum.FULL_REDUCE.getValue());
//            //4、返回数据
//                List<Map<String,Object>> discountInfoVOs = new ArrayList<>();
//                if(fullNUseM!=null) {
//                    Map<String,Object> discMap=new HashMap<>();
//                    discMap.put("skuId", fullNUseM.getSkuId());
//                    discMap.put("itemId", fullNUseM.getItemId());
//                    discMap.put("itemCount", fullNUseM.getItemCount());
//                    DiscountInfoVO discountInfoVO = handleDisountInfo(fullNUseM);
//                    discMap.put("discountInfo",discountInfoVO);
//                    discountInfoVOs.add(discMap);
//                    resultJson.put("objects",discountInfoVOs);
//                }
//
//                DiscountInfoVO discountInfoVOSum = handleDisountInfo(fullReduce);
//                resultJson.put("discountInfo",discountInfoVOSum);
//
//                String freight = getFreight(discountInfoVOSum.getCurrPrice());
//            if(StringUtils.isEmpty(freight))
//                resultJson.put("freightDesc","您已享受，满59元，免配送费");
//            else
//            {
//                BigDecimal freghtDesc = new BigDecimal("59").subtract(new BigDecimal(discountInfoVOSum.getCurrPrice()));
//                resultJson.put("freightDesc","再满"+freghtDesc.toString()+"元，可免配送费");
//            }
        }
        return (result) ? JSON.toJSONString(ConstantUtils.normalResponse(resultJson)) : JSON.toJSONString(ConstantUtils.errorReponse("更新商品数量失败"));
    }


    /**
     * 更新购物车中商品的数量
     *
     * @param
     *
     * @param skuId
     *            SKU ID
     * @return ""
     * @see ResponseModel
     */
    @RequestMapping(value = "/sc/select.json", method = { RequestMethod.POST })
    public @ResponseBody String select(@RequestParam String token,  @RequestParam(value = "skuId",required = false) Long skuId,@RequestParam(value = "checkList") String checkList) {
        Result<BaseUserDTO> userResult = null;
        try {
            userResult = userService.queryBaseUserByToken(token);
        }catch (Exception e){
            log.error("查询用户异常"+e.getMessage());
        }

        if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
            return  JSON.toJSONString(ConstantUtils.errorReponse("未登录"));

        }
        BaseUserDTO user = userResult.getModule();
        long uid = user.getUserId();
        //移除优惠上下文
//        try {
//            discountInfoService.removePromotionChainContext(uid);
//        }catch (Exception e){
//            log.error("优惠清除context异常"+e.getMessage());
//        }

        Map<String,Object> resultJson = new HashMap<>();
        //2、从购物车获取选中商品的件数等信息
        //先获取所有
        checkList = StringUtils.isNotEmpty(checkList)?checkList:"[]";
        final List<Long> checkedSkuList  = JSON.parseArray(checkList, Long.class);
        if(CollectionUtils.isEmpty(checkedSkuList)){
            resultJson.put("freightDesc","再满59元，可免配送费");
            return JSON.toJSONString(ConstantUtils.normalResponse(resultJson));
        }
        PageList<ShoppingCart> cartList = shoppingCartManager.getItemsByUserId(uid, 0, 99);
        CollectionUtils.filter(cartList,new Predicate(){
            @Override
            public boolean evaluate(Object object){
                if(checkedSkuList.contains(((ShoppingCart)object).getItemSkuId())){
                    return true;
                }
                return false;
            }
        });
        if(CollectionUtils.isEmpty(cartList)){
            resultJson.put("freightDesc","再满59元，可免配送费");
            return JSON.toJSONString(ConstantUtils.normalResponse(resultJson));
        }
        
      //取优惠
        DiscountResultDTO disResult = shoppingCartManager.getShoppingCartPromotionInfo(cartList, uid);
        //更新格式到前台可用vo对象
        PageListVO<ShoppingCarItemVO> returnCartList = changeResultDOToVO(cartList);
        //合并优惠
        appendPromotionInfoToShoppingCarInfo(returnCartList,disResult,false);
        DiscountInfoVO discountInfo = returnCartList.getDiscountInfo();
        if(null == discountInfo){//异常情况处理
          setDiscountInfoVOByCar(returnCartList);
          discountInfo = returnCartList.getDiscountInfo();
        }
        returnCartList.setFreightDesc(getFreightInfo(discountInfo.getCurrPrice()));
        returnCartList.setObjects(null);//购物车变更数据时，不用商品优惠
        
        return JSON.toJSONString(ConstantUtils.normalResponse(returnCartList),new SimplePropertyPreFilter(CART_RESPONSE_PROPERTIES_FILTER));
            
            
//            List<BuySkuInfoDTO> buySkuDetail = new ArrayList<>();
//            if (CollectionUtils.isNotEmpty(cartList)) {
//                for (ShoppingCart cart : cartList) {
////                  ShoppingCarItemVO vo = shoppingCarItem2VO(cart);
//                    BuySkuInfoDTO buySkuInfoDTO = new BuySkuInfoDTO();
//                    buySkuInfoDTO.setSkuId(cart.getItemSkuId());
//                    buySkuInfoDTO.setBuyCount(String.valueOf(cart.getItemCount()));
//                    buySkuDetail.add(buySkuInfoDTO);
//                }
//            }
//
//            if(CollectionUtils.isEmpty(buySkuDetail)){
//                resultJson.put("freightDesc","再满59元，可免配送费");
//                return JSON.toJSONString(ConstantUtils.normalResponse(resultJson));
//            }
//            //3、计算优惠信息
//            //获取优惠信息
//            DiscountQueryDTO query = new DiscountQueryDTO();
//            query.setUserId(uid);
//            query.setBuySkuDetail(buySkuDetail);
//            Result<Map<String,DiscountCalResultDTO>>  discountResult =  null;
//            try{
//                discountResult = discountInfoService.cal(query);
//            }catch (Exception e){
//                log.error("优惠计算异常"+e.getMessage());
//            }
//            if(null == discountResult||!discountResult.getSuccess()||null==discountResult.getModule()){
//                resultJson.put("freightDesc","再满59元，可免配送费");
//                return JSON.toJSONString(ConstantUtils.normalResponse(resultJson));
//            }
//
//            final Map<String,DiscountCalResultDTO> discountCalResultDTOMap =   discountResult.getModule();
//            DiscountCalResultDTO fullReduce = discountCalResultDTOMap.get(DiscountTypeEnum.FULL_REDUCE.getCode()+DiscountTypeEnum.FULL_REDUCE.getValue());
//            //4、返回数据
//            List<Map<String,Object>> discountInfoVOs = new ArrayList<>();
//
//            DiscountInfoVO discountInfoVOSum = handleDisountInfo(fullReduce);
//            resultJson.put("discountInfo",discountInfoVOSum);
//
//        String freight = getFreight(discountInfoVOSum.getCurrPrice());
//        if(StringUtils.isEmpty(freight))
//            resultJson.put("freightDesc","您已享受，满59元，免配送费");
//        else
//        {
//            BigDecimal freghtDesc = new BigDecimal("59").subtract(new BigDecimal(discountInfoVOSum.getCurrPrice()));
//            resultJson.put("freightDesc","再满"+freghtDesc.toString()+"元，可免配送费");
//        }

//        return JSON.toJSONString(ConstantUtils.normalResponse(resultJson));
    }

    /**
     * 预算接口，只是展示信息
     * @param
     * @param skuIds
     * @param servletRequest
     * @return
     */
    @RequestMapping(value = "/sc/settle.json", method = { RequestMethod.POST })
    @ResponseBody
    public String settle(@RequestParam String token,
                         @RequestParam(value = "skuIds", required = false) Long[] skuIds,
                         HttpServletRequest servletRequest){
        Result<BaseUserDTO> userResult = null;
        try {
            userResult = userService.queryBaseUserByToken(token);
        }catch (Exception e){
            log.error("查询用户异常"+e.getMessage());
        }

        if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
            return  JSON.toJSONString(ConstantUtils.errorReponse("未登录"));
        }
        BaseUserDTO user = userResult.getModule();
        long uid = user.getUserId();
//        try {
//            discountInfoService.removePromotionChainContext(uid);
//        }catch (Exception e){
//            log.error("优惠清除context异常"+e.getMessage());
//        }
        
        PageList<ShoppingCart> userCartList = shoppingCartManager.getItemsByUserId(uid, 0, 99);
        List<Long> skuIdList = new ArrayList<>();
        Map<String,ShoppingCart> voMap = new HashMap<>();
        //1、判断skuId是否在购物车中，若果存在，获取其购物车中的信息
        if (CollectionUtils.isNotEmpty(userCartList)) {
            for (ShoppingCart cart : userCartList) {
                voMap.put(String.valueOf(cart.getItemSkuId()),cart);
                skuIdList.add(cart.getItemSkuId());
            }
        }

        Pagination pagin = Pagination.getPagination(0, skuIds.length, skuIds.length);
        PageList<ShoppingCart> cartList = new PageList<ShoppingCart>();
        cartList.setPagination(pagin);
        if(skuIds!=null&&CollectionUtils.isNotEmpty(skuIdList)){
            for(Long skuId:skuIds){
                if(!skuIdList.contains(skuId)){
                    return JSON.toJSONString(ConstantUtils.errorReponse("数据异常"));
                }else{
                  cartList.add(voMap.get(skuId.toString()));
                }
            }
        }
        //2、判断库存信息，如库存不足，100-200之间，true,设置message信息
        List<Long> cartSkuIds = Arrays.asList(skuIds);
        SimpleResultVO<List<InventoryVO>> storeResult = null;
        try {
            storeResult =  iInventory2CDomainClient.batchGetInventoryByGovAndSKU(1L, cartSkuIds);
        }catch (Exception e){
            log.error("获取库存信息异常"+e.getMessage());
        }
        if(storeResult!=null&&storeResult.isSuccess()){
            List<InventoryVO> inventoryVOs =  storeResult.getTarget();
            for(InventoryVO inventoryVO:inventoryVOs){
                ShoppingCart shoppingCart = voMap.get(String.valueOf(inventoryVO.getSkuId()));
                if(shoppingCart!=null&&isInventoryEnough(false,inventoryVO,shoppingCart)){
                    continue;
                }else{
                    return JSON.toJSONString(ConstantUtils.normalResponse(101,shoppingCart.getItemName()+",库存不足",""));
                }
            }
        }

       //取优惠
        DiscountResultDTO disResult = shoppingCartManager.getShoppingCartPromotionInfo(cartList, uid);
        //更新格式到前台可用vo对象
        PageListVO<ShoppingCarItemVO> returnCartList = changeResultDOToVO(cartList);
        //合并优惠
        appendPromotionInfoToShoppingCarInfo(returnCartList,disResult,false);
        DiscountInfoVO discountInfoVo = returnCartList.getDiscountInfo();
        if(null == discountInfoVo){//异常情况处理
          setDiscountInfoVOByCar(returnCartList);
          discountInfoVo = returnCartList.getDiscountInfo();
        }
        returnCartList.setFreightDesc(getFreightInfo(discountInfoVo.getCurrPrice()));
        
        List<DiscountInfoVO> discountInfos = new ArrayList<>();
        discountInfos.add(discountInfoVo);
        String freight = "6.00";
        if(!isNeedDistributionPrice(discountInfoVo.getCurrPrice())){
          freight = "0.00";
        }
        Map<String,Object> result = new HashMap<>();
        result.put("totalPrice",getPayPrice(discountInfoVo.getCurrPrice()));
        result.put("freight",freight);
        result.put("integral",-1);
        result.put("currPrice",discountInfoVo.getCurrPrice());
        result.put("discountPrice",discountInfoVo.getDiscountPrice());
        result.put("originPrice",discountInfoVo.getOriginPrice());
        if(!"0".equals(discountInfoVo.getTotalDiscountPrice())){
          result.put("discountInfos",discountInfos);
        }else{
          result.put("discountInfos",Lists.newArrayList());
        }
        result.put("objects",returnCartList.getObjects());
        if(CollectionUtils.isNotEmpty(disResult.getUserCouponList())){
          result.put("couponList",disResult.getUserCouponList());
        }
        return JSON.toJSONString(ConstantUtils.normalResponse(result));
        
        
//        //3、计算商品和优惠信息，以及运费信息
//        DiscountQueryDTO query = new DiscountQueryDTO();
//        query.setUserId(uid);
//        query.setBuySkuDetail(buySkuDetail);
//        Result<Map<String,DiscountCalResultDTO>>  discountResult =  null;
//        try{
//            discountResult = discountInfoService.cal(query);
//        }catch (Exception e){
//            log.error("优惠计算异常"+e.getMessage());
//        }
//        final Map<String,DiscountCalResultDTO> discountCalResultDTOMap =   discountResult.getModule();
//        List<DiscountInfoVO> discountInfos = new ArrayList<>();
//        for(BuySkuInfoDTO buySkuInfoDTO:buySkuDetail){
//            DiscountCalResultDTO fullNUseM = discountCalResultDTOMap.get(DiscountTypeEnum.FULL_N_USE_M.getCode()+DiscountTypeEnum.FULL_N_USE_M.getValue()+"_"+buySkuInfoDTO.getSkuId());
//            if(fullNUseM==null)continue;
//            DiscountInfoVO discountInfoVo = handleDisountInfo(fullNUseM);
//            if(discountInfoVo.isGetDiscount())
//            discountInfos.add(discountInfoVo);
//        }
//        DiscountCalResultDTO fullReduce = discountCalResultDTOMap.get(DiscountTypeEnum.FULL_REDUCE.getCode()+DiscountTypeEnum.FULL_REDUCE.getValue());
//        DiscountInfoVO discountInfoVo = handleDisountInfo(fullReduce);
//        if(discountInfoVo.isGetDiscount())
//        discountInfos.add(discountInfoVo);
        //
        
    }

    @Deprecated
    private String getFreight(String currPrice){
        String freight = "";
        if (new BigDecimal(currPrice).compareTo(new BigDecimal("59.00"))<0 ) {
            freight = "6.00";
        }
        return freight;
    }
    
    private String getFreightInfo(String currPrice){
      if(isNeedDistributionPrice(currPrice)){
        int price = new BigDecimal(currPrice).multiply(new BigDecimal(100)).intValue();
        int tmp = 5900 - price;
        String priceInfo = new BigDecimal(tmp).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString();
        return "再满"+priceInfo.toString()+"元，可免配送费";
      }else{
        return "您已享受，满59元，免配送费";
      }
    }
    
    private String getPayPrice(String currPrice){
      if(isNeedDistributionPrice(currPrice)){
        int price = new BigDecimal(currPrice).multiply(new BigDecimal(100)).intValue();
        int tmp = 600 + price;
        return new BigDecimal(tmp).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString();
      }else{
        return currPrice;
      }
    }
    
    private boolean isNeedDistributionPrice(String currPrice){
      return new BigDecimal(currPrice).compareTo(new BigDecimal("59.00"))<0;
    }

    /**
     * 判断库存是否足够
     * @return
     */
    private boolean isInventoryEnough(boolean offline, InventoryVO inventoryResult,ShoppingCart cart){
        if(offline){//线下直接通过
            return true;
        }
        if(null == cart || null == inventoryResult ){
            return false;
        }
//      if(cart.getSkuType() == 2){
//          if(!inventoryResult.getMaySale()){//不允许销售
//              return false;
//          }
//          int saleInventoryNum = inventoryResult.getNumber();
//          int cmpNum = saleInventoryNum*1000;
//          int userCatNum = new BigDecimal(cart.getItemCount()).multiply(new BigDecimal(cart.getUnitWeightNum())).intValue();
//          if(cmpNum >=userCatNum ){
//              return true;
//          }else{
//              return false;
//          }
//      }else{//历史商品或者非称重商品，暂时都直接比较库存数量
//          if(inventoryResult.getMaySale() && inventoryResult.getNumber() > 0 && inventoryResult.getNumber() >= cart.getItemCount()){
//              return true;//库存数量足够
//          }else{
//              return false;//库存不足
//          }
//      }
        return true;
    }

    /**
     * 更新购物车底层返回数据格式到VO
     * @param cartListResult
     * @return
     */
    private PageListVO<ShoppingCarItemVO> changeResultDOToVO(PageList<ShoppingCart> cartList){
      if(CollectionUtils.isEmpty(cartList)){
        return null;
      }
      
      List<ShoppingCarItemVO> voList = new ArrayList<>();
      for (ShoppingCart cart : cartList) {
        ShoppingCarItemVO vo = shoppingCarItem2VO(cart);
        try {
          SimpleResultVO<InventoryVO> storeResult = iInventory2CDomainClient.getInventoryByGovAndSKU(1L, cart.getItemSkuId());
          vo.setNumber(Integer.toString(storeResult != null && storeResult.isSuccess() ? storeResult.getTarget().getNumber() : 0));
          } catch (Exception e) {
          }
          voList.add(vo);
      }
      Pagination pagin = cartList.getPagination();
      PageListVO<ShoppingCarItemVO> pageList = new PageListVO<ShoppingCarItemVO>(pagin, voList);
      return pageList;
    }

    /**
     * 给原始的购物车对象追加优惠信息
     * @param cartVOList
     * @param discountResult
     * @param appendItemPromotionDetail 是否追加每一个商品的优惠详细
     */
  private void appendPromotionInfoToShoppingCarInfo(PageListVO<ShoppingCarItemVO> cartVOList,
      DiscountResultDTO discountResult, boolean appendItemPromotionDetail) {
    if (null == cartVOList || null == discountResult
        || CollectionUtils.isEmpty(cartVOList.getObjects())) {
      return;
    }

    Integer totalPrice = discountResult.getOrinalPrice();// 总价原价
    if (null == totalPrice || totalPrice <= 0) {
      return;
    }
    Integer dis = discountResult.getDiscountPrice();// 总价优惠后付款金额
    if (null == dis || dis < 0) {
      dis = totalPrice;
    }
    Integer current = dis;
    String originPrice =
        new BigDecimal(totalPrice).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString();
    String discountPrice =
        new BigDecimal(totalPrice - dis).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN)
            .toString();
    String totalDiscountPrice = discountPrice;
    String currPrice =
        new BigDecimal(current).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString();
    boolean getDiscount = false;
    if (totalPrice - dis > 0) {
      getDiscount = true;
    }

    DiscountInfoVO disInfo = new DiscountInfoVO();
    disInfo.setCurrPrice(currPrice);
    String discontDes = getDiscountDes(discountResult.getTotalDiscount());
    if(StringUtils.isEmpty(discontDes) && CollectionUtils.isNotEmpty(discountResult.getUserCouponList())){
      UserCouponDTO coupon = discountResult.getUserCouponList().get(0);
      discontDes = "可享受优惠 "+coupon.getCouponTitle();
    }
    disInfo.setDiscountDesc(discontDes);
    disInfo.setDiscountPrice(totalDiscountPrice);
    disInfo.setGetDiscount(getDiscount);
    disInfo.setOriginPrice(originPrice);
    disInfo.setTotalDiscountPrice(totalDiscountPrice);
    cartVOList.setDiscountInfo(disInfo);// 总体优惠信息

    if(!appendItemPromotionDetail){//是否追加商品的详细优惠信息
      return;
    }
    
    Map<Long, List<DiscountDetailDTO>> discountDetail = discountResult.getDiscountDetailMap();
    if (null != discountDetail && CollectionUtils.isNotEmpty(discountDetail.keySet())) {
      for (ShoppingCarItemVO item : cartVOList.getObjects()) {
        Long skuId = item.getSkuId();
        List<DiscountDetailDTO> detailList = discountDetail.get(skuId);
        if (CollectionUtils.isNotEmpty(detailList)) {
          int disTotal = 0;
          for (DiscountDetailDTO disDetail : detailList) {
            if (disDetail.getIsGetDiscount()) {
              disTotal = disTotal + (disDetail.getOriginPrice() - disDetail.getDiscountPrice());
            }
          }
          DiscountInfoVO disDetailInfo = new DiscountInfoVO();
          disDetailInfo.setDiscountDesc(getDiscountDes(detailList));
          disDetailInfo.setDiscountDesc(new BigDecimal(disTotal).divide(new BigDecimal(100), 2,
              BigDecimal.ROUND_DOWN).toString());
          item.setDiscountInfo(disDetailInfo);
        }
      }
    }
  }
  
  /**
   * 当调用优惠失败时，通过购物车自己计算一次价格，只计算总价
   * @param cartVOList
   * @return
   */
  private void setDiscountInfoVOByCar(PageListVO<ShoppingCarItemVO> cartVOList){
    int total = 0;
    for (ShoppingCarItemVO item : cartVOList.getObjects()) {
      String price = item.getEffeAmount();
      int priceFen = new BigDecimal(price).multiply(new BigDecimal(100)).intValue();
      total = total + priceFen;
    }
    String originPrice =
        new BigDecimal(total).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString();
    DiscountInfoVO disInfo = new DiscountInfoVO();
    disInfo.setCurrPrice(originPrice);
    disInfo.setDiscountDesc("");
    disInfo.setDiscountPrice("0");
    disInfo.setGetDiscount(false);
    disInfo.setOriginPrice(originPrice);
    disInfo.setTotalDiscountPrice("0");
    cartVOList.setDiscountInfo(disInfo);
  }
    
    /**
     * 获取优惠描述
     * @param discountList
     * @return
     */
    private String getDiscountDes(List<DiscountDetailDTO> discountList){
      StringBuilder discountTotalDes= new StringBuilder();//优惠总描述，包括已享优惠和即将享受优惠
      if(CollectionUtils.isEmpty(discountList)){
        return "";
      }
      for(DiscountDetailDTO disDetail:discountList){
        int disPrice = disDetail.getOriginPrice()-disDetail.getDiscountPrice();
        if(disPrice <= 0){
          continue;
        }
        String Price = new BigDecimal(disPrice).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString();
        discountTotalDes.append(disDetail.getDiscountName());
        discountTotalDes.append("已优惠");
        discountTotalDes.append(Price);
        discountTotalDes.append("元 ");
      }
      return discountTotalDes.toString();
    }
    

//    private DiscountInfoVO handleDisountInfo(DiscountCalResultDTO fullNUseM){
//        if(fullNUseM==null) return null;
//        BigDecimal baseNum = new BigDecimal("100");
//        BigDecimal originPrice = new BigDecimal(String.valueOf(fullNUseM.getOriginPrice())).divide(baseNum).setScale(2,BigDecimal.ROUND_DOWN);
//        BigDecimal discountPrice = new BigDecimal(String.valueOf(fullNUseM.getDiscountPrice())).divide(baseNum);
//        BigDecimal currPrice = new BigDecimal(String.valueOf(fullNUseM.getCurrPrice())).divide(baseNum).setScale(2,BigDecimal.ROUND_DOWN);
//        BigDecimal totalDiscountPrice = new BigDecimal(String.valueOf(fullNUseM.getTotalDiscountPrice())).divide(baseNum).setScale(2,BigDecimal.ROUND_DOWN);
//
//
//        DiscountInfoVO discountInfoVo = new DiscountInfoVO();
//        StringBuilder desc = new StringBuilder();
//        DiscountInfoDTO discountInfoDTO = fullNUseM.getDiscountInfoDTO();
//        if(discountInfoDTO!=null) {
//            desc.append(discountInfoDTO.getDiscountDetailName());
//            desc.append(",");
//        if(fullNUseM.isGetDiscount()){//如果享受了优惠
//            //优惠显示享受多少优惠
//            if(fullNUseM.getSingleDiscountPrice()!=0){
//                BigDecimal singleDiscountPrice = new BigDecimal(String.valueOf(fullNUseM.getSingleDiscountPrice())).divide(baseNum).setScale(2,BigDecimal.ROUND_DOWN);
//                desc.append("已优惠￥");
//                desc.append(singleDiscountPrice.toString());
//            }
//        }else{//没有享受优惠
//            if(discountInfoDTO.getType()==DiscountTypeEnum.FULL_N_USE_M.getValue()){
//                desc.append("还差");
//                desc.append(discountPrice.toString());
//                desc.append("件即享");
//            }else{
//                desc.append("还差￥");
//                desc.append(discountPrice.toString());
//                desc.append("即享");
//            }
//        }
//            discountInfoVo.setDiscountDesc(desc.toString());
//        }
//        discountInfoVo.setGetDiscount(fullNUseM.isGetDiscount());
//
//            discountInfoVo.setTotalDiscountPrice(totalDiscountPrice.toString());
//            discountInfoVo.setOriginPrice(originPrice.toString());
//            discountInfoVo.setDiscountPrice(discountPrice.toString());
//            discountInfoVo.setCurrPrice(currPrice.toString());
//        return discountInfoVo;
//    }

//    private DiscountInfoVO handleDisountToSpecialPrice(DiscountCalResultDTO fullNUseM){
//      if(fullNUseM==null) {
//        return null;
//      }
//      if(!fullNUseM.isGetDiscount()){
//        return null;
//      }
//      BigDecimal baseNum = new BigDecimal("100");
//      BigDecimal originPrice = new BigDecimal(String.valueOf(fullNUseM.getOriginPrice())).divide(baseNum).setScale(2,BigDecimal.ROUND_DOWN);
//      BigDecimal discountPrice = new BigDecimal(String.valueOf(fullNUseM.getDiscountPrice())).divide(baseNum).setScale(2,BigDecimal.ROUND_DOWN);
//      BigDecimal currPrice = new BigDecimal(String.valueOf(fullNUseM.getCurrPrice())).divide(baseNum).setScale(2,BigDecimal.ROUND_DOWN);
//
//      DiscountInfoVO discountInfoVo = new DiscountInfoVO();
//      StringBuilder desc = new StringBuilder();
//      DiscountInfoDTO discountInfoDTO = fullNUseM.getDiscountInfoDTO();
//      if(discountInfoDTO!=null) {
//          desc.append(discountInfoDTO.getDiscountDetailName());
//      }
//      if(fullNUseM.isGetDiscount()){//如果享受了优惠
//          //优惠显示享受多少优惠
//          if(fullNUseM.getSingleDiscountPrice()!=0){
//              BigDecimal singleDiscountPrice = discountPrice;
//              desc.append("已优惠￥");
//              desc.append(singleDiscountPrice.toString());
//          }
//      }
//      discountInfoVo.setDiscountDesc(desc.toString());
//      discountInfoVo.setGetDiscount(fullNUseM.isGetDiscount());
//
//      discountInfoVo.setOriginPrice(originPrice.toString());
//      discountInfoVo.setDiscountPrice(discountPrice.toString());
//      discountInfoVo.setCurrPrice(currPrice.toString());
////    }
//      return discountInfoVo;
//  }

    private final ShoppingCarItemVO shoppingCarItem2VO(ShoppingCart cart) {
        ShoppingCarItemVO vo = new ShoppingCarItemVO();
        vo.setId(cart.getId());
        vo.setItemCount(String.format("%.2f", cart.getItemCount()));
        vo.setItemIconUrl(cart.getItemIconUrl());
        vo.setItemName(cart.getItemName());
        vo.setItemPayPrice(String.format("%.2f", cart.getItemBillPrice()));
        vo.setSkuInfo(JsonConvert.json2Object(cart.getItemSkuInfo(), SkuInfoVO.class));
        vo.setItemEffePrice(String.format("%.2f", cart.getItemEffePrice()));
        vo.setEffeAmount(String.format("%.2f", cart.getEffeAmount()));
        vo.setPayAmount(String.format("%.2f", cart.getBillAmount()));
        vo.setItemEffePrice(String.format("%.2f", cart.getItemEffePrice()));
        vo.setItemId(cart.getItemId());
        vo.setSkuId(cart.getItemSkuId());
        vo.setItemTags(cart.getItemTags());
        return vo;
    }

//  public static void main(String[] args) {
//      List<Long> ll =  JSON.parseArray("[]", Long.class);
//      for()
//      System.out.println(ll+"------");
//  }

}