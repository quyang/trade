package com.xianzaishi.trade.core.coupon.impl.promotionchain;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.trade.core.coupon.impl.promotionchain.unit.ProcessorUnit;

public class PromotionProcessor {

  protected static final Logger log = LoggerFactory.getLogger(PromotionProcessor.class);

  /**
   * 通过优惠券降价列表
   */
  private List<ProcessorUnit> processCalculateDiscountPrice;

  /**
   * 选择用户匹配优惠券列表
   */
  private List<ProcessorUnit> processMathFirstCoupon;

  /**
   * 选择用户匹配优惠券列表
   */
  private List<ProcessorUnit> processMathAllCoupon;
  
  /**
   * 计算打折价格
   * @param context
   * @return
   */
  public PromotionChainContext calculateDiscountPriceWithCoupon(PromotionChainContext context) {
    return getResult(context, processCalculateDiscountPrice);
  }

  /**
   * 查询符合条件第一个优惠券
   * @param context
   * @return
   */
  public PromotionChainContext selectMathFirstCoupon(PromotionChainContext context) {
    return getResult(context, processMathFirstCoupon);
  }
  
  /**
   * 查询符合条件所有优惠券
   * @param context
   * @return
   */
  public PromotionChainContext selectMathAllCoupon(PromotionChainContext context) {
    return getResult(context, processMathAllCoupon);
  }
  
  private PromotionChainContext getResult(PromotionChainContext context, List<ProcessorUnit> process){
    if (null == context) {
      log.error("[PromotionProcessorUnit]Input parameter context is empty,return null");
      return null;
    }
    for (ProcessorUnit processUnit : process) {
      Result<Boolean> processResult = processUnit.process(context);
      if (null == processResult || !processResult.getSuccess()) {
        return null;
      }
    }
    return context;
  }

  public List<ProcessorUnit> getProcessCalculateDiscountPrice() {
    return processCalculateDiscountPrice;
  }

  public void setProcessCalculateDiscountPrice(List<ProcessorUnit> processCalculateDiscountPrice) {
    this.processCalculateDiscountPrice = processCalculateDiscountPrice;
  }

  public List<ProcessorUnit> getProcessMathFirstCoupon() {
    return processMathFirstCoupon;
  }

  public void setProcessMathFirstCoupon(List<ProcessorUnit> processMathFirstCoupon) {
    this.processMathFirstCoupon = processMathFirstCoupon;
  }

  public List<ProcessorUnit> getProcessMathAllCoupon() {
    return processMathAllCoupon;
  }

  public void setProcessMathAllCoupon(List<ProcessorUnit> processMathAllCoupon) {
    this.processMathAllCoupon = processMathAllCoupon;
  }

}
