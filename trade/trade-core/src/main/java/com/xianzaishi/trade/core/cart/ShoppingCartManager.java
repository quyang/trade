package com.xianzaishi.trade.core.cart;

import java.util.Date;
import java.util.List;

import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.trade.dal.model.ShoppingCart;
import com.xianzaishi.trade.utils.PageList;

public interface ShoppingCartManager {
	/**
	 * 根据ID删除购物车记录
	 * @param shoppingCartId
	 * @return
	 */
	boolean deleteById(long shoppingCartId, long userId);

	/**
	 * 根据用户ID，商品ID，sku ID删除购物车记录
	 * @param userId
	 * @param itemId
	 * @param skuId
	 * @return
	 */
	boolean delete(long userId, long itemId, long skuId);

	/**
	 * 获取一个用户的购物车单条记录
	 * @param userId
	 * @param itemId
	 * @param skuId
	 * @return
	 */
	ShoppingCart get(long userId, long itemId, long skuId);

	/**
	 * 添加购物车记录
	 * @param userId
	 * @param itemId
	 * @param skuId
	 * @param itemCount
	 * @param deviceId
	 * @param operator
	 * @return
	 */
	long insert(long userId, long itemId, long skuId, double itemCount, int deviceId, int operator);

	/**
	 * 添加购物车记录
	 * @param userId
	 * @param itemId
	 * @param skuId
	 * @param itemCount
	 * @return
	 */
	long insert(long userId, long itemId, long skuId, double itemCount);
	
	boolean subtract(long userId, long itemId, long skuId, double itemCount);

	/**
	 * 清空用户购物车
	 * @param userId
	 * @return
	 */
	boolean clean(long userId);

	/**
	 * 获取购物车所有的条目数量
	 * @param userId
	 * @return
	 */
	int getAllItemCount(long userId);

	/**
	 * 获取购物车的商品列表
	 * @param userId
	 * @return
	 */
	List<ShoppingCart> getAllItem(long userId);

	PageList<ShoppingCart> getItemsByUserId(long userId, int pageNo, int pageSize);

	/**
	 * 购物车创建订单
	 * @param userId
	 * @param shopId
	 * @param sciIds
	 * @param couponId
	 * @param addressId
	 * @param distribution
	 * @param credit
	 * @param description
	 * @return
	 */
	long toOrder(long userId, int shopId, Long[] sciIds, long couponId, long addressId, Date distribution,int credit,String description);

	boolean updateItemCount(long userId, long itemId, long skuId, int itemCount);

	int getSumItemCount(long userId);
	
	/**
	 * 购物车接口追加优惠信息
	 * @param pageList
	 * @param userId
	 */
	DiscountResultDTO getShoppingCartPromotionInfo(PageList<ShoppingCart> pageList,Long userId);
}
