package com.xianzaishi.trade.core.coupon;

import java.util.List;

import com.xianzaishi.trade.dal.model.Coupon;
import com.xianzaishi.trade.dal.model.UserCoupon;
import com.xianzaishi.trade.utils.enums.Status;

public interface UserCouponManager {

    /**
     * 无调用
     * @param userId
     * @param couponId
     * @return
     */
	long insert(long userId, long couponId);
	
	/**
	 * 只有客服平台还在使用
	 * @param userId
	 * @param coupon
	 * @return
	 */
	long insertCoupon(long userId, Coupon coupon);

	/**
     * 无调用
     * @param userId
     * @param couponId
     * @return
     */
	List<UserCoupon> getUserCoupons(long userId);

	/**
     * 无调用
     * @param userId
     * @param couponId
     * @return
     */
	List<UserCoupon> getUserCouponsByCouponType(long userId, List<Short> couponTypes);

	/**
     * 无调用
     * @param userCouponId
     * @param status
     * @return
     */
    boolean updateStatus(long userCouponId, Status status);

    /**
     * 无调用
     * @param userCouponId
     * @param amount
     * @return
     */
    double calculate(long userCouponId, double amount);

    /**
     * 无调用
     * @param userCouponId
     * @param amount
     * @return
     */
    double useUserCoupon(long userId, long userCouponId, long orderId, double amount);
    
    /**
     * 无调用
     * @param userCouponId
     * @param amount
     * @return
     */
    public boolean updateUserCouponOrderId(long userId, long userCouponId, long orderId);
    
	List<UserCoupon> getUserCouponByOrderId(long userId, long oid);

	List<UserCoupon> getUserCouponsByStatus(long userId, List<Short> couponTypes, Status status);

	double calculate(UserCoupon userCoupon, double amount);

	UserCoupon getUserCouponById(long couponId);

}