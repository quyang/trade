#!/bin/bash

echo -------------------------------------------------------------------------
echo Sofa Build/Config/Deploy Script for Linux
echo -------------------------------------------------------------------------

# build
if [ "$1" == "build" ]; then
    mvn clean install -Dmaven.test.skip=true -Denv=$ENV
elif [ "$1" == "build" ]; then
    mvn clean -o install -Dmaven.test.skip=true
fi

# check maven run
if [ "$?" == 1 ]; then
    echo "Error: Maven Running Not Success!";
    echo "Please Check your program!"; 
    exit 1;
fi

