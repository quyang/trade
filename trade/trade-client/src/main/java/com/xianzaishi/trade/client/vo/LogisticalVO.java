package com.xianzaishi.trade.client.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * 物流信息实例
 * @author Croky.Zheng
 * 2016年8月3日
 */
public class LogisticalVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1259030452909380128L;

	/**
	 * 描述
	 */
	private String description;
	
	/**
	 * 第N步
	 */
	private byte step;
	
	/**
	 * 操作时间
	 */
	private Date gmtOperation;
	
	/**
	 * 操作者
	 */
	private String operatorId;
	
	/**
	 * 操作者信息
	 */
	private String operatorInfo;
	
	public LogisticalVO(){}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte getStep() {
		return step;
	}

	public void setStep(byte step) {
		this.step = step;
	}

	public Date getGmtOperation() {
		return gmtOperation;
	}

	public void setGmtOperation(Date gmtOperation) {
		this.gmtOperation = gmtOperation;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorInfo() {
		return operatorInfo;
	}

	public void setOperatorInfo(String operatorInfo) {
		this.operatorInfo = operatorInfo;
	}
}
