package com.xianzaishi.trade.core.order;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.xianzaishi.trade.client.vo.DiscountInfoVO;
import com.xianzaishi.trade.dal.model.OrderPayLog;
import com.xianzaishi.trade.dal.model.ShoppingCart;
import com.xianzaishi.trade.dal.model.extend.OrderInfo;
import com.xianzaishi.trade.pay.weixin.GongZongHaoPayModel;
import com.xianzaishi.trade.pay.weixin.MobilePayModel;
import com.xianzaishi.trade.utils.PageList;
import com.xianzaishi.trade.utils.enums.OrderStatus;
import com.xianzaishi.trade.utils.enums.PayWay;

public interface OrdersManager {

	/**
	 * 1元送礼券活动使用
	 * @param userId
	 * @param openId
	 * @return
	 */
	GongZongHaoPayModel getWeixinGongZongHaoPaySignature(long userId, String openId);

	int getOrderCountByUserId(long uid, OrderStatus[] status);

	PageList<OrderInfo> getOrdersByUserId(long uid, OrderStatus[] status, int pageNo, int pageSize, boolean isOrderByDesc);

	/**
	 * 统计订单数量
	 * @param begin
	 * @param end
	 * @param status
	 * @return
	 */
	int getOrderCountByTime(Date begin, Date end, OrderStatus[] status);

	/**
	 * 取出时间区间内的订单，按照修改时间取数据
	 * @param begin
	 * @param end
	 * @param status
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PageList<OrderInfo> getOrdersByTime(Date begin, Date end, OrderStatus[] status, int pageNo, int pageSize);

	/**
     * 支付宝创建支付前签名
     * @param orderId
     * @return
     */
	String getAlipaySignature(long orderId);

	/**
     * 支付宝创建支付前签名，并且创建预付订单，手机端就可以用二维码别人扫我们就支付
     * @param orderId
     * @return
     */
	String getAlipayQrSignature(long orderId);

	/**
	 * 我们扫码扫别人。通过orderId获取交易和金额，调用用户收款
	 * @param orderId
	 * @param authCode 用户手机的授权码
	 * @return
	 */
	boolean alipayByAuthCode(long orderId, String authCode);

	OrderInfo getOrder(long id);

	/**
	 * 创建订单
	 * @param userId
	 * @param shopId
	 * @param items
	 * @param userCouponId
	 * @param addressId
	 * @param deviceCode
	 * @param casherId
	 * @param distribution
	 * @param credit
	 * @param description
	 * @param orderType
	 * @return
	 */
	long createOrder(long userId, int shopId, List<ShoppingCart> items, long userCouponId, long addressId, String deviceCode, int casherId,
			Date distribution, int credit, String description, short orderType);

	long createOrder(long userId, int shopId, List<ShoppingCart> items, long userCouponId, long addressId, String deviceCode, int casherId,
			Date distribution, int credit, String description);

	/**
	 * 更新订单状态
	 * @param orderId
	 * @param status
	 * @return
	 */
	boolean updateStatus(long orderId, OrderStatus status);

	/**
	 * 微信支付前，创建签名并且创建预付订单
	 * @param orderId
	 * @param ip
	 * @return
	 */
	MobilePayModel getWeixinpaySignature(long orderId, String ip);

	/**
	 * 回调到这个地方，更新订单状态，增加用户积分，通知物流
	 * @param orderId
	 * @param amount
	 * @param payWay
	 * @param account
	 * @param content
	 * @return
	 */
	boolean pay(long orderId, double amount, PayWay payWay, String account, String content);

	/**
	 * 客服调用，只是改订单状态
	 * @param orderId
	 * @param amount
	 * @param payWay
	 * @param account
	 * @param payContent
	 * @param cause
	 * @param memberId
	 * @return
	 */
	boolean refund(long orderId, double amount, PayWay payWay, String account, String payContent, String cause, int memberId);

	/**
	 * 真实调用退款退钱
	 * @param orderId
	 * @param amount
	 * @param payWay
	 * @param account
	 * @param payContent
	 * @param cause
	 * @param memberId
	 * @return
	 */
	public boolean payback(long orderId, double amount, PayWay payWay, String account, String payContent, String cause, int memberId);

	/**
     * 可能收银机调用，也是退钱
     * @param orderId
     * @param amount
     * @param payWay
     * @param account
     * @param payContent
     * @param cause
     * @param memberId
     * @return
     */
	public boolean paybackDirect(long orderId, double amount, PayWay payWay, String account, String cause, int memberId);

	/**
	 * 查询支付日志
	 * @param orderId
	 * @return
	 */
	public List<OrderPayLog> getOrderPayLog(long orderId);
	
	/**
	 * 取订单中的优惠信息
	 * @param orderId
	 * @return
	 */
	public Map<String,DiscountInfoVO> getOrderDiscount(long orderId);
}
